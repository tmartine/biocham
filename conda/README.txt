conda install conda-build

Currently, swi-prolog is not available in conda. To get around this:
- clone the brown data science recipes.
- perform a build of swi-prolog (`conda build .`)
- install it locally (`conda install --use-local swi-prolog`)

libSBML is unavailable in conda too.
- use the local recipe
- perform a build (`conda build .`)
- install it locally (`conda install --use-local libsbml`)

NuSMV is packaged in colomoto.

To build the biocham prototype package:

conda-build -c colomoto -c conda-forge .

ls /opt/conda/conda-bld/linux-64/

conda install --use-local biocham -c conda-forge -c colomoto

rpath + install target in Makefile...

There seem to be relocation issues... -> solved by instructing conda to not attempt automatic relocation

There seem to be dependency downgrading issues -> solution ?

Right now, an environment variable must be set : 
BIOCHAMLIB=/opt/conda/bin/biochamlib biocham

Running unit tests :
echo "run_tests_and_halt." | BIOCHAMLIB=/opt/conda/bin/biochamlib biocham_debug

We could have the 'make install' target set that up... ?

## Misc. notes

When building swi-prolog, pass --merge-build-host
