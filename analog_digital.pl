:- module(
	  analog_digital,
	  [
	      tokens/3,
	      program/3,
	      % number/3,
	      
	      test_compile/1,
	      test_sequential/4,
	      test_linker/4,
	      test_absence/1,
	      test_bufabsence/1,
	      test_auto/0,
	      test_hill2/0,

	      test_sigma/0,
	      test_cycle/1,
	      test_cell_cycle/0
			   
	   ]).

:- dynamic(fresh_index/1,
	   slow/1,
	   fast/1).


fresh_index(0).

get_fresh(N) :-
	retract(fresh_index(N)),
	M is N + 1,
	assert(fresh_index(M)).

slow(1).
fast(100).


% LEXER ------------------------------------------------------------------------

tokens(Z) --> "while", tokens(Y), {Z = [while | Y]}.
tokens(Z) --> "do", tokens(Y), {Z = [do | Y]}.
tokens(Z) --> "endwhile", tokens(Y), {Z = [endwhile | Y]}.
tokens(Z) --> "if", tokens(Y), {Z = [if | Y]}.
tokens(Z) --> "then", tokens(Y), {Z = [then | Y]}.
tokens(Z) --> "else", tokens(Y), {Z = [else | Y]}.
tokens(Z) --> "endif", tokens(Y), {Z = [endif | Y]}.

tokens(Z) --> ":=", tokens(Y), {Z = [:= | Y]}.

tokens(Z) --> "true", tokens(Y), {Z = [true | Y]}.  
tokens(Z) --> "false", tokens(Y), {Z = [false | Y]}.  
tokens(Z) --> "and", tokens(Y), {Z = [and | Y]}.  
tokens(Z) --> "or", tokens(Y), {Z = [or | Y]}.

tokens(Z) --> " ", tokens(Y), {Z = Y}.
tokens(Z) --> "	", tokens(Y), {Z = Y}.

% tokens([Number|Tokens]) -->  number(Number), tokens(Tokens).
tokens(Z) --> [C], tokens(Y), {atom_codes(X, [C]), Z = [X | Y]}.
tokens(Z) --> [], {Z = []}.

% number(N) --> code_types(digit, [C|Cs]), {number_codes(N,[C|Cs])}.

% code_types(Type, [C|Cs]) --> [C], {code_type(C,Type)}, !, code_types(Type, Cs).
% code_types(_, []) --> [].

% PARSER -----------------------------------------------------------------------

program(Z0,Z,X) :-
	statements(Z0,Z,X).

statements(Z0,Z,X):-
	statement(Z0,Z1,X0) ,
	reststatements(Z1,Z,X0,X).

reststatements([';'|Z0],Z,X0,Y) :-
	statements(Z0,Z,X),
	append(X0,X,Y).
reststatements(Z,Z,X,X).

statement([Var1, :=, Var2 | Z], Z, [assign(name(Var1), name(Var2))]) :-
	atom(Var1),
	atom(Var2).

statement([if|Z0],Z,[if(Test,Then,Else)]):-               
	compoundboolexpr(Z0,[then|Z1],Test) ,
	statements(Z1,[else|Z2],Then) ,
	statements(Z2,[endif|Z],Else).

statement([while|Z0],Z,[while(Test,Do)]):-                 
	compoundboolexpr(Z0,[do|Z1],Test) ,
	statements(Z1,[endwhile|Z],Do).

compoundboolexpr(Z0,Z,X) :-
	boolsubexpr(2,Z0,Z,X).

boolsubexpr(N,Z0,Z,X) :-
	N>0, N1 is N-1,
	boolsubexpr(N1,Z0,Z1,X0),
	restboolexpr(N,Z1,Z,X0,X).

boolsubexpr(0,[X|Z],Z,name(X)) :-
	atom(X).

restboolexpr(N,[Op|Z0],Z,X1,X) :-
	logicop(N,Op), N1 is N-1,
	boolsubexpr(N1,Z0,Z1,X2),
	restboolexpr(N,Z1,Z,logicexpr(Op,X1,X2),X).
restboolexpr(_,Z,Z,X,X).

boolval(true).  boolval(false).

comparisonop(==).  comparisonop(<>).
comparisonop(<). comparisonop(>).

logicop(1,and).  logicop(2,or).

boolVal(true). boolVal(false).

% ENCODER -----------------------------------------------------------------------

encodestatement([], [], _, _, _).
    % write('encoding : void'), nl.

encodestatement([S1|S2], System, G, T, D) :-
    get_fresh(I1),
    get_fresh(I2),
    get_fresh(I3),

    atom_concat('t', I1, T1),
    atom_concat('d', I2, D1),
    atom_concat('g', I3, G2),
    
    encodestatement(S1, Sys1, G, T1, D1),
    encodestatement(S2, Sys2, G2, T, D),

    senum_sequential(G2, D1, T1, SysSeq),

    append([Sys1, Sys2, SysSeq], System).


encodestatement(assign(name(X), name(Y)), System, G, T, D) :-
    senum_copy(Y, T, X, G, D, System).
	  

compile([]).
compile([R|System]) :-
    add_reaction(R),
    compile(System).
    

% FUNCTIONS -----------------------------------------------------------------------


% CHEMICAL TEMPLATES ------------------------------------------------------------

% taiwanese absence indicator
absence(X,
	System) :-
    slow(Slow),
    fast(Fast),
    
    atom_concat(X, 'ab', Xab),
    atom_concat(X, 's', Xs),

    System = [Slow for _=> Xab,
	      Fast*[X]*[Xab] for Xab=[X]=>_,
	      Slow*[Xab]*[Xab] for Xab + Xab => Xs,
	      Fast*[Xs] for Xs => Xab + Xab].

% work in progress
sequential(G, A, B, Gp,
	   System) :-
    slow(Slow),
    fast(Fast),
    
    atom_concat(G, 's', Gs),
    atom_concat(A, 's', As),

    absence(G, SysG),
    absence(A, SysA),

    System0 = [Slow*[A]*[G] for A=[G]=>B,
	       Fast*[G]*[As] for G=[As]=>Gp,
	       Slow*[B]*[Gs] for B=[Gs]=>A],

    append([SysG, SysA, System0], System).


% work in progress
linker(_, B, Gp, Gnext,
       System) :-
    fast(Fast),
    atom_concat(B, 's', Bs),
    
    absence(B, SysB),

    append([SysB, [Fast*[Gp]*[Bs] for Gp=[Bs]=>Gnext]], System).


senum_absence(X, System) :-
    slow(Slow),
    fast(Fast),
    atom_concat(X, 'ab', Xab),

    System = [Slow for _=> Xab,
	      Fast*[X]*[Xab] for X + Xab=>X,
	      Fast*[Xab]*[Xab] for 2*Xab=>Xab].

senum_bufabsence(X, System) :-
    slow(Slow),
    fast(Fast),
    atom_concat(X, 's', Xs),
    atom_concat(X, 'ab', Xab),

    senum_absence(X, SysAb),
    Sys0 = [Slow*[Xab] for Xab => Xs,
	    Fast*[X]*[Xs] for X + Xs => X,
	    Fast*[Xs]*[Xs] for 2*Xs => Xs],

    append([SysAb, Sys0], System).

senum_copy(A, T, B, G, D,
	   System) :-
    slow(Slow),
    fast(Fast),
    atom_concat(D, 's', Ds),
    atom_concat(A, 's', As),

    senum_bufabsence(D, SysD),
    senum_bufabsence(A, SysA),

    Sys0 = [Slow*[A]*[G]*[Ds] for A + G + Ds => T + G,
	    Slow*[G]*[As] for G + As => D,
	    Fast*[D]*[D] for D + D => D,
	    Slow*[D]*[T] for D + T => D + A + B,
	    Fast*[D]*[G] for D + G => D],

    append([SysD, SysA, Sys0], System).

senum_clear(A, T, G, D,
	    System) :-
    slow(Slow),
    fast(Fast), 
    atom_concat(D, 's', Ds),
    atom_concat(A, 's', As),

    senum_bufabsence(D, SysD),
    senum_bufabsence(A, SysA),

    Sys0 = [Slow*[A]*[G]*[Ds] for A + G + Ds => T + G,
	    Slow*[G]*[As] for G + As => D,
	    Fast*[D]*[D] for D + D => D,
	    Slow*[D]*[T] for D + T => D,
	    Fast*[D]*[G] for D + G => D],

    append([SysD, SysA, Sys0], System).

senum_sequential(Gnext, D, T,
		 System) :-
    slow(Slow),
    fast(Fast),
    atom_concat(T, 's', Ts),
    atom_concat(Gnext, 'p', Gnextp),

    senum_absence(T, SysT),

    Sys0 = [Slow*[Ts]*[D] for Ts + D => Gnextp + D,
	    Fast*[Gnextp]*[T] for Gnextp + T => T,
	    Slow*[Gnextp] for Gnextp => Gnext,
	    Fast*[Gnext]*[D] for Gnext + D => Gnext],

    append([SysT, Sys0], System).

sigma_presence(X, G, Y, Z, P) :-
    System = [G=>_,
	      _=[X]=>G,
	      2*Y=[X]=>Y,
	      2*Y=[G]=>3*Y,
	      Z=[P+X+Y]=>2*Z,
	      2*Z=[P+X+Y]=>Z,
	      Z=[P+G+Y]=>_,
	      2*Z=[P+G+Y]=>3*Z],
    compile(System).

sigma_absence(X, G, Y, Z, P) :-
    System = [G=>_,
	      _=[X]=>G,
	      2*Y=[X]=>Y,
	      2*Y=[G]=>3*Y,
	      2*Z=[P+Y+X]=>3*Z,
	      Z=[P+Y+X]=>_,
	      2*Z=[P+Y+G]=>Z,
	      Z=[P+Y+G]=>2*Z],
    compile(System).


% DEBUG - TEST  ------------------------------------------------------------

test_cell_cycle :- 
    biocham_command,
    doc('Compilation of the program "while true {croissance; replication; verification; mitosis;};".'),
    devdoc('The commands of this section are not finished. \\command{clear_model} does not reset all the internal parameters of this section.'),
    
    sigma_absence(croissance, g1, y1, z1, p),
    sigma_absence(replication, g2, y2, z2, p),
    sigma_absence(verification, g3, y3, z3, p),
    sigma_absence(mitosis, g4, y4, z4, p),

    present([g1], 0.5), present([y1], 2), present([z1], 0.5),
    present([g2], 0.5), present([y2], 2), present([z2], 0.5),
    present([g3], 0.5), present([y3], 2), present([z3], 0.5),
    present([g4], 0.5), present([y4], 2), present([z4], 0.5),

    present([p], 2),
    present([croissance], 10), 

    S = [croissance=[z4]=>replication,
	 replication=[z1]=>verification,
	 verification=[z2]=>mitosis,
	 mitosis=[z3]=>croissance],
    compile(S),
    with_option(time:18, numerical_simulation),
    with_option(show:[croissance,replication,verification,mitosis,z1,z2,z3,z4],plot).

test_cycle(C) :-
    biocham_command,

    sigma_absence(x1, g1, y1, z1, p),
    sigma_absence(x2, g2, y2, z2, p),
    sigma_absence(x3, g3, y3, z3, p),
    sigma_absence(x4, g4, y4, z4, p),

    present([g1], 0.5), present([y1], 2), present([z1], 0.5),
    present([g2], 0.5), present([y2], 2), present([z2], 0.5),
    present([g3], 0.5), present([y3], 2), present([z3], 0.5),
    present([g4], 0.5), present([y4], 2), present([z4], 0.5),

    present([p], 5),
    present([x1], C),

    S = [x1=[z4]=>x2,
	 x2=[z1]=>x3,
	 x3=[z2]=>x4,
	 x4=[z3]=>x1],
    compile(S).

test_sigma :-
    biocham_command,
    X = 'x', G = 'g', Y = 'y', Z = 'z', P = 'p',
    sigma_absence(X, G, Y, Z, P),
    present([G], 0.5),
    present([Y], 2),
    present([Z], 0.5),
    present([P], 5).

test_compile(String) :-
    biocham_command,

    retract(fresh_index(_)),
    assert(fresh_index(0)),
    
    atom_codes(String, C),
    phrase(tokens(Z), C), !,
    program(Z, _, X), !,
    
    write(X), nl,
    write('encoding : STARTING'), nl,
    encodestatement(X, System, 'start', 'temp', 'done'),
    write('encoding : DONE'), nl,
    
    compile(System).

test_sequential(G, A, B, Gp) :-
    biocham_command,

    sequential(G, A, B, Gp, System),
    compile(System).

test_linker(G, B, Gp, Gnext) :-
    biocham_command,

    linker(G, B, Gp, Gnext, System),
    compile(System).
    
test_absence(X) :-
    biocham_command,
    
    senum_absence(X, Sys),
    compile(Sys).

test_bufabsence(X) :-
    biocham_command,
    
    senum_bufabsence(X, Sys),
    compile(Sys).

test_auto :-
    biocham_command,

    R = r, G = g, B = b,
    Rab = rab, Gab = gab, Bab = bab,

    senum_absence(R, SysR), senum_absence(G, SysG), senum_absence(B, SysB),

    Sys = [R =[Bab]=> G, G =[Rab]=> B, B =[Gab]=>R],
    
    append([SysR, SysG, SysB, Sys], System),
    compile(System).

test_hill2 :-
    biocham_command,

    System = [x=>x+g,
	      g=>_,
	      2*[g] for g=>z2+g,
	      x+z2+2*z3=>x+z2+z3,
	      g+z2+2*z3=>g+z2+3*z3,
	      x+z2=>x+z2+z4p,
	      g+z2=>g+z2+z4m,
	      1000*[z4p]*[z4m] for z4p+z4m=>_,
	      x+z2+2*z3+z4m=>x+z2+2*z3+z4m+z5p,
	      x+z2+z3=>x+z2+z3+z5p,
	      g+z2+2*z3+z4p=>g+z2+2*z3+z4p+z5p,
	      x+z2+2*z3+z4p=>x+z2+2*z3+z4p+z5m,
	      g+z2+2*z3+z4m=>g+z2+2*z3+z4m+z5m,
	      g+z2+z3=>g+z2+z3+z5m,
	      1000*[z5p]*[z5m] for z5p+z5m=>_],

    compile(System).
