:- module(
  plot,
  [
    axes/1,
    floatorauto/1,
    % Commands
    plot/0,
    export_plot/1,
    export_plot_to_png/1,
    export_plot_to_svg/1,
    export_plot_to_pdf/1,
    % Public API
    set_plot_driver/1,
   gnu_plot/0,
   execute_plot/1
  ]
).


:- grammar(axes).

axes('').

axes(x).

axes(y).

axes(xy).


:- grammar(floatorauto).

floatorauto(auto).

floatorauto(X) :-
  number(X).


:- devdoc('\\section{Commands}').

% store plot options for a future export_plot
:- dynamic(logscale/1).
logscale('').
:- dynamic(show/1).
:- dynamic(against/1).

:- initial(option(plot_table: '')).
:- initial(option(show: {})).
:- initial(option(logscale: '')).
:- initial(option(against: 'Time')).
:- initial(option(xmin: auto)).
:- initial(option(ymin: auto)).
:- initial(option(xmax: auto)).
:- initial(option(ymax: auto)).

plot :-
  biocham_command,
  option(
    plot_table, name, Table,
    'Determined the table to be plotted'
    ),
  option(
    logscale, axes, LogScale,
    'Apply log-scaling to the specified axes.'
  ),
  option(
    show, {object}, Show,
    'Restricts the plot to the given set of objects and functions; everything will be
    plotted if the set is empty'
  ),
  option(
    against, object, Against,
    'Selects the X axis for the plot, defaulting to Time.'
  ),
  option(
    xmin, floatorauto, _Xmin,
    'Select the axes for the current plot (auto is overwrite to *)'
  ),
  option(
    ymin, floatorauto, _Ymin,
    'Select the axes for the current plot (auto is overwrite to *)'
  ),
  option(
    xmax, floatorauto, _Xmax,
    'Select the axes for the current plot (auto is overwrite to *)'
  ),
  option(
    ymax, floatorauto, _Ymax,
    'Select the axes for the current plot (auto is overwrite to *)'
  ),
  doc('
    plots the current trace. After a simulation, the trace is composed of molecular concentrations and user-defined functions over time.
    \\begin{example}'
  ),
  (
    Table = ''
  ->
    true
  ;
    select_table(Table)
  ),
  handle_show(Show),
  retractall(logscale(_)),
  assertz(logscale(LogScale)),
  retractall(against(_)),
  assertz(against(Against)),
  biocham_silent(clear_model),
  biocham(load(library:examples/mapk/mapk)),
  biocham(numerical_simulation(method: msbdf)),
  biocham(plot),
  biocham('plot(show: {MAPK~{p1,p2}, MEK~{p1,p2}})'),
  biocham(numerical_simulation(filter: only_extrema)),
  biocham(plot),
  doc('
    \\end{example}'
  ),
  get_plot_driver(Driver),
  Driver.


:- doc('\\begin{example}').
:- biocham_silent(clear_model).
:- biocham(a -> b).
:- biocham(a -< a).
:- biocham(present(a, 10)).
:- biocham(numerical_simulation(time:5, method: ssa)).
:- biocham(plot).
:- biocham(numerical_simulation(time:5, method: msbdf)).
:- biocham(plot).
:- biocham(plot(logscale: 'y')).
:- biocham(plot(show: a, against: b)).
:- biocham(plot(show: a, against: b, logscale: 'xy')).
:- doc('\\end{example}').

export_plot(FileTemplate) :-
  biocham_command,
  type(FileTemplate, output_file),
  doc('
    saves the current trace into two files:
    \\argument{FileTemplate}\\texttt{.csv} and \\texttt{.plot}.
  '),
  export_plot(FileTemplate, []).


export_plot_to_png(OutputFile) :-
  biocham_command,
  type(OutputFile, output_file),
  doc('plots the current trace in a PNG file'),
  export_plot(plot, [png(OutputFile)]),
  execute_plot_clean([file('plot.plot')]).


export_plot_to_svg(OutputFile) :-
  biocham_command,
  type(OutputFile, output_file),
  doc('plots the current trace in a SVG file'),
  export_plot(plot, [svg(OutputFile)]),
  execute_plot_clean([file('plot.plot')]).


export_plot_to_pdf(OutputFile) :-
  biocham_command,
  type(OutputFile, output_file),
  doc('plots the current trace in a PDF file'),
  export_plot(plot, [pdf(OutputFile)]),
  execute_plot_clean([file('plot.plot')]).


:- devdoc('\\section{Public API}').


set_plot_driver(Driver) :-
  nb_setval(plot_driver, Driver).


gnu_plot :-
  export_plot(plot),
  execute_plot_clean(['-persist', file('plot.plot')]).


:- devdoc('\\section{Private predicates}').


get_plot_driver(Driver) :-
  nb_getval(plot_driver, Driver).


gnu_plot_png :-
  count(plot_png, Index),
  format(atom(Filename), 'plot~d.png', [Index]),
  export_plot_to_png(Filename),
  view_image(Filename).


gnu_plot_svg :-
  count(plot_svg, Index),
  format(atom(Filename), 'plot~d.svg', [Index]),
  export_plot_to_svg(Filename),
  view_image(Filename).


gnu_plot_pdf :-
  count(plot_pdf, Index),
  format(atom(Filename), 'plot~d.pdf', [Index]),
  export_plot_to_pdf(Filename),
  view_image(Filename).


gnu_plot_csv :-
  count(plot_csv, Index),
  get_option(logscale, Axes),
  format(atom(Filename), 'plot-~d.csv', [Index]),
  export_table(Filename),
  atom_concat(Filename, 'X', MetaDataFilename),
  get_option(show, Show),
  get_option(against, Against),
  get_option(xmin,Xmin),
  get_option(xmax,Xmax),
  get_option(ymin,Ymin),
  get_option(ymax,Ymax),
  maplist(unquoted_term_to_atom, Show, ShowAtoms),
  atomic_list_concat(ShowAtoms, ' ', ShowAtom),
  get_axes_names(Against, XAxis, YAxis),
  with_output_to_file(
    MetaDataFilename,
    (
      format('logscale: ~a~n', [Axes]),
      format('show: ~a~n', [ShowAtom]),
      format('against: ~a~n', [Against]),
      format('axes: ~w ~w ~w ~w~n', [Xmin, Xmax, Ymin, Ymax]),
      format('xaxis_name: ~w~n', [XAxis]),
      format('yaxis_name: ~w~n', [YAxis])
    )
  ),
  view_image(Filename).


execute_plot(Options) :-
  process_create(path(gnuplot), Options, []).

execute_plot_clean(Options) :-
  process_create(path(gnuplot), Options, []),
  member(file(File), Options),
  clean_plot(File).


export_plot(FileTemplate, Options) :-
  format(atom(PlotFile), '~a.plot', [FileTemplate]),
  format(atom(CsvFile), '~a.csv', [FileTemplate]),
  export_table(CsvFile),
  get_table_headers([_Time | Headers]),
  absolute_file_name(PlotFile, AbsolutePlotFile),
  setup_call_cleanup(
    open(AbsolutePlotFile, write, Stream),
    export_plot_stream(Stream, CsvFile, Headers, Options),
    close(Stream)
  ).

export_plot_stream(Stream, CsvFile, Headers, Options) :-
  (
    member(png(Filename), Options)
  ->
    format(Stream, '\c
if (strstrt(GPVAL_TERMINALS, "pngcairo") > 0) {
   set terminal pngcairo size 800,600
} else {
   set terminal png size 800,600
}
set output "~a"
', [Filename])
  ;
    member(svg(Filename), Options)
  ->
    format(Stream, '\c
set terminal svg
set output "~a"
', [Filename])
  ;
    member(pdf(Filename), Options)
  ->
    format(Stream, '\c
set terminal pdfcairo
set output "~a"
', [Filename])
  ;
    true
  ),
  write(Stream, '\c
set termoption noenhanced
set termoption linewidth 3
set termoption fontscale 0.8
set key outside reverse
set border back linecolor rgb "#808080"
set format "%g"
set style data lines
set datafile separator ","
'),
  %set format "%.5g"
  get_axes(Xmin, Xmax, Ymin, Ymax),
  format(Stream, "set xrange [~w:~w]~nset yrange [~w:~w]~n", [Xmin, Xmax, Ymin, Ymax]),
  (logscale(Axes) -> true ; get_option(logscale, Axes)), %fetch default option
  (
    Axes == ''
  ->
    true
  ;
    format(Stream, 'set logscale ~a~n', [Axes])
  ),
  (against(Against) -> true ; get_option(against, Against)), %fetch default option
  get_axes_names(Against, XAxis, YAxis),
  % heisenbug if choicepoints before process_create
  once(nth1(IndexX, ['Time' | Headers], Against)),
  format(Stream, 'set xlabel "~a"~n', [XAxis]),
  format(Stream, 'set ylabel "~a"~n', [YAxis]),
  nb_setval(plotted, 0),
  forall(
    (
      nth1(Index, Headers, Header),
      show(Header)
    ),
    (
      nb_getval(plotted, Plotted),
      (
        Plotted > 0
      ->
        write(Stream, ', ')
      ;
        write(Stream, 'plot '),
        nb_setval(plotted, 1)
      ),
      ColumnIndex is Index + 1,
      format(
        Stream,
        '"~a" using ~d:~d title "~a"',
        [CsvFile, IndexX, ColumnIndex, Header]
      )
    )
  ),
  write(Stream, '\n').


clean_plot(PlotFile) :-
  atom_concat(Base, '.plot', PlotFile),
  atom_concat(Base, '.csv', CsvFile),
  delete_file(PlotFile),
  delete_file(CsvFile).


handle_show(Show) :-
  retractall(show(_)),
  (
    Show = [{}]
  ->
    assertz(show(_))
  ;
    forall(
      member(Object, Show),
      assertz(show(Object))
    )
  ).


%! get_axes(-Xmin, -Xmax, -Ymin, -Ymax)
%
% retrieve the option values for the axes of gnuplot

get_axes(Xmin, Xmax, Ymin, Ymax) :-
  get_one_axe(xmin, Xmin),
  get_one_axe(xmax, Xmax),
  get_one_axe(ymin, Ymin),
  get_one_axe(ymax, Ymax).

get_one_axe(Name, Value) :-
  get_option(Name, RawV),
  (
    RawV = auto
  ->
    Value = '*'
  ;
    Value = RawV
  ).


unquoted_term_to_atom(T, A) :-
  with_output_to(atom(A), write(T)).
