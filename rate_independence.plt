:- use_module(library(plunit)).


:- begin_tests(rate_independence).


test('fork', [true(Reason == fork('B1')), setup(max_model), cleanup(clear_model)]) :-
  command('B1 => C'),
  reaction_graph,
  get_current_graph(Id),
  \+ rate_independence:is_fork_free(Id),
  nb_getval(rate_dependence_reason, Reason).

test('synthesis', [true(Reason = synthesis(_)), setup(max_model), cleanup(clear_model)]) :-
  command('C => C + D'),
  reaction_graph,
  \+ rate_independence:is_synthesis_free,
  nb_getval(rate_dependence_reason, Reason).

test('loop', [true(Reason = loop(_)), setup(max_model), cleanup(clear_model)]) :-
  command('delete_reaction(Sum + Min => _)'),
  command('Sum + Min => B'),
  reaction_graph,
  get_current_graph(Id),
  \+ rate_independence:is_loop_free(Id),
  nb_getval(rate_dependence_reason, Reason).

test('Max', [setup(max_model), cleanup(clear_model)]) :-
  with_output_to(atom(Result), test_rate_independence),
  atom_concat('The model is rate independent', _, Result).

test('invariants', [
  condition(flag(slow_test, true, true)),
  forall((expand_file_name('./library/examples/tests/rate_independence_*.bc', FileList), member(File, FileList))),
  setup((with_output_to(atom(ExpectedLong), command(load(File))), sub_string(ExpectedLong, 0, 3, _, Expected))),
  true(File-Output == File-Expected)
]) :-
  with_output_to(atom(OutputFull), command(test_rate_independence_invariants)),
  split_string(OutputFull, "\n", "\r\t ", [OutputLong | _]),
  sub_string(OutputLong, 0, 3, _, Output).

:- end_tests(rate_independence).

max_model :-
  clear_model,
  command('A => A1 + A2'),
  command('B => B1 + B2'),
  command('A1 => Sum'),
  command('B1 => Sum'),
  command('A2 + B2 => Min'),
  command('Sum + Min => _').
