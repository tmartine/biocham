:- module(
  events,
  [
    % Public API
    add_event/2,
    add_time_event/2,
    list_events/0,
    delete_events/0,
    list_model_events/0
  ]
).

% Only for separate compilation/linting
:- use_module(doc).
:- use_module(reaction_rules).


:- doc('Events can be used to change some parameter values once a condition gets satisfied. This is useful to implement discrete events in a variety of situations. 
Events can be used in modelling, for instance for dividing the cell mass by 2 at each division in a model of the cell cycle, 
or for creating hybrid automata model,  which chain different continuous semantics (ODEs).
Events can also be intensively used to implement stochastic simulators defined by events, and hybrid differential-stochastic simulators \\cite{CFJS15tomacs}.  ').

:- devdoc('\\section{Commands}').


add_event(Condition, ParameterValues) :-
  biocham_command(*),
  type(Condition, condition),
  type(ParameterValues, '*'(parameter_name = arithmetic_expression)),
  doc('
    sets up an event that will be fired each time the condition given as first
    argument goes from false to true.
    This command is effective in numerical simulations only.
    Upon firing, the parameters receive new values
    computed from the expression.
    The initial values of the parameters are restored after the simulation.
    \\begin{example}
  '),
  biocham_silent(clear_model),
  biocham('MA'(k) for a => b),
  biocham(parameter(k = 1)),
  biocham(add_event(b > 0.4, k = 0)),
  biocham(present(a)),
  biocham(numerical_simulation(time:2)),
  biocham(plot),
  biocham(numerical_simulation(time:2, method:ssa)),
  biocham(plot),
  doc('
    \\end{example}
    Note that the continuous \\command{numerical_simulation/0} engine will
    attempt to interpolate linear event conditions as per \\doi{10.1007/3-540-45351-2_19}.
  '),
  add_item([kind: event, item: event(Condition, ParameterValues)]).


add_time_event(Expression, ParameterValues) :-
  biocham_command(*),
  type(Expression, arithmetic_expression),
  type(ParameterValues, '*'(parameter_name = arithmetic_expression)),
  doc('Introduce a special kind of event that will be fired when the time
    reaches the given threshold. They may be used for efficiency reason during
    numerical integration.'),
  (
    atom(Expression),
    read_term_from_atom(Expression, _ > Threshold, [])
  ->
    true
  ;
    atom(Expression),
    read_term_from_atom(Expression, _ >= Threshold, [])
  ->
    true
  ;
    Threshold = Expression
  ),
  add_item([kind: time_event, item: event('Time'>Threshold, ParameterValues)]).


list_events :-
  biocham_command,
  doc('lists all the declared events.'),
  list_items([kind: event]),
  list_items([kind: time_event]).


delete_events :-
  biocham_command,
  doc('deletes all the declared events.'),
  delete_items([kind: event]),
  delete_items([kind: time_event]).


:- devdoc('\\section{Private predicates}').


list_model_events :-
  devdoc('
    lists all the events in a loadable syntax
    (auxiliary predicate of list_model).
  '),
  % List the regular events
  \+ (
    item(
      [no_inheritance, kind: event, item: event(Condition, ParameterValues)]
    ),
    \+ (
      format('add_event(~w, ', [Condition]),
      write_successes(
        member(Parameter = Value, ParameterValues),
        write(', '),
        format('~w = ~w', [Parameter, Value])
      ),
      write(').\n')
    )
  ),
  % List the time events
  \+ (
    item(
      [no_inheritance, kind: time_event, item: event(Condition, ParameterValues)]
    ),
    \+ (
      format('add_time_event(~w, ', [Condition]),
      write_successes(
        member(Parameter = Value, ParameterValues),
        write(', '),
        format('~w = ~w', [Parameter, Value])
      ),
      write(').\n')
    )
  ).
