:- use_module(library(plunit)).


:- begin_tests(search, [condition(flag(slow_test, true, true)),
                        setup((clear_model, reset_options))]).

test('search_parameters/3') :-
  clear_model,
  command('k for a => b. present(a).'),
  with_output_to(
    atom(Result),
    command('search_parameters(G(Time * 10 < X => a > b), [0 <= k <= 1], [X -> 20], cmaes_init_center: yes).')
  ),
  once(sub_atom(Result, _, _, _, '[0] parameter(k=')).


test('search_parameters/2') :-
  clear_model,
  command('ka for _ => a. kb for _ => b. parameter(ka=1, kb=1).'),
  with_output_to(
    atom(Result),
    command('search_parameters([0 <= ka <= 5], [(G(a-b<x/\\b-a<x), [], [x -> 10]), (G(a-b<y/\\b-a<y), [kb=2], [y -> 10])]).')
  ),
  once(sub_atom(Result, _, _, _, 'Best satisfaction degree: 0.99')).


%test('last_period') :-
%  clear_model,
%  command('present(A). present(B).'),
%  command('k1*A*B for A+B => 2*B.'),
%  command('k2*A for A => 2*A.'),
%  command('k3*B for B => _.'),
%  command('parameter(k1=2, k2=2, k3=1).'),
%  command('numerical_simulation.'),
%  with_output_to(
%    atom(_Result),
%    command('search_parameters(last_period(A,p),
%    [0<=k1<=2, 0<=k2<=2, 0<=k3<=2], [p -> 2]).')
%  ).


test('robustness') :-
  clear_model,
  command('k for a => b. present(a, 10). parameter(k=0.7).'),
  with_output_to(
    atom(Result),
    command('robustness(G(Time < T => a > b), [k], [T -> 5]).')
  ),
  (
    sub_atom(Result, _, _, _, 'degree: 0.9')
  ->
    true
  ;
    sub_atom(Result, _, _, _, 'degree: 1.0')
  ->
    true
  ;
    write(user_error, Result),
    nl(user_error),
    false
  ).


test('robustness_pl') :-
  clear_model,
  command('k for a => b. present(a, 10). parameter(k=0.7).'),
  command('option(method: rsbk).'),
  with_output_to(
    atom(Result),
    command('robustness(G(Time < T => a > b), [k], [T -> 5]).')
  ),
  (
    sub_atom(Result, _, _, _, 'degree: 0.9')
  ->
    true
  ;
    sub_atom(Result, _, _, _, 'degree: 1.0')
  ->
    true
  ;
    write(user_error, Result),
    nl(user_error),
    false
  ).


:- end_tests(search).
