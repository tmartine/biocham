:- module(
  reaction_editor,
  [
    % Commands
    add_reaction/1,
    list_reactions/0,
    list_reactions_with_species/1,
    list_reactions_with_reactant/1,
    list_reactions_with_product/1,
    list_reactions_with_catalyst/1,
    list_reactions_with_strict_catalyst/1,
    list_reactions_with_autocatalyst/1,
    list_reactions_with_inhibitor/1,
    list_rules/0,
    delete_reaction/1,
    delete_reactions/0,
    delete_reaction_named/1,
    delete_reaction_prefixed/1,
    merge_reactions/2,
    simplify_all_reactions/0,
				% Public API
    reactions_with_species/2,
    reactions_with_reactant/2,
    reactions_with_product/2,
    reactions_with_catalyst/2,
    reactions_with_strict_catalyst/2,
    reactions_with_catalyst/2,
    reactions_with_inhibitor/2,
    is_reactant/2,
    is_product/2,
    is_strict_reactant/2,
    is_strict_product/2,
    is_catalyst/2,
    is_strict_catalyst/2,
    is_autocatalyst/2,
    is_inhibitor/2,
    find_reaction_prefixed/2,
    is_reaction_model/0,
    check_reaction_model/0,
    list_model_reactions/0,
    reaction/2,
    reaction/5,
    add_reaction/5,
    add_reaction/6,
    solution_to_list/2,
    list_to_solution/2,
    compute_ode_for_reactions/0
  ]
).

% Only for separate compilation/linting
:- use_module(doc).
:- use_module(reaction_rules).
:- use_module(objects).


:- devdoc('\\section{Commands}').


add_reaction(Reaction) :-
  biocham_command,
  type(Reaction, reaction),
  doc('
    adds reaction rules to the current set of reactions.
  This command is implicit: reaction rules are directly added when reading them.
'),
\+ (
  reaction_named(Reaction, Name, Kinetics, Reactants, Inhibitors, Products),
  \+ (
    add_reaction(Name, Kinetics, Reactants, Inhibitors, Products, false)
  )
).

:- doc('\\begin{remark}In Biocham v4, a reaction network is represented by a multiset of reactions. Reactions can thus be added in multiple copies in which case their reaction rates are summed.\\end{remark}').



delete_reaction(Reaction):-
  biocham_command,
  type(Reaction, reaction),
  doc('
    removes one reaction rule from the current set of reactions.
  '),
  simplify_reaction(Reaction, ReactionFormated),
  catch(
    delete_item([kind: reaction, item: ReactionFormated]),
    error(unknown_item(_O)),
    delete_item([kind: reaction, item: Reaction])
  ).


delete_reaction_named(Name):-
  biocham_command,
  type(Name, name),
  doc('
    removes  reaction rules by their name from the current set of reactions.
  '),
  Item=(Name--_),
  \+ (
    item([kind:reaction, id: Id, item:Item]),
    \+ (
      delete_item(Id)
    )
  ).


delete_reactions :-
  biocham_command,
  doc('Removes all reaction rules from the current model.'),
  forall(item([kind: reaction, id: Id]), delete_item(Id)).


delete_reaction_prefixed(Prefix):-
  biocham_command,
  type(Prefix, name),
  doc('
    removes all the reaction rules that match a name prefix.
  '),
  Item=(Name--_),
  \+ (
    item([kind:reaction, id: Id, item:Item]),
    atom_concat(Prefix,_,Name),
    \+ (
      delete_item(Id)
    )
  ).


find_reaction_prefixed(Prefix,Name):-
  %% biocham_command,
  %% type(Prefix, name),
  %% type(Name, name),
  devdoc('
    Name is the name of a reaction prefixed by Prefix.
  '),
  Item=(Name--_),
  item([kind:reaction, id: _Id, item:Item]),
  atom_concat(Prefix,_,Name).


merge_reactions(Reaction1, Reaction2):-
  biocham_command,
  type(Reaction1, reaction),
  type(Reaction2, reaction),
  doc('
    merges two reaction rules by removing them and replacing them by a new reaction with reactants the sum of reactants, with products the sum of the products, and with kinetics the product of the kinetics (using mass action law kinetics if MA, MAI, MM or Hill kinetics).
  '),
  reaction(Reaction1, Kinetics1, Reactants1, Inhibitors1, Products1),
  reaction(Reaction2, Kinetics2, Reactants2, Inhibitors2, Products2),
  delete_reaction(Reaction1),
  delete_reaction(Reaction2),
  append(Reactants1,Reactants2,Reactants),
  append(Inhibitors1,Inhibitors2,Inhibitors),
  append(Products1,Products2,Products),
  kinetics(Reactants1, Inhibitors1, Kinetics1, Kin1),
  kinetics(Reactants2, Inhibitors2, Kinetics2, Kin2),
  add_reaction(Kin1*Kin2,Reactants,Inhibitors,Products,false).

  
list_reactions :-
  biocham_command,
  doc('lists the current set of reaction rules.'),
  list_items([kind: reaction]).


list_rules :-
  biocham_command,
  doc('lists the current set of reaction and influence rules.'),
  list_items([kind: [reaction, influence]]).



list_reactions_with_species(ObjectSet):-
  biocham_command,
  type(ObjectSet, {object}),
  doc('Lists reactions having a reactant, product or inhibitor in the set of molecular species \\argument{Objectset}.'),
  reactions_with_species(ObjectSet, Reactions),
  forall(member(R,Reactions), writeln(R)).

reactions_with_species(ObjectSet, Reactions):-
  realsetof(Reaction, reaction_editor:species(ObjectSet, Reaction), Reactions).

species(ObjectSet, Reaction):-
  member(M,ObjectSet),
  (is_reactant(M, Reaction); is_product(M, Reaction); is_inhibitor(M, Reaction)).


	
list_reactions_with_reactant(ObjectSet):-
  biocham_command,
  type(ObjectSet, {object}),
  doc('Lists reactions having a reactant in the set of molecular species \\argument{Objectset}.'),
  reactions_with_reactant(ObjectSet, Reactions),
  forall(member(R,Reactions), writeln(R)).

:- devdoc('set (possibly empty) of reactions with given reactant, product, ..').

reactions_with_reactant(ObjectSet, Reactions):-
  realsetof(Reaction, reaction_editor:reactant(ObjectSet, Reaction), Reactions).

reactant(ObjectSet, Reaction):-
  member(M, ObjectSet),
  is_reactant(M, Reaction).


list_reactions_with_product(ObjectSet):-
  biocham_command,
  type(ObjectSet, {object}),
  doc('Lists reactions having a product in the set of molecular species \\argument{Objectset}.'),
  reactions_with_product(ObjectSet, Reactions),
  forall(member(R,Reactions), writeln(R)).

reactions_with_product(ObjectSet, Reactions):-
  realsetof(Reaction, reaction_editor:product(ObjectSet, Reaction), Reactions).

product(ObjectSet, Reaction):-
  member(M,ObjectSet),
  is_product(M, Reaction).


list_reactions_with_catalyst(ObjectSet):-
  biocham_command,
  type(ObjectSet, {object}),
  doc('Lists reactions with a catalyst (i.e. both reactant and product) in the set of molecular species \\argument{Objectset}.'),
  reactions_with_catalyst(ObjectSet, Reactions),
  forall(member(R,Reactions), writeln(R)).



list_reactions_with_strict_catalyst(ObjectSet):-
  biocham_command,
  type(ObjectSet, {object}),
  doc('Lists reactions with a strict catalyst (i.e. same stoichiometry as reactant and product) in the set of molecular species \\argument{Objectset}.'),
  reactions_with_strict_catalyst(ObjectSet, Reactions),
  forall(member(R,Reactions), writeln(R)).


list_reactions_with_autocatalyst(ObjectSet):-
  biocham_command,
  type(ObjectSet, {object}),
  doc('Lists reactions with an autocatalyst (i.e. different stoichiometry as reactant and product) in the set of molecular species \\argument{Objectset}.'),
  reactions_with_autocatalyst(ObjectSet, Reactions),
  forall(member(R,Reactions), writeln(R)).

reactions_with_catalyst(ObjectSet, Reactions):-
  realsetof(Reaction, reaction_editor:catalyst(ObjectSet, Reaction), Reactions).

reactions_with_strict_catalyst(ObjectSet, Reactions):-
  realsetof(Reaction, reaction_editor:strict_catalyst(ObjectSet, Reaction), Reactions).

reactions_with_autocatalyst(ObjectSet, Reactions):-
  realsetof(Reaction, reaction_editor:autocatalyst(ObjectSet, Reaction), Reactions).

catalyst(ObjectSet, Reaction):-
  member(M,ObjectSet),
  is_catalyst(M, Reaction).

strict_catalyst(ObjectSet, Reaction):-
  member(M,ObjectSet),
  is_strict_catalyst(M, Reaction).

autocatalyst(ObjectSet, Reaction):-
  member(M,ObjectSet),
  is_autocatalyst(M, Reaction).


list_reactions_with_inhibitor(ObjectSet):-
  biocham_command,
  type(ObjectSet, {object}),
  doc('Lists reactions with an inhibitor in the set of molecular species \\argument{Objectset}.'),
  reactions_with_inhibitor(ObjectSet, Reactions),
  forall(member(R,Reactions), writeln(R)).

reactions_with_inhibitor(ObjectSet, Reactions):-
  realsetof(Reaction, reaction_editor:inhibitor(ObjectSet, Reaction), Reactions).

inhibitor(ObjectSet, Reaction):-
  member(M,ObjectSet),
  is_inhibitor(M, Reaction).


:- devdoc('\\section{Public API}').


:- devdoc('tests/enumerates reactions with reactant, product, ... ').

is_reactant(S, Reaction):-
  item([kind: reaction, item: Reaction]),
  reaction(Reaction, [kinetics: _,reactants: Reactants,inhibitors: _,   products: _]),
  member((_*S), Reactants).

is_strict_reactant(S, Reaction):-
  item([kind: reaction, item: Reaction]),
  reaction(Reaction, [kinetics: _,reactants: Reactants,inhibitors: _,   products: Products]),
  member((M*S), Reactants),
  \+ member((M*S), Products).

is_product(S, Reaction):-
  item([kind: reaction, item: Reaction]),
  reaction(Reaction, [kinetics: _,reactants: _,inhibitors: _,   products: Products]),
  member((_*S), Products).

is_strict_product(S, Reaction):-
  item([kind: reaction, item: Reaction]),
  reaction(Reaction, [kinetics: _,reactants: Reactants,inhibitors: _,   products: Products]),
  member((M*S), Products),
  \+ member((M*S), Reactants).


is_catalyst(S, Reaction):-
  item([kind: reaction, item: Reaction]),
  reaction(Reaction, [kinetics: _,reactants: Reactants,inhibitors: _,   products: Products]),
  member((_*S), Reactants),
  member((_*S), Products).

% a strict catalyst has same stoichiometry (cannot be an autocatalyst)
is_strict_catalyst(S, Reaction):-
  item([kind: reaction, item: Reaction]),
  reaction(Reaction, [kinetics: _,reactants: Reactants,inhibitors: _,   products: Products]),
  member((C*S), Reactants),
  member((C*S), Products).

is_autocatalyst(S, Reaction):-
  item([kind: reaction, item: Reaction]),
  reaction(Reaction, [kinetics: _,reactants: Reactants,inhibitors: _,   products: Products]),
  member((C*S), Reactants),
  member((D*S), Products),
  C\=D.


is_inhibitor(S, Reaction):-
  item([kind: reaction, item: Reaction]),
  reaction(Reaction, [kinetics: _,reactants: _,inhibitors: Inhibitors,   products: _]),
  member(S, Inhibitors).


is_reaction_model :-
   devdoc('
     succeeds if the current model is a reaction model
     (i.e., does not contain any influence rules).
   '),
   \+ item([kind: influence]).


check_reaction_model :-
  (
    is_reaction_model
  ->
    true
  ;
    throw(error(not_a_reaction_model, check_reaction_model))
  ).


list_model_reactions :-
  devdoc('
    lists all the reaction rules in a loadable syntax
    (auxiliary predicate of list_model).
  '),
  \+ (
    item([no_inheritance, kind: reaction, item: Reaction]),
    \+ (
      format('~p.\n', [Reaction])
    )
  ).


prolog:message(error(not_a_reaction_model)) -->
  ['Not a reaction model'].


add_reaction(Kinetics, Left, Inhibors, Right, Reversible) :-
    add_reaction('', Kinetics, Left, Inhibors, Right, Reversible).

add_reaction(Name, Kinetics, Left, Inhibors, Right, Reversible) :-
  debug(reactions, "checking identifier kind for ~w", [Name]),
  check_identifier_kind(Name, rule_name),
  forall(
    (
      member(_ * Object, Left)
    ;
      member(Object, Inhibors)
    ;
      member(_ * Object, Right)
    ),
    (
      debug(reactions, "checking identifier kind for ~w", [Object]),
      check_identifier_kind(Object, object)
    )
  ),
  debug(reactions, "all identifiers checked", []),
  make_reaction(Name, Kinetics, Left, Inhibors, Right, Reversible, Reaction),
  debug(reactions, "built: ~w", [Reaction]),
  add_item([kind: reaction, item: Reaction]).


simplify_all_reactions :-
%  biocham_command,
%  doc('
%    replaces each reaction by a simplified form, by grouping common molecules,
%    identifying catalysts, and by using the canonical molecule for aliases.
%  '),
  \+ (
    item([kind: reaction, id: Id, item: Reaction]),
    \+ (
      delete_item(Id),
      simplify_reaction(Reaction, SimplifiedReaction),
      add_item([kind: reaction, item: SimplifiedReaction])
    )
  ).

reaction_named(
  Item, Name, Kinetics, Reactants, Inhibitors, Products, Reversible
) :-
    (
        Item=(Name -- _)
     ->
         true
     ;
     Name=''
    ),
    reaction(Item, Kinetics, Reactants, Inhibitors, Products, Reversible).
    

reaction_named(Item, Name, Kinetics, Reactants, Inhibitors, Products) :-
    (
        Item=(Name -- _)
     ->
         true
     ;
     Name=''
    ),
    reaction(Item, Kinetics, Reactants, Inhibitors, Products).
    



reaction(ItemNamed, Kinetics, Reactants, Inhibitors, Products, Reversible) :-
    (
        ItemNamed=(_ -- Item)
     ->
         true
     ;
     Item=ItemNamed
    ),
    
  (
    Item = (Kinetics for Body)
  ->
    true
  ;
    Kinetics = 'MA'(1),
    Body = Item
  ),
  (
    Body = (Left =[ Catalyst ]=> Right)
  ->
    Reversible = false
  ;
    Body = (Left <=[ Catalyst ]=> Right)
  ->
    Reversible = true
  ;
    Body = (Left => Right)
  ->
    Reversible = false,
    Catalyst = '_'
  ;
    Body = (Left <=> Right)
  ->
    Reversible = true,
    Catalyst = '_'
  ),
  (
    Left = LeftPositive / LeftNegative
  ->
    true
  ;
    LeftPositive = Left,
    LeftNegative = '_'
  ),
  solution_to_canonical_list(LeftPositive, LeftMolecules),
  enumeration_to_canonical_list(LeftNegative, Inhibitors),
  solution_to_canonical_list(Right, RightMolecules),
  solution_to_canonical_list(Catalyst, CatalystMolecules),
  append(CatalystMolecules, LeftMolecules, AllReactants),
  append(CatalystMolecules, RightMolecules, AllProducts),
  maplist(
    simplify_solution,
    [AllReactants, AllProducts],
    [Reactants, Products]
  ).

reaction(Item, Kinetics, Reactants, Inhibitors, Products) :-
  reaction(
    Item, PairKinetics, LeftMolecules, Inhibitors, RightMolecules, Reversible
  ),
  (
    Reversible = true
  ->
    pair_kinetics(PairKinetics, ForwardKinetics, BackwardKinetics),
    (
      Kinetics = ForwardKinetics,
      Reactants = LeftMolecules,
      Products = RightMolecules
    ;
      Kinetics = BackwardKinetics,
      Reactants = RightMolecules,
      Products = LeftMolecules
    )
  ;
    Kinetics = PairKinetics,
    Reactants = LeftMolecules,
    Products = RightMolecules
  ).


reaction(Reaction, Components) :-
  devdoc('
builds or decomposes a reaction rule.
\\argument{Reaction} is a reaction term.
\\argument{Components} is a record-style list with items of the form
\\begin{itemize}
\\item \\texttt{name: Name} where \\texttt{Name} is an atom,
\\item \\texttt{kinetics: Kinetics} where \\texttt{Kinetics} is an arithmetic
expression,
\\item \\texttt{reactants: Reactants},
\\item \\texttt{products: Products} where \\texttt{Reactants} and
\\texttt{Products} are lists of items that can either be objects or terms of the
form \\texttt{Coefficient * Object},
\\item \\texttt{inhibitors: Inhibitors} where \\texttt{Inhibitors} is a list of
objects.
\\end{itemize}
  '),
  (
    var(Reaction)
  ->
    field_default(name, '', Components, Name),
    field_default(kinetics, 'MA'(1), Components, Kinetics),
    field_default(reactants, [], Components, Reactants),
    field_default(inhibitors, [], Components, Inhibitors),
    field_default(products, [], Components, Products),
    maplist(put_coefficient, Reactants, ReactantsWithCoefficients),
    maplist(put_coefficient, Products, ProductsWithCoefficients),
    make_reaction(
      Name, Kinetics, ReactantsWithCoefficients, Inhibitors,
      ProductsWithCoefficients, false, Reaction
    )
  ;
    reaction_named(Reaction, Name, Kinetics, Reactants, Inhibitors, Products),
    unify_records(Components, [
      name: Name,
      kinetics: Kinetics,
      reactants: Reactants,
      inhibitors: Inhibitors,
      products: Products
    ])
  ).


put_coefficient(Coefficient * Object, Coefficient * Object) :-
  !.

put_coefficient(Object, 1 * Object).


pair_kinetics(PairKinetics, ForwardKinetics, BackwardKinetics) :-
  (
    PairKinetics = (ForwardKinetics, BackwardKinetics)
  ->
    true
  ;
    ForwardKinetics = PairKinetics,
    BackwardKinetics = PairKinetics
  ).


solution_to_canonical_list(Solution, List) :-
  solution_to_list(Solution, NonCanonicalList),
  maplist(coefficient_canonical, NonCanonicalList, List).


enumeration_to_canonical_list(Enumeration, List) :-
  list_enumeration(NonCanonicalList, Enumeration),
  maplist(canonical, NonCanonicalList, List).


solution_to_list('_', []) :-
  !.

solution_to_list(A + B, Solution) :-
  !,
  solution_to_list(A, SolutionA),
  solution_to_list(B, SolutionB),
  append(SolutionA, SolutionB, Solution).

solution_to_list(Coefficient * Object, Solution) :-
  !,
  Solution = [Coefficient * Object].

solution_to_list(Object, [1 * Object]).


list_to_solution([], '_') :-
  !.

list_to_solution(List, Solution) :-
  reverse(List, [Head | Tail]),
  list_to_solution(Tail, Head, Solution).

list_to_solution([], CoefficientObject, Object) :-
  coefficient_object(CoefficientObject, Object).

list_to_solution([Head | Tail], CoefficientObject, Solution) :-
  coefficient_object(CoefficientObject, Object),
  Solution = SolutionTail + Object,
  list_to_solution(Tail, Head, SolutionTail).


coefficient_object(1 * Object, Object) :-
  !.

coefficient_object(1.0 * Object, Object) :-
  !.

coefficient_object(CoefficientObject, CoefficientObject).


simplify_reaction(Reaction, SimplifiedReaction) :-
  reaction_named(
    Reaction, Name, Kinetics, Reactants, Inhibitors, Products, Reversible
  ),
  make_reaction(
    Name, Kinetics, Reactants, Inhibitors, Products, Reversible,
    SimplifiedReaction
  ).


simplify_kinetics(Kinetics, KineticsSimplified) :-
  (
    Kinetics = (LeftKinetics, RightKinetics)
  ->
    simplify(LeftKinetics, LeftKineticsSimplified),
    simplify(RightKinetics, RightKineticsSimplified),
    (
      LeftKineticsSimplified = RightKineticsSimplified
    ->
      KineticsSimplified = LeftKineticsSimplified
    ;
      KineticsSimplified = (LeftKineticsSimplified, RightKineticsSimplified)
    )
  ;
    simplify(Kinetics, KineticsSimplified)
  ).


simplify_solution(Solution, SimplifiedSolution) :-
  factorize_solution(Solution, SimplifiedSolution).


coefficient_canonical(Coefficient * Object, Coefficient * Canonical) :-
  canonical(Object, Canonical).


factorize_solution([], []).

factorize_solution([Coefficient * Object | Tail], SimplifiedSolution) :-
  collect_object(Tail, Object, TailCoefficient, Others),
  % simplify(Coefficient + TailCoefficient, SimplifiedCoefficient),
  SimplifiedCoefficient is Coefficient + TailCoefficient,
  (
    SimplifiedCoefficient = 0
  ->
    simplify_solution(Others, SimplifiedSolution)
  ;
    SimplifiedSolution = [SimplifiedCoefficient * Object | SimplifiedTail],
    simplify_solution(Others, SimplifiedTail)
  ).


collect_object([], _Object, 0, []).

collect_object([Coefficient * Object | Tail], Object, Sum, Others) :-
  !,
  Sum = Coefficient + TailCoefficient,
  collect_object(Tail, Object, TailCoefficient, Others).

collect_object([CoefficientObject | Tail], Object, Sum, Others) :-
  Others = [CoefficientObject | OthersTail],
  collect_object(Tail, Object, Sum, OthersTail).


simplify_catalyst([], Right, [], [], Right).

simplify_catalyst([LeftCoeff*Head | Tail], Right, NewLeft, Catalyst, NewRight) :-
  (
    select(RightCoeff*Head, Right, OthersRight)
  ->
    Coeff is min(LeftCoeff, RightCoeff),
    Catalyst = [Coeff*Head | CatalystTail],
    (
      Coeff < LeftCoeff
    ->
      Remain is LeftCoeff - Coeff,
      NewLeft = [Remain*Head | RemainLeft],
      NewRight = RemainRight
    ;
      Coeff < RightCoeff
    ->
      Remain is RightCoeff - Coeff,
      NewLeft = RemainLeft,
      NewRight = [Remain*Head | RemainRight]
    ;
      NewLeft = RemainLeft,
      NewRight = RemainRight
    ),
    simplify_catalyst(Tail, OthersRight, RemainLeft, CatalystTail, RemainRight)
  ;
    NewLeft = [LeftCoeff*Head | LeftTail],
    simplify_catalyst(Tail, Right, LeftTail, Catalyst, NewRight)
  ).


make_reaction(
  Name, Kinetics, LeftMolecules, Inhibitors, RightMolecules, Reversible,
  Reaction
) :-
  simplify_kinetics(Kinetics, KineticsSimplified),
  simplify_solution(LeftMolecules, LeftSimplified),
  simplify_solution(RightMolecules, RightSimplified),
  simplify_catalyst(LeftSimplified, RightSimplified, Left, Catalyst, Right),
  (
    append(Left, Catalyst, Reactants),
    kinetics(Reactants, Inhibitors, 'MA'(1), KineticsSimplified)
  ->
    KineticsSimplifiedBis = 'MA'(1)
  ;
    KineticsSimplifiedBis = KineticsSimplified
  ),
  make_reaction(
    Name, KineticsSimplifiedBis, Left, Inhibitors, Catalyst, Right,
    Reversible, Reaction
  ).


:- devdoc(' The left and right hand sides of reactions are now sorted (canonical form) and contain the catalysts').

make_reaction(
  Name, Kinetics, UnsortedLeft, UnsortedInhibitors, UnsortedCatalyst, UnsortedRight, Reversible, ReactionNamed
	     ) :-
  append(UnsortedLeft, UnsortedCatalyst, UnsortedLC),
  simplify_solution(UnsortedLC,SimplifiedLC),
  
  append(UnsortedRight, UnsortedCatalyst, UnsortedRC),
  simplify_solution(UnsortedRC,SimplifiedRC),
  
  sort(SimplifiedLC, Left),
  sort(SimplifiedRC, Right),
  sort(UnsortedInhibitors, Inhibitors),
 
    (
        Name=''
     ->
         ReactionNamed=Reaction
     ;
     ReactionNamed=(Name -- Reaction)
    ),
  Reaction = (Kinetics for Body),
  list_to_solution(Left, LeftSolution),
  list_enumeration(Inhibitors, InhibitorsEnumeration),
  list_to_solution(Right, RightSolution),
  (
    InhibitorsEnumeration = '_'
  ->
    Reactants = LeftSolution
  ;
    Reactants = LeftSolution / InhibitorsEnumeration
  ),
  (
    Reversible = true
  ->
   Body = (Reactants <=> RightSolution)
  ;
   Body = (Reactants => RightSolution)
  ).


:- dynamic(show/2).


compute_ode_for_reactions :-
  enumerate_molecules(AllMolecules),
  forall(
    item([kind: reaction, item: Item]),
    (
      get_stoichiometry_and_kinetics(Item, SimplifiedSolution, KineticsExpression),
      add_molecules(SimplifiedSolution, KineticsExpression)
    )
  ),
  get_option(second_order_closure, Close),
  (
    Close = yes
  ->
    retractall(show(_)),
    forall(
      (
        member(Xi, AllMolecules),
        member(Xr, AllMolecules),
        Xi @=< Xr
      ),
      add_covariance_molecules(Xi, Xr)
    ),
    forall(
      member(X, AllMolecules),
      (
        covariance_name(X, X, Cxx),
        atom_concat(X, '_minus_sd', Xm),
        atom_concat(X, '_plus_sd', Xp),
        % our variance can be negative through approx, so max(0, Var)
        set_macro(function, Xm, X - sqrt(max(0, Cxx))),
        set_macro(function, Xp, X + sqrt(max(0, Cxx))),
        assertz(show(X)),
        assertz(show(Xm)),
        assertz(show(Xp))
      )
    ),
    get_option(show, Show),
    (
      Show = [{}]
    ->
      findall(M, show(M), ToShow),
      change_item([], option, show, option(show: ToShow))
    ;
      true
    )
  ;
    forall(
      member(X, AllMolecules),
      (
        atom_concat(X, '_minus_sd/0', Xm),
        atom_concat(X, '_plus_sd/0', Xp),
        (
          item([kind: function, key: Xm, id: Idm])
        ->
          delete_item(Idm)
        ;
          true
        ),
        (
          item([kind: function, key: Xp, id: Idp])
        ->
          delete_item(Idp)
        ;
          true
        )
      )
    )
  ).


% create ODEs for the model
% normal case only adds to each molecule the rate of each reaction that modifies it times the modification
% second order moment closure adds a compensation term, and equations for the covariance variables
add_molecules(Molecules, Kinetics) :-
  get_option(second_order_closure, Close),
  forall(
    member(Coefficient * Molecule, Molecules),
    (
      Close = no
    ->
      ode_add_expression_to_molecule(Coefficient * Kinetics, Molecule)
    ;
      second_order_compensation(Kinetics, Compensation),
      ode_add_expression_to_molecule(
        Coefficient * (Kinetics + Compensation),
        Molecule
      )
    )
  ).


add_covariance_molecules(Xi, Xr) :-
  covariance_name(Xi, Xr, Sigmair),
  forall(
    item([kind: reaction, item: Item]),
    (
      get_stoichiometry_and_kinetics(Item, SimplifiedSolution, KineticsExpression),
      (
        memberchk(Vji * Xi, SimplifiedSolution)
      ->
        first_order_compensation(KineticsExpression, Xr, F1),
        ode_add_expression_to_molecule(Vji * F1, Sigmair)
      ;
        Vji = 0
      ),
      (
        memberchk(Vjr * Xr, SimplifiedSolution)
      ->
        first_order_compensation(KineticsExpression, Xi, F2),
        ode_add_expression_to_molecule(Vjr * F2, Sigmair)
      ;
        Vjr = 0
      ),
      second_order_compensation(KineticsExpression, Compensation),
      ode_add_expression_to_molecule(
        Vji * Vjr * (KineticsExpression + Compensation),
        Sigmair
      )
    )
  ).


second_order_compensation(Alphaj, Compensation) :-
  enumerate_molecules(Molecules),
  nb_setval(compensation, 0),
  forall(
    (
      member(Xk, Molecules),
      member(Xl, Molecules)
    ),
    (
      derivate(Alphaj, Xk, D1),
      derivate(D1, Xl, D2),
      covariance_name(Xk, Xl, Sigmakl),
      nb_getval(compensation, C1),
      C2 = C1 + D2 * Sigmakl / 2,
      nb_setval(compensation, C2)
    )
  ),
  nb_getval(compensation, Compensation).


first_order_compensation(Alphaj, Xl, Compensation) :-
  enumerate_molecules(Molecules),
  nb_setval(compensation, 0),
  forall(
    member(Xk, Molecules),
    (
      derivate(Alphaj, Xk, D),
      covariance_name(Xk, Xl, Sigmakl),
      nb_getval(compensation, C1),
      C2 = C1 + D * Sigmakl,
      nb_setval(compensation, C2)
    )

  ),
  nb_getval(compensation, Compensation).


covariance_name(Xi, Xj, Name) :-
  msort([Xi, Xj], Xij),
  atomic_list_concat(['cov' | Xij], '_', Name).


negate_coefficient(C*X, D*X) :-
  (
    number(C)
  ->
    D is -C
  ;
    D = -C
  ).


get_stoichiometry_and_kinetics(Item, SimplifiedSolution, KineticsExpression) :-
  reaction(Item, Kinetics, Reactants, Inhibitors, Products),
  kinetics(Reactants, Inhibitors, Kinetics, KineticsExpression),
  maplist(negate_coefficient, Reactants, NegatedReactants),
  append(NegatedReactants, Products, StoichiometricSolution),
  simplify_solution(StoichiometricSolution, SimplifiedSolution).

