:- module(
  revision,
  [
    revise_model/1,
    revise_model/0
  ]
).

:- use_module(ctl).

% Only for separate compilation/linting
:- use_module(doc).
:- use_module(nusmv).
:- use_module(util).

:- doc('This section describes commands to revise a reaction model in order to satisfy a given CTL specification of the behavior. The revision algorithm is described in \\cite{CCFS05tcsb}').

:- devcom('\\begin{todo}These commands should be generalized, first, to influence systems and hybrid reaction-influence systems, and perhaps second, to hybrid specifications of the behavior with attractors and CTL formulas, and per any chance third, in combination with parameter search, to numerical specifications of the behavior with  FOLTL formulas and traces.\\end{todo}').

revise_model(Query) :-
  biocham_command,
  type(Query, ctl),
  doc(
    'Use theory-revision on the current model to satisfy the query given as
    argument.'
  ),
  revise_model(Query, Added, Removed),
  print_message(informational, added(Added)),
  print_message(informational, removed(Removed)).


revise_model :-
  biocham_command,
  doc('Use theory-revision as above, using the currently defined CTL
    specification.'),
  all_items([kind: ctl_spec], Specs),
  join_with_op(Specs, '/\\', Spec),
  revise_model(Spec).


% Trivial protections: do not remove something we added, do not add something
% we removed
:- dynamic(added/1).
:- dynamic(removed/1).

revise_model(Query, Added, Removed) :-
  all_items([kind: reaction], Reactions),
  nb_setval(need_recheck, false),
  retractall(added(_)),
  retractall(removed(_)),
  normalize_query(Query, NQuery, _),
  categorize_query(NQuery, ECTL, UCTL, ACTL),
  revise_model(ECTL, UCTL, ACTL, [], [], []),
  all_items([kind: reaction], NewReactions),
  subtract(NewReactions, Reactions, Added),
  subtract(Reactions, NewReactions, Removed).


% Everything copied as much as possible from CCFS05tcsb

revise_model(ECTL, UCTL, ACTL, Et, Ut, At) :-
  debug(
    revision,
    'calling revise_model(~w, ~w, ~w, ~w, ~w, ~w)',
    [ECTL, UCTL, ACTL, Et, Ut, At]
  ),
  fail.

revise_model([E | ECTL], UCTL, ACTL, Et, Ut, At) :-
  get_option(boolean_initial_states, Init),
  (
    with_option(
      boolean_trace: no,
      check_ctl_impl(E, Init, true)
    )
  ->
    debug(revision, 'Entering step ~w', ['E'])
  ;
    debug(revision, 'Entering step ~w', ['E''']),
    find_and_add_rule,
    % FIXME check Et too since self-loops can be broken by adding a reaction
    append([E | Ut], At, AllSpec),
    recheck_spec(AllSpec, Init)
  ),
  revise_model(ECTL, UCTL, ACTL, [E | Et], Ut, At).

revise_model([], [U | UCTL], ACTL, Et, Ut, At) :-
  get_option(boolean_initial_states, Init),
  (
    with_option(
      boolean_trace: no,
      check_ctl_impl(U, Init, true)
    )
  ->
    debug(revision, 'Entering step ~w', ['U'])
  ;
    (
      debug(revision, 'Entering step ~w', ['U''']),
      find_and_add_rule,
      % FIXME check Et too since self-loops can be broken by adding a reaction
      append([U | Ut], At, AllSpec)
    ;
      debug(revision, 'Entering step ~w', ['U''''']),
      delete_rules,
      % FIXME check At too since self-loops can be added by removing a reaction
      append([U | Ut], Et, AllSpec)
    ),
    recheck_spec(AllSpec, Init)
  ),
  revise_model([], UCTL, ACTL, Et, [U | Ut], At).

revise_model([], [], [A | ACTL], Et, Ut, At) :-
  get_option(boolean_initial_states, Init),
  with_option(
    boolean_trace: silent,
    check_ctl_impl(A, Init, Result)
  ),
  (
    Result = true
  ->
    debug(revision, 'Entering step ~w', ['A']),
    % we only consider adding U' and E' when A is fully satisfied, thus moving
    % a step from the A' step of the algorithm to the A step
    % FIXME check At too since self-loops can be added by removing a reaction
    check_and_split(Et, Ut, [], Ett, Utt, [], Eprime, Uprime, [], Init),
    revise_model(Eprime, Uprime, ACTL, Ett, Utt, [A | At])
  ;
    debug(revision, 'Entering step ~w', ['A''']),
    (
      % always true? Not sure we always have a counter-example
      nusmv:trace(_)
    ->
      % might not be enough to make A true, thus we do not add A to the
      % treated formulae
      delete_rules_from_trace(A, Et, Ut, Init)
    ;
      delete_rules,
      % we delete blindly, so we need to check A now to avoid useless
      % recursion
      with_option(
        boolean_trace: no,
        check_ctl_impl(A, Init, true)
      )
    ),
    nb_setval(need_recheck, true),
    revise_model([], [], [A | ACTL], Et, Ut, At)
  ).


revise_model([], [], [], _, _, _) :-
  all_items([kind: reaction], Reactions),
  debug(revision, 'Success!~nThe new model is: ~w', [Reactions]).


recheck_spec([], _) :-
  !.

recheck_spec(Specs, Init) :-
  join_with_op(Specs, '/\\', Spec),
  debug(revision, 'Checking consolidated query ~w', [Spec]),
  with_option(
    boolean_trace: no,
    check_ctl_impl(Spec, Init, true)
  ).


check_and_split(E, U, A, Et, Ut, At, Ef, Uf, Af, Init) :-
  (
    nb_getval(need_recheck, true)
  ->
    nb_setval(need_recheck, false),
    % TODO use a single NuSMV process, with a single model loading step
    check_and_split(E, Et, Ef, Init),
    check_and_split(U, Ut, Uf, Init),
    check_and_split(A, At, Af, Init)
  ;
    Et = E,
    Ut = U,
    At = A,
    Ef = [],
    Uf = [],
    Af = []
  ).


check_and_split([], [], [], _Init).

check_and_split([F | Formulae], Ft, Ff, Init) :-
  (
    with_option(
      boolean_trace: no,
      check_ctl_impl(F, Init, true)
    )
  ->
    Ft = [F | FFt],
    Ff = FFf
  ;
    Ft = FFt,
    Ff = [F | FFf]
  ),
  check_and_split(Formulae, FFt, FFf, Init).


categorize_query(Q1 /\ Q2, ECTL, UCTL, ACTL) :-
  !,
  categorize_query(Q1, E1, U1, A1),
  categorize_query(Q2, E2, U2, A2),
  append(E1, E2, ECTL),
  append(U1, U2, UCTL),
  append(A1, A2, ACTL).

categorize_query(not(Q), ECTL, UCTL, ACTL) :-
  !,
  categorize_query(Q, NACTL, NUCTL, NECTL),
  maplist(negate_query, NACTL, ACTL),
  maplist(negate_query, NUCTL, UCTL),
  maplist(negate_query, NECTL, ECTL).

categorize_query(Query, [], [Query], []) :-
  object(Query),
  !.

categorize_query(Q, ECTL, UCTL, ACTL) :-
  (
    is_ectl(Q)
  ->
    ECTL = [Q],
    UCTL = [],
    ACTL = []
  ;
    is_actl(Q)
  ->
    ECTL = [],
    UCTL = [],
    ACTL = [Q]
  ;
    ECTL = [],
    UCTL = [Q],
    ACTL = []
  ).


negate_query(Y, not(Y)).


is_actl(not(Q)) :-
  !,
  is_ectl(Q).

is_actl(Q) :-
  Q =.. [Functor | QQ],
  (
    memberchk(Functor, ['EX', 'EF', 'EG', 'EU'])
  ->
    fail
  ;
    maplist(is_actl, QQ)
  ).


is_ectl(not(Q)) :-
  !,
  is_actl(Q).

is_ectl(Q) :-
  Q =.. [Functor | QQ],
  (
    memberchk(Functor, ['AX', 'AF', 'AG', 'AU'])
  ->
    fail
  ;
    maplist(is_ectl, QQ)
  ).


find_and_add_rule :-
  find_rule(R),
  debug(revision, 'Considering adding reaction ~w', [R]),
  add_reaction_backtracking(R).

find_rule(R) :-
  enumerate_all_molecules(Molecules),
  AllMolecules = ['_' | Molecules],
  member(M1, AllMolecules),
  member(M2, AllMolecules),
  (
    M1 \= '_'
  ->
    M1 @< M2
  ;
    M1 @=< M2
  ),
  (
    % both '_'
    M1 = M2
  ->
    M3 = '_'
  ;
    true
  ),
  member(M3, AllMolecules),
  member(M4, AllMolecules),
  memberchk('_', [M1, M2, M3, M4]),
  (
    M3 \= '_'
  ->
    M3 @< M4
  ;
    M3 @=< M4
  ),
  (M1, M2) \= (M3, M4),
  (
    % both '_'
    M3 = M4
  ->
    M1 = '_'
  ;
    true
  ),
  reaction_editor:simplify_reaction('=>'(M1 + M2, M3 + M4), R),
  % Do not add a reaction we removed ourselves
  reaction(R, [reactants: Left, inhibitors: Inhibitors, products: Right]),
  \+ removed((Left, Inhibitors, Right)),
  % Do not add a reaction that already is in the model
  % FIXME should check with different order/kinetics/…
  \+ item([kind: reaction, item: R]).


add_reaction_backtracking(R) :-
  (
    add_item([kind: reaction, item: R]),
    assertz(added(R)),
    debug(revision, 'Adding reaction ~w', [R])
  ;
    delete_item([kind: reaction, item: R]),
    retract(added(R)),
    debug(revision, 'Failure, removing added reaction ~w', [R]),
    fail
  ).


:- dynamic(ruleset_score/2).


delete_rules_from_trace(A, Et, Ut, Init) :-
  retractall(ruleset_score(_, _)),
  enumerate_all_molecules(Molecules),
  findall(State, nusmv:trace(State), States),
  rules_from_trace(States, Molecules, Rules),
  % We cut a single state-transition, if necessary by removing several rules
  % we score the cutsets by checking what they broke
  (
    debugging(revision)
  ->
    Debug = true
  ;
    Debug = false
  ),
  nodebug(revision),
  nb_getval(need_recheck, NeedRecheck),
  (
    (
      member(ToBeDeleted, Rules),
      % Avoid the no-state, or single-state traces
      % FIXME what do we do then?
      ToBeDeleted \= [],
      % Do not delete what we added ourselves
      forall(member(Rule, ToBeDeleted), \+ added(Rule))
    ),
    (
      maplist(del_reaction_backtracking, ToBeDeleted),
      nb_setval(need_recheck, true),
      check_and_split(Et, Ut, [A], _, _, _, Etf, Utf, Atf, Init),
      length(Etf, Etl),
      length(Utf, Utl),
      length(Atf, Atl),
      TrueFormulae is Etl + Atl + Utl,
      assertz(ruleset_score(ToBeDeleted, TrueFormulae))
    ),
    fail
  ;
    true
  ),
  (
     Debug = true
  ->
     debug(revision)
  ;
     true
  ),
  nb_setval(need_recheck, NeedRecheck),
  findall(Score-RuleSet, ruleset_score(RuleSet, Score), Choices),
  keysort(Choices, SortedChoices),
  % now iterate on the cutsets in order
  member(_-ChoiceToBeDeleted, SortedChoices),
  maplist(del_reaction_backtracking, ChoiceToBeDeleted).


rules_from_trace([_], _, []).

rules_from_trace([S1, S2 | States], Molecules, [R | Rules]) :-
  rules_from_states(S1, S2, Molecules, R),
  debug(
    revision,
    "possible use of reaction ~p between states ~w and ~w", [R, S1, S2]
  ),
  rules_from_trace([S2 | States], Molecules, Rules).


rules_from_states(S1, S2, Molecules, R) :-
  rules_from_states(S1, S2, Molecules, [], [], [], R).


rules_from_states([], [], [], Inc, Dec, Pres, Reactions) :-
  msort(Inc, SInc),
  msort(Dec, SDec),
  msort(Pres, SPres),
  findall(Reaction, compatible(SInc, SDec, SPres, Reaction), Reactions).

rules_from_states(["TRUE" | S1], ["TRUE" | S2], [M | Molecules], I, D, P, R) :-
  rules_from_states(S1, S2, Molecules, I, D, [M | P], R).

rules_from_states(["TRUE" | S1], ["FALSE" | S2], [M | Molecules], I, D, P, R) :-
  rules_from_states(S1, S2, Molecules, I, [M | D], [M | P], R).

rules_from_states(["FALSE" | S1], ["TRUE" | S2], [M | Molecules], I, D, P, R) :-
  rules_from_states(S1, S2, Molecules, [M | I], D, P, R).

rules_from_states(["FALSE" | S1], ["FALSE" | S2], [_ | Molecules], I, D, P, R) :-
  rules_from_states(S1, S2, Molecules, I, D, P, R).


delete_rules :-
  % FIXME Biocham v3 never deletes blindly
  fail,
  all_items([kind: reaction], Reactions),
  extract_sublist(Reactions, ToBeDeleted),
  ToBeDeleted \= [],
  % Do not delete what we added ourselves
  forall(member(Rule, ToBeDeleted), \+ added(Rule)),
  maplist(del_reaction_backtracking, ToBeDeleted).


del_reaction_backtracking(R) :-
  reaction(R, [reactants: Left, inhibitors: Inhibitors, products: Right]),
  sort(Left, SLeft),
  sort(Inhibitors, SInhibitors),
  sort(Right, SRight),
  (
    delete_item([kind: reaction, item: R]),
    assertz(removed((SLeft, SInhibitors, SRight))),
    debug(revision, 'Removing reaction ~w', [R])
  ;
    add_item([kind: reaction, item: R]),
    retract(removed((SLeft, SInhibitors, SRight))),
    debug(revision, 'Failure, adding back reaction ~w', [R]),
    fail
  ).


compatible(SInc, SDec, SPres, Reaction) :-
  debug(
    revision,
    "Testing compatibility with SInc: ~w, SDec: ~w and SPres: ~w",
    [SInc, SDec, SPres]
  ),
  item([kind: reaction, item: Reaction]),
  reaction(Reaction, [reactants: Reactants, products: Products]),
  % TODO something with inhibitors
  remove_stoichiometry_and_sort(Reactants, Products, UReac, UProd, UCata),
  debug(
    revision,
    "Reaction ~w (~w, ~w) has UReac: ~w, UProd: ~w and UCata: ~w",
    [Reaction, Reactants, Products, UReac, UProd, UCata]
  ),
  % products are true at the end
  ord_union(SInc, SPres, SAfter),
  ord_subset(UProd, SAfter),
  % only products can increase
  ord_subset(SInc, UProd),
  % only reactants can decrease
  ord_subset(SDec, UReac),
  % all reactants and catalyst had to be present
  ord_subset(UReac, SPres),
  ord_subset(UCata, SPres).


remove_stoichiometry_and_sort(Reactants, Products, UReac, UProd, UCata) :-
  maplist(remove_stoich, Reactants, UR),
  maplist(remove_stoich, Products, UP),
  msort(UR, SR),
  msort(UP, SP),
  ord_subtract(SR, SP, UReac),
  ord_subtract(SP, SR, UProd),
  ord_intersection(SR, SP, UCata).


remove_stoich(_*X, X).


prolog:message(added(Added)) -->
  { maplist(term_to_atom, Added, AddedAtoms),
    atomic_list_concat(AddedAtoms, ', ', AddedAtom) },
  ['Added reactions: ~w'-[AddedAtom]].

prolog:message(removed(Removed)) -->
  { maplist(term_to_atom, Removed, RemovedAtoms),
    atomic_list_concat(RemovedAtoms, ', ', RemovedAtom) },
  ['Removed reactions: ~w'-[RemovedAtom]].
