:- module(
  influence_graphs,
  [
    % Commands
    influence_graph/0,
    influence_hypergraph/0,
    draw_influences/0,
    draw_influence_hypergraph/0,
    % Public API
    influence_graph/1,
    influence_hypergraph/1
  ]
).


:- use_module(doc).


:- doc('Biocham can manipulate three notions of influence graph of a model.').

:- doc('
First, the influence hypergraph of an influence network or a reaction network depicts the structure of the influences between the constituants of a model. It is represented by a bipartite molecule-influence graph.'). 

:- devdoc('\\section{Commands}').


influence_hypergraph :-
  biocham_command,
  doc('builds the hypergraph of the influence rules of the current model.'),
  delete_items([kind: graph, key: influence_hypergraph]),
  new_graph,
  set_graph_name(influence_hypergraph),
  get_current_graph(GraphId),
  influence_hypergraph(GraphId).

draw_influence_hypergraph :-
  biocham_command,
  doc('
    Draws the hypergraph of the influence rules of the current model. The hypergraph distinguishes each influence. Equivalent to \\texttt{influence_hypergraph. draw_graph}.
  '),
  setup_call_cleanup(
    new_graph(GraphId),
    (
      influence_hypergraph(GraphId),
      draw_graph(GraphId)
    ),
    delete_item(GraphId)
  ).


:- doc('Second, the influence graph of a reaction or influence network is a signed directed simple graph between the molecular species.
This graph an abstraction defined from the stoichiometry of the reactions, which is equivalent, under general conditions, to the influence graph defined by the signs of the Jacobian matrix of the ODEs \\cite{FS08fmsb,FGS15tcs}.').


influence_graph :-
  biocham_command,
  doc('builds the influence graph between molecular species of the current model without distinguishing between reaction and influence rules.'),
  delete_items([kind: graph, key: influence_graph]),
  new_graph,
  set_graph_name(influence_graph),
  get_current_graph(GraphId),
  influence_graph(GraphId).


draw_influences :-
  biocham_command,
  doc('
    draws the influence graph between molecular species of the current model. Equivalent to \\texttt{influence_graph. draw_graph}.
    \\begin{example}
  '),
  biocham_silent(clear_model),
  biocham(load(library:examples/mapk/mapk)),
  biocham(draw_influences),
  doc('
    \\end{example}
  '),
  setup_call_cleanup(
    new_graph(GraphId),
    (
      influence_graph(GraphId),
      draw_graph(GraphId)
    ),
    delete_item(GraphId)
  ).

:- devdoc('\\section{Public API}').


influence_graph(GraphId) :-
  \+ (
    (
      item([kind: reaction, item: Item]),
      reaction(Item, [
        reactants: Reactants,
        inhibitors: Inhibitors,
        products: Products
      ]),
      Origin = Item,
      substract_list(Reactants, Products, Difference),
      (
        member(_ * Input, Reactants),
        InputSign = +
      ;
        member(Input, Inhibitors),
        InputSign = -
      ),
      member(Coefficient * Output, Difference),
      (
        Coefficient > 0
      ->
        RuleSign = +
      ;
        RuleSign = -
      )
    ;
      item([kind: influence, item: Item]),
      influence(Item, [
        positive_inputs: PositiveInputs,
        negative_inputs: NegativeInputs,
        sign: RuleSign,
        output: Output
      ]),
      Origin = Item,
      (
        member(Input, PositiveInputs),
        InputSign = +
      ;
        member(Input, NegativeInputs),
        InputSign = -
      )
    ),
    \+ (
      sign_rule(RuleSign, InputSign, Sign),
      add_vertex(GraphId, Input, InputId),
      add_vertex(GraphId, Output, OutputId),
      add_edge(GraphId, InputId, OutputId, EdgeId),
      % graph structure only has one edge per pair source -> dest, so we
      % handle incompatible arcs with a third kind of sign
      (
        get_attribute(EdgeId, sign = OldSign)
      ->
        (
          OldSign == Sign
        ->
          true
        ;
          set_attribute(EdgeId, sign = '±')
        )
      ;
        set_attribute(EdgeId, sign = Sign)
      ),
      (
        get_attribute(EdgeId, origin = OldOrigin)
      ->
        set_attribute(EdgeId, origin = [(Sign, Origin) | OldOrigin])
      ;
        set_attribute(EdgeId, origin = [(Sign, Origin)])
      )
    )
  ),
  set_sources(GraphId).


influence_hypergraph(GraphId) :-
  set_counter(influence, 0),
  \+ (
    item([kind: influence, item: Item]),
    influence(Item, [
      force: Force,
      name: Name,
      positive_inputs: PositiveInputs,
      negative_inputs: NegativeInputs,
      sign: Sign,
      output: Output
    ]),
    \+ (
      (
        Name = ''
      ->
        count(influence, InfluenceCount),
        format(atom(TransitionName), 'influence~d', [InfluenceCount])
      ;
        TransitionName = Name
      ),
      transition(GraphId, TransitionName, TransitionId),
      set_attribute(TransitionId, sign = Sign),
      set_attribute(TransitionId, force = Force),
      \+ (
        member(PositiveInput, PositiveInputs),
        \+ (
          place(GraphId, PositiveInput, PlaceId),
          add_edge(GraphId, PlaceId, TransitionId, EdgeId),
          set_attribute(EdgeId, sign = '+')
        )
      ),
      \+ (
        member(NegativeInput, NegativeInputs),
        \+ (
          place(GraphId, NegativeInput, PlaceId),
          add_edge(GraphId, PlaceId, TransitionId, EdgeId),
          set_attribute(EdgeId, sign = '-')
        )
      ),
      place(GraphId, Output, OutputId),
      add_edge(GraphId, TransitionId, OutputId, _)
    )
  ),
  set_sources(GraphId).


:- devdoc('\\section{Private predicates}').



sign_rule(+, A, A).

sign_rule(-, +, -).

sign_rule(-, -, +).
