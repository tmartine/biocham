:- use_module(library(plunit)).

:- begin_tests(aliases, [setup((clear_model, reset_options))]).

test('alias', [true(Reactions == ['MA'(1) for 2 * a => c])]) :-
  clear_model,
  command(a + b => c),
  command(alias(a = b)),
  all_items([kind: reaction], Reactions).

:- end_tests(aliases).
