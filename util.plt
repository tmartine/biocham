:- use_module(library(plunit)).

:- begin_tests(util, [setup((clear_model, reset_options))]).

test('new_parameter') :-
  clear_model,
  new_parameter('k_1'),
  new_parameter('k_2').

test('new_molecule') :-
  clear_model,
  command('A' => 'B'),
  new_molecule('C').

test('expand_polynomial') :-
  expand_polynomial(a*(b+c)/d, a/d*b + a/d*c),
  expand_polynomial(a*(b+c-d), (a*b)+(a*c)-(a*d)),
  expand_polynomial((a+b)*(c-d), a*c+b*c-a*d-b*d),
  expand_polynomial(p*r1*n1*(1-(n0+n1+n2)/k)/co, p/co*r1*n1*1-p/co*r1*n1*(n0/k)-p/co*r1*n1*(n1/k)-p/co*r1*n1*(n2/k)),
  expand_polynomial(bb/e*(pa+ -1)*db, bb/e*pa*db + bb/e*(-1)*db).

test('substitute_once', []) :-
  substitute_once(a, z, e*b*c(d), e*b*c(d)),
  substitute_once(a, z, a*b*c(d), z*b*c(d)),
  substitute_once(a, z, a*b*c(a), z*b*c(a)),
  substitute_once(a, z, b*a*c(a), b*z*c(a)),
  substitute_once(a, z, e*b*c(d,a), e*b*c(d,z)),
  substitute_once(a, z, e*b*a(d), e*b*z(d)),
  substitute_once(a, z, e*b*a(a), e*b*z(a)).

:- end_tests(util).
