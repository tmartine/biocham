:- module(
  pattern,
  [
    % Public API
    pattern_reduction/2,
    yesnomaybe/1
  ]
).

% Only for separate compilation/linting
:- use_module(doc).
:- use_module(biocham).
:- use_module(sepi_infos).


:- doc('This section describes commands to rewrite a reaction graph by reducing some specified patterns (for example Michaelis-Menten patterns). The idea behind this function is to use graph rewriting before searching SEPIs (with the command \\command{search_reduction/2}. It is also possible to expand reduced Michaelis-Menten patterns, in order to exhibit the intermediary step of reversible complexation enzyme-substrate.').


:- initial(option(michaelis_menten: yes)).
:- initial(option(r_1: yes)).
:- initial(option(r_2: no)).
:- initial(option(ep: no)).
:- initial(option(enzyme: yes)).
:- initial(option(hill_reaction: no)).
:- initial(option(partial_hill_reaction: no)).
:- initial(option(double_michaelis_menten: no)).
:- initial(option(michaelis_menten_expansion: no)).


:- grammar(yesnomaybe).

yesnomaybe(yes).
:- grammar_doc('\\emphright{Value yes.}').

yesnomaybe(no).
:- grammar_doc('\\emphright{Value no.}').

yesnomaybe(maybe).
:- grammar_doc('\\emphright{Value maybe.}').


:- dynamic(seen/1).


pattern_reduction(Input_file, Output_file) :-
  biocham_command,
  type(Input_file, input_file),
  type(Output_file, input_file),
  doc('The patterns to be searched into the input graph (the graph associated to the Biocham model given as \\argument{Input_file}) are specified by the options. For a pattern, being found into a graph means that there is a subgraph isomorphism from the graph to the pattern and that only the vertices sent to the image vertices enzyme, substrate, product can have edges with other vertices in the graph (for example it constrains the complex not to intervene in any other reaction). The number of found patterns is printed in the terminal. In the output model (written in a new file .bc, with name specified by the user as \\argument{Output_file}), all occurrences of these patterns are reduced to the one-reaction Michaelis-Menten pattern, with a double-arrow from the enzyme. The michaelis_menten_expansion option has the opposite effect to expand reduced Michaelis-Menten patterns'),
  option(michaelis_menten, yesno, MM_complete, 'specifies if reducing Michaelis-Menten patterns'),
  option(r_1, yesnomaybe, MM_R_1, 'R-1 reaction (from the commplex ES to enzyme E and substrate S) in the searched Michaelis-Menten pattern'),
  option(r_2, yesnomaybe, MM_R_2, 'R-2 reaction (from the product P and enzyme E to the complex EP or ES) in the searched Michaelis-Menten pattern'),
  option(ep, yesnomaybe, MM_EP, 'irreversible reaction from the complex ES to the complex EP in the searched Michaelis-Menten pattern'),
  option(hill_reaction, yesno, Hill, 'specifies if reducing Hill patterns'),
  option(partial_hill_reaction, yesno, Part_Hill, 'specifies if reducing partial Hill patterns'),
  option(double_michaelis_menten, yesno, Double_MM, 'specifies if reducing double Michaelis-Menten'),
  option(enzyme, yesno, MM_E, 'specifies if the reduced Michaelis-Menten pattern (that will appear in the output graph) is with enzyme or no'),
  option(michaelis_menten_expansion, yesno, MM_expansion, 'expansion of Michaelis-Menten patterns (form the reduced form with enzyme to form with complex ES and reaction R-1), should be used with option michaelis_menten:no to avoid confusion'),
  debug(motif, "Debugging motif reduction", []),
  once(get_graph_info(Input_file, Number_species, Edges, Number_vertex, Id)),
  Graph = [Number_vertex, Number_species, Edges, Id],
  once(construct_dict_arcs(Edges, Dict_normal_arcs, Dict_reversed_arcs)),
  debug(motif, "Edges:~w ~nNormal arcs: ~w ~nReversed arcs: ~w", [Edges, Dict_normal_arcs, Dict_reversed_arcs]),
  retractall(seen(_)),
  assert(seen(Number_vertex)),
  open(Output_file, write, Stream),
  (
    MM_complete = yes
  ->
    michaelis_menten_detection(Graph, Stream, Dict_normal_arcs, Dict_reversed_arcs, MM_R_1, MM_R_2, MM_EP, MM_E),
    debug(motif, "Done looking for Michaelis Menten motifs", [])
  ;
    true
  ),
  (
    Hill = yes
  ->
    hill_detection(Graph, Stream, Dict_normal_arcs, Dict_reversed_arcs),
    debug(motif, "Done looking for Hill motifs", [])
  ;
    true
  ),
  (
    Part_Hill = yes
  ->
    partial_hill_detection(Graph, Stream, Dict_normal_arcs, Dict_reversed_arcs),
    debug(motif, "Done looking for partial Hill motifs", [])
  ;
    true
  ),
  (
    Double_MM = yes
  ->
    partial_michaelis_menten(Graph, Stream, Dict_normal_arcs, Dict_reversed_arcs),
    debug(motif, "Done looking for double Michaelis Menten", [])
  ;
    true
  ),
  (
    MM_expansion = yes
  ->
    michaelis_menten_expansion(Graph, Stream, Dict_normal_arcs, Dict_reversed_arcs),
    debug(motif, "Done looking for reduced Michaelis Menten", [])
  ;
    true
  ),
  write_reactions(Graph, Stream, Dict_normal_arcs, Dict_reversed_arcs),
  close(Stream),
  retractall(seen(_X)).

:- doc("\\begin{example}
Example of reduction of a Michaelis-Menten pattern with reactions R-1, R-2 and complex EP.
Input graph :

\\html{<div><img alt=\"reaction graph with the reversible formation of the complex enzyme-substrate ES, the irreversible transformation of the complex from ES to EP and the reversible decomplexation from EP to enzyme and product\"  src=\"MM_ESP.png\"/></div>}

\\trace{
biocham: load(library:examples/sepi/MM_variants/MM_ESP.bc).
biocham: option(michaelis_menten:yes).
biocham: option(r_1:maybe,r_2:maybe,ep:maybe).
biocham: pattern_reduction(library:examples/sepi/MM_variants/MM_ESP.bc, MM_ESP_reduced.bc).

Number of Michaelis Menten patterns: 1
biocham: load(MM_ESP_reduced.bc).
}

Output graph (reduced Michaelis-Menten pattern) :

\\html{<div><img alt=\"Reduced Michaelis-Menten reaction\"  src=\"MM_reduced.png\"/></div>}


\\end{example}
").
:- biocham_silent(prolog("delete_file('MM_ESP_reduced.bc')")).

:- doc("\\begin{example}
Hill pattern (can be reduced with option hill_reaction) :
\\html{<div><img alt=\"Hill reaction (two ligant sites on the enzyme)\"  src=\"hill.png\"/></div>}
\\end{example}
").

:- doc("\\begin{example}
Partial Hill pattern (can be reduced with option partial_hill_reaction) :
\\html{<div><img alt=\"Partial Hill reaction (twice the same ligand site on the enzyme)\"  src=\"hill_partial.png\"/></div>}
\\end{example}
").

:- doc("\\begin{example}
Double Michaelis-Menten pattern (can be reduced with option double_michaelis_menten) :
\\html{<div><img alt=\"Double Michaelis-Menten reaction (two forms of the complex, ES and ESS, can release the product)\"  src=\"double_MM.png\"/></div>}
\\end{example}
").

:- doc("\\begin{example}
Example of expansion of reduced Michaelis-Menten in a MAPK cascade.
\\trace{
biocham: load(library:examples/sepi/mapk0.bc).
biocham: draw_reactions.
biocham: option(michaelis_menten:no).
biocham: option(michaelis_menten_expansion:yes).
biocham: pattern_reduction(library:examples/sepi/mapk0.bc, mapk0_expanded.bc).

Number of Michaelis Menten patterns: 2
biocham: load(mapk0_expanded.bc).
biocham: draw_reactions.

}
\\end{example}
").
:- biocham_silent(prolog("delete_file('mapk0_expanded.bc')")).

%! get_type(+A, +ID, -Type)
%
% get type of a vertex
% 0 if specie 1 if reaction
%
% @arg A    number representing the vertex
% @arg ID   id of the graph
% @arg Type 0/1
get_type(A, ID, Type):-
  species(NameA, A, ID),
  (
    atom_concat("reaction", _, NameA)
  ->
    Type is 1
  ;
    Type is 0
  ).


%! write_reaction(+Reactant_list, +Product_list, +Id, +Stream)
%
% write a reaction in a biocham format
%
% @arg Reactant_list  List of numbers representing reactants
% @arg Product_list   List of numbers representing products
% @arg ID             id of the graph
% @arg Stream         Stream of the output file
write_reaction(Reactant_list, Product_list, Id, Stream):-
  write_list(Reactant_list, Id, Stream),
  format(Stream, "=>", []),
  write_list(Product_list, Id, Stream),
  format(Stream, ".~n", []).


%! write_list(+List, +Id, +Stream)
%
% write a reaction in a biocham format
%
% @arg List   List of species
% @arg ID     id of the graph
% @arg Stream Stream of the output file
write_list([], _, Stream):-
  format(Stream, "_", []).

write_list([Specie], Id, Stream):-
  (
    species(Name, Specie, Id)
  ->
    format(Stream, "~w", [Name])
  ;
    format(Stream, "~w", [Specie])
  ).

write_list([Head|Tail], Id, Stream):-
  not(Tail = []),
  write_list(Tail, Id, Stream),
  (
    species(Name, Head, Id)
  ->
    format(Stream, "+~w", [Name])
  ;
    format(Stream, "+~w", [Head])
  ).


%! write_reactions(+Graph, +Stream, +Dict_normal_arcs, +Dict_reversed_arcs)
%
% write all the remaining reactions (not concerned by a motif)
%
% @arg Graph              the graph
% @arg Stream             Output file for the new graph
% @arg Dict_normal_arcs   dict of normal arcs
% @arg Dict_reversed_arcs dict of reversed arcs 
write_reactions(Graph, Stream, Dict_normal_arcs, Dict_reversed_arcs):-
  Graph = [Number_vertex, Number_species, _, Id],
  Number_reactions is Number_vertex - Number_species - 1,
  numlist(0,Number_reactions,Reaction_list),
  forall(
    (
      member(Reaction, Reaction_list)
    ),
    (
      R is Reaction + Number_species,
      (
        not(seen(R))
      ->
        (
          get_dict(R, Dict_normal_arcs, Product_list)
        ;
          Product_list = []
        ),
        (
          get_dict(R, Dict_reversed_arcs, Reactant_list)
        ;
          Reactant_list = []
        ),
        write_reaction(Reactant_list, Product_list, Id, Stream),
        assert(seen(R))
      ;
        true
      )
    )
  ).


%! construct_dict_arcs(+Arcs, -Dict_normal_arcs, -Dict_reversed_arcs)
%
% constructs dictionary from list of edges
%
% @arg Arcs               list of edges
% @arg Dict_normal_arcs   dict of normal arcs
% @arg Dict_reversed_arcs dict of reversed arcs
construct_dict_arcs([(Vertex_A, Vertex_B)], Dict_normal_arcs, Dict_reversed_arcs):-
  Dict_normal_arcs = arcs1{}.put(Vertex_A, [Vertex_B]),
  Dict_reversed_arcs = arcs_1{}.put(Vertex_B, [Vertex_A]).

construct_dict_arcs([Arc, Head|Tail], New_dict_normal_arcs, New_dict_reversed_arcs):-
  construct_dict_arcs([Head | Tail], Sub_dict_normal_arcs, Sub_dict_reversed_arcs),
  Arc = (Vertex_A, Vertex_B),
  (
    get_dict(Vertex_A, Sub_dict_normal_arcs, Outgoing_vertex_A)
  ->
    append([Vertex_B], Outgoing_vertex_A, Updated_Outgoing_vertex_A),
    get_dict(Vertex_A, Sub_dict_normal_arcs, Outgoing_vertex_A, New_dict_normal_arcs, Updated_Outgoing_vertex_A)
  ;
    New_dict_normal_arcs = Sub_dict_normal_arcs.put(Vertex_A, [Vertex_B])
  ),
  (
    get_dict(Vertex_B, Sub_dict_reversed_arcs, Outgoing_vertex_B)
  ->
    append([Vertex_A], Outgoing_vertex_B, Updated_Outgoing_vertex_B),
    get_dict(Vertex_B, Sub_dict_reversed_arcs, Outgoing_vertex_B, New_dict_reversed_arcs, Updated_Outgoing_vertex_B)
  ;
    New_dict_reversed_arcs = Sub_dict_reversed_arcs.put(Vertex_B, [Vertex_A])
  ).


%! michaelis_menten_detection(+Graph, +Stream, +Dict_normal_arcs, +Dict_reversed_arcs, +MM_R_1, +MM_R_2, +MM_EP, +MM_E)
%
% looks for michaelis menten motifs
%
% @arg Graph              the input graph
% @arg Stream             output file for the new graph
% @arg Dict_normal_arcs   dict of normal arcs
% @arg Dict_reversed_arcs dict of reversed arcs 
% @arg MM_R_1             yes/no/maybe
% @arg MM_R_2             yes/no/maybe
% @arg MM_EP              yes/no/maybe
% @arg MM_E               yesno
michaelis_menten_detection(Graph, Stream, Dict_normal_arcs, Dict_reversed_arcs, MM_R_1, MM_R_2, MM_EP, MM_E):-
  debug(motif, "Option: MM_R_1:~w MM_R_2:~w MM_EP:~w", [MM_R_1, MM_R_2, MM_EP]),
  (
    MM_R_1 = maybe
  ->
    Possible_R_1 = [yes, no]
  ;
    Possible_R_1 = [MM_R_1]
  ),
  (
    MM_R_2 = maybe
  ->
    Possible_R_2 = [yes, no]
  ;
    Possible_R_2 = [MM_R_2]
  ),
  (
    MM_EP = maybe
  ->
    Possible_EP = [yes, no]
  ;
    Possible_EP = [MM_EP]
  ),
  Graph = [_, Number_species, _, Id],
  Number_species_1 is Number_species - 1,
  numlist(0, Number_species_1, Species_list),
  set_counter(michaelis_menten_motif_count, 0),
  forall(
    member(ES, Species_list),
    forall(
      (
        member(R_1_option, Possible_R_1),
        member(R_2_option, Possible_R_2),
        member(EP_option, Possible_EP)
      ),
      (
        once(michaelis_menten_motif(ES, Dict_normal_arcs, Dict_reversed_arcs, E, S, P, R_1_option, R_2_option, EP_option))
      ->
        debug(motif, "Michaelis Menten motif with E:~w S:~w P:~w ES:~w", [E, S, P, ES]),
        (
          MM_E = yes
        ->
          write_reaction([E, S], [E, P], Id, Stream)
        ;
          write_reaction([S], [P], Id, Stream)
        ),
        count(michaelis_menten_motif_count, _)
      ;
        true
      )
    )
  ),
  peek_count(michaelis_menten_motif_count, Nb_michelis_menten),
  write('\nNumber of Michaelis Menten patterns: '), 
  write(Nb_michelis_menten), 
  write('\n').


%! michaelis_menten_motif(+ES, +Dict_normal_arcs, +Dict_reversed_arcs, -E, -S, -P, +MM_R_1, +MM_R_2, +MM_EP)
%
% checks if the concerned species and reactions form a michaelis menten pattern
%
% @arg ES                 catalyser, where starts the pattern 
% @arg Dict_normal_arcs   dict of normal arcs
% @arg Dict_reversed_arcs dict of reversed arcs 
% @arg E                  enzyme
% @arg S                  substrate
% @arg P                  product 
% @arg MM_R_1             yes/no/maybe
% @arg MM_R_2             yes/no/maybe
% @arg MM_EP              yes/no/maybe
michaelis_menten_motif(ES, Dict_normal_arcs, Dict_reversed_arcs, E, S, P, MM_R_1, MM_R_2, MM_EP):-
  get_dict(ES, Dict_reversed_arcs, Ingoing_ES),
  (
    MM_R_2 = yes,
    MM_EP = no
  ->
    (
      Ingoing_ES = [R1, R_2]
    ;
      Ingoing_ES = [R_2, R1]
    )
  ;
    Ingoing_ES = [R1]
  ),
  get_dict(ES, Dict_normal_arcs, Outgoing_ES),
  debug(motif_test, "ES partially identified: ES:~w R1:~w Outgoing_ES:~w", [ES, R1, Outgoing_ES]),
  (
    MM_R_1 = yes
  ->
    (
      MM_EP = yes
    ->
      (
        Outgoing_ES = [R_1, R3]
      ;
        Outgoing_ES = [R3, R_1]
      )
    ;
      (
        Outgoing_ES = [R_1, R2]
      ;
        Outgoing_ES = [R2, R_1]
      )
    )
  ;
    (
      MM_EP = yes
    ->
      Outgoing_ES = [R3]
    ;
      Outgoing_ES = [R2]
    )
  ),
  debug(motif_test, "ES identified: ES:~w R1:~w R_1:~w R2:~w", [ES, R1, R_1, R2]),
  get_dict(R1, Dict_normal_arcs, Outgoing_R1),
  Outgoing_R1 = [ES],
  get_dict(R1, Dict_reversed_arcs, Ingoing_R1),
  (
    Ingoing_R1 = [E, S]
  ;
    Ingoing_R1 = [S, E]
  ),
  debug(motif_test, "R1 confirmed", []),
  (
    MM_R_1 = yes
  ->
    get_dict(R_1, Dict_normal_arcs, Outgoing_R_1),
    (
      Outgoing_R_1 = [E, S]
    ;
      Outgoing_R_1 = [S, E]
    ),
    get_dict(R_1, Dict_reversed_arcs, Ingoing_R_1),
    Ingoing_R_1 = [ES]
  ;
    true
  ),
  debug(motif_test, "R_1 confirmed", []),
  (
    MM_EP = yes
  ->
    get_dict(R3, Dict_normal_arcs, Outgoing_R3),
    Outgoing_R3 = [EP],
    get_dict(R3, Dict_reversed_arcs, Ingoing_R3),
    Ingoing_R3 = [ES],
    get_dict(EP, Dict_normal_arcs, Outgoing_EP),
    Outgoing_EP = [R2],
    get_dict(EP, Dict_reversed_arcs, Ingoing_EP),
    (
      MM_R_2 = yes
    ->
      (
        Ingoing_EP = [R3, R_2]
      ;
        Ingoing_EP = [R_2, R3]
      )
    ;
      Ingoing_EP = [R3]
    ),
    debug(motif_test, "EP confirmed", [])
  ;
    true
  ),
  (
    MM_R_2 = yes
  ->
    get_dict(R_2, Dict_normal_arcs, Outgoing_R_2),
    (
      MM_EP = yes
    ->
      Outgoing_R_2 = [EP]
    ;
      Outgoing_R_2 = [ES]
    ),
    get_dict(R_2, Dict_reversed_arcs, Ingoing_R_2),
    (
      Ingoing_R_2 = [P, E]
    ;
      Ingoing_R_2 = [E, P]
    ),
    debug(motif_test, "R_2 confirmed", [])
  ;
    true
  ),
  get_dict(R2, Dict_normal_arcs, Outgoing_R2),
  (
    Outgoing_R2 = [E, P]
  ;
    Outgoing_R2 = [P, E]
  ),
  get_dict(R2, Dict_reversed_arcs, Ingoing_R2),
  (
    MM_EP = yes
  ->
    Ingoing_R2 = [EP]
  ;
    Ingoing_R2 = [ES]
  ),
  debug(motif_test, "R2 confirmed", []),
  (
    MM_R_1 = yes
  ->
    assert(seen(R_1))
  ;
    true
  ),
  (
    MM_R_2 = yes
  ->
    assert(seen(R_2))
  ;
    true
  ),
  (
    MM_EP = yes
  ->
    assert(seen(R3))
  ;
    true
  ),
  assert(seen(R1)),
  assert(seen(R2)).


/*  michaelis_menten_expansion  */
/*  michaelis_menten_expansion(+Graph, +Stream, +Dict_normal_arcs, +Dict_reversed_arcs)
    -> looks for michaelis menten motifs
    +Graph: the graph
    +Stream: where to write the new graph
    +Dict_normal_arcs: dict of directed arcs
    +Dict_reversed_arcs: dict of undirected arcs */
michaelis_menten_expansion(Graph, Stream, Dict_normal_arcs, Dict_reversed_arcs):-
  Graph = [Number_vertex, Number_species, _, Id],
  Number_vertex_1 is Number_vertex - 1,
  numlist(Number_species, Number_vertex_1, Reaction_list),
  set_counter(michaelis_menten_expense_count, 0),
  forall(
    member(R, Reaction_list),
    (
      once(michaelis_menten_reduced_motif(R, Dict_normal_arcs, Dict_reversed_arcs, E, S, P))
    ->
      debug(motif, "Michaelis Menten motif with E:~w S:~w P:~w", [E, S, P]),
      peek_count(michaelis_menten_expense_count, Nb_tmp_michelis_menten),
      atom_concat("ES_", Nb_tmp_michelis_menten, ES),
      write_reaction([E, S], [ES], Id, Stream),
      write_reaction([ES], [E, S], Id, Stream),
      write_reaction([ES], [E, P], Id, Stream),
      count(michaelis_menten_expense_count, _)
    ;
      true
    )
  ),
  peek_count(michaelis_menten_expense_count, Nb_michelis_menten),
  write('\nNumber of Michaelis Menten patterns: '), 
  write(Nb_michelis_menten), 
  write('\n').


/*  michaelis_menten_reduced_motif  */
/*  michaelis_menten_reduced_motif(+R, +Dict_normal_arcs, +Dict_reversed_arcs, +E, +S, +P)
    -> checks if the concerned species and reactions form a michaelis menten motif
    +R: reaction
    +Dict_normal_arcs: dict of directed arcs
    +Dict_reversed_arcs: dict of undirected arcs
    +E: enzyme
    +S: substrate
    +P: product */
michaelis_menten_reduced_motif(R, Dict_normal_arcs, Dict_reversed_arcs, E, S, P):-
  get_dict(R, Dict_normal_arcs, Outgoing_R),
  (
    Outgoing_R = [E, P]
  ;
    Outgoing_R = [P, E]
  ),
  get_dict(R, Dict_reversed_arcs, Ingoing_R),
  (
    Ingoing_R = [E, S]
  ;
    Ingoing_R = [S, E]
  ),
  assert(seen(R)).


/*  hill_detection  */
/*  hill_detection(+Graph, +Stream, +Dict_normal_arcs, +Dict_reversed_arcs)
    -> looks for hill reactions
    +Graph: the graph
    +Stream: where to write the new graph
    +Dict_normal_arcs: dict of directed arcs
    +Dict_reversed_arcs: dict of undirected arcs */
hill_detection(Graph, Stream, Dict_normal_arcs, Dict_reversed_arcs):-
  Graph = [_, Number_species, _, Id],
  Number_species_1 is Number_species - 1,
  numlist(0, Number_species_1, Species_list),
  set_counter(hill_pattern_count, 0),
  forall(
    (
      member(SES, Species_list)
    ),
    (
      (
        not(seen(SES)),
        once(hill_reaction(SES, Dict_normal_arcs, Dict_reversed_arcs, E, S, P))
      ->
        debug(motif, "Hill reaction with E:~w S:~w P:~w SES:~w", [E, S, P, SES]),
        write_reaction([E, S], [E, P], Id, Stream),
        count(hill_pattern_count, _)
      ;
        true
      )
    )
  ),
  peek_count(hill_pattern_count, Nb_hill_reaction),
  write('\nNumber of Hill patterns: '), 
  write(Nb_hill_reaction), 
  write('\n').


/*  hill_reaction  */
/*  hill_reaction(+SES, +Dict_normal_arcs, +Dict_reversed_arcs, +E, +S, +P)
    -> checks if the concerned species and reactions form a hill reaction
    +SES: catalyser
    +Dict_normal_arcs: dict of directed arcs
    +Dict_reversed_arcs: dict of undirected arcs
    +E: enzyme
    +S: substrate
    +P: product */
hill_reaction(SES, Dict_normal_arcs, Dict_reversed_arcs, E, S, P):-
  get_dict(SES, Dict_reversed_arcs, Ingoing_SES),
  (
    Ingoing_SES = [R12, R22]
  ;
    Ingoing_SES = [R22, R12]
  ),
  get_dict(SES, Dict_normal_arcs, Outgoing_SES),
  Outgoing_SES = [R3],
  get_dict(R3, Dict_reversed_arcs, Ingoing_R3),
  Ingoing_R3 = [SES],
  get_dict(R3, Dict_normal_arcs, Outgoing_R3),
  (
    Outgoing_R3 = [E, P]
  ;
    Outgoing_R3 = [P, E]
  ),
  get_dict(R12, Dict_reversed_arcs, Ingoing_R12),
  (
    Ingoing_R12 = [S, ES]
  ;
    Ingoing_R12 = [ES, S]
  ),
  get_dict(R12, Dict_normal_arcs, Outgoing_R12),
  Outgoing_R12 = [SES],
  get_dict(R22, Dict_reversed_arcs, Ingoing_R22),
  (
    Ingoing_R22 = [S, SE]
  ;
    Ingoing_R22 = [SE, S]
  ),
  get_dict(R22, Dict_normal_arcs, Outgoing_R22),
  Outgoing_R22 = [SES],
  get_dict(ES, Dict_reversed_arcs, Ingoing_ES),
  Ingoing_ES = [R11],
  get_dict(ES, Dict_normal_arcs, Outgoing_ES),
  Outgoing_ES = [R12],
  get_dict(SE, Dict_reversed_arcs, Ingoing_SE),
  Ingoing_SE = [R21],
  get_dict(SE, Dict_normal_arcs, Outgoing_SE),
  Outgoing_SE = [R22],
  get_dict(R11, Dict_reversed_arcs, Ingoing_R11),
  (
    Ingoing_R11 = [S, E]
  ;
    Ingoing_R11 = [E, S]
  ),
  get_dict(R11, Dict_normal_arcs, Outgoing_R11),
  Outgoing_R11 = [ES],

  get_dict(R21, Dict_reversed_arcs, Ingoing_R21),
  (
    Ingoing_R21 = [S, E]
  ;
    Ingoing_R21 = [E, S]
  ),
  get_dict(R21, Dict_normal_arcs, Outgoing_R21),
  Outgoing_R21 = [SE],
  assert(seen(R11)),
  assert(seen(R12)),
  assert(seen(R21)),
  assert(seen(R22)),
  assert(seen(R3)),
  assert(seen(ES)),
  assert(seen(SE)),
  assert(seen(SES)).


/*  partial_hill_detection  */
/*  partial_hill_detection(+Graph, +Stream, +Dict_normal_arcs, +Dict_reversed_arcs)
    -> looks for hill reactions
    +Graph: the graph
    +Stream: where to write the new graph
    +Dict_normal_arcs: dict of directed arcs
    +Dict_reversed_arcs: dict of undirected arcs */
partial_hill_detection(Graph, Stream, Dict_normal_arcs, Dict_reversed_arcs):-
  Graph = [_, Number_species, _, Id],
  Number_species_1 is Number_species - 1,
  numlist(0, Number_species_1, Species_list),
  set_counter(patrial_hill_pattern_count, 0),
  forall(
    (
      member(ES, Species_list)
    ),
    (
      (
        not(seen(ES)),
        once(partial_hill_reaction(ES, Dict_normal_arcs, Dict_reversed_arcs, E, S, P))
      ->
        debug(motif, "Hill reaction with E:~w S:~w P:~w ES:~w", [E, S, P, ES]),
        write_reaction([E, S], [E, P], Id, Stream),
        count(patrial_hill_pattern_count, _)
      ;
        true
      )
    )
  ),
  peek_count(patrial_hill_pattern_count, Nb_partial_hill_reaction),
  write('\nNumber of partial Hill patterns: '), 
  write(Nb_partial_hill_reaction), 
  write('\n').


/*  partial_hill_reaction  */
/*  partial_hill_reaction(+SES, +Dict_normal_arcs, +Dict_reversed_arcs, +E, +S, +P)
    -> checks if the concerned species and reactions form a hill reaction
    +SES: catalyser
    +Dict_normal_arcs: dict of directed arcs
    +Dict_reversed_arcs: dict of undirected arcs
    +E: enzyme
    +S: substrate
    +P: product */
partial_hill_reaction(ES, Dict_normal_arcs, Dict_reversed_arcs, E, S, P):-
  get_dict(ES, Dict_reversed_arcs, Ingoing_ES),
  Ingoing_ES = [R1],
  get_dict(ES, Dict_normal_arcs, Outgoing_ES),
  Outgoing_ES = [R2],
  get_dict(R2, Dict_reversed_arcs, Ingoing_R2),
  (
    Ingoing_R2 = [S, ES]
  ;
    Ingoing_R2 = [ES, S]
  ),
  get_dict(R2, Dict_normal_arcs, Outgoing_R2),
  Outgoing_R2 = [ESS],

  get_dict(ESS, Dict_reversed_arcs, Ingoing_ESS),
  Ingoing_ESS = [R2],
  get_dict(ESS, Dict_normal_arcs, Outgoing_ESS),
  Outgoing_ESS = [R3],
  get_dict(R3, Dict_reversed_arcs, Ingoing_R3),
  Ingoing_R3 = [ESS],
  get_dict(R3, Dict_normal_arcs, Outgoing_R3),
  (
    Outgoing_R3 = [E, P]
  ;
    Outgoing_R3 = [P, E]
  ),
  get_dict(R1, Dict_reversed_arcs, Ingoing_R1),
  (
    Ingoing_R1 = [S, E]
  ;
    Ingoing_R1 = [E, S]
  ),
  get_dict(R1, Dict_normal_arcs, Outgoing_R1),
  Outgoing_R1 = [ES],
  assert(seen(R1)),
  assert(seen(R2)),
  assert(seen(R3)),
  assert(seen(ES)),
  assert(seen(ESS)).


/*  partial_michaelis_menten  */
/*  partial_michaelis_menten(+Graph, +Stream, +Dict_normal_arcs, +Dict_reversed_arcs)
    -> looks for hill reactions
    +Graph: the graph
    +Stream: where to write the new graph
    +Dict_normal_arcs: dict of directed arcs
    +Dict_reversed_arcs: dict of undirected arcs */
partial_michaelis_menten(Graph, Stream, Dict_normal_arcs, Dict_reversed_arcs):-
  Graph = [_, Number_species, _, Id],
  Number_species_1 is Number_species - 1,
  numlist(0, Number_species_1, Species_list),
  set_counter(patrial_michaelis_menten_count, 0),
  forall(
    (
      member(ES, Species_list)
    ),
    (
      (
        not(seen(ES)),
        once(partial_michaelis_menten(ES, Dict_normal_arcs, Dict_reversed_arcs, E, S, P))
      ->
        debug(motif, "Hill reaction with E:~w S:~w P:~w ES:~w", [E, S, P, ES]),
        write_reaction([E, S], [E, P], Id, Stream),
        count(patrial_michaelis_menten_count, _)
      ;
        true
      )
    )
  ),
  peek_count(patrial_michaelis_menten_count, Nb_partial_michaelis_menten),
  write('\nNumber of double Michaelis Menten: '), 
  write(Nb_partial_michaelis_menten), 
  write('\n').


/*  partial_michaelis_menten  */
/*  partial_michaelis_menten(+ES, +Dict_normal_arcs, +Dict_reversed_arcs, +E, +S, +P)
    -> checks if the concerned species and reactions form a hill reaction
    +ES: catalyser
    +Dict_normal_arcs: dict of directed arcs
    +Dict_reversed_arcs: dict of undirected arcs
    +E: enzyme
    +S: substrate
    +P: product */
partial_michaelis_menten(ES, Dict_normal_arcs, Dict_reversed_arcs, E, S, P):-
  get_dict(ES, Dict_reversed_arcs, Ingoing_ES),
  (
    Ingoing_ES = [R11, R22]
  ;
    Ingoing_ES = [R22, R11]
  ),
  get_dict(ES, Dict_normal_arcs, Outgoing_ES),
  (
    Outgoing_ES = [R12, R21]
  ;
    Outgoing_ES = [R21, R12]
  ),
  get_dict(R21, Dict_reversed_arcs, Ingoing_R21),
  (
    Ingoing_R21 = [S, ES]
  ;
    Ingoing_R21 = [ES, S]
  ),
  get_dict(R21, Dict_normal_arcs, Outgoing_R21),
  Outgoing_R21 = [ESS],
  get_dict(ESS, Dict_reversed_arcs, Ingoing_ESS),
  Ingoing_ESS = [R21],
  get_dict(ESS, Dict_normal_arcs, Outgoing_ESS),
  Outgoing_ESS = [R22],
  get_dict(R22, Dict_reversed_arcs, Ingoing_R22),
  Ingoing_R22 = [ESS],
  get_dict(R22, Dict_normal_arcs, Outgoing_R22),
  (
    Outgoing_R22 = [ES, P]
  ;
    Outgoing_R22 = [P, ES]
  ),
  get_dict(R12, Dict_reversed_arcs, Ingoing_R12),
  Ingoing_R12 = [ES],
  get_dict(R12, Dict_normal_arcs, Outgoing_R12),
  (
    Outgoing_R12 = [E, P]
  ;
    Outgoing_R12 = [P, E]
  ),
  get_dict(R11, Dict_reversed_arcs, Ingoing_R11),
  (
    Ingoing_R11 = [S, E]
  ;
    Ingoing_R11 = [E, S]
  ),
  get_dict(R11, Dict_normal_arcs, Outgoing_R11),
  Outgoing_R11 = [ES],
  assert(seen(R11)),
  assert(seen(R12)),
  assert(seen(R21)),
  assert(seen(R22)),
  assert(seen(ES)),
  assert(seen(ESS)).
