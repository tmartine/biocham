:- use_module(library(plunit)).
:- use_module(library(readutil)).


has_start(Start, String) :-
  atom_concat(Start, _, String).


count_start(Atom, List, Count) :-
  include(has_start(Atom), List, L),
  length(L, Count).


:- begin_tests(pattern, [setup((clear_model, reset_options))]).

test('pattern_fail', [true(String = "S+E=>ES.\nES=>S+E.\nES=>P+E.\nES=>_.\n")]) :-
  with_output_to(
    atom(_),
    command('pattern_reduction("library:examples/sepi/MM5.bc", "MM5_reduced.bc").')
  ),
  read_file_to_string("MM5_reduced.bc", String, []),
  delete_file("MM5_reduced.bc").


test('pattern_success', [true(String = "S+E=>P+E.\n")]) :-
  with_output_to(
    atom(_),
    command('pattern_reduction("library:examples/sepi/MM1.bc", "MM1_reduced.bc").')
  ),
  read_file_to_string("MM1_reduced.bc", String, []),
  delete_file("MM1_reduced.bc").


test('pattern_intermediary_success', [true(String = "S+E=>P+E.\n")]) :-
  with_option(
    [r_1: no],
  with_output_to(
    atom(_),
      pattern_reduction("library:examples/sepi/MM6.bc", "MM6_reduced.bc"
  ))
  ),
  read_file_to_string("MM6_reduced.bc", String, []),
  delete_file("MM6_reduced.bc").


test(
  'pattern_mapk1_to_mapk3',
  [condition(flag(slow_test, true, true)), true((Count1, Count2) = (1, 1))]
) :-
  with_output_to(
    atom(_),
    command('pattern_reduction("library:examples/sepi/mapk1.bc", "mapk1_reduced.bc").')
  ),
  with_output_to(
    atom(Result),
    command('search_reduction("mapk1_reduced.bc", "library:examples/sepi/mapk3.bc").')
  ),
  atomic_list_concat(Split, '\n', Result),
  count_start('sepi', Split, Count1),
  with_output_to(
    atom(Result2),
    command('search_reduction("library:examples/sepi/mapk3.bc", "mapk1_reduced.bc").')
  ),
  delete_file("mapk1_reduced.bc"),
  atomic_list_concat(Split2, '\n', Result2),
  count_start('sepi', Split2, Count2).


test(
  'pattern_mapk2_to_mapk3',
  [condition(flag(slow_test, true, true)), true((Count1, Count2) = (1, 1))]
) :-
  with_option(
    [r_1: no],
    pattern_reduction("library:examples/sepi/mapk2.bc", "mapk2_reduced.bc")
  ),
  with_output_to(
    atom(Result),
    command('search_reduction("mapk2_reduced.bc", "library:examples/sepi/mapk3.bc").')
  ),
  atomic_list_concat(Split, '\n', Result),
  count_start('sepi', Split, Count1),
  with_output_to(
    atom(Result2),
    command('search_reduction("library:examples/sepi/mapk3.bc", "mapk2_reduced.bc").')
  ),
  delete_file("mapk2_reduced.bc"),
  atomic_list_concat(Split2, '\n', Result2),
  count_start('sepi', Split2, Count2).


test('pattern_R-2_EP', [true(Output = '\nNumber of Michaelis Menten patterns: 2\n')]) :-
  with_output_to(
    atom(Output),
    command('pattern_reduction("library:examples/sepi/mapk26.bc", "mapk26_reduced.bc").')
  ),
  delete_file("mapk26_reduced.bc").


test('maybe_option', [true(Output = '\nNumber of Michaelis Menten patterns: 4\n')]) :-
  with_output_to(
    atom(Output),
    with_option(
      [r_1: maybe, r_2: maybe, ep: maybe],
      pattern_reduction("library:examples/sepi/mapk26.bc", "mapk26_reduced.bc")
    )
  ),
  delete_file("mapk26_reduced.bc").


test('hill_reaction', [true(Output3 = '\nNumber of Hill patterns: 0\n')]) :-
  with_output_to(
    atom(Output),
    with_option([michaelis_menten: no, hill_reaction: yes], pattern_reduction("library:examples/sepi/MM_variants/MM_SES.bc", "MM_SES_reduced.bc"))
  ),
  delete_file("MM_SES_reduced.bc"),
  Output = '\nNumber of Hill patterns: 1\n',
  with_output_to(
    atom(Output2),
    with_option([michaelis_menten: no, hill_reaction: yes], pattern_reduction("library:examples/sepi/MM_variants/MM_SES_true.bc", "MM_SES_reduced.bc"))
  ),
  delete_file("MM_SES_reduced.bc"),
  Output2 = '\nNumber of Hill patterns: 1\n',
  with_output_to(
    atom(Output3),
    with_option([michaelis_menten: no, hill_reaction: yes], pattern_reduction("library:examples/sepi/MM_variants/MM_SES_false.bc", "MM_SES_reduced.bc"))
  ),
  delete_file("MM_SES_reduced.bc").


test('partial_hill_reaction', [true(Output3 = '\nNumber of partial Hill patterns: 0\n')]) :-
  with_output_to(
    atom(Output),
    with_option([michaelis_menten: no, partial_hill_reaction: yes], pattern_reduction("library:examples/sepi/MM_variants/MM_ESS.bc", "MM_ESS_reduced.bc"))
  ),
  delete_file("MM_ESS_reduced.bc"),
  Output = '\nNumber of partial Hill patterns: 1\n',
  with_output_to(
    atom(Output2),
    with_option([michaelis_menten: no, partial_hill_reaction: yes], pattern_reduction("library:examples/sepi/MM_variants/MM_ESS_true.bc", "MM_ESS_reduced.bc"))
  ),
  delete_file("MM_ESS_reduced.bc"),
  Output2 = '\nNumber of partial Hill patterns: 1\n',
  with_output_to(
    atom(Output3),
    with_option([michaelis_menten: no, partial_hill_reaction: yes], pattern_reduction("library:examples/sepi/MM_variants/MM_ESS_false.bc", "MM_ESS_reduced.bc"))
  ),
  delete_file("MM_ESS_reduced.bc").


test('double_michaelis_menten', [true(Output3 = '\nNumber of double Michaelis Menten: 0\n')]) :-
  with_output_to(
    atom(Output),
    with_option([michaelis_menten: no, double_michaelis_menten: yes], pattern_reduction("library:examples/sepi/MM_variants/MM_SEE.bc", "MM_SEE_reduced.bc"))
  ),
  delete_file("MM_SEE_reduced.bc"),
  Output = '\nNumber of double Michaelis Menten: 1\n',
  with_output_to(
    atom(Output2),
    with_option([michaelis_menten: no, double_michaelis_menten: yes], pattern_reduction("library:examples/sepi/MM_variants/MM_SEE_true.bc", "MM_SEE_reduced.bc"))
  ),
  delete_file("MM_SEE_reduced.bc"),
  Output2 = '\nNumber of double Michaelis Menten: 1\n',
  with_output_to(
    atom(Output3),
    with_option([michaelis_menten: no, double_michaelis_menten: yes], pattern_reduction("library:examples/sepi/MM_variants/MM_SEE_false.bc", "MM_SEE_reduced.bc"))
  ),
  delete_file("MM_SEE_reduced.bc").


test('expansion', [true(Count = 1)]) :-
  with_output_to(
    atom(_),
    command('pattern_reduction("library:examples/sepi/MM1.bc", "MM1_reduced.bc").')
  ),
  with_output_to(
    atom(_),
    with_option(
      [michaelis_menten: no, michaelis_menten_expansion: yes],
      pattern_reduction("MM1_reduced.bc", "MM1_expanded.bc")
    )
  ),
  with_output_to(
    atom(Result),
    command('search_reduction("MM1_expanded.bc", "library:examples/sepi/MM1.bc").')
  ),
  atomic_list_concat(Split, '\n', Result),
  count_start('sepi', Split, 1),
  with_output_to(
    atom(Result2),
    command('search_reduction("library:examples/sepi/MM1.bc", "MM1_expanded.bc").')
  ),
  delete_file("MM1_reduced.bc"),
  delete_file("MM1_expanded.bc"),
  atomic_list_concat(Split2, '\n', Result2),
  count_start('sepi', Split2, Count).

:- end_tests(pattern).
