:- use_module(library(plunit)).


:- begin_tests(molecules, [setup((clear_model, reset_options))]).


test(
  'compute_neighborhood',
  [true(Neighbors = [(a, b), (a, c), (a, d), (b, c), (b, d), (c, d), (e, f)])]
) :-
  command(add_reaction(a@b + c@d => c@a + b@c)),
  command(add_reaction([k@e]*[k@f] for d => _)),
  molecules:compute_neighborhood,
  findall(
    (X, Y),
    molecules:neighbor(X, Y),
    Neighbors
  ).

:- end_tests(molecules).
