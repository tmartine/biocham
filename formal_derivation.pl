:- module(
  formal_derivation,
  [
    derivate/3,
    jacobian/2
  ]).

:- use_module(library(error)).

derivate(Expression, Variable, Result) :-
  derivate_raw(Expression, Variable, UnsimplifiedResult),
  simplify(UnsimplifiedResult, Result).

derivate_raw(p(_ParameterIndex), _Variable, 0) :-
  !.

derivate_raw(Value, _Variable, 0) :-
  number(Value),
  !.

derivate_raw(- A, Variable, - Aprime) :-
  !,
  derivate_raw(A, Variable, Aprime).

derivate_raw(A + B, Variable, Aprime + Bprime) :-
  !,
  derivate_raw(A, Variable, Aprime),
  derivate_raw(B, Variable, Bprime).

derivate_raw(A - B, Variable, Aprime - Bprime) :-
  !,
  derivate_raw(A, Variable, Aprime),
  derivate_raw(B, Variable, Bprime).

derivate_raw(A * B, Variable, Aprime * B) :-
  derivate(B, Variable, 0),
  !,
  derivate_raw(A, Variable, Aprime).

derivate_raw(A * B, Variable, A * Bprime) :-
  derivate(A, Variable, 0),
  !,
  derivate_raw(B, Variable, Bprime).

derivate_raw(A * B, Variable, A * Bprime + Aprime * B) :-
  !,
  derivate_raw(A, Variable, Aprime),
  derivate_raw(B, Variable, Bprime).

% simplify a'b - ab' especially for MM case
derivate_raw(A / B, Variable, D / (B ^ 2)) :-
  derivate(A, Variable, 0),
  !,
  derivate_raw(B, Variable, Bprime),
  additive_normal_form((- A * Bprime), C),
  simplify(C, D).

derivate_raw(A / B, Variable, Aprime/B) :-
  derivate(B, Variable, 0),
  !,
  derivate_raw(A, Variable, Aprime).

derivate_raw(A / B, Variable, D / (B ^ 2)) :-
  !,
  derivate_raw(A, Variable, Aprime),
  derivate_raw(B, Variable, Bprime),
  additive_normal_form((Aprime * B - A * Bprime), C),
  % C = (Aprime * B - A * Bprime),
  simplify(C, D).

derivate_raw(A ^ B, Variable, Result) :-
  !,
  (
    derivate(B, Variable, 0)
  ->
    derivate(A, Variable, Aprime),
    Result = Aprime*B*A^(B-1)
  ;
    derivate_raw(exp(B * log(A)), Variable, Result)
  ).

% we insert here the formal derivation for all the arithmetic functions fo prolog in
% alphabetical order, see: https://www.swi-prolog.org/pldoc/man?section=funcsummary

derivate_raw(acos(A), Variable, - Aprime * (1 - A^2)^ -0.5) :-
  !,
  derivate_raw(A, Variable, Aprime).

derivate_raw(acosh(A), Variable, Aprime * (A^2 - 1)^ -0.5) :-
  !,
  derivate_raw(A, Variable, Aprime).

derivate_raw(asin(A), Variable, Aprime * (1 - A^2)^ -0.5) :-
  !,
  derivate_raw(A, Variable, Aprime).

derivate_raw(asinh(A), Variable, Aprime * (A^2 + 1)^ -0.5) :-
  !,
  derivate_raw(A, Variable, Aprime).

derivate_raw(atan(A), Variable, Aprime / (1 + A^2)) :-
  !,
  derivate_raw(A, Variable, Aprime).

derivate_raw(atanh(A), Variable, Aprime / (1 - A^2)) :-
  !,
  derivate_raw(A, Variable, Aprime).

derivate_raw(cos(A), Variable, - Aprime * sin(A)) :-
  !,
  derivate_raw(A, Variable, Aprime).

derivate_raw(cosh(A), Variable, Aprime * sinh(A)) :-
  !,
  derivate_raw(A, Variable, Aprime).

derivate_raw(erf(A), Variable, 2/sqrt(math_pi) * Aprime * exp(- A^2)) :-
  !,
  derivate_raw(A, Variable, Aprime).

derivate_raw(erfc(A), Variable, - Result) :-
  !,
  derivate_raw(erf(A), Variable, Result).

derivate_raw(exp(A), Variable, Aprime * exp(A)) :-
  !,
  derivate_raw(A, Variable, Aprime).

derivate_raw(log(A), Variable, Aprime / A) :-
  !,
  derivate_raw(A, Variable, Aprime).

derivate_raw(log10(A), Variable, Aprime / (A*log(10)) ) :-
  !,
  derivate_raw(A, Variable, Aprime).

derivate_raw(sin(A), Variable, Aprime * cos(A)) :-
  !,
  derivate_raw(A, Variable, Aprime).

derivate_raw(sinh(A), Variable, Aprime * cosh(A)) :-
  !,
  derivate_raw(A, Variable, Aprime).

derivate_raw(sqrt(A), Variable, Result) :-
  !,
  derivate_raw(A ^ 0.5, Variable, Result).

derivate_raw(tan(A), Variable, Aprime * (1 + tan(A)^2)) :-
  !,
  derivate_raw(A, Variable, Aprime).

derivate_raw(tanh(A), Variable, Aprime * (1 - tanh(A)^2)) :-
  !,
  derivate_raw(A, Variable, Aprime).

derivate_raw(Variable0, Variable1, Result) :-
  !,
  (
    Variable0 = Variable1
  ->
    Result = 1
  ;
    Result = 0
  ).


jacobian(Equations, Jacobian) :-
  length(Equations, VariableCount),
  findall(
    Line,
    (
      member(Equation, Equations),
      jacobian_line(Equation, VariableCount, Line)
    ),
    Jacobian).


jacobian_line(Equation, VariableCount, Line) :-
  VariableMax is VariableCount - 1,
  findall(
    Derivative,
    (
      between(0, VariableMax, VariableIndex),
      derivate(Equation, [VariableIndex], Derivative)
    ),
    Line).
