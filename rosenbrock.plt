:- use_module(rosenbrock).

near(X, Y, Epsilon) :-
  (Epsilon >= abs(X-Y),!);
  (Error is abs(X-Y),write(Error),fail).

check_integration_1([], _Epsilon) :- !.
check_integration_1([Head|Tail], Epsilon) :-
  Head = row(T,A,B),
  Sum is A+B, near(1.0, Sum, Epsilon),
  Value is exp(-T), near(A, Value, Epsilon),
  check_integration_1(Tail, Epsilon).

check_integration_2([], _Epsilon) :- !.
check_integration_2([Head|Tail], Epsilon) :-
  Head = row(T,Cos,Sin),
  ValueC is cos(T), near(ValueC, Cos, Epsilon),
  ValueS is sin(T), near(ValueS, Sin, Epsilon),
  check_integration_2(Tail, Epsilon).

check_integration_ball([], _Epsilon) :- !.
check_integration_ball([Head|Tail], Epsilon) :-
  Head = row(_T,_TT,X,_Y),
  X >= -Epsilon,
  check_integration_ball(Tail, Epsilon).


:- begin_tests(rosenbrock, [setup((clear_model, reset_options))]).

test(eva_coeff_1) :-
  nb_setval(hdid,0.01),
  set_vector([1.5,2.5,3.5], State),
  set_vector([-1,2], Param),
  rosenbrock:eval_coeff(1*[0], State, Param, 1, R1), R1 =:= 1.5,
  rosenbrock:eval_coeff(2*[0] + p(1), State, Param, 1, R2), R2 =:= 5.0,
  rosenbrock:eval_coeff(t, State, Param, 1.23, R3), R3 =:= 1.23,
  rosenbrock:eval_coeff(2*[0] - p(0), State, Param, 1, R4), R4 =:=4.0,
  rosenbrock:eval_coeff(-p(1), State, Param, 1, R5), R5 =:= -2,
  rosenbrock:eval_coeff(25/[1], State, Param, 1, R6), R6 =:= 10,
  rosenbrock:eval_coeff([1]^p(1), State, Param, 1, R7), R7 =:= 6.25.

test(eva_coeff_2) :-
  nb_setval(hdid,0.01),
  set_vector([1.5,2.5,3.5], State),
  set_vector([-1,2], Param),
  rosenbrock:eval_coeff(floor(1*[0]), State, Param, 1, R1), R1 =:= 1,
  rosenbrock:eval_coeff(min([0], p(1)), State, Param, 1, R2), R2 =:= 1.5,
  rosenbrock:eval_coeff(infinity, State, Param, 1, R3), R3 =:= inf,
  rosenbrock:eval_coeff(random, State, Param, 1, _R4).

test(simple_integration, [setup(command(clear_model)), cleanup(command(clear_model))]) :-
  command(a => b),
  command(present(a,1.0)),
  command(numerical_simulation(method:rsbk)),
  get_table_data(D),
  check_integration_1(D, 1e-5).

test(continue, [setup(command(clear_model)), cleanup(command(clear_model))]) :-
  command(a => b),
  command(present(a,1.0)),
  command(numerical_simulation(method:rsbk)),
  command(continue),
  get_table_data(D),
  check_integration_1(D, 1e-5).

test(cosinus_integration, [setup(command(clear_model)), cleanup(command(clear_model))]) :-
  command(1.0*a for '_' => b),
  command(-1.0*b for '_' => a),
  command(present(a,1.0)),
  command(numerical_simulation(method:rsbk)),
  get_table_data(D),
  check_integration_2(D, 1e-5).

test(test_ball, [setup(command(clear_model)), cleanup(command(clear_model))]) :-
  get_option(time, Time_default),
  command(load("library/examples/RobustMonitoring/ball.bc")),
  command(option(time:5)),
  command(option(maximum_step_size:1e-2)),
  command(numerical_simulation(method:rsbk)),
  get_table_data(D),
  check_integration_ball(D, 1e-3),
  set_option(time, Time_default).

test(test_cell_cyle, [setup(command(clear_model)), cleanup(command(clear_model))]) :-
  get_option(time, Time_default),
  command(load("library/examples/cell_cycle/BIOMD0000000008.xml")),
  command(option(time:5)),
  command(numerical_simulation(method:rsbk)),
  get_table_data(_D),
  set_option(time, Time_default).

test(test_time_event, [setup(command(clear_model)), cleanup(command(clear_model))]) :-
  command(k*a for a => b),
  command(present(a,1.0)),
  command(parameter(k=1)),
  command(add_event('Time'>2, k=0)),
  command(add_event('Time'>=4, k=1)),
  command(numerical_simulation(method:rsbk, time:5)),
  get_table_data(D),
  last(D, row(T,A,_B)),
  near(A, exp(2-T), 1e-5).

:- end_tests(rosenbrock).
