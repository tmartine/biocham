/** <module> Biochemical Compiler
 *
 *  ---+ Commands
 *
 *       * add_function/1
 *       * compile_program_file/1
 *       * compile_program/1
 *
 *  ---+ Commands Backend
 *
 *       * create_ode/2
 *       * compile/1
 *       * compile/3
 *       * read_program/2
 *       * read_program/3
 *       * first_condition/3
 *       * remove_para/2
 *       * simplify_variable_initialization/3
 *       * get_pure_expression/2
 *       * build_AST/3
 *       * else_statement/3
 *
 *  ---+ Helper Functions
 *
 *  ---++ Symbol Table
 *
 *  ---+++ High Level API
 *
 *         * get_symbol_name/1
 *         * new_symbol/1
 *         * new_symbol/2
 *         * change_symbol_attr/2
 *         * get_symbol_attr/2
 *
 *  ---+++ Low Level API
 *
 *         * add_symbol/2
 *         * remove_symbol/1
 *         * replace_symbol/2
 *         * default_symbol_attr/2
 *         * replace_attr/3
 *         * instantiate_symbol_attr/2
 *
 *  ---++ Condition
 *
 *        The fact of each condition should exist only one.
 *        Here are some functions to handle the control flow condition global variables.
 *        Supported conditions is recorded in the control_flow_condition/1 predicate.
 *
 *        * set_condition/2
 *        * get_condition/2
 *        * clear_condition/2
 *
 *  ---++ xxx_expression_to_ODE
 *
 *        Here are some function to recursively parse and check the expression.
 *
 *        * nnary_expression_to_ODE/4
 *        * unary_expression_to_ODE/4
 *        * binary_expression_to_ODE/6
 *        * trinary_expression_to_ODE/8
 *        * arithmetic_expression_to_ODE/4
 *        * arithmetic_expression_to_ODE/6
 *        * condition_expression_to_ODE/4
 *        * condition_expression_to_ODE/6
 *
 *  ---++ Add Fast Annihilation
 *
 *        Generate fast annihilation ode for a variable.
 *        The *fast* rate constant is initialized with the *fast_rate* option." ).
 *        The *fast* rate parameter shoulod rather depend on the ODE." ).
 *
 *        * add_fast_annihilation/1
 *        * add_fast_annihilation/2
 *        * add_fast_annihilation/3
 *
 *  ---++ Others
 *
 *        * init_variable/3
 *        * add_ode/4
 *        * get_ode/4
 *        * variable/2
 *        * get_variable_positive/2
 *        * new_variable/1
 *        * new_variable/2
 *        * new_variable/3
 *        * add_ode_initialization/1
 *        * add_ode_initialization/2
 *        * is_variable_in_expression/2
 *
 *  ---+ From Expression to PIVP
 *
 *  ---++ AST Compilation
 *
 *        * compile_AST/2
 *        * compile_AST/4
 *        * get_control_flow_init/4
 *        * compile_AST_with_conditions/6
 *        * split_self_assignment/4
 *
 *  ---++ Expression Parser
 *
 *        * expression_to_ODE/3
 *        * expression_to_ODE/5
 *        * eval/3
 *        * eval_condition/2
 *
 *  ---++ Compilation Unit
 *
 *        Basically, the compilation units have the form xxx_to_PIVP( OdeSystem, VariableEName, ..., VariableName ).
 *        These functions compile a specific expression unit into PIVP and store it in the _OdeSystem_.
 *        An expression may consist of many subexpressions, these subexpressions are _VarialbeEName_s.
 *        The meaning of the variable with name _VariableName_ will be the result of the expression unit.
 *        The internal of many *xxx_to_PIVP*s separate into two part.
 *        *xxx_to_ODE* deals with the ode formula, and *init_xxx* deals with the initial value.
 *        More details are writen in the comments of corresponding predicates.
 *
 *  ---+++ Arithmetic Operations
 *
 *         * positive_to_PIVP/3
 *         * negative_to_PIVP/3
 *         * power_to_PIVP/4
 *         * multiplication_to_PIVP/4
 *         * multiplication_to_PIVP/5
 *         * reciprocal_to_PIVP/4
 *         * init_reciprocal/4
 *         * reciprocal_to_ODE/6
 *         * division_to_PIVP/4
 *         * addition_to_PIVP/4
 *         * addition_to_PIVP/5
 *         * substraction_to_PIVP/4
 *
 *  ---+++ Functions
 *
 *         * abs_to_PIVP/3
 *         * abs_to_ODE/3
 *         * min_to_PIVP/4
 *         * max_to_PIVP/4
 *         * log_to_PIVP/3
 *         * init_log/5
 *         * log_to_ODE/6
 *         * exp_to_PIVP/3
 *         * init_exp/3
 *         * exp_to_ODE/4
 *         * cos_to_PIVP/3
 *         * init_cos/4
 *         * cos_to_ODE/5
 *         * sin_to_PIVP/3
 *         * init_sin/4
 *         * pos_sigmoid_to_PIVP/3
 *         * neg_sigmoid_to_PIVP/3
 *         * pos_number_to_bool_to_PIVP/3
 *         * pos_number_to_bool_to_PIVP/4
 *         * neg_number_to_bool_to_PIVP/3
 *         * neg_number_to_bool_to_PIVP/4
 *         * hill_to_PIVP/4
 *         * init_hill/4
 *         * hill_to_ODE/6
 *         * general_tanh_to_PIVP/6
 *         * init_general_tanh/3
 *         * general_tanh_to_ODE/5
 *         * pos_tanh_to_PIVP/3
 *         * pos_tanh_to_ODE/5
 *         * neg_tanh_to_PIVP/3
 *         * neg_tanh_to_ODE/5
 *         * general_step_to_PIVP/6
 *         * init_general_step/5
 *         * general_step_to_ODE/8
 *         * pos_step_to_PIVP/3
 *         * neg_step_to_PIVP/3
 *
 *  ---+++ Conditional Operations
 *
 *         * equal_to_PIVP/4
 *         * lessthan_to_PIVP/4
 *         * lessequal_to_PIVP/4
 *         * greaterthan_to_PIVP/4
 *         * greaterequal_to_PIVP/4
 *         * or_to_PIVP/4
 *         * and_to_PIVP/4
 *         * not_to_PIVP/3
 *
 *  ---+++ Control Flow
 *
 *         * inter_to_PIVP/7
 *         * inter_post_pre_to_PIVP/6
 *         * inter_post_to_PIVP/4
 *         * pre_to_PIVP/4
 *         * post_to_PIVP/8
 *         * post_to_PIVP/9
 *         * post_post_pre_to_PIVP/8
 *         * parallel_to_PIVP/4
 *         * branch_to_PIVP:
 *
 *           The compilation unit for all branch related unit.
 *           The first argument after _OdeSystem_ will indicate which control flow keyword it is related.
 *           A special version without control flow keyword is for the conditional addignment 'if then else'.
 *
 *           * branch_to_PIVP/9
 *           * branch_to_PIVP/10
 *           * branch_to_PIVP/5
 *           * branch_to_ODE/5
 *
 *         * loop_to_PIVP:
 *
 *           The compilation unit for all loop related unit.
 *           The first argument after _OdeSystem_ will indicate which control flow keyword it is related.
 *
 *           * loop_to_PIVP/10
 *           * loop_to_PIVP/11
 *           * loop_pre_to_PIVP/5
 *
 *         * condition_to_PIVP/8
 *         * condition_to_PIVP/9
 *
 *  ---+++ Others
 *
 *         * argument_to_PIVP/2
 *         * assign_to_PIVP/3
 *         * assign_to_ODE/4
 *         * switch_to_PIVP/5
 *         * switch_to_PIVP/6
 *         * init_switch/4
 *         * switch_core_to_reaction/3
 *         * switch_control_to_reaction/4
 *         * zero_order_to_PIVP/5
 *         * zero_order_to_PIVP/6
 *         * zero_order_to_PIVP/8
 *         * init_zero_order/12
 *         * gk_to_reaction/8
 *         * mm_to_reaction/5
 *         * zero_order_ma_to_PIVP/5
 *         * zero_order_ma_to_PIVP/6
 *         * zero_order_ma_to_PIVP/8
 *         * init_zero_order_ma/12
 *         * gk_to_reaction/9
 *
 *  ---++ Database
 *
 *        * constant/2
 *        * operator/1
 *        * keyword/1
 *        * control_flow_condition/1
 *
 *  ---+ Predicates
 */
:- module(
  odefunction,
  [
    add_function/1,
    compile_program_file/1,
    compile_program/1,
    op( 800, fx, para ),
    op( 800, fx, pre  ),
    op( 800, yf, post )
  ]
).

% compilation flow
:- devdoc( "\\section{Compilation Flow}" ).

:- devdoc( "Either use 'compile_program' to compile the program from file, or use the 'add_function' directly.
            'compile_program' transforms the program to the list passed to 'add_function' and call it, so it is just like a syntax sugar." ).

:- devdoc( "The remaining flow is described below:" ).

:- devdoc( "\\begin{itemize}" ).
:- devdoc( "\\item 'compile_AST' deal with the control flow and call the corresponding expression_to_ODE." ).
:- devdoc( "\\item 'expression_to_ODE' parses the expression and call the corresponding xxx_to_PIVP." ).
:- devdoc( "\\item 'xxx_to_PIVP' uses the OdeSystem to store the corresponding PIVP." ).
:- devdoc( "\\item 'import_reactions_from_ode_system' compiles the overall PIVP into chemical reactions." ).
:- devdoc( "\\end{itemize}" ).

:- devdoc( "More detail of the implementation can be found in the comments or the private predicates's pldoc." ).
% end compilation flow

% program syntax
:- doc("In this simple program syntax each statement must be terminated by ',' or '.'." ).

:- devdoc( "Internally, it uses prolog 'read' to parse the file." ).

:- grammar( statement ).

statement( Statement ) :-
  assignment( Statement ).

statement( Statement ) :-
  control_flow_statement( Statement ).

:- grammar( assignment ).

assignment( Variable := Expression ) :-
  variable( Variable ),
  expression( Expression ).

:- grammar( variable ).

variable( Variable ) :-
  name( Variable ).

:- grammar( expression ).

expression( Expression ) :-
  arithmetic_expression( Expression ).

expression( Expression ) :-
  condition( Expression ).

:- grammar_doc( "The support of arithmetic_expression is not complete, but most of them are supported." ).

:- grammar( control_flow_statement ).

control_flow_statement( if( Condition ) ) :-
  condition( Condition ).

control_flow_statement( while( Condition ) ) :-
  condition( Condition ).

control_flow_statement( 'else' ).
control_flow_statement( 'endif' ).
control_flow_statement( 'endwhile' ).

:- doc( "Statements terminated by ',' will execute parallelly with next statement." ).

:- devdoc( "It is the backend of 'compile_program'." ).

:- grammar( lhs ).

lhs( Variable ) :-
  variable( Variable ).

lhs( Keyword ) :-
  control_flow_keyword( Keyword ).

:- grammar( rhs ).

rhs( para Expression ) :-
  expression( Expression ).

rhs( pre Expression ) :-
  expression( Expression ).

rhs( pre Expression post ) :-
  expression( Expression ).

rhs( Expression post ) :-
  expression( Expression ).

rhs( Expression ) :-
  expression( Expression ).

:- grammar( control_flow_keyword ).

control_flow_keyword( 'if_tag'    ).
control_flow_keyword( 'while'     ).

:- grammar_doc( "When using these two keywords, the rhs serves as the condition." ).

control_flow_keyword( 'else_tag'  ).
control_flow_keyword( 'endif'     ).
control_flow_keyword( 'endwhile'  ).

:- grammar_doc( "When using these three keywords, the rhs does not matter." ).

:- devdoc( "The reason of using 'if_tag' and 'else_tag' rather than 'if' and 'else' is that 'if' and 'else' are operators in biocham, which will cause parsing error." ).

:- doc( "The operators 'pre' and 'post' are used to describe the procedural structure of a program,
         so if not specified, the list element will execute parallelly, which makes it possible to describe sequential and parallel part of a program." ).

:- doc( "To mix the sequential and parallel computation, statements which executed parallelly need to add 'para' before the expression, except the last statement." ).

:- doc( "Currently it is user's responsibility to ensure the execution of last statement finishs after other parallelly executed statements." ).
% end program syntax

% interface
:- devdoc('\\section{Commands}').

%% add_function(+FunctionList:list) is det
%
%   Compile the statement into chemical reactions.
add_function(FunctionList) :-
  biocham_command(*),
  type(FunctionList, '*'(term = term)),
  option( 'zero_ultra_mm', yesno, _,
  'set the kinetics of zero-order ultrasensitivity to Michaelis-Menten kinetics' ),
  devdoc( "Compile the statement into chemical reactions." ),
  doc('
    adds reactions to compute the result of a function of the current variables in the concentration of a new variable, at steady state.
    This command only accepts input list with each element of this form: <lhs> = <rhs>.
    \\begin{example}
  '),
  biocham_silent(clear_model),
  biocham(present(x,1)),
  biocham(present(y,3)),
  biocham(add_function(z=x+y)),
  biocham(list_ode),
  biocham(list_model),
  biocham(numerical_simulation(time: 10)),
  biocham(plot),
  doc('
    \\end{example}
  '),
  new_ode_system(OdeSystem),
  get_option(fast_rate, Fast), % for import_reactions it is better to use a kinetic parameter instead of a number
  set_parameter(fast, Fast),
  create_ode(FunctionList, OdeSystem),
  import_reactions_from_ode_system(OdeSystem),
  delete_item(OdeSystem).


:- initial(option(simplify_variable_init: no)).
:- initial(option(zero_ultra_mm: no)).

%% compile_program(+InputFile:atom) is det
%
%   Compiles a high-level imperative program from a string.
%   Transform the terms in the string into an internal list format for 'add_function'.
compile_program( Program ) :-
  biocham_command,
  type( Program, atom ),
  option( 'simplify_variable_init', yesno, _,
          'set if the variable initialization in program need to be simplified to \\emph{present} command' ),
  option( 'zero_ultra_mm', yesno, _,
          'set the kinetics of zero-order ultrasensitivity to Michaelis-Menten kinetics' ),
  doc( "Using atom \\emph{Program} as the program code." ),
  doc( "We can write the follwing program and compile it in reactions." ),
  biocham_silent( clear_model ),
  biocham( compile_program( 'z := 2. x := z + 1. y := x - z.', zero_ultra_mm: yes ) ),
  biocham( numerical_simulation( time: 60 ) ),
  biocham( plot( show:{ x_p, x_n, y_p, y_n, z_p, z_n } ) ),
  doc( "This is equivalent to '\\texttt{add_function( z = ( 2 post ), x = ( pre z + 1 post ), y = ( pre x - z )).}' and can be compared with the parallel version of the program, without pre and post conditions."),
  biocham( clear_model ),
  biocham( compile_program( 'z := 2, x := z + 1, y := x - z.', zero_ultra_mm: yes  ) ),
  biocham( numerical_simulation( time: 20 ) ),
  biocham( plot( show:{ x_p, x_n, y_p, y_n, z_p, z_n } ) ),

%  doc( "Here is an example shows the compilation result of euclid division: "),
%  biocham_silent( clear_model ),
%  biocham( compile_program( 'a := 7, b := 3. while( a >= b ). t1 := a - b, t2 := q + 1. a := t1, q := t2. endwhile. r := a.', simplify_variable_init: yes, zero_ultra_mm: yes ) ),
  % FIXME tooooooo slow
  % biocham( numerical_simulation( time: 200 ) ),
  % biocham( plot( show:{ a_p, a_n, b_p, b_n, q_p, q_n, r_p, r_n } ) ),
  doc( "\\end{example}" ),
  atom( Program ),
  !,
  atom_string( Program, String ),
  open_string( String, Stream ),
  compile( Stream ),
  close( Stream ).

%% compile_program_file(+InputFile:input_file) is det
%
%   Compiles a high-level imperative program file.
%   Transforms the terms in the file into an internal list format for 'add_function'.
compile_program_file( InputFile ) :-
  biocham_command,
  type( InputFile, input_file ),
  option( 'simplify_variable_init', yesno, _,
          'set if the variable initialization in program need to be simplified to \\emph{present} command' ),
  option( 'zero_ultra_mm', yesno, _,
          'set the kinetics of zero-order ultrasensitivity to Michaelis-Menten kinetics' ),
  devdoc( "Transform the terms in a file into internal list format of 'add_function'." ),

  doc( "Compile the high-level language program from file." ),
  access_file( InputFile, read ),
  !,
  open( InputFile, read, File ),
  compile( File ),
  close( File ).


%% create_ode(+FunctionList:list, +OdeSystem:'ode system') is det
%
%   Compile the _FunctionList_ into PIVP.

% pre process of PIVP compilation
create_ode(_, OdeSystem) :-
  constant( counter, CounterName ),
  (
    nb_current( CounterName, CounterInit )
  ->
    true
  ;
    CounterInit = 0
  ),
  set_counter( CounterName, CounterInit ),
  new_variable( OdeSystem, [ Pre, PostPre ] ),
  set_condition( precondition,      Pre     ),
  set_condition( postcondition_pre, PostPre ),
  fail.

% initialize variables with initial value ( variable initialize with 'present' command )
create_ode(_, OdeSystem) :-
  enumerate_molecules(L), % useless if item backtracks on Object
  member(Object, L),
  get_initial_concentration(Object, Concentration),
  new_variable(OdeSystem, Object, Concentration),
  fail.

% compile the statement into PIVP
create_ode(FunctionList, OdeSystem) :-
  build_AST( AST, FunctionList, [] ),
  compile_AST( OdeSystem, AST ),
  fail.

% post process of PIVP compilation
create_ode(_, OdeSystem) :-
  add_ode_initialization( OdeSystem ),
  add_fast_annihilation( OdeSystem ),
  remove_symbol( _ ),
  fail.

create_ode(_,_).

%% compile(+In:stream) is det
%
%   Parse the input file or atom of *compile_program* and transform it into *add_function* format.
compile( In ) :-
  is_stream( In ),
  read_program( In, FunctionList ),
  compile( end_of_file, FunctionList, NewFunctionList ),
  add_function( NewFunctionList ).

%% compile(++End_of_file, +FunctionList:list, -NewFunctionList:list) is det
%
%   Post process of the _FunctionList_ and generate _NewFunctionList_
compile( end_of_file, FunctionList, NewFunctionList ) :-
  !,
  (
    last( FunctionList, Variable = ( Term post ) )
  ->
    % remove post in the last statement
    append( FunctionListPre, [ Variable = ( Term post ) ], FunctionList   ),
    append( FunctionListPre, [ Variable = ( Term ) ],      FunctionListT  )
  ;
    FunctionListT = FunctionList
  ),
  get_option( 'simplify_variable_init', SimVarInit ),
  simplify_variable_initialization( FunctionListT, FunctionListT2, SimVarInit ),
  (
    member( _ = ( _ post ), FunctionListT2 )
  ->
    NewFunctionList = FunctionListT2
  ;
    remove_para( FunctionListT2, NewFunctionList )
  ).

%% compile( +Term:term, -FunctionList:list, +Options:'option list' ) is det
%
%   Convert _Term_ to a _FunctionList_ in *add_function* format.
%   The provided options are:
%
%   * first(+Boolean)
%     Indicate whether the _Term_ is in the first statement.
%   * parallel(+Boolean)
%     Indicate whether the _Term_ is in a statement executed parallelly with others.
compile( if( Condition ), [ if_tag = NewCondition ], Options ) :-
  condition( Condition ),
  !,
  first_condition( Condition, NewCondition, Options ).

compile( else, [ else_tag = 0 ], _ ) :- !.

compile( while( Condition ), [ while = NewCondition ], Options ) :-
  condition( Condition ),
  !,
  first_condition( Condition, NewCondition, Options ).

compile( Variable := Value, [ Variable = Expression ], Options ) :-
  arithmetic_expression( Value ),
  !,
  (
    option( parallel(true), Options )
  ->
    Expression = ( para Value )
  ;
    option( first(true), Options )
  ->
    Expression = ( Value post ) % first statement
  ;
    Expression = ( pre Value post )
  ).

compile( Keyword, [ Keyword = 0 ], _ ) :-
  keyword( Keyword ), !.

compile( A ',' B, FunctionList, Options ) :-
  !,
  compile( A, Function,       [ parallel(true) ]  ),
  compile( B, FunctionListT,  Options             ),
  append( Function, FunctionListT, FunctionList ).

%% read_program(+In:stream, -Program:list) is det
%
%   Read the program code from _In_ and generate the corresponding *add_function* program _Program_.
read_program( In, Program ) :-
  is_stream( In ),
  read_program( In,[], Program ).

%% read_program(+In:stream, +Program:list, -NewProgram) is det
%
%   Read the program code from _In_ and append the corresponding *add_function* program _NewProgram_ to _Program_.
read_program( In, Program, NewProgram ) :-
  read( In, Term ),
  (
    Term == end_of_file
  ->
    NewProgram = Program
  ;
    (
      Program == []
    ->
      Options = [ first(true) ]
    ;
      Options = []
    ),
    compile( Term, Function, Options ),
    append( Program, Function, ProgramT ),
    read_program( In, ProgramT, NewProgram )
  ).

%% first_condition(+Condition:condition, -NewCondition:condition, +Options:'option list') is det
%
%   This is just a helper function to deal with the condition in the first statement.
%   The provided options are:
%
%   * first(Boolean)
%     Indicate whether this condition is in the first statement.
first_condition( Condition, NewCondition, Options ) :-
  (
    option( first(true), Options )
  ->
    NewCondition = Condition
  ;
    NewCondition = ( pre Condition )
  ).

%% remove_para(+FunctionList:list, -NewFunctionList:list) is det
%
%   This is a helper function to remove the unnecessary *para* operator in the functions when there is no sequential control flow in the program.
remove_para( [ Variable = ( para Term ) | FunctionList ], [ Variable = ( Term ) | ResultFunctionList ] ) :-
  !, remove_para( FunctionList, ResultFunctionList ).

remove_para( [ Function | FunctionList ], [ Function | ResultFunctionList ] ) :-
  !, remove_para( FunctionList, ResultFunctionList ).

remove_para( [], [] ).

%% simplify_variable_initialization(+FunctionList:list, -NewFunctionList:list, +Simplify:yesno ) is det
%
%   This pridicate performs the simplification of variable initialization if _Simplify_ is *yes*.
simplify_variable_initialization( [ Variable = Expression | FunctionList ], NewFunctionList, yes ) :-
  \+ operator( Variable ),
  get_pure_expression( Expression, PureExpression ),
  number( PureExpression ),
  !,

  new_symbol( Variable, dual, PureExpression ),
  simplify_variable_initialization( FunctionList, NewFunctionList, yes ).

simplify_variable_initialization( [ Variable = ( pre Expression ) | FunctionList ], NewFunctionList, yes ) :-
  !,
  append( [ Variable = ( Expression ) ], FunctionList, NewFunctionList ).

simplify_variable_initialization( [ Variable = ( pre Expression post ) | FunctionList ], NewFunctionList, yes ) :-
  !,
  append( [ Variable = ( Expression post ) ], FunctionList, NewFunctionList ).

simplify_variable_initialization( FunctionList, FunctionList, _ ).

%% get_pure_expression(+Expression:rhs, -PureExpression:expression) is det
%
%   This predicate removes the *pre*, *post* and *para* operator in the _Expression_.
get_pure_expression( pre Expression, PureExpression ) :-
  !, get_pure_expression( Expression, PureExpression ).

get_pure_expression( Expression post, PureExpression ) :-
  !, get_pure_expression( Expression, PureExpression ).

get_pure_expression( para Expression, PureExpression ) :-
  !, get_pure_expression( Expression, PureExpression ).

get_pure_expression( PureExpression, PureExpression ) :- !.

%% build_AST(-AST:list, +FunctionList:list, +NewFunctionList:list ) is det
%
%   This predicate build the abstract syntax tree _AST_ of the _FunctionList_.
%   It is written in *DCG grammar rule* in prolog language for easier maintenance.
%
%   The abstract syntax tree is a list with three kinds of element
%
%   * *Assignment*:
%
%     This is an element '_Variable_ = _Expression_'.
%
%   * *If Branch*:
%
%     This is a list with the first element being 'if_tag = _Expression_',
%     and the second, third element in the list are the then part AST and else part AST of the branch.
%
%   * *While Loop*:
%
%     This is a list with the first element being 'while = _Expression_',
%     and the second element is the loop AST.
build_AST([]) --> [].
build_AST( [ Variable = Expression | AST ] ) -->
  [ Variable = Expression ],
  {
    \+ control_flow_keyword( Variable )
  },
  build_AST( AST ).

build_AST( [ [ if_tag = Expression, ThenBlock, ElseBlock ] | AST ] ) -->
  [ if_tag = Expression ],
  build_AST( ThenBlock ),
  else_statement( ElseBlock ),
  [ endif = _ ],
  build_AST( AST ).

build_AST( [ [ while = Expression, LoopBlock ] | AST ] ) -->
  [ while = Expression ],
  build_AST( LoopBlock ),
  [ endwhile = _ ],
  build_AST( AST ).

%% else_statement(-AST:list, +FunctionList:list, +NewFunctionList:list ) is det
%
%   The predicate generate the AST of the else part of a if branch.
else_statement([]) --> [].
else_statement( AST ) -->
  [ else_tag = _ ],
  build_AST( AST ).
% end interface

% helper functions
% symbol table
:- dynamic symbol/2.

%% add_symbol(+Symbol:atom, +AttributeList:list) is det
%
%   Add the _Symbol_ with _AttributeList_ to the symbol table.
%   This is a low-level API of symbol table,
%   it should be used only in the high-level API of symbol table.
add_symbol( Symbol, AttributeList ) :-
  (
    symbol( Symbol, _ )
  ->
    true
  ;
    assertz( symbol( Symbol, AttributeList ) )
  ).

%% remove_symbol(+Symbol:atom) is det
%
%   Remove the _Symbol_ from the symbol table.
%   This is a low-level API of symbol table,
%   it should be used only in the high-level API of symbol table.
remove_symbol( Symbol ) :-
  retractall( symbol( Symbol, _ ) ).

%% replace_symbol(+Symbol:atom, +AttributeList:list) is det
%
%   Replace the attribute of _Symbol_ with _AttributeList_ in the symbol table.
%   This is a low-level API of symbol table,
%   it should be used only in the high-level API of symbol table.
replace_symbol( Symbol, AttributeList ) :-
  remove_symbol( Symbol ),
  add_symbol( Symbol, AttributeList ).

%% default_symbol_attr(+Symbol:atom, -AttributeList:list) is det
%
%   This predicate generates the default attribute list _AttributeList_ for a symbol _Symbol_.
%   The attributes of a symbol are:
%
%   * *const*: yesno, indicates whether the symbol is a constant.
%   * *dual*: yesno, indicates whether the symbol is a dual symbol.
%   * *name*: atom, the name of the symbol, if it is a dual symbol, the name will be [ PName, NName ].
%   * *value*: int, the value of the symbol, if it is a dual symbol, the value will be [ PValue, NValue ].
%     This is intended to be used in the data flow analysis.
%   * *init*: int, the initial value of the symbol, if it is a dual symbol, the init will be [ PInit, NInit ].
%   * *max*: int, the maximum value of the symbol.
%     This is intended to be used in the data flow analysis.
%   * *min*: int, the minimum value of the symbol.
%     This is intended to be used in the data flow analysis.
default_symbol_attr( Constant,
  [ const:  yes,
    dual:   yes,
    name:   [ P, N ],
    value:  [ P, N ],
    init:   [ P, N ],
    max:    Constant,
    min:    Constant,
    'not':  Not
  ], dual ) :-
  number( Constant ),
  !,
  (
    Constant >= 0
  ->
    P is Constant,  N is 0
  ;
    P is 0,         N is -Constant
  ),
  constant( notNull, Not ).

default_symbol_attr( Constant,
  [ const:  yes,
    dual:   no,
    name:   Constant,
    value:  Constant,
    init:   Constant,
    max:    Constant,
    min:    Constant,
    'not':  Not
  ] ) :-
  number( Constant ), !,
  constant( notNull, Not ).

default_symbol_attr( Variable,
  [ const:  no,
    dual:   no,
    name:   Variable,
    value:  0,
    init:   0,
    max:    0,
    min:    0,
    'not':  Not
  ] ) :-
  !, constant( notNull, Not ).

%% get_symbol_name(-Symbol:atom) is det
%
%   This predicate generate a symbol name if _Symbol_ is a variable,
%   otherwise, nothing happens.
get_symbol_name( Symbol ) :-
  nonvar( Symbol ), !.

get_symbol_name( Symbol ) :-
  var( Symbol ),
  !,
  constant( counter, CounterName ),

  peek_count( CounterName, Counter ),
  constant( tempPrefix, Prefix ),
  atom_concat( Prefix, Counter, Symbol ),
  count( CounterName, Counter ).

%% new_symbol(?Symbol:atom) is det
%
%   This predicate add a new symbol _Symbol_ with the default attributes.
%   It generates the symbol name if necessary.
new_symbol( Symbol ) :-
  new_symbol( Symbol, [] ).

%% new_symbol(?Symbol:atom, +Init:int) is det
%
%   This predicate add a new symbol _Symbol_ with the default attributes and initial value _Init_.
%   It generates the symbol name if necessary.
new_symbol( Symbol, Init ) :-
  number( Init ),
  !,
  new_symbol( Symbol, [ init: Init ] ).

%% new_symbol(?Symbol:atom, ++Dual) is det
%
%   This predicate add a new dual symbol _Symbol_ with the default attributes.
%   It generates the symbol name if necessary.
new_symbol( Symbol, dual ) :-
  !,
  new_symbol( Symbol, dual, [] ).

%% new_symbol(?Symbol:atom, +InitAttributeList:list) is det
%
%   This predicate add a new symbol _Symbol_ with the initial attributes _InitAttribute_.
%   It generates the symbol name if necessary.
%   This is the backend of other *new_symbol* predicates.
new_symbol( Symbol, InitAttributeList ) :-
  list( InitAttributeList ),
  !,
  (
    name( Symbol ),
    symbol( Symbol, _ )
  ->
    change_symbol_attr( Symbol, InitAttributeList )
  ;
    get_symbol_name( Symbol ),
    default_symbol_attr( Symbol, DefaultAttributeList ),
    replace_attr( DefaultAttributeList, InitAttributeList, AttributeList ),
    add_symbol( Symbol, AttributeList )
  ).

%% new_symbol(?Symbol:atom, ++Dual, +Init:int) is det
%
%   This predicate add a new dual symbol _Symbol_ with the default attributes and initial value _Init_.
%   It generates the symbol name if necessary.
new_symbol( Symbol, dual, Init ) :-
  number( Init ),
  !,
  (
    Init >= 0
  ->
    P is Init,  N is 0
  ;
    P is 0,     N is -Init
  ),
  new_symbol( Symbol, dual, [ init: [ P, N ] ] ).

%% new_symbol(?Symbol:atom, +InitAttributeList:list) is det
%
%   This predicate add a new dual symbol _Symbol_ with the initial attributes _InitAttribute_.
%   It generates the symbol name if necessary.
new_symbol( Symbol, dual, InitAttributeList ) :-
  list( InitAttributeList ),
  !,
  (
    symbol( Symbol, _ )
  ->
    change_symbol_attr( Symbol, InitAttributeList )
  ;
    get_symbol_name( Symbol ),
    atom_concat( Symbol, '_p', P ),
    atom_concat( Symbol, '_n', N ),
    DualAttributeList = [ dual: yes, name: [ P, N ] ],
    (
      member( init: _, InitAttributeList )
    ->
      NewDualAttributeList = DualAttributeList
    ;
      append( DualAttributeList, [ init: [ 0, 0 ] ], NewDualAttributeList )
    ),
    append( InitAttributeList, NewDualAttributeList, NewInitAttributeList ),
    new_symbol( Symbol, NewInitAttributeList )
  ).

%% change_symbol_attr(+Symbol:atom, +ChangeList:list) is det
%
%   Change the attributes of _Symbol_ to _ChangeList_.
change_symbol_attr( Symbol, ChangeList ) :-
  (
    symbol( Symbol, AttributeList )
  ->
    SymbolReal      = Symbol,
    ChangeListReal  = ChangeList
  ;
    get_symbol_attr( SymbolReal, [ name: [ Symbol, _ ] ] ),
    symbol( SymbolReal, AttributeList ),
    (
      member( init: Init, ChangeList )
    ->
      get_symbol_attr( SymbolReal, [ init: [ _, NInit ] ] ),
      select( init: _, ChangeList, init: [ Init, NInit ], ChangeListReal )
    ;
      ChangeListReal = ChangeList
    )
  ),
  replace_attr( AttributeList, ChangeListReal, ResultList ),
  replace_symbol( SymbolReal, ResultList ).

%% replace_attr(+AttributeList:list, +ChangeList:list, -NewAttributeList:list) is det
%
%   This predicate changes the attributes in _AttributeList_ to _ChangeList_ and generates the new attribute list _NewAttributeList_.
%   This is a helper function used in *change_symbol_attr*.
replace_attr( AttributeList, [], AttributeList ).

replace_attr( AttributeList, [ Key: Value | ReplaceList ], ResultList ) :-
  select( Key: _, AttributeList, Key: Value, AttributeListT ),
  replace_attr( AttributeListT, ReplaceList, ResultList ).

%% get_symbol_attr(+Symbol:atom, ?QueryList:list) is det
%
%   Get the attributes of _Symbol_ by giving a key-value list _QueryList_.
%   The *key* is the attribute, and the *value* will be instantiate to the value of the attribute.
get_symbol_attr( Constant, QueryList ) :-
  number( Constant ),
  !,
  (
    member( dual: yes, QueryList )
  ->
    default_symbol_attr( Constant, AttributeList, dual )
  ;
    default_symbol_attr( Constant, AttributeList )
  ),
  instantiate_symbol_attr( AttributeList, QueryList ).

get_symbol_attr( Symbol, QueryList ) :-
  symbol( Symbol, AttributeList ),
  instantiate_symbol_attr( AttributeList, QueryList ).

%% instantiate_symbol_attr(+AttributeList:list, ?QueryList:list) is det
%
%   This predicate instantiates the *value* in the key-value _QueryList_.
%   This is a helper function used in the *get_symbol_attr*.
instantiate_symbol_attr( _, [] ).

instantiate_symbol_attr( AttributeList, [ Key: Value | QueryList ] ) :-
  member( Key: Value, AttributeList ),
  instantiate_symbol_attr( AttributeList, QueryList ).

% end symbol table

%% init_variable(+OdeSystem:'ode system', +VariableName:atom, +Value:int) is det
%
%   Initialize variable with name _VariableName_ to _Value_ in the _OdeSystem_.
init_variable( _, VariableName, Value ) :-
  (
    Value >= 0
  ->
    PInit is Value, NInit is 0
  ;
    PInit is 0,     NInit is -Value
  ),
  change_symbol_attr( VariableName, [ init: [ PInit, NInit ] ] ).

%% add_ode(+OdeSystem:'ode system', +Variable:'dual var', +DP:'ode formula', +DN:'ode formula') is det
%
%   Add ode formula to variable _Variable_ in the ode system _OdeSystem_.
%   _DP_ is added to the positive part, and _DN_ is added to the negative part.
add_ode( OdeSystem, [ P, N ], DP, DN ) :-
  add_ode( OdeSystem, d( P ) / dt = DP ),
  add_ode( OdeSystem, d( N ) / dt = DN ).

%% get_ode(+OdeSystem:'ode system', +Variable:'dual var', -DP:'ode formula', -DN:'ode formula') is det
%
%   Get ode formula of variable _Variable_ in the ode system _OdeSystem_.
%   _DP_ the formula of the positive part, and _DN_ is the formula of the negative part.
get_ode( OdeSystem, [ P, N ], DP, DN ) :-
  findall(
    DPT,
    item( [ parent: OdeSystem, kind: ode, key: P, item: ( d( P ) / dt = DPT ) ] ),
    DP
  ),
  findall(
    DNT,
    item( [ parent: OdeSystem, kind: ode, key: N, item: ( d( N ) / dt = DNT ) ] ),
    DN
  ).

%% variable(?VariableName:atom, ?VariableDiff:'dual var') is det
%
%   A variable with name *x* uses dual var, and is stored as [ *x_p*, *x_n* ].
%   This predicate can be used to get variable information from the database.
%   Either using variable name to get the dual var, or using dual var to get the variable name.
variable( Constant, ConstantDiff ) :-
  number( Constant ),
  !,
  get_symbol_attr( Constant, [ dual: yes, name: ConstantDiff ] ).

variable( VariableName, VariableDiff ) :-
  get_symbol_attr( VariableName, [ name: VariableDiff ] ).

variable( [], [], list ).

variable( [ Name | NameList ], [ Variable | VariableList ], list ) :-
  variable( Name, Variable ),
  variable( NameList, VariableList, list ).

%% get_variable_positive(?VariableName:name, -VariableP:name) is det
%
%   Get the positive part of a variable.
%   Add new symbol if necessary.
get_variable_positive( VariableName, VariableP ) :-
  % get_variable_positive( ?VariableName, -VariableP )
  (
    var( VariableName )
  ->
    new_symbol( VariableP )
  ;
    variable( VariableName, [ VariableP, _ ] )
  ).

%% get_variable_symbol(+VariableName:name, -VariableSymbol:name) is det
%
%   Get the symbol from a variable of positive part of variable.
get_variable_symbol( VariableName, VariableSymbol ) :-
  (
    symbol( VariableName, _ )
  ->
    VariableSymbol = VariableName
  ;
    get_symbol_attr( VariableSymbol, [ name: [ VariableName, _ ] ] )
  ).

%% new_variable(?Name:name) is det
%
%   Add variable with name _Name_ with default attributes.
new_variable( Name ) :-
  \+ list( Name ),
  get_symbol_name( Name ),
  new_symbol( Name, dual ).

%% new_variable(?NameList:list) is det
%
%   Add variables with name in _NameList_ with default attributes.
new_variable( [ Name | NameList ] ) :-
  \+ list( Name ),
  list( NameList ),
  new_variable( Name ),
  (
    NameList == []
  ->
    true
  ;
    new_variable( NameList )
  ).

%% new_variable(+OdeSystem:'ode system', ?Name:name) is det
%
%   Add variable with name _Name_ and set initial value to 0 in the ode system _OdeSystem_.
new_variable( _, Name ) :-
  \+ list( Name ),

  new_variable( Name ).

%% new_variable(+OdeSystem:'ode system', ?NameList:list) is det
%
%   Add variables with name in _NameList_ and set initial value to 0 in the ode system _OdeSystem_.
new_variable( OdeSystem, [ Name | NameList ] ) :-
  \+ list( Name ),
  list( NameList ),
  new_variable( OdeSystem, Name ),
  (
    NameList == []
  ->
    true
  ;
    new_variable( OdeSystem, NameList )
  ).

%% new_variable(+OdeSystem:'ode system', ?Name:name, +Value:number) is det
%
%   Add variable with name _Name_ and set initial value to _Value_ in the ode system _OdeSystem_.
new_variable( OdeSystem, Name, Value ) :-
  new_variable( Name ),
  init_variable( OdeSystem, Name, Value ).

% condition

%% set_condition(+Condition:control_flow_condition, +Name:name ) is det
%
%   Set the condition _Condition_ to be the variable with name _Name_.
set_condition( Condition, Name ) :-
  control_flow_condition( Condition ),
  nb_setval( Condition, Name ).

%% get_condition(+Condition:control_flow_condition, -Name:name ) is det
%
%   Get the condition _Condition_ variable with name _Name_ that is set previously.
%   If there is no such condition being set, a constant *conNull* is returned.
get_condition( Condition, Name ) :-
  control_flow_condition( Condition ),
  (
    nb_current( Condition, Name )
  ->
    true
  ;
    constant( conNull, Name )
  ).

%% clear_condition(+Condition:control_flow_condition, -Name:name ) is det
%
%   This is the same as get_condition/2, but it also delete the global condition _Condition_.
clear_condition( Condition, Name ) :-
  get_condition( Condition, Name ),
  (
    constant( conNull, Name )
  ->
    true
  ;
    nb_delete( Condition )
  ).

% end condition

%% nnary_expression_to_ODE(+OdeSystem:'ode system', +ExpressionList:expression_list, -VariableENameList:name_list, ?VariableZName:name ) is det
%
%   Generate temperary variables with name in _VariableENameList_ in the ode system _OdeSystem_ to record the value of _ExpressionList_.
%   Generate temperary variable with name _VariableZName_ if necessary.
%  The length of _VariableENameList_ should be the same as _ExpressionList_.
%  This function is used to recursively parse the expression to be assigned to *Z*.
nnary_expression_to_ODE( _, [], [], Z ) :-
  new_variable( Z ).

nnary_expression_to_ODE( OdeSystem, [ Expression | ExpressionList ], [ E | EList ], Z ) :-
  expression_to_ODE( OdeSystem, Expression, E ),
  nnary_expression_to_ODE( OdeSystem, ExpressionList, EList, Z ).

%% unary_expression_to_ODE(+OdeSystem:'ode system', +Expression0:expression, -VariableE0Name:name, ?VariableZName:name) is det
%
%   nnary_expression_to_ODE(OdeSystem, [Expression0], [VariableE0Name], VariableZName).
unary_expression_to_ODE( OdeSystem, Expression0, E0, Z ) :-
  nnary_expression_to_ODE( OdeSystem, [ Expression0 ], [ E0 ], Z ).

%% binary_expression_to_ODE(+OdeSystem:'ode system', +Expression0:expression, +Expression1:expression, -VariableE0Name:name, -VariableE1Name:name, ?VariableZName:name ) is det
%
%   nnary_expression_to_ODE(OdeSystem, [Expression0, Expression1], [VariableE0Name, VariableE1Name], VariableZName).
binary_expression_to_ODE( OdeSystem, Expression0, Expression1, E0, E1, Z ) :-
  nnary_expression_to_ODE( OdeSystem, [ Expression0, Expression1 ], [ E0, E1 ], Z ).

%% trinary_expression_to_ODE(+OdeSystem:'ode system', +Expression0:expression, +Expression1:expression, +Expression2:expression, -VariableE0Name:name, -VariableE1Name:name, -VariableE2Name:name, ?VariableZName:name ) is det
%
%   nnary_expression_to_ODE(OdeSystem, [Expression0, Expression1, Expression2], [VariableE0Name, VariableE1Name, VariableE2Name], VariableZName).
trinary_expression_to_ODE( OdeSystem, Expression0, Expression1, Expression2, E0, E1, E2, Z ) :-
  nnary_expression_to_ODE( OdeSystem, [ Expression0, Expression1, Expression2 ], [ E0, E1, E2 ], Z ).

%% arithmetic_expression_to_ODE(+OdeSystem:'ode system', +ExpressionList:arithmetic_expression_list, -VariableENameList:name_list, ?VariableZName:name ) is det
%
%   nnary_expression_to_ODE/4 with the test of arithmetic_expression.
%   This should be deprecated and move the test of expression to the build_AST/3.
arithmetic_expression_to_ODE( OdeSystem, ExpressionList, EList, Z ) :-
  list( ExpressionList ),
  list( EList ),
  !,
  forall( member( Expression, ExpressionList ), arithmetic_expression( Expression ) ),
  nnary_expression_to_ODE( OdeSystem, ExpressionList, EList, Z ).

%% arithmetic_expression_to_ODE(+OdeSystem:'ode system', +Expression:arithmetic_expression, -VariableEName:name, ?VariableZName:name) is det
%
%   arithmetic_expression_to_ODE/4 for unary expression.
%   This should be deprecated and move the test of expression to the build_AST/3.
arithmetic_expression_to_ODE( OdeSystem, Expression, E, Z ) :-
  !,
  arithmetic_expression_to_ODE( OdeSystem, [ Expression ], [ E ], Z ).

%% arithmetic_expression_to_ODE(+OdeSystem:'ode system', +ExpressionX:arithmetic_expression, +ExpressionY:arithmetic_expression, -VariableEXName:name, -VariableEYName:name, ?VariableZName:name ) is det
%
%   arithmetic_expression_to_ODE/4 for binary expression.
%   This should be deprecated and move the test of expression to the build_AST/3.
arithmetic_expression_to_ODE( OdeSystem, ExpressionX, ExpressionY, X, Y, Z ) :-
  arithmetic_expression_to_ODE( OdeSystem, [ ExpressionX, ExpressionY ], [ X, Y ], Z ).

%% condition_expression_to_ODE(+OdeSystem:'ode system', +ExpressionList:condition_list, -VariableENameList:name_list, ?VariableZName:name ) is det
%
%   nnary_expression_to_ODE/4 with the test of condition.
%   This should be deprecated and move the test of expression to the build_AST/3.
condition_expression_to_ODE( OdeSystem, ExpressionList, EList, Z ) :-
  list( ExpressionList ),
  list( EList ),
  !,
  forall( member( Expression, ExpressionList ), condition( Expression ) ),
  nnary_expression_to_ODE( OdeSystem, ExpressionList, EList, Z ).

%% condition_expression_to_ODE(+OdeSystem:'ode system', +Expression:condition, -VariableEName:name, ?VariableZName:name) is det
%
%   condition_expression_to_ODE/4 for unary expression.
%   This should be deprecated and move the test of expression to the build_AST/3.
condition_expression_to_ODE( OdeSystem, Expression, E, Z ) :-
  !,
  condition_expression_to_ODE( OdeSystem, [ Expression ], [ E ], Z ).

%% condition_expression_to_ODE(+OdeSystem:'ode system', +ExpressionX:condition, +ExpressionY:condition, -VariableEXName:name, -VariableEYName:name, ?VariableZName:name ) is det
%
%   condition_expression_to_ODE/4 for binary expression.
%   This should be deprecated and move the test of expression to the build_AST/3.
condition_expression_to_ODE( OdeSystem, ExpressionX, ExpressionY, X, Y, Z ) :-
  condition_expression_to_ODE( OdeSystem, [ ExpressionX, ExpressionY ], [ X, Y ], Z ).

%% add_ode_initialization(+OdeSystem:'ode system') is det
%
%   Add the initiai value in symbol table to _OdeSystem_.
add_ode_initialization( OdeSystem ) :-
  forall(
    get_symbol_attr( Variable, [ const: no ] ),
    add_ode_initialization( OdeSystem, Variable )
  ).

%% add_ode_initialization(+OdeSystem:'ode system', +Variable:name) is det
%
%   Add the initiai value of _Variable_ in symbol table to _OdeSystem_.
add_ode_initialization( OdeSystem, Variable ) :-
  get_symbol_attr( Variable, [ name: Name, init: Init ] ),
  (
    get_symbol_attr( Variable, [ dual: yes ] )
  ->
    Name = [ P,     N     ],
    Init = [ PInit, NInit ],
    set_ode_initial_value( OdeSystem, P, PInit ),
    set_ode_initial_value( OdeSystem, N, NInit )
  ;
    set_ode_initial_value( OdeSystem, Name, Init )
  ).

%% add_fast_annihilation(+OdeSystem:'ode system') is det
%
%   Generate all the fast annihilation ode for variable database and add to the ode system _OdeSystem_.
add_fast_annihilation( OdeSystem ) :-
  forall(
    get_symbol_attr( _, [ const:no, dual:yes, name: Variable ] ),
    add_fast_annihilation( OdeSystem, Variable )
  ).

%% add_fast_annihilation(+OdeSystem:'ode system', +Variable:dual_var) is det
%
%   Generate fast annihilation ode for variable _Variable_ and add to the ode system _OdeSystem_.
add_fast_annihilation( OdeSystem, Variable ) :-
  get_ode( OdeSystem, Variable, DP, DN ),
  (
    DP == [] ; DN == [] % add fast annihilation only if both derivatives of diffential semantic are not 0
  ->
    true
  ;
    %  constant( fast, Fast ),
    % better to use the fast parameter
    add_fast_annihilation( OdeSystem, Variable, fast )
  ).

%% add_fast_annihilation(+OdeSystem:'ode system', +Variable:name, +Fast:number) is det
%
%   Generate fast annihilation ode for variable _Variable_ with fast coefficient _Fast_ and add to the ode system _OdeSystem_.
add_fast_annihilation( OdeSystem, [ P, N ], Fast ) :-
  add_ode( OdeSystem, [ P, N ], -Fast * P * N, -Fast * P * N ).

%% is_variable_in_expression(+Expression:expression, +VariableName:name) is det
%
%   Test if a variable with name _VariableName_ is in the _Expression_.
is_variable_in_expression( Expression, Variable ) :-
  compound( Expression ),
  atomic( Variable ),

  Expression =.. TermList,
  (
    member( Variable, TermList )
  ->
    true
  ;
    member( Term, TermList ),
    is_variable_in_expression( Term, Variable )
  ).

% end helper functions

% synthesis from expression to pivp
% AST compilation

%% compile_AST(+OdeSystem:'ode system', +AST:AST) is det
%
%   Compile the _AST_ into chemical reactions with ode system _OdeSystem_.
%   This should be merged with compile_AST/3.
compile_AST( OdeSystem, [ if_tag = Expression, ThenBlock, ElseBlock ] ) :-
  !,
  new_variable( OdeSystem, [ Post, PostPreNext ] ),
  compile_AST( OdeSystem, [ if_tag = Expression, ThenBlock, ElseBlock ], PostPreNext, Post ).

compile_AST( OdeSystem, [ while = Expression, LoopBlock ] ) :-
  !,
  new_variable( OdeSystem, [ Post, PostPreNext ] ),
  compile_AST( OdeSystem, [ while = Expression, LoopBlock ], PostPreNext, Post ).

compile_AST( OdeSystem, Variable = Value ) :-
  !,
  (
    is_variable_in_expression( Value, Variable )
  ->
    % deal with statement with variable in the expression
    new_variable( OdeSystem, T ),
    split_self_assignment( Value, T, ExpressionT, Expression ),
    expression_to_ODE( OdeSystem, ExpressionT, T )
  ;
    Expression = Value
  ),
  expression_to_ODE( OdeSystem, Expression, Variable ).

compile_AST( OdeSystem, AST ) :-
  member( NodeAST, AST ),
  compile_AST( OdeSystem, NodeAST ),
  fail.

compile_AST( _, _ ).

%% compile_AST(+OdeSystem:'ode system', +AST:AST, +PostPreNext:namef, +Post:name) is det
%
%   Compile the _AST_ into chemical reactions with ode system _OdeSystem_, next postpre _PostPreNext_ and postcondition _Post_.
%   This should be merged with compile_AST/2.
compile_AST( OdeSystem, [ if_tag = Expression, ThenBlock, ElseBlock ], PostPreNext, Post ) :-
  /* if_tag, else_tag, end_if
   *
   *        statements before branch
   *                   |
   *                   | pre
   *                   |
   *              if statement
   *                   |
   *          +--------+--------+
   *          |                 |
   *          | then_pre        | else_pre
   *          |                 |
   *  then statements     else stataments
   *          |                 |
   *          | then_post       | else_post
   *          |                 |
   *          +--------+--------+
   *                   |
   *                   | then_post || else_post
   *                   |
   *         statements after branch
   */
  !,
  get_pure_expression( Expression, Condition ),
  get_control_flow_init( Expression, Pre, PostPre, ProgramInit ),

  new_variable( OdeSystem,
                [ C, ThenPostPreNext, ElsePostPreNext, ThenPre, ElsePre, ThenPostPre, ElsePostPre,
                  ThenPost ] ),

  expression_to_ODE( OdeSystem, Condition, C ),
  (
    constant( conNull, ProgramInit )
  ->
    branch_to_PIVP( OdeSystem, if_tag, C, Pre, PostPre, ThenPostPreNext, ElsePostPreNext, ThenPre, ElsePre )
  ;
    eval( OdeSystem, Condition, EvalValue ),
    branch_to_PIVP( OdeSystem, if_tag, C, EvalValue, Pre, PostPre, ThenPostPreNext, ElsePostPreNext, ThenPre, ElsePre )
  ),

  compile_AST_with_conditions( OdeSystem, ThenPre, ThenPostPre, ThenBlock, ThenPost, PostPreNext ),
  (
    ElseBlock == []
  ->
    ElsePost = ElsePre
  ;
    new_variable( OdeSystem, ElsePost ),
    compile_AST_with_conditions( OdeSystem, ElsePre, ElsePostPre, ElseBlock, ElsePost, PostPreNext )
  ),
  branch_to_PIVP( OdeSystem, endif, ThenPost, ThenPostPre, ThenPostPreNext, ElsePost, ElsePostPre, ElsePostPreNext, Post, PostPreNext ),
  set_condition( precondition,      Post        ),
  set_condition( postcondition_pre, PostPreNext ).

compile_AST( OdeSystem, [ while = Expression, LoopBlock ], NoLoopPostPre, NoLoopPre ) :-
  /* while, end_while
   *
   *   statements before loop
   *              |
   *              | pre
   *              |
   *  +--- while statement
   *  |           |
   *  |           +--------------+
   *  |           |              |
   *  |           | in_loop_pre  |
   *  |           |              |
   *  |   loop statements        | no_loop_pre
   *  |           |              |
   *  |           | loop_post    |
   *  |           |              |
   *  +-----------+              |
   *                             |
   *              +--------------+
   *              |
   *              |
   *    statements after loop
   */
  !,
  get_pure_expression( Expression, Condition ),
  get_control_flow_init( Expression, Pre, PostPre, ProgramInit ),

  new_variable( OdeSystem, [ Post, InLoopPre, InLoopPostPre ] ),

  expression_to_ODE( OdeSystem, Condition, C ),
  (
    constant( conNull, ProgramInit )
  ->
    loop_to_PIVP( OdeSystem, while, C, Pre, PostPre, InLoopPre, InLoopPostPre, NoLoopPre, NoLoopPostPre, Post )
  ;
    eval( OdeSystem, Condition, EvalValue ),
    loop_to_PIVP( OdeSystem, while, C, EvalValue, Pre, PostPre, InLoopPre, InLoopPostPre, NoLoopPre, NoLoopPostPre, Post )
  ),
  compile_AST_with_conditions( OdeSystem, InLoopPre, InLoopPostPre, LoopBlock, Post, PostPre ),
  set_condition( precondition,      NoLoopPre     ),
  set_condition( postcondition_pre, NoLoopPostPre ).

compile_AST( OdeSystem, Variable = Value, PostPreNext, Post ) :-
  !,
  (
    is_variable_in_expression( Value, Variable )
  ->
    % deal with statement with variable in the expression
    new_variable( OdeSystem, T ),
    split_self_assignment( Value, T, ExpressionT, Expression ),
    expression_to_ODE( OdeSystem, ExpressionT, T )
  ;
    Expression = Value
  ),
  expression_to_ODE( OdeSystem, Expression, Variable, PostPreNext, Post ).

compile_AST( OdeSystem, AST, PostPreNext, Post ) :-
  member( NodeAST, AST ),
  (
    last( AST, NodeAST )
  ->
    compile_AST( OdeSystem, NodeAST, PostPreNext, Post )
  ;
    compile_AST( OdeSystem, NodeAST )
  ),
  fail.

compile_AST( _, _, _, _ ).

%% get_control_flow_init( +Expression:expression, -Pre:name, -PrePost:name, -ProgramInit ) is det
%
%   This is a helper function to get some conditions used in control flow.
get_control_flow_init( Expression, Pre, PostPre, ProgramInit ) :-
  get_condition( precondition,      Pre     ),
  get_condition( postcondition_pre, PostPre ),
  (
    Expression = ( pre _ )
  ->
    constant( conNull, ProgramInit )
  ;
    ProgramInit = true
  ).

%% compile_AST_with_conditions(+OdeSystem:'ode system', +Pre:name, +PostPre:name, +AST:AST, -Post:name, -PostPostPre:name) is det
%
%   This is a helper function to pass some conditions used in control flow.
compile_AST_with_conditions( OdeSystem, Pre, PostPre, AST, Post, PostPostPre ) :-
  set_condition( precondition,      Pre     ),
  set_condition( postcondition_pre, PostPre ),
  compile_AST( OdeSystem, AST, PostPostPre, Post ).

% split_self_assignment(+Expression:expression, +T:name, -InterExpression:expression, -NewExpression:expression) is det
%
%   This is a helper function to split a self reference assignment into two assignments.
%   _T_ is the intermediat variable used to combine the two assignments.
split_self_assignment( Expression, T, InterExpression, NewExpression ) :-
  get_pure_expression( Expression, PureExpression ),
  (
    Expression = ( pre _ ) ; Expression = ( pre _ post )
  ->
    InterExpression = ( pre PureExpression post )
  ;
    InterExpression = ( PureExpression post )
  ),
  (
    Expression = ( _ post )
  ->
    NewExpression = ( pre T post )
  ;
    NewExpression = ( pre T )
  ).

% end AST compilation

% expression parser
:- discontiguous expression_to_ODE/3.
:- discontiguous expression_to_ODE/5.
% constant

%% expression_to_ODE(+OdeSystem:'ode system', +Expression:expression, +VariableName:name) is semidet
%
%   Compile the expression into ODE with dual variables so that it can then be compiled into chemical reactions.
%   The ode will be stored in the _OdeSystem_.
%   The meaning of the variable with name _VariableName_ will be the result value of the _Expression_.
%   More details of implementation are described in the comments of each expression.

%% expression_to_ODE(+OdeSystem:'ode system', +Expression:expression, +VariableName:name, +PostPreNext:name, +Post:name) is semidet
%
%   The expression_to_ODE/3 with conditions _PostPreNext_ and _Post_.
expression_to_ODE( _, Constant, Constant ) :-
  % expression_to_ODE( ?OdeSystem, +Constant, -Constant )
  number( Constant ), !.

expression_to_ODE( _, BooleanConstant, Constant ) :-
  % expression_to_ODE( ?OdeSystem, +BooleanConstant, -Constant )
  constant( BooleanConstant, Constant ), !.

% end constant

% branch
expression_to_ODE( OdeSystem, if Condition then ExpressionT else ExpressionE, Z ) :-
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName )
  condition( Condition ),
  arithmetic_expression( ExpressionT ),
  arithmetic_expression( ExpressionE ),
  !,
  trinary_expression_to_ODE( OdeSystem, Condition, ExpressionT, ExpressionE, C, T, E, Z ),
  branch_to_PIVP( OdeSystem, C, T, E, Z ).

% end branch

% procedural
expression_to_ODE( OdeSystem, pre Expression post, Z ) :-
  !,
  new_variable( OdeSystem, [ Post, PostPreNext ] ),
  expression_to_ODE( OdeSystem, pre Expression post, Z, PostPreNext, Post ).

expression_to_ODE( OdeSystem, pre Expression post, Z, PostPreNext, Post ) :-
  % expression_to_ODE( +OdeSystem, +Expression, ?VariableName )
  !,
  unary_expression_to_ODE( OdeSystem, Expression, E, Z ),
  get_condition( precondition,      Pre     ),
  get_condition( postcondition_pre, PostPre ),
  inter_to_PIVP( OdeSystem, E, Z, Pre, PostPre, PostPreNext, Post ),
  set_condition( precondition,      Post        ),
  set_condition( postcondition_pre, PostPreNext ).

expression_to_ODE( OdeSystem, pre Expression, Z ) :-
  % expression_to_ODE( +OdeSystem, +Expression, ?VariableName )
  !,
  unary_expression_to_ODE( OdeSystem, Expression, E, Z ),
  clear_condition( postcondition_pre, _   ),
  clear_condition( precondition,      Pre ),
  pre_to_PIVP( OdeSystem, E, Z, Pre ).

expression_to_ODE( OdeSystem, Expression post, Z ) :-
  !,
  new_variable( OdeSystem, [ Post, PostPreNext ] ),
  expression_to_ODE( OdeSystem, Expression post, Z, PostPreNext, Post ).

expression_to_ODE( OdeSystem, Expression post, Z, PostPreNext, Post ) :-
  % expression_to_ODE( +OdeSystem, +Expression, ?VariableName )
  !,
  eval( OdeSystem, Expression, EvalValue ),
  unary_expression_to_ODE( OdeSystem, Expression, E, Z ),
  get_condition( precondition,      Pre     ),
  get_condition( postcondition_pre, PostPre ),
  post_to_PIVP( OdeSystem, E, Z, EvalValue, Pre, PostPre, PostPreNext, Post ),
  set_condition( precondition,      Post        ),
  set_condition( postcondition_pre, PostPreNext ).

expression_to_ODE( OdeSystem, para Expression, Z ) :-
  % expression_to_ODE( +OdeSystem, +Expression, ?VariableName )
  !,
  unary_expression_to_ODE( OdeSystem, Expression, E, Z ),
  get_condition( precondition, Pre ),
  parallel_to_PIVP( OdeSystem, E, Z, Pre ).

% end procedural

% conditional operations
expression_to_ODE( OdeSystem, ExpressionX = ExpressionY, Z ) :-
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName )
  !,
  arithmetic_expression_to_ODE( OdeSystem, ExpressionX, ExpressionY, X, Y, Z ),
  equal_to_PIVP( OdeSystem, X, Y, Z ).

expression_to_ODE( OdeSystem, ExpressionX < ExpressionY, Z ) :-
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName )
  !,
  arithmetic_expression_to_ODE( OdeSystem, ExpressionX, ExpressionY, X, Y, Z ),
  lessthan_to_PIVP( OdeSystem, X, Y, Z ).

expression_to_ODE( OdeSystem, ExpressionX <= ExpressionY, Z ) :-
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName )
  !,
  arithmetic_expression_to_ODE( OdeSystem, ExpressionX, ExpressionY, X, Y, Z ),
  lessequal_to_PIVP( OdeSystem, X, Y, Z ).

expression_to_ODE( OdeSystem, ExpressionX > ExpressionY, Z ) :-
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName )
  !,
  arithmetic_expression_to_ODE( OdeSystem, ExpressionX, ExpressionY, X, Y, Z ),
  greaterthan_to_PIVP( OdeSystem, X, Y, Z ).

expression_to_ODE( OdeSystem, ExpressionX >= ExpressionY, Z ) :-
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName )
  !,
  arithmetic_expression_to_ODE( OdeSystem, ExpressionX, ExpressionY, X, Y, Z ),
  greaterequal_to_PIVP( OdeSystem, X, Y, Z ).

expression_to_ODE( OdeSystem, ExpressionX or ExpressionY, Z ) :-
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName )
  !,
  condition_expression_to_ODE( OdeSystem, ExpressionX, ExpressionY, X, Y, Z ),
  or_to_PIVP( OdeSystem, X, Y, Z ).

expression_to_ODE( OdeSystem, ExpressionX and ExpressionY, Z ) :-
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName )
  !,
  condition_expression_to_ODE( OdeSystem, ExpressionX, ExpressionY, X, Y, Z ),
  and_to_PIVP( OdeSystem, X, Y, Z ).

expression_to_ODE( OdeSystem, not Expression , Z ) :-
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName )
  !,
  condition_expression_to_ODE( OdeSystem, Expression, T, Z ),
  not_to_PIVP( OdeSystem, T, Z ).

% end conditional operations

% arithmetic operations
expression_to_ODE( OdeSystem, ExpressionX + ExpressionY, Z ) :-
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName )
  !,
  arithmetic_expression_to_ODE( OdeSystem, ExpressionX, ExpressionY, X, Y, Z ),
  addition_to_PIVP( OdeSystem, X, Y, Z ).

expression_to_ODE( OdeSystem, ExpressionX - ExpressionY, Z ) :-
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName )
  !,
  arithmetic_expression_to_ODE( OdeSystem, ExpressionX, ExpressionY, X, Y, Z ),
  substraction_to_PIVP( OdeSystem, X, Y, Z ).

expression_to_ODE( OdeSystem, ExpressionX * ExpressionY, Z ) :-
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName )
  !,
  arithmetic_expression_to_ODE( OdeSystem, ExpressionX, ExpressionY, X, Y, Z ),
  multiplication_to_PIVP( OdeSystem, X, Y, Z ).

expression_to_ODE( OdeSystem, ExpressionX / ExpressionY, Z ) :-
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName )
  !,
  arithmetic_expression_to_ODE( OdeSystem, ExpressionX, ExpressionY, X, Y, Z ),
  division_to_PIVP( OdeSystem, X, Y, Z ).

expression_to_ODE( OdeSystem, ExpressionX ^ ExpressionY, Z ) :-
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName )
  !,
  arithmetic_expression_to_ODE( OdeSystem, ExpressionX, ExpressionY, X, Y, Z ),
  power_to_PIVP( OdeSystem, X, Y, Z ).

expression_to_ODE( OdeSystem, Operator + Expression, Z ) :-
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName )
  operator( Operator ),
  arithmetic_expression( Expression ),
  !,
  expression_to_ODE( OdeSystem, + Expression, Z ).

expression_to_ODE( OdeSystem, + Expression, Z ) :-
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName )
  !,
  arithmetic_expression_to_ODE( OdeSystem, Expression, T, Z ),
  positive_to_PIVP( OdeSystem, T, Z ).

expression_to_ODE( OdeSystem, Operator - Expression, Z ) :-
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName )
  operator( Operator ),
  arithmetic_expression( Expression ),
  !,
  expression_to_ODE( OdeSystem, - Expression, Z ).

expression_to_ODE( OdeSystem, - Expression, Z ) :-
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName )
  !,
  arithmetic_expression_to_ODE( OdeSystem, Expression, T, Z ),
  negative_to_PIVP( OdeSystem, T, Z ).

% functions
expression_to_ODE( OdeSystem, abs( Expression ), Z ) :-
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName )
  !,
  arithmetic_expression_to_ODE( OdeSystem, Expression, E, Z ),
  abs_to_PIVP( OdeSystem, E, Z ).

expression_to_ODE( OdeSystem, min( ExpressionX ',' ExpressionY ), Z ) :-
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName )
  !,
  arithmetic_expression_to_ODE( OdeSystem, ExpressionX, ExpressionY, X, Y, Z ),
  min_to_PIVP( OdeSystem, X, Y, Z ).

expression_to_ODE( OdeSystem, max( ExpressionX ',' ExpressionY ), Z ) :-
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName )
  !,
  arithmetic_expression_to_ODE( OdeSystem, ExpressionX, ExpressionY, X, Y, Z ),
  max_to_PIVP( OdeSystem, X, Y, Z ).

expression_to_ODE( OdeSystem, log( Expression ), Z ) :-
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName )
  !,
  arithmetic_expression_to_ODE( OdeSystem, Expression, T, Z ),
  log_to_PIVP( OdeSystem, T, Z ).

expression_to_ODE( OdeSystem, exp( Expression ), Z ) :-
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName )
  !,
  arithmetic_expression_to_ODE( OdeSystem, Expression, T, Z ),
  exp_to_PIVP( OdeSystem, T, Z ).

expression_to_ODE( OdeSystem, cos( Expression ), Z ) :-
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName )
  !,
  arithmetic_expression_to_ODE( OdeSystem, Expression, T, Z ),
  cos_to_PIVP( OdeSystem, T, Z ).

expression_to_ODE( OdeSystem, sin( Expression ), Z ) :-
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName )
  !,
  arithmetic_expression_to_ODE( OdeSystem, Expression, T, Z ),
  sin_to_PIVP( OdeSystem, T, Z ).

expression_to_ODE( OdeSystem, hill( Expression ), Z ) :-
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName )
  !,
  arithmetic_expression_to_ODE( OdeSystem, Expression, T, Z ),
  hill_to_PIVP( OdeSystem, T, Z, _ ).

expression_to_ODE( OdeSystem, pos_tanh( Expression ), Z ) :-
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName )
  !,
  arithmetic_expression_to_ODE( OdeSystem, Expression, T, Z ),
  pos_tanh_to_PIVP( OdeSystem, T, Z ).

expression_to_ODE( OdeSystem, neg_tanh( Expression ), Z ) :-
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName )
  !,
  arithmetic_expression_to_ODE( OdeSystem, Expression, T, Z ),
  neg_tanh_to_PIVP( OdeSystem, T, Z ).

expression_to_ODE( OdeSystem, pos_step( Expression ), Z ) :-
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName )
  !,
  arithmetic_expression_to_ODE( OdeSystem, Expression, T, Z ),
  pos_step_to_PIVP( OdeSystem, T, Z ).

expression_to_ODE( OdeSystem, neg_step( Expression ), Z ) :-
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName )
  !,
  arithmetic_expression_to_ODE( OdeSystem, Expression, T, Z ),
  neg_step_to_PIVP( OdeSystem, T, Z ).

% end functions
% end arithmetic operations

% switch
expression_to_ODE( OdeSystem, switch_y( ExpressionSX, ExpressionSY ), Z ) :-
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName )
  !,
  arithmetic_expression_to_ODE( OdeSystem, ExpressionSX, ExpressionSY, SX, SY, Z ),
  switch_to_PIVP( OdeSystem, SX, SY, _, Z ).

expression_to_ODE( OdeSystem, switch_x( ExpressionSX, ExpressionSY ), Z ) :-
  % expression_to_ODE( +OdeSystem, +Expression, -VariableName )
  !,
  arithmetic_expression_to_ODE( OdeSystem, ExpressionSX, ExpressionSY, SX, SY, Z ),
  switch_to_PIVP( OdeSystem, SX, SY, Z, _ ).
% end switch

% variable
expression_to_ODE( OdeSystem, X, X ) :-
  % expression_to_ODE( +OdeSystem, +VariableName, -VariableName )
  !,
  new_symbol( X, dual ),
  argument_to_PIVP( OdeSystem, X ).

expression_to_ODE( OdeSystem, X, Z ) :-
  % expression_to_ODE( +OdeSystem, +VariableXName, -VariableZName )
  !,
  unary_expression_to_ODE( OdeSystem, X, T, Z ),
  assign_to_PIVP( OdeSystem, T, Z ).
% end variable

% expression evaluation

%% eval(+InitSystem, +Expression:expression, +EvalResult:number) is det
%
%   This predicate evaluate the _Expression_ with initial value of variable given by _InitSystem_.
%   Ths results will be _EvalResult_.
%   Currently, the initial value of variable only given by symbol table.
eval( _,  Constant, Constant ) :-
  number( Constant ), !.

eval( _, BooleanConstant, Constant ) :-
  constant( BooleanConstant, Constant ), number( Constant ), !.

eval( InitSystem, X = Y, Z ) :-
  !,
  eval( InitSystem, X, T1 ),
  eval( InitSystem, Y, T2 ),
  eval_condition( T1 == T2, Z ).

eval( InitSystem, X < Y, Z ) :-
  !,
  eval( InitSystem, X, T1 ),
  eval( InitSystem, Y, T2 ),
  eval_condition( T1 < T2, Z ).

eval( InitSystem, X <= Y, Z ) :-
  !,
  eval( InitSystem, X, T1 ),
  eval( InitSystem, Y, T2 ),
  eval_condition( T1 =< T2, Z ).

eval( InitSystem, X > Y, Z ) :-
  !, eval( InitSystem, Y < X, Z ).

eval( InitSystem, X >= Y, Z ) :-
  !, eval( InitSystem, Y <= X, Z ).

eval( InitSystem, X or Y, Z ) :-
  !,
  eval( InitSystem, X, T1 ),
  eval( InitSystem, Y, T2 ),
  eval_condition( ( constant( true, T1 ) ; constant( true, T2 ) ), Z ).

eval( InitSystem, X and Y, Z ) :-
  !,
  eval( InitSystem, X, T1 ),
  eval( InitSystem, Y, T2 ),
  eval_condition( ( constant( true, T1 ), constant( true, T2 ) ), Z ).

eval( InitSystem, not X, Z ) :-
  !,
  eval( InitSystem, X, T ),
  eval_condition( constant( false, T ), Z ).

eval( InitSystem, X + Y, Z ) :-
  !,
  eval( InitSystem, X, T1 ),
  eval( InitSystem, Y, T2 ),
  Z is T1 + T2.

eval( InitSystem, X - Y, Z ) :-
  !,
  eval( InitSystem, X, T1 ),
  eval( InitSystem, Y, T2 ),
  Z is T1 - T2.

eval( InitSystem, X * Y, Z ) :-
  !,
  eval( InitSystem, X, T1 ),
  eval( InitSystem, Y, T2 ),
  Z is T1 * T2.

eval( InitSystem, X / Y, Z ) :-
  !,
  eval( InitSystem, X, T1 ),
  eval( InitSystem, Y, T2 ),
  Z is T1 / T2.

eval( InitSystem, X ^ Y, Z ) :-
  !,
  eval( InitSystem, X, T1 ),
  eval( InitSystem, Y, T2 ),
  Z is T1 ^ T2.

eval( InitSystem, +X, Z ) :-
  !,
  eval( InitSystem, X, T ),
  Z is T.

eval( InitSystem, -X, Z ) :-
  !,
  eval( InitSystem, X, T ),
  Z is -T.

eval( InitSystem, abs( X ), Z ) :-
  !,
  eval( InitSystem, X, T ),
  Z is abs( T ).

eval( InitSystem, min( X, Y ), Z ) :-
  !,
  eval( InitSystem, X, T1 ),
  eval( InitSystem, Y, T2 ),
  Z is min( T1, T2 ).

eval( InitSystem, max( X, Y ), Z ) :-
  !,
  eval( InitSystem, X, T1 ),
  eval( InitSystem, Y, T2 ),
  Z is max( T1, T2 ).

eval( InitSystem, log( X ), Z ) :-
  !,
  eval( InitSystem, X, T ),
  Z is log( T ).

eval( InitSystem, exp( X ), Z ) :-
  !,
  eval( InitSystem, X, T ),
  Z is exp( T ).

eval( InitSystem, cos( X ), Z ) :-
  !,
  eval( InitSystem, X, T ),
  Z is cos( T ).

eval( InitSystem, sin( X ), Z ) :-
  !,
  eval( InitSystem, X, T ),
  Z is sin( T ).

eval( InitSystem, hill( X ), Z ) :-
  !,
  constant( true, True ),
  eval( InitSystem, X, T ),
  Z is True * T ^ 5 / ( 0.1 + T ^ 5 ).

eval( InitSystem, pos_tanh( X ), Z ) :-
  !,
  constant( true, True ),
  eval( InitSystem, X, T ),
  Z is True * tanh( 6 * ( T - 0.8 ) ).

eval( InitSystem, neg_tanh( X ), Z ) :-
  !,
  constant( true, True ),
  eval( InitSystem, X, T ),
  Z is True * tanh( -6 * ( T - 0.8 ) ).

eval( InitSystem, pos_step( X ), Z ) :-
  !,
  constant( true, True ),
  eval( InitSystem, X, T ),
  Z is True * ( 1 + tanh( 6 * ( T - 0.8 ) ) ).

eval( InitSystem, neg_step( X ), Z ) :-
  !,
  constant( true, True ),
  eval( InitSystem, X, T ),
  Z is True * ( 1 + tanh( -6 * ( T - 0.8 ) ) ).

eval( OdeSystem, X, Z ) :-
  item( [ kind: ode_system, id: OdeSystem ] ),
  !,
  (
    get_symbol_attr( X, [ dual: yes, init: Init ] )
  ->
    Init = [ PInit, NInit ],
    Z is PInit - NInit
  ;
    get_symbol_attr( X, [ init: Z ] )
  ).

%% eval_condition(+Condition:condition, +Result:number) is det
%
%   This is a helper function to evaluate the conditions with true and false value given by constant/2.
eval_condition( Condition, Z ) :-
  (
    call( Condition )
  ->
    constant( true, Z )
  ;
    constant( false, Z )
  ).
% end expression evaluation
% end expression parser

% unit synthesizer
/*  z = x :
*
*     z_p - z_n = ( x_p - x_n )
*
*     dz_p = x_p - z_p
*     dz_n = x_n - z_n
*/
%% assign_to_PIVP(+OdeSystem:'ode system', +VariableXName:name, +VariableZName:name) is det
assign_to_PIVP( OdeSystem, X, Z ) :-
  variable( [ Z, X ], [ ZDiff, XDiff ], list ),
  (
    XDiff = [ XP, XN ]
  ->
    true
  ;
    XP = XDiff,
    XN = 0
  ),

  assign_to_ODE( OdeSystem, ZDiff, XP, XN ).

%% assign_to_ODE(+OdeSystem:'ode system', +Variable:'dual var', +PValue:polynomial, +NValue:polynomial ) is det
assign_to_ODE( OdeSystem, [ P, N ], PValue, NValue ) :-
  add_ode( OdeSystem, [ P, N ], PValue - P, NValue - N ).

% arithmetic operations
/*  z = +x :
*
*     z = x
*/
%% positive_to_PIVP(+OdeSystem:'ode system', +VariableXName:name, +VariableZName:name ) is det
positive_to_PIVP( OdeSystem, X, Z ) :-
  assign_to_PIVP( OdeSystem, X, Z ).

/*  z = -x :
*
*     z_p - z_n = -( x_p - x_n )
*               = x_n - x_p
*
*     dz_p = x_n - z_p
*     dz_n = x_p - z_n
*/

%% negative_to_PIVP(+OdeSystem:'ode system', +VariableXName:name, +VariableZName:name ) is det
negative_to_PIVP( OdeSystem, X, Z ) :-
  variable( [ Z, X ], [ ZDiff, [ XP, XN ] ], list ),

  assign_to_ODE( OdeSystem, ZDiff, XN, XP ).

/*  z = x ^ y :
*
*     z = exp( y * log( x ) )
*/
%% power_to_PIVP(+OdeSystem:'ode system', +VariableXName:name, +VariableYName:name, +VariableZName:name) is det
power_to_PIVP( OdeSystem, X, Y, Z ) :-
  new_variable( OdeSystem, [ T0, T1 ] ),

  log_to_PIVP(            OdeSystem, X,   T0      ),
  multiplication_to_PIVP( OdeSystem, Y,   T0, T1  ),
  exp_to_PIVP(            OdeSystem, T1,  Z       ).

/*  z = x * y :
*
*     z_p - z_n = ( x_p - x_n ) * ( y_p - y_n )
*               = ( x_p * y_p + x_n * y_n ) - ( x_p * y_n + x_n * y_p )
*
*     dz_p = x_p * y_n + x_n * y_n - z_p
*     dz_n = x_p * y_n + x_n * y_p - z_n
*/
%% multiplication_to_PIVP(+OdeSystem:'ode system', +VariableXName:name, +VariableYName:name, +VariableZName:name) is det
multiplication_to_PIVP( OdeSystem, X, Y, Z ) :-
  variable( [ Z, X, Y ], [ ZDiff, [ XP, XN ], [ YP, YN ] ], list ),

  assign_to_ODE( OdeSystem, ZDiff, XP * YP + XN * YN, XP * YN + XN * YP ).

%% multiplication_to_PIVP(+OdeSystem:'ode system', +VariableXName:name, +VariableYName:name, +VariableZName:name, ++Pos ) is det
%
%   Non-negative value version of multiplication_to_PIVP/4
multiplication_to_PIVP( OdeSystem, X, Y, Z, pos ) :-
  variable( [ Z, X, Y ], [ [ ZP, _ ], [ XP, _ ], [ YP, _ ] ], list ),

  add_ode( OdeSystem, d( ZP ) / dt = XP * YP - ZP ).

/*  z = constant / x :
*
*   Note: currently do not support negative x.
*
*     dz      = -constant * temp0
*     dtemp0  = -2 * constant * z * temp0
*
*     =>
*
*     dg      = x - g
*     dz      = -constant * temp0 * dg
*     dtemp0  = -2 * constant * z * temp0 * dg
*
*     d( g_p - g_n          ) = ( x_p - x_n ) - ( g_p - g_n )
*                             = ( x_p + g_n ) - ( x_n + g_p )
*     d( z_p - z_n          ) = -constant * ( temp0_p - temp0_n ) * ( dg_p - dg_n )
*                             = constant * (  temp0_p * dg_n + temp0_n * dg_p ) -
*                               constant * (  temp0_p * dg_p + temp0_n * dg_n )
*     d( temp0_p - temp0_n  ) = -2 * constant * ( z_p - z_n ) * ( temp0_p - temp0_n ) * ( dg_p - dg_n )
*                             = 2 * constant * (  z_p * ( temp0_p * dg_n + temp0_n * dg_p ) +
*                                                 z_n * ( temp0_p * dg_p + temp0_n * dg_n ) ) -
*                               2 * constant * (  z_p * ( temp0_p * dg_p + temp0_n * dg_n ) +
*                                                 z_n * ( temp0_p * dg_n + temp0_n * dg_p ) )
*
*     dg_p      = x_p + g_n
*     dg_n      = x_n + g_p
*     dz_p      = constant * (  temp0_p * dg_n + temp0_n * dg_p )
*     dz_n      = constant * (  temp0_p * dg_p + temp0_n * dg_n )
*     dtemp0_p  = 2 * constant * (  z_p * ( temp0_p * dg_n + temp0_n * dg_p ) +
*                                   z_n * ( temp0_p * dg_p + temp0_n * dg_n ) )
*     dtemp0_n  = 2 * constant * (  z_p * ( temp0_p * dg_p + temp0_n * dg_n ) +
*                                   z_n * ( temp0_p * dg_n + temp0_n * dg_p ) )
*
*     g init      = x + 1 = 1
*     z init      = 1 / ( x + 1 ) = 1
*     temp0 init  = 1 / ( x + 1 ) ^ 2 = 1
*/
%% reciprocal_to_PIVP(+OdeSystem:'ode system', +Constant:number, +VariableXName:name, +VariableZName:name) is det
reciprocal_to_PIVP( OdeSystem, Constant, X, Z ) :-
  init_reciprocal( OdeSystem, Z, T, G ),

  variable( [ X, G, Z, T ], [ XDiff, GDiff, ZDiff, TDiff ], list ),

  reciprocal_to_ODE( OdeSystem, Constant, GDiff, XDiff, TDiff, ZDiff ).

%% init_reciprocal(+OdeSystem:'ode system', +VariableZName:name, -VariableT0Name:name, -VariableGName:name) is det
init_reciprocal( OdeSystem, Z, T, G ) :-
  init_variable(  OdeSystem, Z, 1 ),
  new_variable(   OdeSystem, T, 1 ),
  new_variable(   OdeSystem, G, 1 ).

%% reciprocal_to_ODE(+OdeSystem:'ode system', +Constant:number, +VariableG:'dual var', +VariableX:'dual var', +VariableT:'dual var', +VariableZ:'dual var') is det
reciprocal_to_ODE( OdeSystem, Constant, [ GP, GN ], [ XP, XN ], [ TP, TN ], [ ZP, ZN ] ) :-
  DGP = XP + GN,
  DGN = XN + GP,

  add_ode(  OdeSystem, [ GP, GN ], DGP, DGN ),
  add_ode(  OdeSystem, [ ZP, ZN ],
            Constant * ( TP * DGN + TN * DGP ),
            Constant * ( TP * DGP + TN * DGN ) ),
  add_ode(  OdeSystem, [ TP, TN ],
            2 * Constant * ( ZP * ( TP * DGN + TN * DGP ) + TN * ( TP * DGP + TN * DGN ) ),
            2 * Constant * ( ZP * ( TP * DGP + TN * DGN ) + TN * ( TP * DGN + TN * DGP ) ) ).

/*  z = x / y :
*
*   Note: currently do not support negative y ( because constant / x do no support negative x ).
*
*     z = 1 / y * x
*/
%% division_to_PIVP(+OdeSystem:'ode system', +VairableXName:name, +VariableYName:name, +VariableZName:name) is det
division_to_PIVP( OdeSystem, X, Y, Z ) :-
  new_variable( OdeSystem, T ),

  reciprocal_to_PIVP(     OdeSystem, 1, Y, T ),
  multiplication_to_PIVP( OdeSystem, X, T, Z ).

/*  z = x + y :
*
*     z_p - z_n = ( x_p - x_n ) + ( y_p - y_n )
*               = ( x_p + y_p ) - ( x_n + y_n )
*
*     dz_p = x_p + y_p - z_p
*     dz_n = x_n + y_n - z_n
*/
%% addition_to_PIVP(+OdeSystem:'ode system', +VairableXName:name, +VariableYName:name, +VariableZName:name) is det
addition_to_PIVP( OdeSystem, X, Y, Z ) :-
  variable( [ Z, X, Y ], [ ZDiff, [ XP, XN ], [ YP, YN ] ], list ),

  assign_to_ODE( OdeSystem, ZDiff, XP + YP, XN + YN ).

%% addition_to_PIVP(+OdeSystem:'ode system', +VairableXName:name, +VariableYName:name, +VariableZName:name, ++Pos) is det
%
%   Non-negative value version of addition_to_PIVP/4
addition_to_PIVP( OdeSystem, X, Y, Z, pos ) :-
  variable( [ Z, X, Y ], [ [ ZP, _ ], [ XP, _ ], [ YP, _ ] ], list ),

  add_ode( OdeSystem, d( ZP ) / dt = XP + YP - ZP ).

/*  z = x - y :
*
*     z_p - z_n = ( x_p - x_n ) - ( y_p - y_n )
*               = ( x_p + y_n ) - ( x_n + y_p )
*
*     dz_p = x_p + y_n - z_p
*     dz_n = x_n + y_p - z_n
*/
%% substraction_to_PIVP(+OdeSystem:'ode system', +VairableXName:name, +VariableYName:name, +VariableZName:name) is det
substraction_to_PIVP( OdeSystem, X, Y, Z ) :-
  variable( [ Z, X, Y ], [ ZDiff, [ XP, XN ], [ YP, YN ] ], list ),

  assign_to_ODE( OdeSystem, ZDiff, XP + YN, XN + YP ).

% arithmetic operations

/*  x = x_p - x_n :
*
*     x >= 0  : x_p = x, x_n = 0
*     x < 0   : x_p = 0, x_n = -x
*/
%% argument_to_PIVP(+OdeSystem:'ode system', +VariableXName:name) is det
argument_to_PIVP( _, _ ).

% functions
/*  z = abs( x ):
*
*     dz = x_p + x_n - z
*
*     =>
*
*     dz_p  = x_p + x_n - z_p
*     dz_n  = 0 - z_n
*/
%% abs_to_PIVP(+OdeSystem:'ode system', +VariableXName:name, +VariableZName:name) is det
abs_to_PIVP( OdeSystem, X, Z ) :-
  variable( [ X, Z ], [ XDiff, ZDiff ], list ),

  abs_to_ODE( OdeSystem, XDiff, ZDiff ).

%% abs_to_ODE(+OdeSystem:'ode system', +VariableX:'dual var', +VariableZ:'dual var') is det
abs_to_ODE( OdeSystem, [ XP, XN ], [ ZP, _ ] ) :-
  add_ode( OdeSystem, d( ZP ) / dt = XP + XN - ZP ).

/*  z = min( x, y ):
*
*     z = ( x > y ) ? y : x
*/
%% min_to_PIVP(+OdeSystem:'ode system', +VariableXName:name, +VariableYName:name, +VariableZName:name) is det
min_to_PIVP( OdeSystem, X, Y, Z ) :-
  new_variable( OdeSystem, T ),

  greaterthan_to_PIVP(  OdeSystem, X, Y, T ),
  branch_to_PIVP(       OdeSystem, T, Y, X, Z ).

/*  z = max( x, y ):
*
*     z = ( x > y ) ? x : y
*/
%% max_to_PIVP(+OdeSystem:'ode system', +VariableXName:name, +VariableYName:name, +VariableZName:name) is det
max_to_PIVP( OdeSystem, X, Y, Z ) :-
  new_variable( OdeSystem, T ),

  greaterthan_to_PIVP(  OdeSystem, X, Y, T ),
  branch_to_PIVP(       OdeSystem, T, X, Y, Z ).

/*  z = log( x ):
*
*     dz      = temp0
*     dtemp0  = -temp1
*     dtemp1  = -2 * temp0 * temp1
*
*     =>
*
*     dg      = x - g
*     dz      = temp0 * dg
*     dtemp0  = - temp1 * dg
*     dtemp1  = -2 * temp0 * temp1 * dg
*
*     d( g_p - g_n          ) = ( x_p - x_n ) - ( g_p - g_n )
*                             = ( x_p + g_n ) - ( x_n + g_p )
*     d( z_p - z_n          ) = ( temp0_p - temp0_n ) * ( dg_p - dg_n )
*                             = ( temp0_p * dg_p + temp0_n * dg_n ) -
*                               ( temp0_p * dg_n + temp0_n * dg_p )
*     d( temp0_p - temp0_n  ) = -( ( temp1_p - temp1_n ) * ( dg_p - dg_n ) )
*                             = ( temp1_p * dg_n + temp1_n * dg_p ) -
*                               ( temp1_p * dg_p + temp1_n * dg_n )
*     d( temp1_p - temp1_n  ) = -2 * ( ( temp0_p - temp0_n ) * ( temp1_p - temp1_n ) * ( dg_p - dg_n ) )
*                             = 2 * ( temp0_p * ( temp1_p * dg_n + temp1_n * dg_p ) +
*                                     temp0_n * ( temp1_p * dg_p + temp1_n * dg_n ) ) -
*                               2 * ( temp0_p * ( temp1_p * dg_p + temp1_n * dg_n ) +
*                                     temp0_n * ( temp1_p * dg_n + temp1_n * dg_p ) )
*
*     dg_p      = x_p + g_n
*     dg_n      = x_n + g_p
*     dz_p      = temp0_p * dg_p + temp0_n * dg_n
*     dz_n      = temp0_p * dg_n + temp0_n * dg_p
*     dtemp0_p  = temp1_p * dg_n + temp1_n * dg_p
*     dtemp0_n  = temp1_p * dg_p + temp1_n * dg_n
*     dtemp1_p  = 2 * ( temp0_p * ( temp1_p * dg_n + temp1_n * dg_p ) +
*                       temp0_n * ( temp1_p * dg_p + temp1_n * dg_n ) )
*     dtemp1_n  = 2 * ( temp0_p * ( temp1_p * dg_p + temp1_n * dg_n ) +
*                       temp0_n * ( temp1_p * dg_n + temp1_n * dg_p ) )
*
*     g init      = x + 1 = 1
*     z init      = log( x + 1 ) = 0
*     temp0 init  = 1 / ( x + 1 ) = 1
*     temp1 init  = 1 / ( x + 1 ) ^ 2 = 1
*/
%% log_to_PIVP(+OdeSystem:'ode system', +VariableXName:name, +VariableZName:name) is det
log_to_PIVP( OdeSystem, X, Z ) :-
  init_log( OdeSystem, Z, T0, T1, G ),

  variable( [ X, Z, T0, T1, G ], [ XDiff, ZDiff, T0Diff, T1Diff, GDiff ], list ),

  log_to_ODE( OdeSystem, GDiff, XDiff, T0Diff, T1Diff, ZDiff ).

%% init_log(+OdeSystem:'ode system', +VairableZName:name, -VariableT0Name:name, -VariableT1Name:name, -VariableGName:name) is det
init_log( OdeSystem, Z, T0, T1, G ) :-
  init_variable(  OdeSystem, Z,   0 ),
  new_variable(   OdeSystem, T0,  1 ),
  new_variable(   OdeSystem, T1,  1 ),
  new_variable(   OdeSystem, G,   1 ).

%% log_to_ODE(+OdeSystem:'ode system', +VariableG:'dual var', +VariableX:'dual var', +VariableT0:'dual var', +VariableT1:'dual var', +VariableZ:'dual var' ) is det
log_to_ODE( OdeSystem, [ GP, GN ], [ XP, XN ], [ T0P, T0N ], [ T1P, T1N ], [ ZP, ZN ] ) :-
  DGP = XP + GN,
  DGN = XN + GP,

  add_ode(  OdeSystem, [ GP, GN ],    DGP,                    DGN                   ),
  add_ode(  OdeSystem, [ ZP, ZN ],    T0P * DGP + T0N * DGN,  T0P * DGN + T0N * DGP ),
  add_ode(  OdeSystem, [ T0P, T0N ],  T1P * DGN + T1N * DGP,  T1P * DGP + T1N * DGN ),
  add_ode(  OdeSystem, [ T1P, T1N ],
            2 * ( T0P * ( T1P * DGN + T1N * DGP ) + T0N * ( T1P * DGP + T1N * DGN ) ),
            2 * ( T0P * ( T1P * DGP + T1N * DGN ) + T0N * ( T1P * DGN + T1N * DGP ) ) ).

/*  z = exp( x ):
*
*     dz = z
*
*     =>
*
*     dg  = x - g
*     dz  = z * dg
*
*     d( g_p - g_n  ) = ( x_p - x_n ) - ( g_p - g_n )
*                     = ( x_p + g_n ) - ( x_n + g_p )
*     d( z_p - z_n  ) = ( z_p - z_n ) * ( dg_p - dg_n )
*                     = ( z_p * dg_p + z_n * dg_n ) -
*                       ( z_p * dg_n + z_n * dg_p )
*
*     dg_p  = x_p + g_n
*     dg_n  = x_n + g_p
*     dz_p  = z_p * dg_p + z_n * dg_n
*     dz_n  = z_p * dg_n + z_n * dg_p
*
*     g init  = x = 0
*     z init  = exp( x ) = 1
*/
%% exp_to_PIVP(+OdeSystem:'ode system', +VariableXName:name, +VariableZName:name) is det
exp_to_PIVP( OdeSystem, X, Z ) :-
  init_exp( OdeSystem, Z, G ),

  variable( [ X, Z, G ], [ XDiff, ZDiff, GDiff ], list ),

  exp_to_ODE( OdeSystem, GDiff, XDiff, ZDiff ).

%% init_exp(+OdeSystem:'ode system', +VairableZName:name, -VariableGName:name) is det
init_exp( OdeSystem, Z, G ) :-
  init_variable(  OdeSystem, Z, 1 ),
  new_variable(   OdeSystem, G, 0 ).

%% exp_to_ODE(+OdeSystem:'ode system', +VariableG:'dual var', +VariableX:'dual var', +VariableZ:'dual var' ) is det
exp_to_ODE( OdeSystem, [ GP, GN ], [ XP, XN ], [ ZP, ZN ] ) :-
  DGP = XP + GN,
  DGN = XN + GP,

  add_ode(  OdeSystem, [ GP, GN ], DGP,                 DGN                 ),
  add_ode(  OdeSystem, [ ZP, ZN ], ZP * DGP + ZN * DGN, ZP * DGN + ZN * DGP ).

/*  z = cos( x ):
*
*     dz      = temp0
*     dtemp0  = -z
*
*     =>
*
*     dg      = x - g
*     dz      = temp0 * dg
*     dtemp0  = -z * dg
*
*     d( g_p - g_n          ) = ( x_p - x_n ) - ( g_p - g_n )
*                             = ( x_p + g_n ) - ( x_n + g_p )
*     d( z_p - z_n          ) = ( temp0_p - temp0_n ) * ( dg_p - dg_n )
*                             = ( temp0_p * dg_p + temp0_n * dg_n ) -
*                               ( temp0_p * dg_n + temp0_n * dg_p )
*     d( temp0_p - temp0_n  ) = -( ( z_p - z_n ) * ( dg_p - dg_n ) )
*                             = ( z_p * dg_n + z_n * dg_p ) -
*                               ( z_p * dg_p + z_n * dg_n )
*
*     dg_p      = x_p + g_n
*     dg_n      = x_n + g_p
*     dz_p      = temp0_p * dg_p + temp0_n * dg_n
*     dz_n      = temp0_p * dg_n + temp0_n * dg_p
*     dtemp0_p  = z_p * dg_n + z_n * dg_p
*     dtemp0_n  = z_p * dg_p + z_n * dg_n
*
*     g init      = x = 0
*     z init      = cos( x ) = 1
*     temp0 init  = -sin( x ) = 0
*/
%% cos_to_PIVP(+OdeSystem:'ode system', +VariableXName:name, +VariableZName:name) is det
cos_to_PIVP( OdeSystem, X, Z ) :-
  init_cos( OdeSystem, Z, T, G ),

  variable( [ X, Z, T, G ], [ XDiff, ZDiff, TDiff, GDiff ], list ),

  cos_to_ODE( OdeSystem, GDiff, XDiff, TDiff, ZDiff ).

%% init_cos(+OdeSystem:'ode system', +VairableZName:name, -VariableTName:name, -VariableGName:name) is det
init_cos( OdeSystem, Z, T, G ) :-
  init_variable(  OdeSystem, Z, 1 ),
  new_variable(   OdeSystem, T, 0 ),
  new_variable(   OdeSystem, G, 0 ).

%% cos_to_ODE(+OdeSystem:'ode system', +VariableG:'dual var', +VariableX:'dual var', +VariableT:'dual var', +VariableZ:'dual var') is det
cos_to_ODE( OdeSystem, [ GP, GN ], [ XP, XN ], [ TP, TN ], [ ZP, ZN ] ) :-
  DGP = XP + GN,
  DGN = XN + GP,

  add_ode(  OdeSystem, [ GP, GN ], DGP,                 DGN                 ),
  add_ode(  OdeSystem, [ ZP, ZN ], TP * DGP + TN * DGN, TP * DGN + TN * DGP ),
  add_ode(  OdeSystem, [ TP, TN ], ZP * DGN + ZN * DGP, ZP * DGP + ZN * DGN ).

/*  z = sin( x ):
*
*     dz      = temp0
*     dtemp0  = -z
*
*     =>
*
*     dg      = x - g
*     dz      = temp0 * dg
*     dtemp0  = -z * dg
*
*     d( g_p - g_n          ) = ( x_p - x_n ) - ( g_p - g_n )
*                             = ( x_p + g_n ) - ( x_n + g_p )
*     d( z_p - z_n          ) = ( temp0_p - temp0_n ) * ( dg_p - dg_n )
*                             = ( temp0_p * dg_p + temp0_n * dg_n ) -
*                               ( temp0_p * dg_n + temp0_n * dg_p )
*     d( temp0_p - temp0_n  ) = -( ( z_p - z_n ) * ( dg_p - dg_n ) )
*                             = ( z_p * dg_n + z_n * dg_p ) -
*                               ( z_p * dg_p + z_n * dg_n )
*
*     dg_p      = x_p + g_n
*     dg_n      = x_n + g_p
*     dz_p      = temp0_p * dg_p + temp0_n * dg_n
*     dz_n      = temp0_p * dg_n + temp0_n * dg_p
*     dtemp0_p  = z_p * dg_n + z_n * dg_p
*     dtemp0_n  = z_p * dg_p + z_n * dg_n
*
*     g init      = x = 0
*     z init      = sin( x ) = 0
*     temp0 init  = cos( x ) = 1
*/
%% sin_to_PIVP(+OdeSystem:'ode system', +VariableXName:name, +VariableZName:name) is det
sin_to_PIVP( OdeSystem, X, Z ) :-
  init_sin( OdeSystem, Z, T, G ),

  variable( [ X, Z, T, G ], [ XDiff, ZDiff, TDiff, GDiff ], list ),

  cos_to_ODE( OdeSystem, GDiff, XDiff, TDiff, ZDiff ).

%% init_sin(+OdeSystem:'ode system', +VairableZName:name, -VariableTName:name, -VariableGName:name) is det
init_sin( OdeSystem, Z, T, G ) :-
  init_variable(  OdeSystem, Z, 0 ),
  new_variable(   OdeSystem, T, 1 ),
  new_variable(   OdeSystem, G, 0 ).

/*  z = pos_sigmoid( x ):
*
*     z = step( x )
*/
%% pos_sigmoid_to_PIVP(+OdeSystem:'ode system', +VariableXName:name, +VariableZName:name) is det
%
%   This is an interface of sigmoid function used when viewing the input as Boolean value.
%   The output is true when input is larger than a theshold value, otherwise it is false.
%   Ideally, the theshold should be half of constant 'true'.
pos_sigmoid_to_PIVP( OdeSystem, X, Z ) :-
  constant( true, True ),
  Threshold is True / 2,

  get_option( 'zero_ultra_mm', MM ),
  (
    MM == no
  ->
    new_variable( T ),

    zero_order_ma_to_PIVP( OdeSystem, Threshold, X, _, T ),
    multiplication_to_PIVP( OdeSystem, T, 0.01, Z )
  ;
    zero_order_to_PIVP( OdeSystem, Threshold, X, _, Z )
  ).

/*  z = neg_sigmoid( x ):
*
*     z = step( -x )
*/
%% neg_sigmoid_to_PIVP(+OdeSystem:'ode system', +VariableXName:name, +VariableZName:name) is det
%
%   This is an interface of sigmoid function used when viewing the input as Boolean value.
%   The output is true when input is smaller than a theshold value, otherwise it is false.
%   Ideally, the theshold should be half of constant 'true'.
neg_sigmoid_to_PIVP( OdeSystem, X, Z ) :-
  constant( true, True ),
  Threshold is True / 2,

  get_option( 'zero_ultra_mm', MM ),
  (
    MM == no
  ->
    new_variable( T ),

    zero_order_ma_to_PIVP( OdeSystem, Threshold, X, T, _, true ),
    multiplication_to_PIVP( OdeSystem, T, 0.01, Z )
  ;
    zero_order_to_PIVP( OdeSystem, Threshold, X, Z, _, true )
  ).

/*  z = pos_number_to_bool( x ):
*
*     z = step( x )
*/
%% pos_number_to_bool_to_PIVP(+OdeSytem:'ode system', +VariableXName:name, +VariableZName:name) is det
%
%   This is an interface of sigmoid function used when viewing the input as real number.
%   The output is true when input is greater than a theshold, otherwise it is false.
%   Ideally, the theshold should be close to 0.
pos_number_to_bool_to_PIVP( OdeSystem, X, Z ) :-
  get_option( 'zero_ultra_mm', MM ),
  (
    MM == no
  ->
    new_variable( T ),

    zero_order_ma_to_PIVP( OdeSystem, 0.2, X, _, T ),
    multiplication_to_PIVP( OdeSystem, T, 0.01, Z )
  ;
    zero_order_to_PIVP( OdeSystem, 0.2, X, _, Z )
  ).

%% pos_number_to_bool_to_PIVP(+OdeSytem:'ode system', +VariableXName:name, +VariableZName:name, ++Bidir ) is det
%
%   The bidirection version of pos_number_to_bool_to_PIVP/3
pos_number_to_bool_to_PIVP( OdeSystem, X, Z, bidir ) :-
  !,
  new_variable( OdeSystem, T ),

  abs_to_PIVP(                OdeSystem, X, T ),
  pos_number_to_bool_to_PIVP( OdeSystem, T, Z ).


/*  z = neg_number_to_bool( x ):
*
*     z = step( -x )
*/
%% neg_number_to_bool_to_PIVP(+OdeSytem:'ode system', +VariableXName:name, +VariableZName:name) is det
%
%   This is an interface of sigmoid function used when viewing the input as real number.
%   The output is true when input is smaller than a theshold, otherwise it is false.
%   Ideally, the theshold should be close to 0.
neg_number_to_bool_to_PIVP( OdeSystem, X, Z ) :-
  get_option( 'zero_ultra_mm', MM ),
  (
    MM == no
  ->
    new_variable( T ),

    zero_order_ma_to_PIVP( OdeSystem, 0.2, X, T, _, true ),
    multiplication_to_PIVP( OdeSystem, T, 0.01, Z )
  ;
    zero_order_to_PIVP( OdeSystem, 0.2, X, Z, _, true )
  ).

%% neg_number_to_bool_to_PIVP(+OdeSytem:'ode system', +VariableXName:name, +VariableZName:name, Bidir ) is det
%
%   The bidirection version of neg_number_to_bool_to_PIVP/3
neg_number_to_bool_to_PIVP( OdeSystem, X, Z, bidir ) :-
  !,
  new_variable( OdeSystem, T ),

  abs_to_PIVP(                OdeSystem, X, T ),
  neg_number_to_bool_to_PIVP( OdeSystem, T, Z ).


/*  hilln( x ):
*
*     h   = k * x ^ n / ( c + x ^ n )
*     nh  = k * 1 / ( c + x ^ n )
*
*     dh  = n * x ^ ( n - 1 ) * nh ^ 2 / ( c * k )
*     dnh = -n * x ^ ( n - 1 ) * nh ^ 2 / ( c * k )
*         = -dh
*
*     =>
*
*     g = x + ( x_0 - x ) * e ^ -t
*
*     dg  = x - g
*     dh  = n * g ^ ( n - 1 ) * nh ^ 2 * ( x - g ) / ( c * k )
*     dnh = -dh
*
*     g init  = x = 0
*     h init  = 0
*     nh init = k
*
*     k = true_value
*     c = 0.1
*     n = 5
*/
%% hill_to_PIVP(+OdeSystem:'ode system', +VariableXName:name, ?VariableHName:name, ?VariableNHName:name) is det
hill_to_PIVP( OdeSystem, X, H, NH ) :- % dual
  variable( X,  [ XP,   _ ] ),
  !,
  get_variable_positive( H,   HP  ),
  get_variable_positive( NH,  NHP ),

  hill_to_PIVP( OdeSystem, XP, HP, NHP ).

hill_to_PIVP( OdeSystem, X, H, NH ) :- % non dual
  !,
  constant( true, Scale ),
  init_hill( Scale, H, NH, G ),
  hill_to_ODE( OdeSystem, Scale, G, X, H, NH ).

%% init_hill(+Scale:number, ?VariableHName:name, ?VariableNHName:name, -VariableGName:name) is det
init_hill( Scale, H, NH, G ) :-
  new_symbol( H,  0     ),
  new_symbol( NH, Scale ),
  new_symbol( G,  0     ).

%% hill_to_ODE(+OdeSystem:'ode system', +K:number, +VariableGName:name, +VariableXName:name, +VariableHName:name, +VariableNHName:name ) is det
hill_to_ODE( OdeSystem, K, G, X, H, NH ) :-
  C = 0.1,
  N = 5,

  add_ode( OdeSystem, d( G ) / dt   = X - G ),
  add_ode( OdeSystem, d( H ) / dt   = N / ( C * K ) * ( G ^ ( N - 1 ) * NH ^ 2 * ( X - G ) ) ),
  add_ode( OdeSystem, d( NH ) / dt  = N / ( C * K ) * ( G ^ ( N - 1 ) * NH ^ 2 * ( G - X ) ) ).

/*  k * tanh( c * ( x - shift ) ):
*
*     k > 0
*
*     tanh   = k * tanh( c * x )
*     dtanh  = c / k * ( k ^ 2 - tanh ^ 2 )
*
*     =>
*
*     g = x + ( x_0 - x ) * e ^ -t
*
*     dg    = x - shift - g
*     dtanh = c / k * ( k ^ 2 - tanh ^ 2 ) * dg
*
*     d( g_p - g_n )        = ( x_p - x_n ) - ( shift_p - shift_n ) - ( g_p - g_n )
*                           = ( x_p + g_n + shift_n ) - ( x_n + g_p + shift_p )
*     d( tanh_p - tanh_n )  = c / k * ( k ^ 2 - ( tanh_p - tanh_n ) ^ 2 / k ) * ( dg_p - dg_n )
*                           = c / k * ( ( k ^ 2 + 2 * tanh_p * tanh_n ) * dg_p + ( tanh_p ^ 2 + tanh_n ^ 2 ) * dg_n ) -
*                             c / k * ( ( k ^ 2 + 2 * tanh_p * tanh_n ) * dg_n + ( tanh_p ^ 2 + tanh_n ^ 2 ) * dg_p )
*
*     dtanh  = -c / k * ( k ^ 2 - tanh ^ 2 )
*
*     =>
*
*     g = x + ( x_0 - x ) * e ^ -t
*
*     dg    = x - shift - g
*     dtanh = c / k * ( k ^ 2 - tanh ^ 2 ) * dg
*
*     d( g_p - g_n )        = ( x_p - x_n ) - ( shift_p - shift_n ) - ( g_p - g_n )
*                           = ( x_p + g_n + shift_n ) - ( x_n + g_p + shift_p )
*     d( tanh_p - tanh_n )  = c / k * ( k ^ 2 - ( tanh_p - tanh_n ) ^ 2 / k ) * ( dg_p - dg_n )
*                           = c / k * ( ( k ^ 2 + 2 * tanh_p * tanh_n ) * dg_n + ( tanh_p ^ 2 + tanh_n ^ 2 ) * dg_p ) -
*                             c / k * ( ( k ^ 2 + 2 * tanh_p * tanh_n ) * dg_p + ( tanh_p ^ 2 + tanh_n ^ 2 ) * dg_n )
*
*     g init    = 0
*     tanh init = k * tanh( c * 0 ) = 0
*
*     shift = 0.8
*     k     = true_value / 2
*     c     = 6
*/
%% general_tanh_to_PIVP(+OdeSystem:'ode system', +K:'non-negative number', +C:number, +Shift:number, +VariableXName:name, +VariableTanhName:name) is det
general_tanh_to_PIVP( OdeSystem, K, C, Shift, X, Tanh ) :-
  init_general_tanh( OdeSystem, Tanh, G ),

  variable( [ Shift, X, G, Tanh ], [ ShiftDiff, XDiff, GDiff, TanhDiff ], list ),

  general_tanh_to_ODE( OdeSystem, ShiftDiff, GDiff, XDiff, DGDiff ),
  (
    C >= 0
  ->
    pos_tanh_to_ODE( OdeSystem, K, C, DGDiff, TanhDiff )
  ;
    CT is -C,
    neg_tanh_to_ODE( OdeSystem, K, CT, DGDiff, TanhDiff )
  ).

%% init_general_tanh(+OdeSystem:'ode system', +VariableTanhName:name, -VariableGName:name) is det
init_general_tanh( OdeSystem, Tanh, G ) :-
  new_variable( OdeSystem, Tanh,  0 ),
  new_variable( OdeSystem, G,     0 ).

%% general_tanh_to_ODE(+OdeSystem:'ode system', +Shift:number, +GDiff:'dual var', +XDiff:'dual var', -DGDiff:'dual var') is det
general_tanh_to_ODE( OdeSystem, [ ShiftP, ShiftN ], [ GP, GN ], [ XP, XN ], [ DGP, DGN ] ) :-
  DGP = XP + GN + ShiftN,
  DGN = XN + GP + ShiftP,

  add_ode(  OdeSystem, [ GP, GN ], DGP, DGN ).

%% pos_tanh_to_PIVP(+OdeSystem:'ode system', +VariableXName:name, +VariableTanhName:name) is det
pos_tanh_to_PIVP( OdeSystem, X, Tanh ) :-
  constant( true, True ),
  K     = True / 2,
  C     = 6,
  Shift = 0.8,

  general_tanh_to_PIVP( OdeSystem, K, C, Shift, X, Tanh ).

%% pos_tanh_to_ODE(+OdeSystem:'ode system', +K:'non-negative number', +C:'non-negative number', +DGDiff:'dual var', +TanhDiff:'dual var') is det
pos_tanh_to_ODE( OdeSystem, K, C, [ DGP, DGN ], [ TanhP, TanhN ] ) :-
  add_ode(  OdeSystem, [ TanhP, TanhN ],
            C / K * ( ( K ^ 2 + 2 * TanhP * TanhN ) * DGP + ( TanhP ^ 2 + TanhN ^ 2 ) * DGN ),
            C / K * ( ( K ^ 2 + 2 * TanhP * TanhN ) * DGN + ( TanhP ^ 2 + TanhN ^ 2 ) * DGP ) ).

%% neg_tanh_to_PIVP(+OdeSystem:'ode system', +VariableXName:name, +VariableTanhName:name) is det
neg_tanh_to_PIVP( OdeSystem, X, Tanh ) :-
  constant( true, True ),
  K     = True / 2,
  C     = -6,
  Shift = 0.8,

  general_tanh_to_PIVP( OdeSystem, K, C, Shift, X, Tanh ).

%% neg_tanh_to_ODE(+OdeSystem:'ode system', +K:'non-negative number', +C:'non-negative number', +DGDiff:'dual var', +TanhDiff:'dual var') is det
neg_tanh_to_ODE( OdeSystem, K, C, [ DGP, DGN ], [ TanhP, TanhN ] ) :-
  add_ode(  OdeSystem, [ TanhP, TanhN ],
            C / K * ( ( K ^ 2 + 2 * TanhP * TanhN ) * DGN + ( TanhP ^ 2 + TanhN ^ 2 ) * DGP ),
            C / K * ( ( K ^ 2 + 2 * TanhP * TanhN ) * DGP + ( TanhP ^ 2 + TanhN ^ 2 ) * DGN ) ).

/*  step( x ):
*
*     step = k / 2 * ( 1 + tanh( c * ( x - shift ) ) )
*
*     dstep = k * c / 2 * ( 1 - tanh ^ 2 )
*     dtanh = c * ( 1 - tanh ^ 2 )
*
*     =>
*
*     g = x + ( x_0 - x ) * e ^ -t
*
*     dg    = x - shift - g
*     dstep = k * c / 2 * ( 1 - tanh ^ 2 ) * ( x - shift - g )
*     dtanh = c * ( 1 - tanh ^ 2 ) * ( x - shift - g )
*
*     d( g_p - g_n )        = ( x_p - x_n ) - ( shift_p - shift_n ) - ( g_p - g_n )
*                           = ( x_p + shift_n + g_n ) - ( x_n + shift_p + g_p )
*     d( step_p - step_n )  = k * c / 2 * ( 1 + 2 * tanh_p * tanh_n - tanh_p ^ 2 - tanh_n ^ 2 ) * ( dg_p - dg_n )
*                           = k * c / 2 * ( ( 1 + 2 * tanh_p * tanh_n ) * dg_p + ( tanh_p ^ 2 + tanh_n ^ 2 ) * dg_n ) -
*                             k * c / 2 * ( ( 1 + 2 * tanh_p * tanh_n ) * dg_n + ( tanh_p ^ 2 + tanh_n ^ 2 ) * dg_p )
*     d( tanh_p - tanh_n )  = c * ( 1 + 2 * tanh_p * tanh_n - tanh_p ^ 2 - tanh_n ^ 2 ) * ( dg_p - dg_n )
*                           = c * ( ( 1 + 2 * tanh_p * tanh_n ) * dg_p + ( tanh_p ^ 2 + tanh_n ^ 2 ) * dg_n ) -
*                             c * ( ( 1 + 2 * tanh_p * tanh_n ) * dg_n + ( tanh_p ^ 2 + tanh_n ^ 2 ) * dg_p )
*
*     g init    = x = 0
*     step init = k / 2 * ( 1 + tanh( c * -shift ) )
*     tanh init = tanh( c * -shift )
*
*     shift = 0.8
*     k     = true_value
*     c     = 6
*/
%% general_step_to_PIVP(+OdeSystem:'ode system', +K:'non-negative number', +C:number, +Shift:number, +VariableXName:name, +VariableStepName:name) is det
general_step_to_PIVP( OdeSystem, K, C, Shift, X, Step ) :-
  !,
  init_general_step( OdeSystem, K, Step, Tanh, G ),

  variable( [ Shift, X, G, Tanh, Step ], [ ShiftDiff, XDiff, GDiff, TanhDiff, StepDiff ], list ),

  general_step_to_ODE( OdeSystem, K, C, ShiftDiff, XDiff, GDiff, TanhDiff, StepDiff ).

%% init_pos_step(+OdeSystem:'ode system', +K:'non-negative number', ?VariableStepName:name, ?VariableTanhName:name, ?VariableGName:name) is det
init_general_step( OdeSystem, K, Step, Tanh, G ) :-
  StepInit is K / 2,

  new_variable( OdeSystem, Step,  StepInit  ),
  new_variable( OdeSystem, Tanh,  0         ),
  new_variable( OdeSystem, G,     0         ).

%% general_step_to_ODE(+OdeSystem:'ode system', +K:'non-negative number', +C:number, +ShiftDiff:'dual var', +XDiff:'dual var', +GDiff:'dual var', +TanhDiff:'dual var', +StepDiff:'dual var') is det
general_step_to_ODE( OdeSystem, K, C, [ ShiftP, ShiftN ], [ XP, XN ], [ GP, GN ], [ TanhP, TanhN ], [ StepP, StepN ] ) :-
  DGP         = XP + ShiftN + GN,
  DGN         = XN + ShiftP + GP,
  TanhTermP   = 1 + 2 * TanhP * TanhN,
  TanhTermN   = TanhP ^ 2 + TanhN ^ 2,
  DTanhTermP  = TanhTermP * DGP + TanhTermN * DGN,
  DTanhTermN  = TanhTermP * DGN + TanhTermN * DGP,
  (
    C >= 0
  ->
    DTanhP = C * DTanhTermP,
    DTanhN = C * DTanhTermN
  ;
    CN is -C,
    DTanhP = CN * DTanhTermN,
    DTanhN = CN * DTanhTermP
  ),
  (
    K >= 0
  ->
    DStepP = K / 2 * DTanhP,
    DStepN = K / 2 * DTanhN
  ;
    KN is -K,
    DStepP = KN / 2 * DTanhN,
    DStepN = KN / 2 * DTanhP
  ),

  add_ode( OdeSystem, [ GP,     GN    ], DGP,     DGN     ),
  add_ode( OdeSystem, [ TanhP,  TanhN ], DTanhP,  DTanhN  ),
  add_ode( OdeSystem, [ StepP,  StepN ], DStepP,  DStepN  ).

%% pos_step_to_PIVP(+OdeSystem:'ode system', +VariableXName:name, +VariableStepName:name) is det
pos_step_to_PIVP( OdeSystem, X, Step ) :-
  C     = 6,
  Shift = 0.8,
  constant( true, Scale ),

  general_step_to_PIVP( OdeSystem, Scale, C, Shift, X, Step ).

%% neg_step_to_PIVP(+OdeSystem:'ode system', +VariableXName:name, +VariableStepName:name) is det
neg_step_to_PIVP( OdeSystem, X, Step ) :-
  C     = -6,
  Shift = 0.8,
  constant( true, Scale ),

  general_step_to_PIVP( OdeSystem, Scale, C, Shift, X, Step ).

% end functions

% conditional operations
/*  z = x == y :
*
*     z = neg_number_to_bool_bidir( x - y )
*/
%% equal_to_PIVP(+OdeSystem:'ode system', +VariableXName:name, +VariableYName:name, +VariableZName:name) is det
equal_to_PIVP( OdeSystem, X, Y, Z ) :-
  new_variable( OdeSystem, T ),

  substraction_to_PIVP(       OdeSystem, X, Y,  T ),
  neg_number_to_bool_to_PIVP( OdeSystem, T, Z, bidir ).

/*  z = x < y :
*
*     z = pos_number_to_bool( y - x )
*/
%% lessthan_to_PIVP(+OdeSystem:'ode system', +VariableXName:name, +VariableYName:name, +VariableZName:name) is det
lessthan_to_PIVP( OdeSystem, X, Y, Z ) :-
  new_variable( OdeSystem, T ),

  substraction_to_PIVP(       OdeSystem, Y, X, T ),
  pos_number_to_bool_to_PIVP( OdeSystem, T, Z ).

/*  z = x <= y :
*
*     z = neg_number_to_bool( x - y )
*/
%% lessequal_to_PIVP(+OdeSystem:'ode system', +VariableXName:name, +VariableYName:name, +VariableZName:name) is det
lessequal_to_PIVP( OdeSystem, X, Y, Z ) :-
  new_variable( OdeSystem, T ),

  substraction_to_PIVP(       OdeSystem, X, Y, T ),
  neg_number_to_bool_to_PIVP( OdeSystem, T, Z ).

/*  z = x > y :
*
*     z = y < x
*/
%% greaterthan_to_PIVP(+OdeSystem:'ode system', +VariableXName:name, +VariableYName:name, +VariableZName:name) is det
greaterthan_to_PIVP( OdeSystem, X, Y, Z ) :-
  lessthan_to_PIVP( OdeSystem, Y, X, Z ).

/*  z = x >= y :
*
*     z = y <= x
*/
%% greaterequal_to_PIVP(+OdeSystem:'ode system', +VariableXName:name, +VariableYName:name, +VariableZName:name) is det
greaterequal_to_PIVP( OdeSystem, X, Y, Z ) :-
  lessequal_to_PIVP( OdeSystem, Y, X, Z ).

/*  z = x or y :
*
*     x and y should be the result of condition
*
*     z = pos_sigmoid( x + y )
*/
%% or_to_PIVP(+OdeSystem:'ode system', +VariableXName:name, +VariableYName:name, +VariableZName:name) is det
or_to_PIVP( OdeSystem, X, Y, Z ) :-
  new_variable( OdeSystem, T ),

  addition_to_PIVP(     OdeSystem, X, Y, T, pos ),
  pos_sigmoid_to_PIVP(  OdeSystem, T, Z ).

/*  z = x and y :
*
*     x and y should be the result of condition
*
*     z = pos_sigmoid( x * y )
*/
%% and_to_PIVP(+OdeSystem:'ode system', +VariableXName:name, +VariableYName:name, +VariableZName:name) is det
and_to_PIVP( OdeSystem, X, Y, Z ) :-
  new_variable( OdeSystem, T ),

  multiplication_to_PIVP( OdeSystem, X, Y, T, pos ),
  pos_sigmoid_to_PIVP(    OdeSystem, T, Z ).

/*  z = not x :
*
*     x should be the result of a condition
*
*     z = neg_sigmoid( x )
*/
%% not_to_PIVP(+OdeSystem:'ode system', +VariableXName:name, +VariableZName:name) is det
not_to_PIVP( OdeSystem, X, Z ) :-
  not_to_PIVP( OdeSystem, X, Z, sim ).
  %neg_sigmoid_to_PIVP( OdeSystem, X, Z ).

not_to_PIVP( OdeSystem, X, Z, sim ) :-
  get_symbol_attr( X, [ 'not': Not ] ),
  (
    constant( notNull, Not )
  ->
    neg_sigmoid_to_PIVP( OdeSystem, X, Z )
  ;
    assign_to_PIVP( OdeSystem, Not, Z )
  ).

% end conditional operations

% switch
/*
*   this implement the "Approximately Majority" bio switch.
*
*       sx
*       |
*   +--+--+
*   |  |  |
*   |<-- <--
*   x   b   y
*    --> -->|
*     |  |  |
*     +--+--+
*       |
*       sy
*
*   sx: switch to x
*   sy: switch to y
*
*   the switch can be viewed as two states.
*
*   - 'x' state: the switch is truned off
*   - 'y' state: the switch is turned on.
*
*   intially, the state of the switch is 'x'.
*   the value of state will change the threshold of the input to switch the state.
*   the larger the state value, the larger threshold, which means larger noise tolerance.
*/
%% switch_to_reaction(+OdeSystem:'ode system', +VariableSXName:name, +VariableSYName:name, +VariableXName:name, +VariableYName:name) is det
%
%   The compilation of 'approxmate majority' bio switch.
%   The internal implementation directly add the reactions, not PIVP.
switch_to_PIVP( OdeSystem, SX, SY, X, Y ) :-
  switch_to_PIVP( OdeSystem, SX, SY, X, Y, [] ).

%% switch_to_reaction(+OdeSystem:'ode system', +VariableSXName:name, +VariableSYName:name, +VariableXName:name, +VariableYName:name, +Options:option_list ) is det
%
%   Provided options:
%
%   * x_init(Boolean)
%
%     Indicates whether _VariableXName_ should be initializaed.
%     This is used when cascading switchs.
%
%   * y_init(Boolean)
%
%     Indicates whether _VariableYName_ should be initializaed.
%     This is used when cascading switchs.
switch_to_PIVP( OdeSystem, SX, SY, X, Y, Options ) :- % dual
  variable( [ SX, SY ], [ [ SXP, _ ], [ SYP, _ ] ], list ),
  !,
  get_variable_positive( X, XP ),
  get_variable_positive( Y, YP ),

  switch_to_PIVP( OdeSystem, SXP, SYP, XP, YP, Options ).

switch_to_PIVP( _, SX, SY, X, Y, Options ) :- % non dual
  !,
  init_switch( X, Y, B, Options ),

  switch_core_to_reaction( X, Y, B ),

  (
    constant( switchNull, SY )
  ->
    true
  ;
    switch_control_to_reaction( SY, X, Y, B )
  ),
  (
    constant( switchNull, SX )
  ->
    true
  ;
    switch_control_to_reaction( SX, Y, X, B )
  ),
  get_variable_symbol( X, SymbolX ),
  get_variable_symbol( Y, SymbolY ),
  change_symbol_attr( SymbolX, [ 'not': SymbolY ] ),
  change_symbol_attr( SymbolY, [ 'not': SymbolX ] ).

%% init_switch(+VariableXName:name, +VariableYName:name, +VariableBName:name, +Options:option_list ) is det
%
%   Provided options:
%
%   * x_init(Boolean)
%
%     Indicates whether _VariableXName_ should be initializaed.
%     This is used when cascading switchs.
%
%   * y_init(Boolean)
%
%     Indicates whether _VariableYName_ should be initializaed.
%     This is used when cascading switchs.
init_switch( X, Y, B, Options ) :-
  (
    option( x_init( XInit ), Options )
  ->
    true
  ;
    constant( true, XInit )
  ),
  (
    option( y_init( YInit ), Options )
  ->
    true
  ;
    constant( false, YInit )
  ),

  new_symbol( B, 0 ),
  change_symbol_attr( X, [ init: XInit ] ),
  change_symbol_attr( Y, [ init: YInit ] ).

%% switch_core_to_reaction(+VariableXName:name, +VariableYName:name, +VairableBName:name) is det
%
%   The connection of switch core.
%   _VariableBName_ is the intermediate species,
%   and _VaraibleXName_, _VariableYName_ are the two states of switch.
switch_core_to_reaction( X, Y, B ) :-
  switch_control_to_reaction( Y, X, Y, B ),
  switch_control_to_reaction( X, Y, X, B ).

%% switch_control_to_reaction(+VariableSYName:name, +VariableXName:name, +VariableYName:name, +VairableBName:name)
%
%   _VariableSYName_ controls the state switching from _VariableXName to _VariableYName.
%   _VariableBName_ is the intermediate species of switch.
switch_control_to_reaction( SY, X, Y, B ) :-
  add_reaction( X =[ SY ]=> B ),
  add_reaction( B =[ SY ]=> Y ).
% end switch

% zero-order ultrasensitivity
/*
 *  This implement the zero order ultrasensitivity and
 *  using Goldbeter-Koshland kinetics.
 *  The mechenism is similar to the CMOS logic gate.
 *
 *     sx
 *     |
 *   <--
 *  x   y
 *   -->
 *   |
 *   sy
 *
 *   sx: switch to x
 *   sy: switch to y
 *
 *  Goldbeter-Koshland kinetics:
 *
 *    z   = x + y
 *    v1  = k_cat_sy * sy
 *    v2  = k_cat_sx * sx
 *    J1  = Km_sy / z
 *    J2  = Km_sx / z
 *    B   = v2 - v1 + J1 * v2 + J2 * v1
 *
 *    y = 2 * v1 * J2 / ( B + sqrt( B ^ 2 - 4 * ( v2 - v1 ) * v1 * J2 ) )
 *
 *  v2 control the threshold of the sigmoid function
 *  J1, J2 control the quality of sigmoid function( smaller J => steeper sigmoid )
 *
 *  current parameter:
 *
 *  z   = true
 *  v1  = sy
 *  v2  = threshold
 *  J1  = 0.001
 *  J2  = 0.001
 */
%% zero_order_to_PIVP(+OdeSystem:'ode system', +Thrshold:'non-negative number', +VariableSYName:name, +VariableXName:name, +VarialbeYName:name) is det
zero_order_to_PIVP( OdeSystem, Threshold, SY, X, Y ) :- % dual
  variable( SY, [ SYP, _ ] ),
  !,
  get_variable_positive( X, XP ),
  get_variable_positive( Y, YP ),

  zero_order_to_PIVP( OdeSystem, Threshold, SYP, XP, YP, true, true, false ).

%% zero_order_to_PIVP(+OdeSystem:'ode system', +Thrshold:'non-negative number', +VariableSYName:name, +VariableXName:name, +VarialbeYName:name, +Inverse:bool ) is det
%
%   The _Inverse_ flag triggers the initialization of _VariableXName_ being true rather than initializing _VariableYName_ to true.
%   This flag should be implemented by option list.
zero_order_to_PIVP( OdeSystem, Threshold, SY, X, Y, Inverse ) :- % dual
  variable( SY, [ SYP, _ ] ),
  !,
  get_variable_positive( X, XP ),
  get_variable_positive( Y, YP ),

  zero_order_to_PIVP( OdeSystem, Threshold, SYP, XP, YP, true, true, Inverse ).

%% zero_order_to_PIVP(+OdeSystem:'ode system', +Thrshold:'non-negative number', +VariableSYName:name, +VariableXName:name, +VarialbeYName:name, +InitX:bool, +InitY:bool, +Inverse:bool ) is det
%
%   The _InitX_ and _InitY_ trigger the initialization of _VariableXName_ and _VariableYName_.
%   This is used for cascading.
%   The _Inverse_ flag triggers the initialization of _VariableXName_ being true rather than initializing _VariableYName_ to true.
%   The flags should be implemented by option list.
zero_order_to_PIVP( OdeSystem, Threshold, SY, X, Y, InitX, InitY, Inverse ) :- % dual
  variable( SY, [ SYP, _ ] ),
  !,
  get_variable_positive( X, XP ),
  get_variable_positive( Y, YP ),

  zero_order_to_PIVP( OdeSystem, Threshold, SYP, XP, YP, InitX, InitY, Inverse ).

zero_order_to_PIVP( OdeSystem, Threshold, SY, X, Y, InitX, InitY, Inverse ) :- % non dual
  !,
  init_zero_order( OdeSystem, Threshold, X, Y, SX, KcatSX, KcatSY, KmSX, KmSY, InitX, InitY, Inverse ),

  gk_to_reaction( KcatSX, KmSX, KcatSY, KmSY, SX, SY, X, Y ),
  get_variable_symbol( X, SymbolX ),
  get_variable_symbol( Y, SymbolY ),
  change_symbol_attr( SymbolX, [ 'not': SymbolY ] ),
  change_symbol_attr( SymbolY, [ 'not': SymbolX ] ).

%% init_zero_order(+OdeSystem:'ode system', +Threshold:'non-negative number', +VariableXName:name, +VariableYName:name, -VariableSXName:name, -KcatSX:'non-negative number', -KcatSY:'non-negative number', -KmSX:'non-negative number', -KmSY:'non-negative number', +InitX:bool, +InitY:bool, +Inverse:bool ) is det
init_zero_order( _, Threshold, X, Y, SX, KcatSX, KcatSY, KmSX, KmSY, InitX, InitY, Inverse ) :-
  constant( true,   True  ),
  constant( false,  False ),
  KcatSX  is 1,
  KcatSY  is 1,
  KmSX    is 0.001 * True,
  KmSY    is 0.001 * True,
  SXInit  is Threshold / KcatSX,

  new_symbol( SX, SXInit ),
  (
    Inverse == true
  ->
    XInit = False,
    YInit = True
  ;
    XInit = True,
    YInit = False
  ),
  (
    InitY == true
  ->
    change_symbol_attr( Y, [ init: YInit ] )
  ;
    true
  ),
  (
    InitX == true
  ->
    change_symbol_attr( X, [ init: XInit ] )
  ;
    true
  ).

%% gk_to_reaction(+KcatSX:'non-negative number', +KmSX:'non-negative number', +KcatSY:'non-negative number', +KmSY:'non-negative number', +VariableSXName:name, +VariableSYName:name, +VariableXName:name, +VariableYName:name) is det
gk_to_reaction( KcatSX, KmSX, KcatSY, KmSY, SX, SY, X, Y ) :-
  mm_to_reaction( KcatSX, KmSX, SX, Y, X ),
  mm_to_reaction( KcatSY, KmSY, SY, X, Y ).

%% mm_to_reaction( +Kcat:'non-negative number', +Km:'non-negative number', +VariableSYName:name, +VariableXName:name, +VariableYName:name) is det
mm_to_reaction( Kcat, Km, SY, X, Y ) :-
  MM = ( Kcat * [SY] * [X] / ( Km + [X] ) ),
  add_reaction( MM for X =[ SY ]=> Y ).

% zero-order ultrasensitivity ma
/*
 *  This implement the zero order ultrasensitivity and
 *  using Goldbeter-Koshland kinetics.
 *  The mechenism is similar to the CMOS logic gate.
 *
 *     sx
 *     |
 *   <--
 *  x   y
 *   -->
 *   |
 *   sy
 *
 *   sx: switch to x
 *   sy: switch to y
 *
 *  Goldbeter-Koshland kinetics:
 *
 *    z   = x + y
 *    v1  = k_cat_sy * sy
 *    v2  = k_cat_sx * sx
 *    J1  = Km_sy / z
 *    J2  = Km_sx / z
 *    B   = v2 - v1 + J1 * v2 + J2 * v1
 *
 *    y = 2 * v1 * J2 / ( B + sqrt( B ^ 2 - 4 * ( v2 - v1 ) * v1 * J2 ) )
 *
 *  v2 control the threshold of the sigmoid function
 *  J1, J2 control the quality of sigmoid function( smaller J => steeper sigmoid )
 *
 *  current parameter:
 *
 *  z   = true
 *  v1  = sy
 *  v2  = threshold
 *  J1  = 0.001
 *  J2  = 0.001
 */
%% zero_order_ma_to_PIVP(+OdeSystem:'ode system', +Thrshold:'non-negative number', +VariableSYName:name, +VariableXName:name, +VarialbeYName:name) is det
zero_order_ma_to_PIVP( OdeSystem, Threshold, SY, X, Y ) :- % dual
  variable( SY, [ SYP, _ ] ),
  !,
  get_variable_positive( X, XP ),
  get_variable_positive( Y, YP ),

  zero_order_ma_to_PIVP( OdeSystem, Threshold, SYP, XP, YP, true, true, false ).

%% zero_order_ma_to_PIVP(+OdeSystem:'ode system', +Thrshold:'non-negative number', +VariableSYName:name, +VariableXName:name, +VarialbeYName:name, +Inverse:bool ) is det
%
%   The _Inverse_ flag triggers the initialization of _VariableXName_ being true rather than initializing _VariableYName_ to true.
%   This flag should be implemented by option list.
zero_order_ma_to_PIVP( OdeSystem, Threshold, SY, X, Y, Inverse ) :- % dual
  variable( SY, [ SYP, _ ] ),
  !,
  get_variable_positive( X, XP ),
  get_variable_positive( Y, YP ),

  zero_order_ma_to_PIVP( OdeSystem, Threshold, SYP, XP, YP, true, true, Inverse ).

%% zero_order_ma_to_PIVP(+OdeSystem:'ode system', +Thrshold:'non-negative number', +VariableSYName:name, +VariableXName:name, +VarialbeYName:name, +InitX:bool, +InitY:bool, +Inverse:bool ) is det
%
%   The _InitX_ and _InitY_ trigger the initialization of _VariableXName_ and _VariableYName_.
%   This is used for cascading.
%   The _Inverse_ flag triggers the initialization of _VariableXName_ being true rather than initializing _VariableYName_ to true.
%   The flags should be implemented by option list.
zero_order_ma_to_PIVP( OdeSystem, Threshold, SY, X, Y, InitX, InitY, Inverse ) :- % dual
  variable( SY, [ SYP, _ ] ),
  !,
  get_variable_positive( X, XP ),
  get_variable_positive( Y, YP ),

  zero_order_ma_to_PIVP( OdeSystem, Threshold, SYP, XP, YP, InitX, InitY, Inverse ).

zero_order_ma_to_PIVP( OdeSystem, Threshold, SY, X, Y, InitX, InitY, Inverse ) :- % non dual
  !,
  init_zero_order_ma( OdeSystem, Threshold, X, Y, SX, KcatSX, KcatSY, KmSX, KmSY, InitX, InitY, Inverse ),

  gk_to_reaction( OdeSystem, KcatSX, KmSX, KcatSY, KmSY, SX, SY, X, Y ),
  get_variable_symbol( X, SymbolX ),
  get_variable_symbol( Y, SymbolY ),
  change_symbol_attr( SymbolX, [ 'not': SymbolY ] ),
  change_symbol_attr( SymbolY, [ 'not': SymbolX ] ).

%% init_zero_order_ma(+OdeSystem:'ode system', +Threshold:'non-negative number', +VariableXName:name, +VariableYName:name, -VariableSXName:name, -KcatSX:'non-negative number', -KcatSY:'non-negative number', -KmSX:'non-negative number', -KmSY:'non-negative number', +InitX:bool, +InitY:bool, +Inverse:bool ) is det
init_zero_order_ma( _, Threshold, X, Y, SX, KcatSX, KcatSY, KmSX, KmSY, InitX, InitY, Inverse ) :-
  constant( true,   True  ),
  constant( false,  False ),
  KcatSX  is 1,
  KcatSY  is 1,
  KmSX    is 0.001 * True,
  KmSY    is 0.001 * True,
  SXInit  is Threshold / KcatSX,

  new_symbol( SX, SXInit ),
  (
    Inverse == true
  ->
    XInit = False,
    YInit is True * 100
  ;
    XInit is True * 100,
    YInit = False
  ),
  (
    InitY == true
  ->
    change_symbol_attr( Y, [ init: YInit ] )
  ;
    true
  ),
  (
    InitX == true
  ->
    change_symbol_attr( X, [ init: XInit ] )
  ;
    true
  ).

%% gk_to_reaction(+OdeSystem:'ode system',+KcatSX:'non-negative number', +KmSX:'non-negative number', +KcatSY:'non-negative number', +KmSY:'non-negative number', +VariableSXName:name, +VariableSYName:name, +VariableXName:name, +VariableYName:name) is det
gk_to_reaction( OdeSystem, KcatSX, KmSX, KcatSY, KmSY, SX, SY, X, Y ) :-
  KfSY is 1 / KmSY,
  KfSX is 1 / KmSX,
  KrSY is 1,
  KrSX is 1,
  new_symbol( IY ),
  new_symbol( IX ),
  new_symbol( SYT ),
  new_symbol( SXT ),
  add_ode( OdeSystem, d( SYT ) / dt = SY - SYT ),
  add_ode( OdeSystem, d( SXT ) / dt = SX - SXT ),
  add_reaction( 'MA'(KfSY)    for X + SYT => IY ),
  add_reaction( 'MA'(KrSY)    for IY => SYT + X ),
  add_reaction( 'MA'(KcatSY)  for IY => SYT + Y ),
  add_reaction( 'MA'(KfSX)    for Y + SXT => IX ),
  add_reaction( 'MA'(KrSX)    for IX => SXT + Y ),
  add_reaction( 'MA'(KcatSX)  for IX => SXT + X ),
  add_reaction( IX + IY => X + Y ).

% control flow
:- discontiguous branch_to_PIVP/5.
/*  z = pre x post:
*
*     the evaluation of 'z' and 'eval_post' only happen between 'pre' and 'post_pre'.
*     the postcondition success only if the evaluation stop.
*
*     z         = ( pre ) ? x : z
*     eval_post = ( z == x )
*
*     there is one bio switches:
*
*     post_pre
*
*       - turn on state: post_pre
*         - turn on by 'eval_post'
*       - turn off state: post_pre_not
*         - turn off by 'post_pre_next'
*
*     post = post_pre && !pre
*
*     the 'post_pre_next' is the 'post_pre' of next statement.
*/
%% inter_to_PIVP(+OdeSystem:'ode system', +VariableXName:name, +VariableZName:name, +VariablePreName:name, VariablePrePostNextName:name, +VairablePostName:name) is det
%
%   The compiltion of 'rhs' being 'pre <expression> post'.
%   This is a core compilation unit for sequentiality.
inter_to_PIVP( OdeSystem, X, Z, Pre, PostPre, PostPreNext, Post ) :-
  branch_to_PIVP(         OdeSystem, Pre, X,        Z,    Z ),
  inter_post_pre_to_PIVP( OdeSystem, X,   Z,        Pre,  PostPreNext, PostPre ),
  inter_post_to_PIVP(     OdeSystem, Pre, PostPre,  Post ).

%% inter_post_to_PIVP(+OdeSystem:'ode system', +VariableXName:name, VariableZName:name, +VariablePreName:name, +VariablePostPreNextName:name, +VariablePostPreName:name) is det
inter_post_pre_to_PIVP( OdeSystem, X, Z, Pre, PostPreNext, PostPre ) :-
  new_variable( OdeSystem, [ Equal, EvalPost, PostPreNot ] ),

  equal_to_PIVP(  OdeSystem, X,           Z,        Equal     ),
  and_to_PIVP(    OdeSystem, Equal,       Pre,      EvalPost  ),
  switch_to_PIVP( OdeSystem, PostPreNext, EvalPost, PostPreNot, PostPre ).

%% inter_post_to_PIVP(+OdeSystem:'ode system', +VariablePreName:name, +VariablePostPreName:name, +VariablePostName:name) is det
inter_post_to_PIVP( OdeSystem, Pre, PostPre, Post ) :-
  new_variable( OdeSystem, PreNot ),

  not_to_PIVP( OdeSystem, Pre,    PreNot        ),
  and_to_PIVP( OdeSystem, PreNot, PostPre, Post ).

/*  z = pre x :
*
*     z = ( pre ) ? x : z
*/
%% pre_to_PIVP(+OdeSystem:'ode system', +VariableEName:name, +VariableZName:name, +VariablePreName:name) is det
%
%   The compiltion of 'rhs' being 'pre <expression>'.
%   This is a core compilation unit for sequentiality.
pre_to_PIVP( OdeSystem, X, Z, Pre ) :-
  branch_to_PIVP( OdeSystem, Pre, X, Z, Z ).

/*  z = x post :
*
*     the first statement should wait for the initialization of program whose initial value may not be the initial value of PIVP.
*     the structure is the same as 'inter_to_PIVP' with additional structure for program initialization.
*
*     one additional bio switch:
*
*     program_start
*
*       - turn on state: program_start
*         - turn on by 'pre_post'
*       - turn off state: program_start_not
*
*     pre = stable( z == x ) && program_start_not
*
*   TODO:
*
*     - seperate the initialzation of program with the statement implimentation.
*     - the initialization of the program seems not robust.
*/
%% post_to_PIVP(+OdeSystem:'ode system', +VariableXName:name, +VariableZName:name, +EvalValue:number, +VariablePreName:name, +VariablePostPreName:name, +VariablePostPreNextName:name, +VariablePostName:name) is det
%
%   The compiltion of 'rhs' being '<expression> post'.
%   This is a core compilation unit for sequentiality.
%   This compilation unit also deals with the initialization of program.
%   It should be merged with post_to_PIVP/9.
post_to_PIVP( OdeSystem, X, Z, EvalValue, Pre, PostPre, PostPreNext, Post ) :-
  branch_to_PIVP(         OdeSystem, Pre, X,        Z,          Z ),
  post_post_pre_to_PIVP(  OdeSystem, X,   Z,        EvalValue,  Pre,  PostPreNext, PostPre, Pre ),
  inter_post_to_PIVP(     OdeSystem, Pre, PostPre,  Post ).

%% post_to_PIVP(+OdeSystem:'ode system', +VariableXName:name, +VariableZName:name, +EvalValue:number, +VariablePreName:name, +VaraibleLoopPreName:name, +VariablePostPreName:name, +VariablePostPreNextName:name, +VariablePostName:name) is det
%
%   The post_to_PIVP/8 for loop control flow.
%   It should be merged with post_to_PIVP/8.
post_to_PIVP( OdeSystem, X, Z, EvalValue, Pre, LoopPre, PostPre, PostPreNext, Post ) :-
  branch_to_PIVP(         OdeSystem, LoopPre, X,        Z,          Z ),
  post_post_pre_to_PIVP(  OdeSystem, X,       Z,        EvalValue,  LoopPre, PostPreNext, PostPre, Pre ),
  inter_post_to_PIVP(     OdeSystem, LoopPre, PostPre,  Post ).

%% post_post_pre_to_PIVP(+OdeSystem:'ode system', +VariableXName:name, +VariableZName:name, +EvalValue:number, +VariablePreRealName:name, +VariablePrePostNextName:name, +VariablePostPreName:name, +VariablePreName:name ) is det
post_post_pre_to_PIVP( OdeSystem, X, Z, EvalValue, PreReal, PostPreNext, PostPre, Pre ) :-
  constant( switchNull, SwitchNull ),
  new_variable( OdeSystem, [ Stable, ProgramStart, ProgramStartNot ] ),

  inter_post_pre_to_PIVP( OdeSystem, X, Z, PreReal, PostPreNext, PostPre ),

  equal_to_PIVP(  OdeSystem, X,       EvalValue,        Stable  ),
  and_to_PIVP(    OdeSystem, Stable,  ProgramStartNot,  Pre     ),

  switch_to_PIVP( OdeSystem, SwitchNull, PostPre, ProgramStartNot, ProgramStart ).

/*  z = para x :
*
*     z = ( pre ) ? x : z
*/
%% parallel_to_PIVP(+OdeSystem:'ode system', +VariableEName:name, +VariableZName:name, +VariablePreName:name) is det
%
%   The compiltion of 'rhs' being 'para <expression>'.
%   This is a core compilation unit for parallel computation.
parallel_to_PIVP( OdeSystem, X, Z, Pre ) :-
  branch_to_PIVP( OdeSystem, Pre, X, Z, Z ).

/*  if_tag = condition :
*/
%% branch_to_PIVP(+OdeSystem:'ode system', ++If_tag, +VariableCName:name, +VariablePreName:name, +VairiablePostPreName:name, +VairableThenPostPreNextName:name, +VariableElsePostPreNextName:name, +VariableThenPreName:name, +VariableElsePreName:name ) is det
branch_to_PIVP( OdeSystem, if_tag, C, Pre, PostPre, ThenPostPreNext, ElsePostPreNext, ThenPre, ElsePre ) :-
  new_variable( OdeSystem, [ NC, PostPreNext ] ),

  not_to_PIVP(        OdeSystem, C, NC ),
  condition_to_PIVP(  OdeSystem, C, NC, Pre, PostPre, PostPreNext, ThenPre, ElsePre ),

  or_to_PIVP( OdeSystem, ThenPostPreNext, ElsePostPreNext, PostPreNext ).

%% branch_to_PIVP(+OdeSystem:'ode system', ++If_tag, +VariableCName:name, +EvalValue:number, +VariablePreName:name, +VairiablePostPreName:name, +VairableThenPostPreNextName:name, +VariableElsePostPreNextName:name, +VariableThenPreName:name, +VariableElsePreName:name ) is det
%
%   The version of branch_to_PIVP/9 with initialization of program.
branch_to_PIVP( OdeSystem, if_tag, C, EvalValue, Pre, PostPre, ThenPostPreNext, ElsePostPreNext, ThenPre, ElsePre ) :-
  new_variable( OdeSystem, [ NC, PostPreNext ] ),

  eval( OdeSystem, not EvalValue, NCEvalValue ),
  not_to_PIVP(        OdeSystem, C, NC ),
  condition_to_PIVP(  OdeSystem, C, NC, NCEvalValue, Pre, PostPre, PostPreNext, ThenPre, ElsePre ),

  or_to_PIVP( OdeSystem, ThenPostPreNext, ElsePostPreNext, PostPreNext ).

/*  endif = _ :
*/
%% branch_to_PIVP( +OdeSystem:'ode system', ++Endif, +VariableThenPostName:name, +VariablePostPreNextName:name, +VariableElsePostName:name, +VariableElsePostPreNextName:name, +VariablePostName:name, +VariablePostPreNextName:name ) is det
branch_to_PIVP( OdeSystem, endif, ThenPost, ThenPostPre, ThenPostPreNext, ElsePost, ElsePostPre, ElsePostPreNext, Post, PostPreNext ) :-
  !,
  or_to_PIVP( OdeSystem, ThenPost,    ElsePost,     Post            ),
  or_to_PIVP( OdeSystem, ThenPostPre, PostPreNext,  ThenPostPreNext ),
  or_to_PIVP( OdeSystem, ElsePostPre, PostPreNext,  ElsePostPreNext ).

/*  while = condition :
*
*     loop_pre_t = loop_post || loop_pre
*
*     'loop_pre_t' is the real precondition of the while loop.
*/
%% loop_to_PIVP(+OdeSystem:'ode system', ++While, +VariableCName:name, +VariableLoopPreName:name, +VariableLoopPostPreName:name, +VariableInLoopPreName:name, +VariableInLoopPostPreName:name, +VariableNoLoopPreName:name, +VariableNoLoopPostPreName:name, +VairableLoopPostName:name) is det
loop_to_PIVP( OdeSystem, while, C, LoopPre, LoopPostPre, InLoopPre, InLoopPostPre, NoLoopPre, NoLoopPostPre, LoopPost ) :-
  new_variable( OdeSystem, [ NC, LoopPostPreT, LoopPostPreNext ] ),

  loop_pre_to_PIVP( OdeSystem, LoopPre, LoopPost, LoopPostPreT, LoopPostPre ),

  not_to_PIVP(        OdeSystem, C, NC ),
  condition_to_PIVP(  OdeSystem, C, NC, LoopPostPre, LoopPostPreT, LoopPostPreNext, InLoopPre, NoLoopPre ),

  or_to_PIVP( OdeSystem, InLoopPostPre, NoLoopPostPre, LoopPostPreNext ).

%% loop_to_PIVP(+OdeSystem:'ode system', ++While, +VariableCName:name, +EvalValue:number, +VariableLoopPreName:name, +VariableLoopPostPreName:name, +VariableInLoopPreName:name, +VariableInLoopPostPreName:name, +VariableNoLoopPreName:name, +VariableNoLoopPostPreName:name, +VairableLoopPostName:name) is det
%
%   The version of loop_to_PIVP/10 with initialization of program.
loop_to_PIVP( OdeSystem, while, C, EvalValue, LoopPre, LoopPostPre, InLoopPre, InLoopPostPre, NoLoopPre, NoLoopPostPre, LoopPost ) :-
  new_variable( OdeSystem, [ NC, LoopPostPreT, LoopPostPreNext ] ),

  loop_pre_to_PIVP( OdeSystem, LoopPre, LoopPost, LoopPostPreT, LoopPostPre ),

  eval( OdeSystem, not EvalValue, NCEvalValue ),
  not_to_PIVP(        OdeSystem, C, NC ),
  condition_to_PIVP(  OdeSystem, C, NC, NCEvalValue, LoopPostPre, LoopPre, LoopPostPreT, LoopPostPreNext, InLoopPre, NoLoopPre ),

  or_to_PIVP( OdeSystem, InLoopPostPre, NoLoopPostPre, LoopPostPreNext ).

%% loop_pre_to_PIVP(+OdeSystem:'ode system', +VariableLoopPreName:name, +VariableLoopPostName:name, +VariableLoopPreTName:name) is det
loop_pre_to_PIVP( OdeSystem, LoopPre, LoopPost, LoopPostPre, LoopPreReal ) :-
  new_variable( OdeSystem, [ LoopPreT, LoopPreRealNot ] ),

  or_to_PIVP(     OdeSystem, LoopPost,    LoopPre,  LoopPreT ),
  switch_to_PIVP( OdeSystem, LoopPostPre, LoopPreT, LoopPreRealNot, LoopPreReal ).

/*  z = if c then t else e:
*     = ( c ) ? t : e
*/
%% branch_to_PIVP(+OdeSystem:'ode system', +VariableCName:name, +VariableTName:name, +VariableEName:name, +VariableZName:name) is det
branch_to_PIVP( OdeSystem, C, T, E, Z ) :-
  !,
  variable( [ C, T, E, Z ], [ [ CP, _ ], TDiff, EDiff, ZDiff ], list ),

  branch_to_ODE( OdeSystem, CP, TDiff, EDiff, ZDiff ).

%% branch_to_ODE(+OdeSystem:'ode system', +VariableCP:'dual var', +VariableT:'dual var', +VariableE:'dual var', +VariableZ:'dual var') is det
branch_to_ODE( OdeSystem, C, [ TP, TN ], [ EP, EN ], [ ZP, ZN ] ) :-
  constant( true, True ),

  (
    ZP == TP
  ->
    ThenP = 0,
    ThenN = 0
  ;
    ThenP = ( TP - ZP ) * C ^ 2 / True ^ 2,
    ThenN = ( TN - ZN ) * C ^ 2 / True ^ 2
  ),
  (
    ZP == EP
  ->
    ElseP = 0,
    ElseN = 0
  ;
    ElseP = ( EP - ZP ) * ( True - C ) ^ 2 / True ^ 2,
    ElseN = ( EN - ZN ) * ( True - C ) ^ 2 / True ^ 2
  ),

  add_ode(  OdeSystem, [ ZP, ZN ], ThenP + ElseP, ThenN + ElseN ).

% helper functions
%% condition_to_PIVP(+OdeSystem:'ode system', +VariableCName:name, +VariableNCName:name, +VariablePreName:name, +VariablePostPreName:name, +VariablePostPreNextName:name, +VariableCPostName:name, +VariableNCPostName:name) is det
%
%   This is a helper function for the true and false condition postcondition of branch and loop.
condition_to_PIVP( OdeSystem, C, NC, Pre, PostPre, PostPreNext, CPost, NCPost ) :-
  new_variable( OdeSystem, [ Condition, ConditionNot, ConditionPost ] ),

  inter_to_PIVP(    OdeSystem, NC,  ConditionNot,  Pre, PostPre,  PostPreNext,  ConditionPost ),
  parallel_to_PIVP( OdeSystem, C,   Condition,     Pre ),

  and_to_PIVP( OdeSystem, Condition,    ConditionPost, CPost  ),
  and_to_PIVP( OdeSystem, ConditionNot, ConditionPost, NCPost ).

%% condition_to_PIVP(+OdeSystem:'ode system', +VariableCName:name, +VariableNCName:name, +EvalValue:number, +VariablePreName:name, +VariablePostPreName:name, +VariablePostPreNextName:name, +VariableCPostName:name, +VariableNCPostName:name) is det
%
%   The version of condition_to_PIVP/8 with initialization of program.
condition_to_PIVP( OdeSystem, C, NC, EvalValue, Pre, PostPre, PostPreNext, CPost, NCPost ) :-
  new_variable( OdeSystem, [ Condition, ConditionNot, ConditionPost ] ),

  post_to_PIVP(     OdeSystem, NC,  ConditionNot,  EvalValue, Pre, PostPre, PostPreNext,  ConditionPost ),
  parallel_to_PIVP( OdeSystem, C,   Condition,     Pre ),

  and_to_PIVP( OdeSystem, Condition,    ConditionPost, CPost  ),
  and_to_PIVP( OdeSystem, ConditionNot, ConditionPost, NCPost ).

condition_to_PIVP( OdeSystem, C, NC, EvalValue, LoopPre, Pre, PostPre, PostPreNext, CPost, NCPost ) :-
  % condition_to_PIVP( +OdeSystem, +VariableCName, +VariablePostName )
  new_variable( OdeSystem, [ Condition, ConditionNot, ConditionPost ] ),

  post_to_PIVP(     OdeSystem, NC,  ConditionNot,  EvalValue, Pre, LoopPre, PostPre, PostPreNext, ConditionPost ),
  parallel_to_PIVP( OdeSystem, C,   Condition,     LoopPre ),

  and_to_PIVP( OdeSystem, Condition,    ConditionPost, CPost  ),
  and_to_PIVP( OdeSystem, ConditionNot, ConditionPost, NCPost ).

% end helper functions
% end control flow
% end unit synthesizer

% database
% constant(?Name:name, ?Value:atomic)
%
%   The constant value _Value_ is associated to name _Name_.
%   The supported constants are:
%
%   * true: the Boolean true.
%   * false: the Boolean false.
%   * tempPrefix: the prefix of the temperary variable name.
%   * conNull: indicate there is no condition set to the global control flow condition.
%   * counter: The counter name used to count the number of temperary variables.
%   * switchNull: indicate there is no turn on or off signal of a switch.
constant( true,       1.5     ).
constant( false,      0       ).
constant( tempPrefix, temp    ).
constant( conNull,    null    ).
constant( counter,    counter ).
constant( switchNull, Value   ) :-
  constant( false, Value ).
constant( notNull,    none    ).

% operator(?Operator:atom)
%
%   The operators used in the syntax of add_function/2.
%   This is for the parsing of the sign operation + and - in expression_to_ODE/3.
operator(+).
operator(-).
operator(*).
operator(/).
operator(^).
operator('(').
operator(')').
operator('<').
operator('<=').
operator('<').
operator('>').
operator('>=').
operator('=').
operator('or').
operator('and').
operator('not').
operator('pre').
operator('post').
operator('if').
operator('then').
operator('else').
operator('if_tag').
operator('else_tag').
operator('endif').
operator('while').
operator('endwhile').

% keyword(?Keyword:atom)
%
%   Some keywords used in the syntax of compile_program/2.
%   This is for the parsing of these keywords in compile/3.
keyword('endif').
keyword('endwhile').

% control_flow_condition(+Condition:atom)
%
%   This is for handling global variables of precondition, postcondition, ..., etc.
control_flow_condition(precondition).
control_flow_condition(postcondition_pre).
% end database
% end synthesis from expression to pivp
