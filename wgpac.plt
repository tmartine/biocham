:- use_module(library(plunit)).

:- begin_tests(wgpac, [setup((clear_model, reset_options))]).

test('compile_wgpac') :-
  command(compile_wgpac((a :: integral integral (-1 * a)))).

:- end_tests(wgpac).
