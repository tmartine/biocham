/* <stable CRN> tools to compile CRN stabilizing algebraic functions

The main function is stabilize_expression that handle the conversion of an
algebraic expression into a PIVP and then delegates to compile_to_biocham_model.

Coder: M. Hemery
*/

:- module(
    stable_crn,
    [
        % Commands
        stabilize_expression/3,
        % API
        list_variables/2
    ]).

:- use_module(biocham).
:- use_module(arithmetic_rules).
:- use_module(formal_derivation).
:- use_module(gpac).
:- use_module(ode).


%! stabilize_expression(+Expression, +Output, +Point)
%
% Ensure that output adjusts itself so that Expression is satisfied in the
% vicinity of Point
% All variables other than Output will be taken as an input

stabilize_expression(Expression, Output, Point) :-
    biocham_command,
    type(Expression, arithmetic_expression),
    type(Output, name),
    doc('
    Construct a CRN such that the Output species will adjust its concentration
    to satisfy the algebraic Expression in the vicinity of Point.
    '),
    % Local init
    nb_setval(variable_id, 0),
    copy_parameters, clear_model, paste_parameters,
    list_variables(Expression, Variables),
    delete(Variables, Output, Inputs),
    convert_expression(Output:Expression, Point, FirstPIVP),
    maplist(get_init_value(Point), Inputs, Values),
    foldl([X,V,L,LL]>>(LL=(L;(V,d(X)/dt=0))), Inputs, Values, FirstPIVP, PIVP),

    format_pivp(PIVP, [_N, PODE, Init], Name_list),
    % Custom quadratization
    scan_order_multivarvector(PODE, Order),
    (
      Order > 2
    ->
      custom_quadratization(PODE, Output, Name_list, Solution),
      rewrite_stab_pode(Solution, PODE, PODE_t),
      generate_names(Name_list, Solution, Names_t),
      reduction:generate_iv(Solution, Init, Init_t)
    ;
      PODE_t = PODE,
      Names_t = Name_list,
      Init_t = Init
    ),
    % Dual_rail encoding
    length(Solution, N_t),
    gpac:negation_step([N_t, PODE_t, Init_t], Names_t, _Keep1, PIVP_f, Names_f, _Keep2),
    gpac:compile_to_biocham_model(PIVP_f, Names_f).

:- doc('
  \\begin{example} Compilation of a CRN to stabilize the Bring radical:
').
:- biocham(stabilize_expression(y^5+y-x, y, [x=2, y=1])).
:- biocham(list_model).
:- biocham(option(time:100, show:{y})).
:- biocham(dose_response(x, 0, 4)).
:- doc('
  \\end{example}
  ').


%! custom_quadratization(+PODE, +Output, +Name_list, -Solution)
%
% Determine the optimal quadratization for this problem
% It is mainly through a hack and a call to maxsat_quadratic_reduction
% The idea is that the last variable is a representation of the state of the variable with
% 1 being calculated and 2 being not reached.

custom_quadratization(PODE, Output, Name_list, Solution) :-
    with_option([quadratic_bound:carothers],
      generate_sufficient_variables(PODE, AllVar)
    ),
    maplist(indicator([Output]), Name_list, Output_list_t),
    append(Output_list_t, [1], Output_list),
    maplist([X,X1]>>append(X,[1],X1), AllVar, AllPVar1),
    findall(Var2,(member(Var1, AllVar), sum_list(Var1,1), append(Var1,[2],Var2)),AllPVar2),
    append(AllPVar1, AllPVar2, AllPVar),
    maplist(pseudo_derivative(PODE), AllPVar, AllDer),
    maxsat_quadratic_reduction(AllPVar, AllDer, [Output_list], Dummy_Solution),
    findall(
      Variable,
      (member(Putative, Dummy_Solution),
      append(Variable, [1], Putative)),
      Solution
    ).

indicator(Set, Element, Out) :-
  (
    member(Element, Set)
  ->
    Out = 1
  ;
    Out = 0
  ).


%! pseudo_derivative(PODE, Var, Der)
%
% Pseudo derivative that only works for our custom quadratization

pseudo_derivative(PODE, Var, Der2) :-
  sum_list(Var, 2),
  nth1(N, Var, 1),
  nth1(N, PODE, Der),
  \+(Der = [[0,_Exp]]),
  !,
  findall(
    Monomial2,
    (
      member([Rate, Exp], Der),
      (
        sum_list(Exp, 0)
      ->
        append(Exp, [0], Exp2),
        Monomial2 = [Rate, Exp2]
      ;
        append(Exp, [2], Exp2),
        Monomial2 = [Rate, Exp2]
      )
    ),
    Der2
  ).

pseudo_derivative(_PODE, Var1, [[1,Var2]]) :-
  append(Var, [1], Var1),
  append(Var, [2], Var2).

pseudo_derivative(_PODE, Var2, [[1,Var1]]) :-
  append(Var, [2], Var2),
  append(Var, [1], Var1).


%! convert_expression(+Output:Expr, +Point, +PIVP)
%
% Output: name
% Expr: any algebraic expression with inputs and output, the curve is Expr=0
% Point: a point on the curve to select the desired branch

convert_expression(Output:Expression, Point, PIVP) :-
    maplist([P,N,V]>>(P=(N=V)), Point, Names, _Values),
    detect_new_variable(Expression, Names, New_var),
    get_init_value(Point, Output, IntVal),
    (
        New_var = []
    ->
        ensure_stability(Expression, Output, Point, SureExpr),
        PIVP = (IntVal, d(Output)/dt = SureExpr)
    ;
        maplist(give_name(Names), New_var, Transf_list),
        substitute_template(Transf_list, Name_list, Head_list),
        substitute_all(Head_list, Name_list, Expression, Polynomial),
        foldl(add_to_point, Transf_list, Point, NewPoint),
        ensure_stability(Polynomial, Output, NewPoint, SurePoly),
        PIVP0 = (IntVal, d(Output)/dt = SurePoly),
        foldl(add_new_var(NewPoint), Transf_list, PIVP0, PIVP)
    ).
    

%! give name(+Names, +Ex, -Transf)
%
% Generate custom species name on the fly

give_name(Names, Ex, Name->Ex) :-
    new_variable(Names, Name).


%! add_to_point
%
% lambda like used to add new variables to the Point argument

add_to_point(Name->Expr, Point, [Name=Value|Point]) :-
    get_init_value(Point, Expr, Value).


%! add_new_var(+Point, +Transf, +PIVP, -NEW_PIVP)
%
% Add the variables needed to polynomialize the PIVP
% Perform basic algebraic operation for this

add_new_var(Point, Name->Expr, PIVP, PIVP_new;PIVP) :-
    (
        Expr = Num/Den
    ->
        Expression = Name*Den-Num
    ;
        (Expr = sqrt(SQRT) ; Expr = SQRT^0.5)
    ->
        Expression = Name^2-SQRT
    ;
        (Expr = sqrt(N,SQRT) ; Expr = SQRT^(1/N) )
    ->
        Expression = Name^N-SQRT
    ),
    convert_expression(Name:Expression, Point, PIVP_new).


%! ensure_stability(+Raw, +Output, +Point, -Relation)
%
% Relation is \pm Raw to be sure that the PIVP is stable around Point 

ensure_stability(Raw, Output, Point, Relation) :-
    maplist([P,N,V]>>(P=(N=V)), Point, Names, Values),
    (
        derivate(Raw, Output, Deriv_expr),
        substitute_all(Names, Values, Deriv_expr, Deriv),
        Deriv >= 0
    ->
        distribute(-Raw, Dist),
        simplify(Dist, Relation)
    ;
        distribute(Raw, Dist),
        simplify(Dist, Relation)
    ).


%! format_polynomial(+Function, -Polynomial)
%
% Rewrite an algebraic function into a Polynomial relation

format_polynomial(Function, Function) :-
    is_polynomial(Function).

format_polynomial(Function, Polynomial) :-
    list_variables(Function, Variables),
    detect_new_variable(Function, Variables, [Trouble|_Tail]),
    (
        Trouble = 1/Den
    ->
        distribute(Function*Den, Dist),
        simplify(Dist, NewFunc),
        format_polynomial(NewFunc, Polynomial)
    ).


%! list_variables(+Expression, -Variables)
%
% List all the variable that appear in the Expression

list_variables(Expr, Variable_set) :-
    Expr =.. [_Func|Args],
    \+(Args = []),
    !,
    maplist(list_variables, Args, Variable_list),
    append(Variable_list, Variables),
    list_to_set(Variables, Variable_set).

list_variables(Number, []) :- number(Number), !.

list_variables(Variable, [Variable]).


%! get_init_value(+Point, +Expr, -Value)
%
% Return the value of Expr at initial point (or default to 0).
    
get_init_value(_Point, Number, Number) :-
    number(Number),
    !.

get_init_value(Point, Expr, Value) :-
    Expr =.. [Func|Tail],
    \+(Tail=[]),!,
    maplist(get_init_value(Point), Tail, TailEv),
    ExprEv =.. [Func|TailEv],
    Value is ExprEv.

get_init_value([Name=Value | _Tail], Name, Value) :- !.

get_init_value([], _Name, 0) :- !.

get_init_value([_Head | Tail], Name, Value) :-
    get_init_value(Tail, Name, Value).
    

%! rewrite_stab_pode(+ListVariable,+PODE,-NewPODE)
%
% Rewrite the MultivarVector corresponding to PODE in the new set of variables

rewrite_stab_pode(ListVariable,PODE,NewPODE) :-
   rewrite_stab_pode(ListVariable,PODE,ListVariable,NewPODE).

rewrite_stab_pode([],_PODE,_ListVariable,[]) :- !.

rewrite_stab_pode([Var|TailVar],PODE,ListVariable,[RW_Der|Tail]) :-
   (
     sum_list(Var, 1) 
   ->
     compute_derivative(Var,PODE,Der),
     reduction:rewrite_der(Der,ListVariable,RW_Der)
   ;
     member(X, ListVariable),
     member(Y, ListVariable),
     add_list(X, Y, Var),
     reduction:rewrite_expo(X, ListVariable, XX),
     reduction:rewrite_expo(Y, ListVariable, YY),
     add_list(XX,YY,First),
     reduction:rewrite_expo(Var, ListVariable, Second),
     RW_Der = [[1,First], [-1,Second]]
   ),
   rewrite_stab_pode(TailVar,PODE,ListVariable,Tail).
