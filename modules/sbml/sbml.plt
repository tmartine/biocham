:- use_module(library(plunit)).

:- use_module(sbml).

echo(Filename) :-
  readSBML(Filename, SBML),
  sbmlDocument_getNumErrors(SBML, Errors),
  (
    Errors > 0
  ->
    sbmlDocument_printErrors(SBML, user_error),
    throw(error(sbml_errors))
  ;
    true
  ).

:- begin_tests(sbml).

test(echo) :-
  echo('../../library/biomodels/BIOMD0000000001.xml').

:- end_tests(sbml).
