:- module(
  graphviz,
  [
    agattr/5,
    agclose/1,
    agedge/5,
    agget/3,
    agnode/4,
    agopen/3,
    agread/2,
    agset/3,
    agwrite/2,
    gvFreeLayout/1,
    gvLayout/2,
    gvRenderFilename/3
  ]
).


:- use_foreign_library(foreign(graphviz_swiprolog)).
