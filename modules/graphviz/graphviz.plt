:- use_module(library(plunit)).

:- use_module(graphviz).

:- use_module('examples/digraph').

:- use_module('examples/simple').

:- use_module('examples/png').

:- begin_tests(graphviz).

test(digraph) :-
  digraph('test_digraph.dot'),
  delete_file('test_digraph.dot').

test(simple) :-
  digraph('test_digraph.dot'),
  simple('test_digraph.dot', 'target.dot'),
  delete_file('test_digraph.dot'),
  delete_file('target.dot').

test(png) :-
  digraph('test_digraph.dot'),
  png('test_digraph.dot', 'target.png'),
  delete_file('test_digraph.dot'),
  delete_file('target.png').

:- end_tests(graphviz).
