:- module(
  digraph,
  [
    digraph/1
  ]
).

:- use_module('../graphviz').

digraph(TargetFilename) :-
  agopen('test', directed, Graph),
  agattr(Graph, graph, size, '7.5,11', _),
  agattr(Graph, graph, ratio, fill, _),
  agnode(Graph, 'a', true, NodeA),
  agnode(Graph, 'b', true, NodeB),
  agnode(Graph, 'reaction_0', true, Reaction_0),
  agedge(Graph, NodeA, Reaction_0, true, _Edge0),
  agedge(Graph, Reaction_0, NodeB, true, _Edge1),
  agattr(Graph, node, shape, circle, _),
  agset(Reaction_0, shape, box),
  agwrite(Graph, TargetFilename),
  agclose(Graph).
