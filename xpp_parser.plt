:- use_module(library(plunit)).

:- use_module(xpp_parser).

:- begin_tests(xpp_parser, [setup((clear_model, reset_options))]).

test(read_xpp, [nondet]) :-
  read_xpp("library/examples/tests/test_xpp.ode"),
  get_current_ode_system(Id),
  item([parent:Id, type:ode, item: d(x)/dt = y]),
  item([parent:Id, type:ode, item: d(y)/dt = -k*f(x*y)]),
  item([parent:Id, type:parameter, item: par(k1=2.5)]),
  item([parent:Id, type:parameter, item: par(k2=20.5)]),
  item([parent:Id, type:function, item: func(f(x)=sqrt(x))]),
  export_xpp("library/examples/tests/test_xpp_out.ode"),
  clear_model,
  read_xpp("library/examples/tests/test_xpp_out.ode"),
  get_current_ode_system(Id2),
  item([parent:Id2, type:ode, item: d(x)/dt = y]),
  item([parent:Id2, type:ode, item: d(y)/dt = -k*f(x*y)]),
  item([parent:Id2, type:parameter, item: par(k1=2.5)]),
  item([parent:Id2, type:parameter, item: par(k2=20.5)]),
  item([parent:Id2, type:function, item: func(f(x)=sqrt(x))]).


test(
  'import_ode',
  [true(
    ODEs == [init(a = 0), init(b = 0), d(a)/dt = -a, par(k = -2),
    par(kk = 1.0), d(b)/dt = a*k])]
) :-
  setup_call_cleanup(
    open('test.ode', write, TestOde),
    write(TestOde, '
# Comment
init a = 0
B(0) = 0
wiener q
da/dt = -a
par k =-2
p kk = 1.0
B''= k*a
@ meth=cvode
done
'),
    close(TestOde)
  ),
  read_xpp('test.ode'),
  delete_file('test.ode'),
  get_current_ode_system(Id),
  all_items([parent: Id], ODEs).


test('export_ode', [nondet]) :-
  clear_model,
  command('MA'(k) for a => b),
  command(parameter(k=2)),
  command(initial_state(a=1.5)),
  export_ode('test_export.ode'),
  clear_model,
  import_ode('test_export.ode'),
  delete_file('test_export.ode'),
  get_current_ode_system(Id),
  all_items([parent: Id], ODEs),
  member(d(b)/dt=a*k, ODEs),
  member(d(a)/dt= - (a*k), ODEs),
  member(init(a=1.5), ODEs),
  member(par(k=2), ODEs),
  member(init(b=0), ODEs).

:- end_tests(xpp_parser).
