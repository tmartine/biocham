:- module(
  biocham,
  [
    % Public API
    start/0,
    initialize/0,
    initial/1,
    biocham_command/0,
    biocham_command/1,
    library_path/1
  ]).


% Only for separate compilation/linting
:- use_module(doc).


:- doc('
The Biochemical Abstract Machine (Biocham) is a modelling software for cell systems biology, with some unique features for static and dynamic analyses using temporal logic constraints.').


:- doc('
  Biocham is a free software protected by the GNU General Public License GPL
  version 2. This is an Open Source license that allows free usage of this
  software.').

:- doc('
This reference manual (as its extended version for developers) is automatically generated from the source code of Biocham.').

:- doc('
Biocham v4 is mainly composed of :
\\begin{itemize}
\\item 
a rule-based language for modeling biochemical reaction networks and influence networks
\\begin{itemize}
\\item compatible with SBML2 \\doi{10.1038/npre.2008.2715.1} and SBML3-qual \\doi{10.1186/1752-0509-7-135} respectively
\\item interpreted in a hierarchy of semantics (differential, stochastic, Petri Net, Boolean)
\\end{itemize}
\\item
a temporal logic based language to formalize possibly imprecise behaviours both qualitative and quantitative \\cite{RBFS11tcs}}
\\item
several unique features for developing/analyzing/correcting/completing/calibrating/coupling/synthesizing reaction and influence networks with respect to formal specifications of their behaviour.
\\end{itemize}

Biocham v4 is a complete rewriting (in SWI-Prolog) of Biocham v3 (written in GNU-Prolog).
It introduces some new features, mainly:
\\begin{itemize}
\\item influence network models \\cite{FMRS18tcbb}
\\item quantitative temporal logic patterns and trace simplifications \\cite{FS14book} 
\\item compilation of mathematical functions and simple programs in reaction networks \\cite{FLBP17cmsb}
\\item PAC learning of influence models from data \\cite{CFS17cmsb}
\\item a notebook based on Jupyter
\\end{itemize}
plus since v4.1:
\\begin{itemize}
\\item multistationarity check \\cite{BFS18jtb}}
\\item robustness optimization \\cite{FS18cmsb}
\\item detection of model reductions by constrained subgraph epimorphisms (SEPI) \\cite{GSF10bi}
\\item tropical equilibrations \\cite{SFR14amb}
\\item a menu of commands in the notebook
\\item a Graphical User Interface based on the notebook
\\end{itemize}

Some features of Biocham v3 are still not implemented in v4, mainly:
\\begin{itemize}
\\item the graphical editor (SBGN compatible)
\\item hybrid stochastic-differential simulations \\cite{CFJS15tomacs}
\\end{itemize}
or less efficiently (because of on-the-fly C compilation instead of native Prolog code):
\\begin{itemize}
\\item numerical integration using GSL library is slower and may be less accurate
\\item parameter search, sensitivity and robustness measures are currently slower than Biocham v3.
\\end{itemize}
').



start :-
  about,
  initialize,
  do_arguments.


:- dynamic(initialized/0).


initialize :-
  initialized,
  !.

initialize :-
  initialize_library_path,
  set_prolog_flag(allow_variable_name_as_functor, true),
  prolog_history(enable),
  set_counter(item_id, 0),
  set_counter(model, 0),
  set_counter(molecule_id, 0),
  set_counter(parameter_id, 1),
  set_plot_driver(gnu_plot),
  set_image_viewer_driver(open_file),
  nb_setval(ode_viewer, inline),
  set_draw_graph_driver(graph_pdf),
  nb_setval(graph_pdf, 0),
  nb_setval(current_models, []),
  new_model,
  set_model_name('initial'),
  \+ (
    initial_command(Command),
    \+ (
      command(Command)
    )
  ),
  store_default_options,
  new_model,
  assertz(initialized).


:- dynamic(initial_command/1).


initial(Command) :-
  devdoc('
    Adds a command to run during initialization
    (in the context of the "initial" model)
  '),
  assertz(initial_command(Command)).


:- dynamic(library_path/1).


initialize_library_path :-
  (
    getenv('BIOCHAMLIB', LibraryPath)
  ->
    true
  ;
    executable_filename(ExecutableFilename),
    absolute_file_name(
      'library/gsl_solver.cc',
      LibraryFile,
      [relative_to(ExecutableFilename)]
    ),
    file_directory_name(LibraryFile, LibraryPath)
  ),
  assertz(library_path(LibraryPath)).


biocham_command :-
  doc('Declares a command for the top-level.').


biocham_command(_) :-
  doc('
    With a * : declares a command for the top-level that can take an
    unlimited number of arguments.
    The additional arguments are given as a list in the last parameter.').

:-devcom('\\begin{todo}Make biocham commands functional. We need to know the result type for the gui... Furthermore, the show, list, enumerate,... commands are not so nice. The print result should be a text or a widget associated to the returned type.\\end{todo}').
