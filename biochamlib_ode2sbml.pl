#!/usr/bin/prolog
/*
 * The prolog script to create the stand-alone saved state for conversion of an ODE to an
 * SBML file. 
 * Coder: M. Hemery
 */

% Loading libraries
:- use_module(aliases).
:- use_module(arithmetic_rules).
:- use_module(biocham).
:- use_module(counters).
:- use_module(doc).
:- use_module(filename).
:- use_module(formal_derivation).
:- use_module(functions).
:- use_module(initial_state).
:- use_module(kinetics).
:- use_module(models).
:- use_module(molecules).
:- use_module(namespace).
:- use_module(objects). 
:- use_module(ode). 
:- use_module(parameters). 
:- use_module(reaction_editor). 
:- use_module(reaction_rules). 
:- use_module(sbml_files). 
:- use_module(toplevel).
:- use_module(types).
:- use_module(util).
:- use_module(xpp_parser).

set_my_option(Option, Value) :-
  change_item([], option, Option, option(Option: Value)).

% parse_arguments(+Arguments, -Input)
%
% Handle the various option of the standalone:
% -i: input file

parse_arguments([], none).

parse_arguments(['-i', Input| Argv], Input) :-
  parse_arguments(Argv, _I).

parse_arguments(['--help'| _Argv], _I, _M, _T) :-
  print_help.

% print_help

print_help :-
  format("help~n", []),
  halt.

% main(+argv)

main(Argv) :-
  (Argv = [] -> print_help ; true),
  % Handling naming and options
  parse_arguments(Argv, Input),
  file_name_extension(InputBase, 'ode', Input),
  file_name_extension(InputBase, 'sbml', Output),
  format("Input: ~w~nOutput: ~w~n", [Input, Output]),
  % Initialization (see biocham:initialize)
  set_prolog_flag(allow_variable_name_as_functor, true),
  set_counter(list_item_counter, 0),
  set_counter(item_id, 0),
  set_counter(model, 0),
  set_counter(molecule_id, 0),
  set_counter(parameter_id, 1),
  nb_setval(ode_viewer, inline),
  nb_setval(current_models, []),
  new_model,
  set_my_option(import_reactions_with_inhibitors, yes),
  % Main procedure
  read_xpp(Input),
  list_ode,
  add_reactions_from_ode_system,
  export_sbml(Output).


