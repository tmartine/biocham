:- module(
  reduce,
  [
    reduce_model/1,
    reduce_model/0
  ]
).

:- use_module(ctl).

% Only for separate compilation/linting
:- use_module(nusmv).
:- use_module(util).

:- doc('This section describes commands to reduce a reaction model by deleting species and  reactions, while preserving a CTL specification of the behaviour.').

:- devcom('\\begin{todo}These commands should be generalized, 
first, to deletions and merge of molecules and reactions,

second, to influence networks and hybrid reaction-influence networks, 

and third, to hybrid specifications of behaviors with attractors, CTL, FOLTL, traces.\\end{todo}').

:- devdoc('\\section{Commands}').


reduce_model(Query) :-
    biocham_command,
    type(Query, ctl),
    doc(
	          'Deletes reactions as long as the specification of the behavior given by a CTL formula passed as
    argument remains satisfied in the current initial state.'
	      ),
    (
	      ctl_truth(Query,true)
     ->
	       reduce_model(Query, Removed),
         writeln('removed:'),
         forall(
           member(R, Removed),
           format("~p~n", [R])
         )
     ;
     print('Error: the specification is not satisfied in the current model.')
    ).

reduce_model :-
    biocham_command,
    doc('Same as above using the current CTL
    specification of the behavior.'),
    findall(Formula, item([kind: ctl_spec, item: Formula]), Specs),
    join_with_op(Specs, '/\\', Spec),
    reduce_model(Spec).

:- doc('\\begin{example}').
:- biocham_silent(clear_model).
:- biocham(a=>b).
:- biocham(b=>c).
:- biocham(c=>d).
:- biocham(present(b)).
:- biocham(make_absent_not_present).
:- biocham(generate_ctl).
:- biocham(reduce_model).
:- doc('\\end{example}').  

:- doc("\\begin{example}
Reduction of the MAPK model with respect to the output reachability property only:
all dephosphorylation reactions can be removed, resulting in a different bifurcation diagram with full memory effect.
\\trace{
biocham: load('library:examples/mapk/mapk.bc').
biocham: reduce_model(reachable('MAPK~{p1,p2}')).
removed ['RAF-RAFK'=>'RAF'+'RAFK','RAF~{p1}'+'RAFPH'=>'RAF~{p1}-RAFPH','RAF~{p1}-RAFPH'=>'RAF~{p1}'+'RAFPH','MEK-RAF~{p1}'=>'MEK'+'RAF~{p1}','MEK~{p1}-RAF~{p1}'=>'MEK~{p1}'+'RAF~{p1}','MEKPH'+'MEK~{p1}'=>'MEK~{p1}-MEKPH','MEK~{p1}-MEKPH'=>'MEKPH'+'MEK~{p1}','MEKPH'+'MEK~{p1,p2}'=>'MEK~{p1,p2}-MEKPH','MEK~{p1,p2}-MEKPH'=>'MEKPH'+'MEK~{p1,p2}','MAPK-MEK~{p1,p2}'=>'MAPK'+'MEK~{p1,p2}','MAPK~{p1}-MEK~{p1,p2}'=>'MAPK~{p1}'+'MEK~{p1,p2}','MAPKPH'+'MAPK~{p1}'=>'MAPK~{p1}-MAPKPH','MAPK~{p1}-MAPKPH'=>'MAPKPH'+'MAPK~{p1}','MAPKPH'+'MAPK~{p1,p2}'=>'MAPK~{p1,p2}-MAPKPH','MAPK~{p1,p2}-MAPKPH'=>'MAPKPH'+'MAPK~{p1,p2}','RAF~{p1}-RAFPH'=>'RAF'+'RAFPH','MEK~{p1}-MEKPH'=>'MEK'+'MEKPH','MEK~{p1,p2}-MEKPH'=>'MEK~{p1}'+'MEKPH','MAPK~{p1}-MAPKPH'=>'MAPK'+'MAPKPH','MAPK~{p1,p2}-MAPKPH'=>'MAPK~{p1}'+'MAPKPH']
biocham: dose_response('RAFK',1e-5,1e-3, time:200, show:'MAPK~{p1,p2}').
}
\\end{example}
").


:- devdoc('\\section{Public API}').


reduce_model(Query,Removed):-
    findall(Item, item([kind: reaction, item: Item]), Reactions),
    reduce_model(Reactions,Query, Removed).


:- devdoc('\\section{Internal predicates}').

reduce_model([R|Reactions],Query, Result) :-
  delete_item([kind: reaction, item: R]),
  get_option(boolean_initial_states, Init),
  with_option(
    boolean_trace: no,
    check_ctl_impl(Query, Init, true)
  ),
  !,
  Result=[R|Removed],
  reduce_model(Reactions,Query,Removed).

reduce_model([R|Reactions],Query, Removed) :-
  add_item([kind: reaction, item: R]),
  reduce_model(Reactions,Query,Removed).

reduce_model([],_,[]).
