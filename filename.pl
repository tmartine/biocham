:- module(
  filename,
  [
    input_file/1,
    output_file/1,
    filename/2,
    automatic_suffix/4
  ]
).

% Only for separate compilation/linting
:- use_module(doc).


:- doc('The syntax of Biocham is described with formal grammar rules which define new syntactic tokens from primitive tokens such as atom (i.e. string), number, term (e.g. atom(..., ...)).').

:- doc('For instance, the syntax of an input or output file is just the syntax of an atom in both cases, but they are distinguished in this manual for documentation purposes:').

:- grammar(input_file).


input_file(Name) :-
  atom(Name).


:- grammar(output_file).


output_file(Name) :-
  atom(Name).

:- doc('Note that these atoms can enclose with single quotes ('') usual UNIX
file expansion wildcards like * or ?.').

filename(Filename, ExpandedFilename) :-
  (
    atom_concat('library:', SubFilename, Filename)
  ->
    library_path(LibraryPath),
    Options = [relative_to(LibraryPath)]
  ;
    SubFilename = Filename,
    %Options = [], !bugwar: relative_to(Dir)!
    working_directory(Dir, Dir),
    Options = [relative_to(Dir)]
  ),
  absolute_file_name(SubFilename, ExpandedFilename, [expand(true) | Options]).


path_delimiter('/').


automatic_suffix(Filename, DefaultSuffix, Mode, FullFilename) :-
  file_name_extension(_, Suffix, Filename),
  (
    Suffix = '',
    atom_concat(Filename, DefaultSuffix, FullFilename),
    (
      Mode = read
    ->
      exists_file(FullFilename)
    ;
      true
    )
  ->
    true
  ;
    FullFilename = Filename
  ).






