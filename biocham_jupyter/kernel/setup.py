"""Biocham Jupyter kernel installation file """

from setuptools import setup
from biocham_kernel import __version__ as version

# pylint: disable=C0103
setup_args = dict(
    name='biocham_kernel',
    version=version,
    packages=['biocham_kernel'],
    install_requires=[
        'jupyter',
        'bokeh',
        'ipywidgets',
    ],
    license='GPL',
    description='A Jupyter wrapper kernel for Biocham',
)

if __name__ == '__main__':
    setup(**setup_args)
