:- use_module(library(plunit)).

roughly_equal(A, B, Epsilon) :-
    abs(A-B) =< Epsilon.

:- use_module(variations).

:- begin_tests(variations, [setup((clear_model, reset_options))]).

test(bug_elea) :-
    command(parameter(k=1)),
    command(d('A')/dt = 1-k*'A'),
    load_reactions_from_ode_system,
    change_parameter_to_variable(k),
    variation(k,1,10),
    command(numerical_simulation),
    get_table_data(OldData),
    last(OldData, LastRow),
    LastRow =.. [row, 20, Conc, 10],
    roughly_equal(Conc, 0.1, 0.01).

:- end_tests(variations).
