:- use_module(library(plunit)).
:- use_module(lazy_negation_gpac).

example_lng(1,PIVP) :-
   PIVP = [2,[ [[1,[0,1]]] , [[-1,[1,0]]] ],[1,0]].

example_lng(2,PIVP) :-
   PIVP = [2,[ [[1,[1,0]]] , [[-1,[1,1]]] ],[-1,0]].

example_lng(3,PIVP) :-
   PIVP = [2,[ [[1,[1,0]],[1,[0,1]]] , [[-1,[1,0]]] ],[1,0]].

example_lng(4,PIVP) :-
   PIVP = [2,[ [[-1,[1,1]],[1,[0,1]]] , [[-1,[1,0]]] ],[1,0]].

example_lng(5,PIVP) :-
   PIVP = [2,[ [[1,[0,0]],[-1,[2,0]]] , [[1,[1,0]],[-1,[0,1]]] ],[1,0]].

:- begin_tests(lazy_negation_gpac, [setup((clear_model, reset_options))]).

test(rewrite_pivp) :-
   example_lng(2,PIVP_2),
   rewrite_PIVP(PIVP_2, [3,[[[1, [1, 0, 0]]], [[1, [0, 1, 0]]], [[-1, [1, 0, 1]], [1, [0, 1, 1]]]], [0,1,0]], [1]).

test(negative_initial_concentration) :-
   lng:negative_initial_concentration([0,0,-1,2],[3]),
   lng:negative_initial_concentration([-4,0,2,-1,2],[1,4]),
   lng:negative_initial_concentration([0,1,2],[]).

test(find_troubling_variables_main) :-
   example_lng(1,[2, ODE1, _Init1]),
   lng:find_troubling_variables_main(ODE1, [], NV1),
   msort(NV1,[1,2]),

   example_lng(2,[2, ODE2, _Init2]),
   lng:find_troubling_variables_main(ODE2, [1], NV2),
   msort(NV2,[1]),

   example_lng(3,[2, ODE3, _Init3]),
   lng:find_troubling_variables_main(ODE3, [], NV3),
   msort(NV3,[1,2]),

   example_lng(4,[2, ODE4, _Init4]),
   lng:find_troubling_variables_main(ODE4, [], NV4),
   msort(NV4,[1,2]),

   example_lng(5,[2, ODE5, _Init5]),
   lng:find_troubling_variables_main(ODE5, [], NV5),
   msort(NV5,[]).

test(find_troubling_monomial) :-
   lng:find_troubling_monomial(1,[-1,[0,1,1]],[]),
   lng:find_troubling_monomial(1,[1,[0,1,1]],[3]),
   lng:find_troubling_monomial(2,[1,[0,0,1]],[3]),
   \+(lng:find_troubling_monomial(1,[-1,[1,0,1]],[])),
   \+(lng:find_troubling_monomial(1,[1,[1,0,1]],[3])).

test(rewrite_initial_concentration) :-
   lng:rewrite_initial_concentration([1,2,3],[2],[1,2,0,3]),
   lng:rewrite_initial_concentration([1,-2,3],[2],[1,0,2,3]).

test(rewrite_derivative_sr) :-
   example_lng(1,[2, ODE1, _Init1]),
   lng:rewrite_derivative_sr(ODE1,2,[ [[1,[0,1,0]], [-1,[0,0,1]]], [[-1,[1,0,0]]], [] ]),

   example_lng(2,[2, ODE2, _Init2]),
   lng:rewrite_derivative_sr(ODE2,1,[ [[1,[1,0,0]], [-1,[0,1,0]]], [] , [[-1,[1,0,1]], [1,[0,1,1]]] ]).

test(rewrite_monomial) :-
   lng:rewrite_monomial([1,[1]], 1, [[1, [1, 0]], [-1, [0, 1]]]),
   lng:rewrite_monomial([1,[2]], 1, [[1, [2, 0]], [1, [0, 2]]]).

:- end_tests(lazy_negation_gpac).
