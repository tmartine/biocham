:- use_module(matrix).

%! check_error(Result, Target, Max_Error)
%
% check if result and target differ by no more than Max_Error

check_error([], [], _) :- !.

check_error([R|TR], [T|TT], Epsilon) :-
  abs(R-T) < Epsilon,
  check_error(TR, TT, Epsilon).


:- begin_tests(matrix, []).

% Test of the matrix implementation

test(constant_matrix) :-
  constant_matrix(2, 1, 0, M),
  get_matrix_as_list(M, [[0],[0]]).

test(access) :-
  set_matrix([[1,2,3],[2,3,4],[3,4,5]], M),
  get_column(M, 2, vector(2,3,4)),
  get_row(M, 1, vector(1,2,3)),
  get_value(M, 2, 2, 3),
  set_vector([1,1,2,3,5], V),
  get_value(V, 5, 5).

test(slice_element) :-
  set_vector([1,2,3], V),
  slice_element(V, 1, 2, VV),
  get_vector_as_list(VV, [1,2]).

test(matrix_sum) :-
  set_matrix([[1,2],[3,4]], A),
  set_matrix([[4,3],[2,1]], B),
  matrix_sum(A,B,C),
  get_matrix_as_list(C, [[5,5],[5,5]]).

test(matrix_transpose) :-
  set_matrix([[1,2],[3,4]], A),
  matrix_transpose(A,B),
  get_matrix_as_list(B, [[1,3],[2,4]]).

test(matrix_prod) :-
  set_matrix([[1,2],[3,4]], A),
  set_matrix([[4,3],[2,1]], B),
  matrix_prod(A,B,C),
  get_matrix_as_list(C, [[8,5],[20,13]]).

% Test of the linalg part

test(ludcmp_matrix_22) :-
  set_matrix([[-5,-4],[1,8]], A),
	matrix:ludcmp_matrix(A, X, _FI),
	get_matrix_as_list(X, [[-5,-4],[-0.2,7.2]]).

test(lusubst_33) :-
  set_matrix([[2,1,4],[1,0,1],[6,4,2]], A),
  set_vector([9,3,6], V),
	matrix:ludcmp_matrix(A, Decmp, FI),
	matrix:lusubst(Decmp, FI, V, X),
	get_matrix_as_list(Decmp, [[1,0,1],[6,4,-4],[2,0.25,3.0]]),
	get_vector_as_list(X, [1.0,-1.0,2.0]).

test(solve_linear_algebra_1) :-
  set_matrix([[ 1.0, 2.0, 0.0,-1.0],
              [ 3.0,-2.0, 1.0, 0.0],
              [-2.0, 0.0, 0.0, 4.0],
              [ 1.0, 1.0,-1.0, 0.0]], A),
  set_vector( [-2.0, 7.0, 2.0,-2.0], V),
  matrix:solve_linear_problem(A, V, X),
  get_vector_as_list(X, List),
  check_error(List, [1,-1,2,1], 0.001).

:- end_tests(matrix).
