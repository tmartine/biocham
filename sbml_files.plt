:- use_module(library(plunit)).
:- use_module(library(http/http_open)).

% Only for separate compilation/linting
:- use_module(reaction_rules).


:- begin_tests(
  sbml_files,
  [condition(flag(slow_test, true, true)),
    setup((clear_model, reset_options))]
).


test(
  'load_sbml_reactions',
  [
    true(Reactions = [
    'B'*kf_0 for 'B' => 'BL',
    'BL'*kr_0 for 'BL' => 'B',
    'BL'*kf_1 for 'BL' => 'BLL',
    'BLL'*kr_1 for 'BLL' => 'BL' | _ ])
  ]
) :-
  load('library:biomodels/BIOMD0000000001.xml'),
  all_items([kind: reaction], Reactions).


test(
  'load_sbml_initial_state',
  [true((
    member(initial_state('BLL' = 0.0), Initial),
    member(initial_state('B' = 1.66057788110262e-5), Initial)
  ))]
) :-
  load('library:biomodels/BIOMD0000000001.xml'),
  all_items([kind: initial_state], Initial).


test(
  'load_sbml_local_parameters_assignment_rules'
) :-
  load('library:biomodels/BIOMD0000000003.xml'),
  command(numerical_simulation(time:1)).


test(
  'download_curated_biomodel',
  [
    fixme('EBI-EMBL API seems broken right now'),
    true(ItemsLocal = ItemsDownloaded),
    % test that we have a working network
    condition(
      catch(
        setup_call_cleanup(
          http_open('http://www.google.com', In, []),
          true,
          close(In)
        ),
        _,
        (
          write('-'),
          fail
        )
      )
    )
  ]
) :-
  load('library:biomodels/BIOMD0000000003.xml'),
  all_items([], ItemsLocal),
  catch(
    (
      command(download_curated_biomodel(3)),
      load('BIOMD0000000003.xml'),
      all_items([], ItemsDownloaded),
      delete_file('BIOMD0000000003.xml')
    ),
    % 500 is server error, not our fault?
    error(existence_error(_Url, _Id), context(_Msg, status(500, _Text))),
    ItemsDownloaded = ItemsLocal
  ).

test(
  'export_sbml',
  []
) :-
  clear_model,
  command(add_reaction(f*[a] for a => b)),
  command(add_reaction('MA'(k) for a + 3*b => 2*a)),
  command(present(b, 3.5)),
  command(parameter(k=1.0e-3)),
  command(parameter(l=3)),
  command(function(f=l*'Time')),
  command(add_event([a] > 10, l=10*l)),
  export_sbml('test.xml'),
  load_sbml('test.xml'),
  delete_file('test.xml').

:- end_tests(sbml_files).
