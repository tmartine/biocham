:- use_module(library(plunit)).
:- use_module(library(lists)).
:- use_module(reaction_rules).

:- begin_tests(conservation_laws, [setup((clear_model, reset_options))]).


test(
   'add_conservation adds a conservation',
   [
      setup(clear_model),
      cleanup(clear_model),
      forall(member(Input, ['a-a' + 2*'a', 'b', 'a' + 'b'])),
      true(Conservations == [Input])
   ]
) :-
   command(add_conservation(Input)),
   all_items([kind: conservation], Conservations).

%%% FIXME should test that conservations are properly used for numerical
%%% integration

test(
   'delete_conservation deletes the correct conservation',
   [
      setup(set_some_conservations),
      cleanup(clear_model),
      true(Conservations == ['a-a'+2*a,'c-c'+2*c])
   ]
) :-
   command(delete_conservation(b-b + 2*b)),
   all_items([kind: conservation], Conservations).


test(
   'delete_conservations deletes all conservations',
   [
      setup(set_some_conservations),
      cleanup(clear_model),
      true(Conservations == [])
   ]
) :-
   command(delete_conservations),
   all_items([kind: conservation], Conservations).


test(
   'list_conservations lists all conservations',
   [
      setup(set_some_conservations),
      cleanup(clear_model),
      true(Conservations ==
        '[0] \'a-a\'+2*a\n[1] \'b-b\'+2*b\n[2] \'c-c\'+2*c\n')
   ]
) :-
  with_output_to(atom(Conservations), command(list_conservations)).


test(
  'check_conservations is ok with trivial P-invariants',
  [
    setup((clear_model, set_prolog_flag(verbose, silent))),
    cleanup((clear_model, set_prolog_flag(verbose, normal)))
  ]
) :-
  command(add_reaction(a => b)),
  command(add_reaction(b => a)),
  command(add_conservation(a + b)),
  with_output_to(atom(_), command(check_conservations)).


test(
  'check_conservations is ok with kinetically conserved moieties',
  [
    setup((clear_model, set_prolog_flag(verbose, silent))),
    cleanup((clear_model, set_prolog_flag(verbose, normal)))
  ]
) :-
  command(add_reaction(k*[a] for a => b)),
  command(add_reaction(k*[a] for a => 2*a)),
  command(add_conservation(a)),
  with_output_to(atom(_), command(check_conservations)).


% intercept trusted but no proof message
user:message_hook(conservation_trusted(_), warning, _).


test(
  'check_conservations is unhappy with non-conserved moieties',
  [
    setup((clear_model, set_prolog_flag(verbose, silent))),
    cleanup((clear_model, set_prolog_flag(verbose, normal))),
    true(Warnings == 1)
  ]
) :-
  set_counter(warnings, 0),
  command(add_reaction(2*a => b)),
  command(add_reaction(a => b)),
  command(add_conservation(a + 2*b)),
  with_output_to(atom(_), command(check_conservations)),
  count(warnings, Warnings).


test(
  'search_conservations',
  [
    condition(flag(slow_test, true, true)),
    forall(
      member(
        (Model, NInvar),
        [
          ('mapk/BIOMD0000000009.xml', 7),
          ('tests/gene_expression.bc', 3)
        ]
      )
    ),
    true(InvarNumber = NInvar)
  ]
) :-
  atom_concat('library:examples/', Model, File),
  load(File),
  with_output_to(string(Invariants), command(search_conservations)),
  split_string(Invariants, "\n", "", InvarList),
  length(InvarList, InvarNumberPlusTwo),
  InvarNumber is InvarNumberPlusTwo - 2.


:- end_tests(conservation_laws).


set_some_conservations :-
  clear_model,
  command(add_conservation(a-a + 2*a)),
  command(add_conservation(b-b + 2*b)),
  command(add_conservation(c-c + 2*c)).


% vi: set ts=2 sts=2 sw=2
