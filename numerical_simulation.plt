:- use_module(library(plunit)).

:- begin_tests(numerical_simulation, [setup((clear_model, reset_options))]).


test('mapk', [condition(flag(slow_test, true, true))]) :-
  clear_model,
  command(load(library:examples/mapk/mapk)),
  command(numerical_simulation(time:100)).

test('mapk Rosenbrock', [condition(flag(slow_test, true, true))]) :-
  clear_model,
  command(load(library:examples/mapk/mapk)),
  command(numerical_simulation(time:100, method: rsbk)).

test('events') :-
  clear_model,
  command('MA'(k) for a => b),
  command(parameter(k = 1)),
  command(add_event(b > 0.5, k = 0)),
  command(add_event('Time' > 1, k = 0)),
  command(present(a)),
  command(numerical_simulation(time: 2)).

test('events Rosenbrock') :-
  clear_model,
  command('MA'(k) for a => b),
  command(parameter(k = 1)),
  command(add_event(b > 0.5, k = 0)),
  command(present(a)),
  command(numerical_simulation(method: rsbk)).

test('conditional') :-
  clear_model,
  command(if a < 0.5 then 'MA'(2) else 'MA'(1) for a => b),
  command(present(a)),
  command(numerical_simulation(initial_step_size:0.001)).

test('conditional Rosenbrock') :-
  clear_model,
  command(if a < 0.5 then 'MA'(2) else 'MA'(1) for a => b),
  command(present(a)),
  command(numerical_simulation(initial_step_size:0.001 ,method:rsbk)).

:- end_tests(numerical_simulation).
