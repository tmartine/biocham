:- module(
  sepi_sat,
  [
    % Commands
    solve_sat/9
  ]
).


:- use_module(biocham).
:- use_module(sepi_infos).
:- use_module(sepi_core).
:- use_module(sat).


%%  SAT Solver part
%%  Call the sat solver and write the solution once all clauses are written in the Stream

%! solve_sat(+G1, +G2, +FileIn, +Reductions, +Extremal_sepi, +Merge, +MaxReduc, +NbDelete, -NbReductions)
%
%  @arg G1          graph 1 [NbVertexe, NbSpecie, EdgesList, Id]
%  @arg G2          graph 2 [NbVertexe, NbSpecie, EdgesList, Id]
%  @arg FileIn      input file for sat solver
%  @arg Reductions  yesno yes if looking for all reductions between G1 and G2
%  @arg Extremal_sepi      no/minimal_deletion/maximal_deletion cf. sepi:extremal_sepi/1
%  @arg Merge       yes if merge reduction
%  @arg MaxReduc    limit the numer of reductions
%  @arg NbDelete    number of deletion in the solution
%  @arg NbReductions number of reductions
solve_sat(_G1, _G2, _FileIn, _Reductions, _Extremal_sepi, _Merge, 0, _NbDeleteInit, 0) :-
  write('no sepi found\n'),
  !.

solve_sat(G1, G2, FileIn, Reductions, Extremal_sepi, Merge, MaxReduc, NbDeleteInit, NbReductions) :-
  run_sat(FileIn, Extremal_sepi, Output),
  (
    (
      Output = []
    ;
      Output = ['UNSAT']
    ;
      Extremal_sepi \= no,
      % we have already started min/maxing Extremal_sepi
      NbDeleteInit \= "-1",
      % we find a worse solution
      Output \= [_, NbDeleteInit | _]
    )
  ->
    write('no sepi found\n'),
    NbReductions = 0
  ;
    Output = [Sepi | OutputRest],
    (
      Extremal_sepi \= no
    ->
      OutputRest = [NbDelete | _]
    ;
      NbDelete = NbDeleteInit
    ),
    debug(sepi, "sepi found", []),
    write('sepi\n'),
    split_string(Sepi, " \n", " \n", Tabint),
    write_morph(Tabint, G1, G2),
    (
      Extremal_sepi = no
    ->
      true
    ;
      Extremal_sepi = minimal_deletion
    ->
      format("Number of deletions: ~w~n", [NbDelete])
    ;
      % maximal_deletion
      G1 = [N1,_, _, _],
      number_string(Number, NbDelete),
      NbDeleteMax is (N1 - Number),
      format("Number of deletions: ~w~n", [NbDeleteMax])
    ),
    (
      Reductions = yes
    ->
      debug(sepi, "Looking for other solutions", []),
      get_option(max_nb_reductions, MaxSol),
      NbSol is (MaxSol - MaxReduc + 1),
      debug(sepi, "MaxSol: ~w - MaxReduc: ~w - NbSol: ~w", [MaxSol, MaxReduc, NbSol]),
      add_solution(G1, G2, FileIn, Tabint, Extremal_sepi, Merge, NbSol),
      MaxSubReduc is MaxReduc - 1,
      solve_sat(G1, G2, FileIn, Reductions, Extremal_sepi,
        Merge, MaxSubReduc, NbDelete, NbSubReductions),
      NbReductions is NbSubReductions + 1
    ;
      NbReductions = 1
    )
  ).


write_morph(Tabint, G1, G2) :-
  write_morph(Tabint, G1, G2, 0).

% We might have more (auxiliary) variables, so stop when we are done with the
% morphism ones
write_morph(_, [N1 | _], [N2 | _], N) :-
  N is N1 * (N2 + 1),
  !.

write_morph([X | Tail], G1, G2, I) :-
  atom_number(X, Xnum),
  m_to_morph(Xnum, G1, G2),
  I1 is I + 1,
  write_morph(Tail, G1, G2, I1).


m_to_morph(I, G1, G2) :-
  get_option(show_support, Support),
  G1 = [N1,_, _, Id1],
  G2 = [N2,_, _, Id2],
  (
    I =< N1 * (N2 + 1),
    I > 0
  ->
    I_1 is I - 1,
    X1 is div(I_1, N2 + 1),
    species(E1, X1, Id1),
    (pretty_reaction(E1, Id1, Pr1) -> PrE1 = Pr1 ; PrE1 = E1),
    X2 is I_1 mod (N2 + 1),
    (
      X2 = N2
    ->
      (
        Support = no
      ->
        format("~s -> deleted~n", [PrE1])
      ;
        true
      )
    ;
      species(E2, X2, Id2),
      (
        Support = no
      ->
        (pretty_reaction(E2, Id2, Pr2) -> PrE2 = Pr2 ; PrE2 = E2),
        format("~s -> ~s~n", [PrE1, PrE2])
      ;
        string_concat("{", Tempo, PrE1),
        string_concat(Print, "}", Tempo)
      ->
        format("~s~n", [Print])
      ;
        format("~s~n", [PrE1])
      )
    )
  ;
    % negative value, i.e., non-matching, we do not care
    true
  ).


%%  Compute all reductions

%! add_solution(+G1, +G2, +FileIn, +Tabint, +Extremal_sepi, +Merge, +NbSol)
%
%  @arg G1      graph 1 [NbVertexe, NbSpecie, EdgesList, Id]
%  @arg G2      graph 2 [NbVertexe, NbSpecie, EdgesList, Id]
%  @arg FileIn  Input file for sat solver
%  @arg Tabint  previous sepi found
%  @arg Extremal_sepi  no/minimal_deletion/maximal_deletion cf. sepi:extremal_sepi/1
%  @arg Merge   yes if merge restriction
%  @arg NbSol   nb of solutions already found
add_solution(G1, G2, FileIn, Tabint, Extremal_sepi, Merge, NbSol) :-
  debug(sepi, "adding solution to input file", []),
  G1 = [N1, Ns1 | _],
  G2 = [N2 | _],
  open(FileIn, update, Stream1),
  write_header(G1, G2, Stream1, Extremal_sepi, Merge, NbSol),
  format(Stream1, "c ", []),
  close(Stream1),
  open(FileIn, append, Stream2),
  (
    Extremal_sepi = no
  ->
    true
  ;
    Top is (N1 + 1),
    format(Stream2, "~w ", [Top])
  ),
  get_option(distinct_species, Distinct_species),
  (
    Distinct_species = yes
  ->
    write_neg_sol(Stream2, Tabint, Ns1, N2)
  ;
    write_neg_sol(Stream2, Tabint, N1, N2)
  ),
  close(Stream2).


write_neg_sol(_, [], _, _).

write_neg_sol(Stream, [H | _], N1, N2) :-
  atom_number(H, Hnum),
  (Hnum is N1 * (N2 + 1); Hnum is -N1 * (N2 + 1)),
  H_ is -Hnum,
  format(Stream, '~d 0~n', H_).

write_neg_sol(Stream, [H | T], N1, N2) :-
  atom_number(H, Hnum),
  H_ is -Hnum,
  format(Stream, '~d ', [H_]),
  write_neg_sol(Stream, T, N1, N2).
