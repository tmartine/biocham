list_reduction:- use_module(library(plunit)).
:- use_module(quadratic_reduction).

:- begin_tests(quadratic_reduction, [setup((clear_model, reset_options))]).

test(polynomial_ODE_True) :-
  command(a => b),
  command(3*a^2 for a+a => c),
  with_output_to(atom(_), reduction:polynomial_ODE).

test(polynomial_ODE_False) :-
  command(a/b for a => b),
  with_output_to(atom(_), \+ reduction:polynomial_ODE).

test(generate_names) :-
  reduction:generate_names([a,'Out'], [[1,2],[1,0]], [aOut2, a]).

test(compute_number_species) :-
  reduction:compute_number_species([3,1],8,0),
  reduction:compute_number_species([0,3,0,1,0],8,3),
  reduction:compute_number_species([2,2,0],9,1).

test(generate_sufficient_variables1) :-
  PODE = [[[1,[1, 1]],[3,[0, 1]]],[[-1,[3, 0]]]],
  reduction:generate_sufficient_variables(PODE,Set),
  once(permutation(Set, [[1,0],[0,1],[2,0],[1,1],[3,0],[2,1]])).

test(generate_sufficient_variables2) :-
  PODE = [[[1,[3, 1]]],[[-1,[1, 3]]]],
  reduction:generate_sufficient_variables(PODE,Set),
  once(permutation(Set, [[1,0],[0,1],[2,0],[1,1],[0,2],[3,0],[1,2],[2,1],[0,3],
  [3,1],[2,2],[1,3],[3,2],[2,3]])).

test(generate_sufficient_variables3) :-
  PODE = [[[1,[0, 3]]],[[-1,[0, 2]]]],
  reduction:generate_sufficient_variables(PODE,Set),
  once(permutation(Set, [[1,0],[0,1],[0,2]])).

test(list_reduction_nomodif, [nondet]) :-
  reduction:list_reduction([2,[[[1,[1, 1]]],[[-1,[1, 0]]]],[1,2]],[a,b],[[1,0]],P,[a,b]),
  P = [2,[[[1,[1, 1]]],[[-1,[1, 0]]]],[1,2]].

test(list_reduction_simple, [nondet]) :-
  reduction:list_reduction([2,[[[1,[1, 2]]],[[-1,[1, 0]]]],[1,2]],[a,b],[[1,0]], P, L),
  P = [3,[[[1,[1,1,0]]],[[-2,[1,0,1]]],[[-1,[1,0,0]]]],[1,4,2]],
  L = [a, b2, b].

test(scan_order_multivar) :-
  reduction:scan_order_multivar([[1,[1, 0]],[1,[1, 1]]],2),
  reduction:scan_order_multivar([[1,[1, 2]],[1,[1, 1]]],3),
  reduction:scan_order_multivar([[1,[1, 2]],[1,[12, 1]]],13).

test(add_all_sons) :-
  reduction:add_all_sons([a, b],[a, b, c],[],[[a, b, c]]),
  reduction:add_all_sons([a],[a, b, c],[[a, c]],[[a, b]]),
  reduction:add_all_sons([a],[a, b, c],[],[[a, b], [a, c]]).

test(derivative_is_quadratic) :-
 reduction:derivative_is_quadratic([[1,0],[0,2]],
                             [[1,[0,0]],[1,[1,0]],[1,[1,2]],[1,[2,0]]]).

%%% Test of PIVP manipulation %%%

test(compute_derivative1) :-
  PODE = [ [[1,[0,1]],[2,[1,1]]] , [[3,[1,0]]] ],
  reduction:compute_derivative([1,1], PODE, Deriv),
  once(permutation(Deriv, [[3,[2,0]],[1,[0,2]],[2,[1,2]]])).

test(compute_derivative2) :-
  PODE = [ [[1,[0,1,0]]] , [[3,[1,0,0]],[2,[1,1,0]]] , [[-2,[1,1,0]]] ],
  reduction:compute_derivative([0,1,1], PODE, Deriv),
  once(permutation(Deriv, [[3,[1,0,1]],[2,[1,1,1]],[-2,[1,2,0]]])).

test(compute_derivative3) :-
  PODE = [ [[1,[0,1,0]]] , [[3,[0,2,0]]] , [[-2,[0,1,1]]] ],
  reduction:compute_derivative([0,1,1], PODE, Deriv),
  once(permutation(Deriv, [[1,[0,2,1]]])).

test(clean_writing) :-
  reduction:clean_writing(1.0*2, 2),
  reduction:clean_writing(0.0*2, 0),
  reduction:clean_writing(1.0*input, input),
  reduction:clean_writing(input^1, input),
  reduction:clean_writing(input^2, input^2).

test(convert_poly_to_ode) :-
  reduction:convert_poly_to_ode([[1, [1,1]]], [a,b], a*b),
  reduction:convert_poly_to_ode([[1, [1,1]], [-2, [0,2]]], [a,b], -2.0*b^2 + a*b).

:- end_tests(quadratic_reduction).
