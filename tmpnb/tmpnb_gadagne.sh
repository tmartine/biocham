#!/bin/bash

export TOKEN=$( head -c 30 /dev/urandom | xxd -p )
docker run -d --restart=always --net=host -e CONFIGPROXY_AUTH_TOKEN=$TOKEN --expose 8000 --name=proxy jupyter/configurable-http-proxy --default-target http://127.0.0.1:9999
# orchestrate.py options for culling (i.e. destroying) containers:
# --cull-max (14400, i.e. 4h) --cull-timeout (3600, i.e. 1h if idle)
docker run -d --restart=always --net=host -e CONFIGPROXY_AUTH_TOKEN=$TOKEN --name=tmpnb -v /var/run/docker.sock:/docker.sock jupyter/tmpnb python orchestrate.py --image='lifeware/biocham:latest' --pool-size=40 --command='start.sh biocham --notebook --NotebookApp.base_url=/{base_path} --NotebookApp.token={token} --ip=0.0.0.0 --port {port}'
