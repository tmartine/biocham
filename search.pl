:- module(
  search,
  [
    % grammar
    search_condition/1,
% sampling/1,
    % commands
    search_parameters/3,
    search_parameters/2,
   robustness/3,
   sensitivity/3,
   sensitivity/2,
   scan_parameters/5,
   scan_parameters/4
  ]
).


% for linting purposes
:- use_module(objects).
:- use_module(biocham).

%:- grammar(sampling).
%
%sampling(normal).
%
%sampling(hypercube).

search_condition((Formula, Event, Objective)) :-
  foltl(Formula),
  list(parameter_name=number, Event),
  list(variable_objective, Objective).

:- doc('The continuous satisfaction degree of an FO-LTL(Rlin) in a given trace with respect to the objective values for the free variables can be used in multiple ways either to compute parameter sensitivity indices and robustness measures with respect to parameter perturbations estimated by sampling, or to visualize the landscape in two dimensions, or to search parameter values in high parameter dimension to satisfying a formula \\cite{RBFS11tcs,FS18cmsb}.').

:- doc('Those commands involve numerical simulations which are executed by samplings around the current parameter values,
      and automatically parallelized by OpenMPI. The number of processors used is given by an option (by default 0 for automatic, temporarily replaced by 1 by default because of a bug on Ubuntu)').

:- initial('option(openmpi_procs: 1)').
:- initial('option(robustness_coeff_var: 0.1)').
%:- initial('option(sampling: normal)').
:- initial('option(robustness_samples: 30)').
:- initial('option(positive_sampling: yes)').


robustness(Formula, Parameters, Objectives) :-
  biocham_command,
  type(Formula, foltl),
  type(Parameters, [parameter_name]),
  type(Objectives, [variable_objective]),
  option(time, time, _Time, 'time horizon of the numerical integration'),
  option(method, method, _Method, 'method for the numerical solver'),
%  option(
%    sampling,
%    distribution,
%    Sampling,
%    'distribution used for sampling (default is normal distribution)'
%  ),
  option(
    robustness_coeff_var,
    number,
    Variation,
    'coefficient of variation (default 0.1, i.e. 10%)'
  ),
  option(
    positive_sampling,
    yesno,
    Positive,
    'taking absolute values of normal sampling (default yes)'
  ),
  option(
    robustness_samples,
    integer,
    Samples,
    'number of samples used for averaging (default 30)'
  ),
  option(
    openmpi_procs,
    integer,
    _NProc,
    'Number of processors to use with OpenMPI.'
  ),
  option(
    error_epsilon_absolute, number, _ErrorEpsilonAbsolute,
    'absolute error for the numerical solver'
  ),
  option(
    error_epsilon_relative, number, _ErrorEpsilonRelative,
    'relative error for the numerical solver'
  ),
  option(
    initial_step_size, number, _InitialStepSize,
    'initial step size for the numerical solver'
  ),
  option(
    maximum_step_size, number, _MaximumStepSize,
    'maximum step size for the numerical solver, as a fraction of time'
  ),
  option(
    precision, number, _Precision,
    'precision for the numerical solver'
  ),
  doc('
    computes the robustness degree defined as the mean satisfaction degree (truncated to at most 1) \\cite{RBFS11tcs},
      of an FOLTL formula \\argument{Formula}, 
     given with a list of \\argument{Objectives} given for each free variable of the formula (variables without objective should be existentially quantified)
    with respect to a variation of the 
     \\argument{Parameters} (i.e. extrinsic variability).'),
  doc('That list of parameters can also be empty to measure the robustness with respect to stochastic simulations (i.e. intrinsic variability).'),
  doc('
    The robustness is  estimated by sampling with a normal perturbation law 
       around the current parameter values according to the coefficient of variation option.
\\clearmodel
\\begin{example}
\\trace{
biocham: k for a => b. present(a,10). parameter(k=0.7).
biocham: robustness(G(Time < T => a > b), [k], [T -> 5]).
}
\\end{example}
  '),
  with_current_ode_system(
    with_clean(
      [
        numerical_simulation:variable/2,
        numerical_simulation:equation/2,
        numerical_simulation:parameter_index/2,
        numerical_simulation:conditional_event/2
      ],
      robustness_aux(Parameters, [(Formula, [], Objectives)], Samples,
        Variation, Positive)
    )
  ).

sensitivity(Formula, Parameters, Objective):-
  biocham_command,
  type(Formula, foltl),
  type(Parameters, [parameter_name]),
  type(Objective, [variable_objective]),
  option(time, time, _Time, 'time horizon of the numerical integration'),
  option(method, method, _Method, 'method for the numerical solver'),
%  option(
%    sampling,
%    distribution,
%    Sampling,
%    'distribution used for sampling (default is normal distribution)'
%  ),
  option(
    robustness_coeff_var,
    number,
    Variation,
    'coefficient of variation of the normal law (default 0.1)'
	),
  option(
    positive_sampling,
    yesno,
    Positive,
    'taking absolute values of normal sampling (default yes)'
  ),
  option(
    robustness_samples,
    integer,
    Samples,
    'number of samples used for averaging (default 30)'
  ),
  option(
    openmpi_procs,
    integer,
    _NProc,
    'Number of processors to use with OpenMPI.'
  ),
  option(
    error_epsilon_absolute, number, _ErrorEpsilonAbsolute,
    'absolute error for the numerical solver'
  ),
  option(
    error_epsilon_relative, number, _ErrorEpsilonRelative,
    'relative error for the numerical solver'
  ),
  option(
    initial_step_size, number, _InitialStepSize,
    'initial step size for the numerical solver'
  ),
  option(
    maximum_step_size, number, _MaximumStepSize,
    'maximum step size for the numerical solver, as a fraction of time'
  ),
  option(
    precision, number, _Precision,
    'precision for the numerical solver'
  ),
  doc('
    computes the sensitivity indices of each parameter in the list \\argument{Parameters}
     as the robustness degree of the formula \\argument{Formula}
     with variable-value objectives in \\argument{Objectives}.
     This command is just a shorthand for calling \\command{robustness} repeatedly on each single parameter in the list.'),
  with_current_ode_system(
    with_clean(
      [
        numerical_simulation:variable/2,
        numerical_simulation:equation/2,
        numerical_simulation:parameter_index/2,
        numerical_simulation:conditional_event/2
      ],
      forall(
        member(Parameter, Parameters),
        (
          write('Sensitivity to parameter '), writeln(Parameter),
          robustness_aux([Parameter], [(Formula, [], Objective)], Samples, Variation, Positive)
        )
      )
    )
  ).



sensitivity(Formula, Objective):-
  biocham_command,
  type(Formula, foltl),
  type(Objective, [variable_objective]),
  option(time, time, _Time, 'time horizon of the numerical integration'),
  option(method, method, _Method, 'method for the numerical solver'),
%  option(
%    sampling,
%    distribution,
%    Sampling,
%    'distribution used for sampling (default is normal distribution)'
%  ),
  option(
    robustness_coeff_var,
    number,
    Variation,
    'coefficient of variation of the normal law (default 0.1)'
  ),
  option(
    positive_sampling,
    yesno,
    Positive,
    'taking absolute values of normal sampling (default yes)'
  ),
  option(
    robustness_samples,
    integer,
    Samples,
    'number of samples used for averaging (default 30)'
  ),
  option(
    openmpi_procs,
    integer,
    _NProc,
    'Number of processors to use with OpenMPI.'
  ),
  option(
    error_epsilon_absolute, number, _ErrorEpsilonAbsolute,
    'absolute error for the numerical solver'
  ),
  option(
    error_epsilon_relative, number, _ErrorEpsilonRelative,
    'relative error for the numerical solver'
  ),
  option(
    initial_step_size, number, _InitialStepSize,
    'initial step size for the numerical solver'
  ),
  option(
    maximum_step_size, number, _MaximumStepSize,
    'maximum step size for the numerical solver, as a fraction of time'
  ),
  option(
    precision, number, _Precision,
    'precision for the numerical solver'
  ),
  doc('
    computes the sensitivity indices of all parameters of the model. This command is a shorthand for calling the previous command with all parameters.'),
  with_current_ode_system(
    with_clean(
      [
        numerical_simulation:variable/2,
        numerical_simulation:equation/2,
        numerical_simulation:parameter_index/2,
        numerical_simulation:conditional_event/2
      ],
      forall(
        item([kind:parameter, key:Parameter]),
        (write('Sensitivity to parameter '),
         writeln(Parameter),
         robustness_aux([Parameter], [(Formula, [], Objective)], Samples, Variation, Positive)
        )
      )
    )
  ).


:- initial('option(cmaes_init_center: no)').
:- initial('option(cmaes_log_normal: no)').
:- initial('option(cmaes_stop_fitness: 0.0001)').


search_parameters(Formula, Parameters, Objective) :-
  biocham_command,
  type(Formula, foltl),
  type(Parameters, [parameter_between_interval]),
  type(Objective, [variable_objective]),
  option(time, time, _Time, 'time horizon of the numerical integration'),
  option(method, method, _Method, 'method for the numerical solver'),
  option(
    error_epsilon_absolute, number, _ErrorEpsilonAbsolute,
    'absolute error for the numerical solver'
  ),
  option(
    error_epsilon_relative, number, _ErrorEpsilonRelative,
    'relative error for the numerical solver'
  ),
  option(
    initial_step_size, number, _InitialStepSize,
    'initial step size for the numerical solver'
  ),
  option(
    maximum_step_size, number, _MaximumStepSize,
    'maximum step size for the numerical solver, as a fraction of time'
  ),
  option(
    precision, number, _Precision,
    'precision for the numerical solver'
  ),
  option(
    cmaes_init_center, yesno, _CmaesInit,
    'initialize parameter values at the center of their domain instead of
    their current value'
  ),
  option(
    cmaes_log_normal, yesno, _Log,
    'search on the log of the parameters (better if very different orders of
    magnitude)'
  ),
  option(
    cmaes_stop_fitness, number, _Fitness,
    'stop when distance to the objective is less than this value (use 0 by default and 
    negative values in [-1,0] to maximize margins)'
  ),
  option(
    openmpi_procs,
    integer,
    _NProc,
    'Number of processors to use with OpenMPI.'
  ),
  doc('
    tries to satisfy a FO-LTL(Rlin) constraint by varying the parameters listed
    in \\argument{Parameters}.
    This search command uses the stochastic optimization procedure CMA-ES
    (covariance matrix adaptation evolution strategy)
    with the continuous satisfaction degree (truncated to 1 or not according to stop option) of the given FO-LTL(Rlin) property as
    fitness function.
\\clearmodel
\\begin{example}
\\trace{
biocham: k for a => b. present(a,10).
biocham: search_parameters(G(Time < T => a > b), [0 <= k <= 2], [T -> 5]).
biocham: search_parameters(G(Time < T => a > b), [0 <= k <= 2], [T -> 5]).
biocham: search_parameters(G(Time < T => a > b), [0 <= k <= 2], [T -> 5], cmaes_stop_fitness: -0.1).
}
\\end{example}
  '),
  with_current_ode_system(
    with_clean(
      [
        numerical_simulation:variable/2,
        numerical_simulation:equation/2,
        numerical_simulation:parameter_index/2,
        numerical_simulation:conditional_event/2
      ],
      search_parameters_aux(Parameters, [(Formula, [], Objective)], Result)
    )
  ),
  set_parameters(Result),
  find_parameter_ids(Parameters, ParameterIds),
  list_ids(ParameterIds).


search_parameters(Parameters, Conditions) :-
  biocham_command,
  type(Parameters, [parameter_between_interval]),
  type(Conditions, [search_condition]),
  option(time, time, _Time, 'time horizon of the numerical integration'),
  option(method, method, _Method, 'method for the numerical solver'),
  option(
    error_epsilon_absolute, number, _ErrorEpsilonAbsolute,
    'absolute error for the numerical solver'
  ),
  option(
    error_epsilon_relative, number, _ErrorEpsilonRelative,
    'relative error for the numerical solver'
  ),
  option(
    initial_step_size, number, _InitialStepSize,
    'initial step size for the numerical solver'
  ),
  option(
    maximum_step_size, number, _MaximumStepSize,
    'maximum step size for the numerical solver, as a fraction of time'
  ),
  option(
    precision, number, _Precision,
    'precision for the numerical solver'
  ),
  option(
    cmaes_init_center, yesno, _CmaesInit,
    'initialize parameter values at the center of their domain instead of
    their current value'
  ),
  option(
    cmaes_log_normal, yesno, _Log,
    'search on the log of the parameters (better if very different orders of
    magnitude)'
  ),
  option(
    cmaes_stop_fitness, number, _Fitness,
    'stop when distance to the objective is less than this value (use
    negative to enforce robustness)'
  ),
  option(
    openmpi_procs,
    integer,
    _NProc,
    'Number of processors to use with OpenMPI.'
  ),
  doc('similar to \\command{search_parameters/3} but uses the list of
    \\argument{Conditions} as triples with an FOLTL formula, a list of parameters instantiations (e.g. describing a mutant), and an objective for the
    variables of the formula.
\\clearmodel
\\begin{example}'),
  biocham_silent('option(time: 20)'),
  biocham('ka for _ => a'),
  biocham('kb for _ => b'),
  biocham('parameter(ka=1, kb=1)'),
  biocham('search_parameters([0 <= ka <= 3], [(G(a-b<x/\\b-a<x), [], [x -> 10]), (G(a-b<y/\\b-a<y), [kb=2], [y -> 10])])'),
  doc('\\end{example}'),
  with_current_ode_system(
    with_clean(
      [
        numerical_simulation:variable/2,
        numerical_simulation:equation/2,
        numerical_simulation:parameter_index/2,
        numerical_simulation:conditional_event/2
      ],
      search_parameters_aux(Parameters, Conditions, Result)
    )
  ),
  set_parameters(Result),
  find_parameter_ids(Parameters, ParameterIds),
  list_ids(ParameterIds).


% store free_variables for each formula
:- dynamic(free_variable_index/3).


search_parameters_aux(Parameters, Conditions, Result) :-
  retractall(free_variable_index(_, _, _)),
  GslCFilename = 'ode.inc',
  DomainCFilename = 'check.inc',
  SearchParametersCFilename = 'search_parameters.inc',
  statistics(walltime, _),
  ExecutableFilename = 'search',
  ensure_parameters(Parameters),
  prepare_numerical_simulation_options(Options),
  with_output_to_file(GslCFilename, write_gsl(Options)),
  nb_setval(total_free_vars, 0),
  with_clean(
    [foltl:column/2],
    (
      populate_field_columns(Options),
      with_output_to_file(DomainCFilename,
        forall(
          nth0(Index, Conditions, (Formula, _, _)),
          (
            expand_formula(Formula, ExpandedFormula),
            atom_number(AIndex, Index),
            generate_domain_cpp(ExpandedFormula, AIndex),
            forall(
              foltl:free_variable_index(X, Y),
              assertz(free_variable_index(Index, X, Y))
            ),
            foltl:real_free_vars(RFV),
            nb_getval(total_free_vars, TFV),
            NTFV is TFV + RFV,
            nb_setval(total_free_vars, NTFV)
          )
        )
      )
    )
  ),
  findall(
    Objective,
    member((_, _, Objective), Conditions),
    Objectives
  ),
  % use our indexed free_var_index/3
  retractall(foltl:free_variable_index(_, _)),
  retractall(foltl:real_free_vars(_)),
  nb_getval(total_free_vars, TotalRFV),
  assertz(foltl:real_free_vars(TotalRFV)),
  with_output_to_file(
    SearchParametersCFilename,
    (
      (
        debugging(search)
      ->
        writeln('#define DEBUG_SEARCH')
      ;
        true
      ),
      generate_random_seed,
      generate_conditions(Conditions),
      generate_search_parameters(Parameters),
      generate_objective(Objectives)
    )
  ),
  compile_search_cpp_program(ExecutableFilename),
  mpi_args(ExecutableFilename, MPIArgs),
  call_subprocess(
    path('mpirun'), MPIArgs, [stdout(pipe(ResultStream))]
  ),
  statistics(walltime, [_, MilliTime]),
  Time is MilliTime / 1000,
  format('Time: ~p s\n', [Time]),
  read_stop_reason(ResultStream, StopReason),
  format('Stopping reason: ~w~n', [StopReason]),
  read_parameter_values(Parameters, ResultStream, Result),
  read_satisfaction_degree(ResultStream, Degree),
  format('Best satisfaction degree: ~p~n', [Degree]),
  (
    have_to_delete_temporary_files
  ->
    delete_file(GslCFilename),
    delete_file(DomainCFilename),
    delete_file(SearchParametersCFilename),
    delete_file(ExecutableFilename)
  ;
    true
  ).


%! mpi_args(+ExecutableFilename, -MPIFullArgs) is det.
%
% gather arguments for MPI
% because reading the result might fail on some MPI warnings -> -q
mpi_args(ExecutableFilename, ['-q' | MPIFullArgs]) :-
  get_option(openmpi_procs, NProc),
  (
    NProc > 0
  ->
    MPIArgs = ['-n', NProc, ExecutableFilename]
  ;
    MPIArgs = [ExecutableFilename]
  ),
  (
    getuid(0)
  ->
    MPIFullArgs = ['--allow-run-as-root' | MPIArgs]
  ;
    MPIFullArgs = MPIArgs
  ).


% Prolog version of robustness computation for Prolog methods
robustness_aux(
  ParameterList, [(Formula, [], Objective)], NSamples,
  VariationCoefficient, Positive
) :-
  get_option(method, M),
  gsl_methods(GSL),
  \+ member(M, GSL),
  !,
  nb_setval(robustness_mean, 0),
  nb_setval(robustness_ssd, 0),
  store_parameters(ParameterList),
  statistics(walltime, _),
  (
    between(1, NSamples, J),
    debug(robustness, "Sample ~w~n", [J]),
    normal_sample(ParameterList, VariationCoefficient, Positive),
    numerical_simulation,
    debug(robustness, "Satisfaction for ~w with ~w~n", [Formula, Objective]),
    (
      J == 1
    ->
      Mode = first
    ;
      J == NSamples
    ->
      Mode = last
    ;
      Mode = loop
    ),
    satisfaction_degree(Formula, Objective, Degree, Mode),
    debug(robustness, "Satisfaction: ~w~n", [Degree]),
    nb_getval(robustness_mean, Mean),
    % nb_getval(robustness_ssd, Ssd),
    Delta is min(1.0, Degree) - Mean,
    NMean is Mean + Delta / J,
    nb_setval(robustness_mean, NMean),
    debug(robustness, "New Satisfaction mean: ~w~n", [NMean]),
    % Delta2 is Degree - NMean,
    % NSsd is Ssd + Delta * Delta2,
    % nb_setval(robustness_ssd, NSsd),
    fail
  ;
    nb_getval(robustness_mean, Degree)
  ),
  statistics(walltime, [_, MilliTime]),
  restore_parameters,
  Time is MilliTime / 1000,
  format('Time: ~p s\n', [Time]),
  format('Robustness degree: ~p~n', [Degree]).

robustness_aux(ParameterList, Conditions, Samples, Variation, Positive) :-
  retractall(free_variable_index(_, _, _)),
  GslCFilename = 'ode.inc',
  DomainCFilename = 'check.inc',
  SearchParametersCFilename = 'search_parameters.inc',
  ExecutableFilename = 'robustness',
  statistics(walltime, _),
  findall(0 <= X <= 0, member(X, ParameterList), Parameters),
  ensure_parameters(Parameters),
  prepare_numerical_simulation_options(Options),
  with_output_to_file(GslCFilename, write_gsl(Options)),
  with_clean(
    [foltl:column/2],
    (
      populate_field_columns(Options),
      with_output_to_file(DomainCFilename,
        forall(
          nth0(Index, Conditions, (Formula, _, _)),
          (
            expand_formula(Formula, ExpandedFormula),
            atom_number(AIndex, Index),
            generate_domain_cpp(ExpandedFormula, AIndex),
            forall(
              foltl:free_variable_index(X, Y),
              assertz(free_variable_index(Index, X, Y))
            )
          )
        )
      )
    )
  ),
  findall(
    Objective,
    member((_, _, Objective), Conditions),
    Objectives
  ),
  % use our indexed free_var_index/3
  retractall(foltl:free_variable_index(_, _)),
  with_output_to_file(
    SearchParametersCFilename,
    (
      (
        debugging(robustness)
      ->
        writeln('#define DEBUG_ROBUSTNESS')
      ;
        true
      ),
      generate_random_seed,
      generate_sample_number_and_var(Samples, Variation, Positive),
      generate_conditions(Conditions),
      generate_search_parameters(Parameters),
      generate_objective(Objectives)
    )
  ),
  compile_robustness_cpp_program(ExecutableFilename),
  mpi_args(ExecutableFilename, MPIFullArgs),
  call_subprocess(
    path('mpirun'), MPIFullArgs, [stdout(pipe(ResultStream))]
  ),
  statistics(walltime, [_, MilliTime]),
  Time is MilliTime / 1000,
  format('Time: ~p s\n', [Time]),
  read_satisfaction_degree(ResultStream, Degree),
  format('Robustness degree: ~p~n', [Degree]),
  (
    have_to_delete_temporary_files
  ->
    delete_file(GslCFilename),
    delete_file(DomainCFilename),
    delete_file(SearchParametersCFilename),
    delete_file(ExecutableFilename)
  ;
    true
  ).


:- dynamic(parameters_initial_value/2).


store_parameters(L) :-
  retractall(parameters_initial_value(_, _)),
  maplist(store_parameter, L).

store_parameter(K) :-
  parameter_value(K, V),
  !,
  assertz(parameters_initial_value(K, V)).


restore_parameters :-
  forall(
    retract(parameters_initial_value(P, V)),
    set_parameter(P, V)
  ).


normal_sample([], _, _).

normal_sample([P | L], Rho, Pos) :-
  debug(robustness, "Sampling for ~w~n", [P]),
  parameters_initial_value(P, P0),
  normal_random(X),
  Normal is (Rho * P0) * X + P0,
  ((Pos=yes, Normal<0)
  ->
   Value is -Normal
  ;
   Value =Normal
  ),
  debug(robustness, "~w with ~w gives ~w~n", [P0, Rho, Value]),
  set_parameter(P, Value),
  normal_sample(L, Rho, Pos).


generate_random_seed :-
  (
    toplevel:random_seed(Seed)
  ->
    NewSeed is Seed + 1
  ;
    NewSeed = 0
  ),
  format('#define SEED ~d~n', [NewSeed]).


generate_sample_number_and_var(Samples, Variation, Positive) :-
  format('#define SAMPLES ~d~n', [Samples]),
  format('#define COEFF_VAR ~p~n', [Variation]),
  (Positive=yes -> Pos=1 ; Pos=0),
  format('#define POSITIVE_SAMPLING ~p~n', [Pos]).


generate_conditions(Conditions) :-
  length(Conditions, N),
  format('#define CONDITIONS ~d\n', [N]),
  MaxCond is N - 1,
  write('static Domain (*compute_condition_domain[CONDITIONS]) (const Table &) = {\n'),
  forall(
    between(0, MaxCond, I),
    format('    compute_domain~d,~n', [I])
  ),
  write('};\n'),
  forall(
    nth0(Index, Conditions, (_, Mutant, _)),
    setup_condition(Index, Mutant)
  ),
  write('static void (*setup_condition[CONDITIONS]) (gsl_solver*) = {\n'),
  forall(
    between(0, MaxCond, I),
    format('    setup_condition~d,~n', [I])
  ),
  write('};\n').


setup_condition(Index, Mutant) :-
  format('static void setup_condition~d (gsl_solver *solver) {~n', [Index]),
  maplist(numerical_simulation:convert_parameter, Mutant, IndexedMutant),
  forall(
    member(ParamIndex = Value, IndexedMutant),
    % Value should be a number
    format('    solver->p[~d] = ~w;~n', [ParamIndex, Value])
  ),
  write('};\n').


generate_search_parameters(Parameters) :-
  get_option(cmaes_init_center, Center),
  length(Parameters, ParameterCount),
  format('#define DIMENSION ~d\n', [ParameterCount]),
  get_option(cmaes_stop_fitness, Fitness),
  format('#define STOPFIT ~p\n', [Fitness]),
  get_option(cmaes_log_normal, Log),
  (
    Log == yes
  ->
    format('#define LOGNORMAL true\n')
  ;
    format('#define LOGNORMAL false\n')
  ),
  write('\c
static const struct search_parameter search_parameters[DIMENSION] = {\n'),
  \+ (
    member(Min <= Parameter <= Max, Parameters),
    \+ (
      numerical_simulation:parameter_index(Parameter, ParameterIndex),
      (
        Center == yes
      ->
        Value is (Min + Max)/2
      ;
        parameter_value(Parameter, Value)
      ),
      (
        Log == yes
      ->
        (
          is_null(Min) %avoid a log(0) error by setting LMin to smallest precision
        ->
          print_message(warning, arithmetic('Lower bound set to 10^-Precision to avoid arithmetic error.')),
          get_option(precision, Prec),
          LMin is log(10^ -Prec)
        ;
          LMin is log(Min)
        ),
        LMax is log(Max),
        LVal is log(Value),
        format('    {~f, ~d, ~f, ~f},\n', [LMin, ParameterIndex, LMax, LVal])
      ;
        format('    {~f, ~d, ~f, ~f},\n', [Min, ParameterIndex, Max, Value])
      )
    )
  ),
  write('};\n').


compile_search_cpp_program(ExecutableFilename) :-
  gsl_compile_options(GslCompileOptions),
  ppl_compile_options(PplCompileOptions),
  flatten(
    [GslCompileOptions, PplCompileOptions],
    AllCompileOptions
  ),
  compile_cpp_program(
    ['search_parameters.cc', 'gsl_solver.o', 'cmaes.o'],
    AllCompileOptions,
    ExecutableFilename
  ).


compile_robustness_cpp_program(ExecutableFilename) :-
  gsl_compile_options(GslCompileOptions),
  ppl_compile_options(PplCompileOptions),
  flatten(
    [GslCompileOptions, PplCompileOptions],
    AllCompileOptions
  ),
  compile_cpp_program(
    ['robustness.cc', 'gsl_solver.o'],
    AllCompileOptions,
    ExecutableFilename
  ).


ensure_parameters(Parameters) :-
  forall(
    member(Min <= Parameter <= _Max, Parameters),
    (
      parameter_value(Parameter, _)
    ->
      true
    ;
      set_parameter(Parameter, Min)
    )
  ).


find_parameter_ids([], []).

find_parameter_ids([_Min <= Parameter <= _Max | Tail], [Id | IdTail]) :-
  find_item([key: Parameter, id: Id]),
  find_parameter_ids(Tail, IdTail).


populate_field_columns(Options) :-
  memberchk(fields: Fields, Options),
  \+ (
    nth0(Index, Fields, Field: _),
    \+ (
      assertz(foltl:column(Field, Index))
    )
  ).



read_stop_reason(Stream, Reason) :-
  read_line_to_string(Stream, Reason).


read_parameter_values([], _ResultStream, []).

read_parameter_values(
  [_Min <= Parameter <= _Max | ParameterTail], ResultStream,
  [Parameter = Value | ResultTail]
) :-
  read_line_to_codes(ResultStream, ValueCode),
  debug(search, "read param ~s~n", [ValueCode]),
  number_codes(Value, ValueCode),
  read_parameter_values(ParameterTail, ResultStream, ResultTail).


read_satisfaction_degree(Stream, Degree) :-
  read_line_to_codes(Stream, Codes),
  debug(foltl, "read degree ~s~n", [Codes]),
  number_codes(Degree, Codes).



				% Parameter scanning for landscape visualization

:- initial('option(resolution: 10)').


scan_parameters(Formula, Parameter1, Parameter2, Objective) :-
  biocham_command,
  type(Formula, foltl),
  type(Parameter1, parameter_between_interval),
  type(Parameter2, parameter_between_interval),
  type(Objective, [variable_objective]),
  option(resolution, number, _Scans, 'number of values tried for each parameter (default is 10)'),
  option(time, time, _Time, 'time horizon of the numerical integration'),
  option(method, method, _Method, 'method for the numerical solver'),
  option(
    error_epsilon_absolute, number, _ErrorEpsilonAbsolute,
    'absolute error for the numerical solver'
  ),
  option(
    error_epsilon_relative, number, _ErrorEpsilonRelative,
    'relative error for the numerical solver'
  ),
  option(
    initial_step_size, number, _InitialStepSize,
    'initial step size for the numerical solver'
  ),
  option(
    maximum_step_size, number, _MaximumStepSize,
    'maximum step size for the numerical solver, as a fraction of time'
  ),
  option(
    precision, number, _Precision,
    'precision for the numerical solver'
  ),
  option(
    openmpi_procs,
    integer,
    _NProc,
    'Number of processors to use with OpenMPI.'
  ),
  doc('
    returns some statistics and draws the landscape of the satisfaction degrees (truncated to 1) of a FO-LTL(Rlin) formula obtained by varying two parameters in intervals given. Note that the number of calls to numerical integration is the square of the resolution option value \\texttt{N}. By default the image and the data are saved in files \\texttt{landscape_Parameter1_Parameter2_N.png} and \\texttt{.txt} respectively. Note that the drawing shows an overapproximation of the best values found for the parameters. The precise values are available in file  \\texttt{landscape_Parameter1_Parameter2_N.txt}.
 \\clearmodel
 \\begin{example}
 \\trace{
 biocham: MA(k)for a => b. present(a,10).
 biocham: MA(l) for b => a.
 biocham: scan_parameters(F(Time > T /\\ a > b), (0 <= k <= 2), (0 <= l <= 2), [T -> 5], resolution:3).
 }
 \\end{example}
     '),

  get_option(resolution, N),
  Parameter1 = (_Min1 <= P1 <= _Max1),
  Parameter2 = (_Min2 <= P2 <= _Max2),
  atomic_list_concat(['landscape', '_', P1, '_', P2, '_', N], Filename),
  scan_parameters(Formula, Parameter1, Parameter2, Objective, Filename).



scan_parameters(Formula, Parameter1, Parameter2, Objective, Filename) :-
  biocham_command,
  type(Formula, foltl),
  type(Parameter1, parameter_between_interval),
  type(Parameter2, parameter_between_interval),
  type(Objective, [variable_objective]),
  type(Filename, output_file),
  option(resolution, number, _Scans, 'number of values tried for each parameter (default is 10)'),
  option(time, time, _Time, 'time horizon of the numerical integration'),
  option(method, method, _Method, 'method for the numerical solver'),
  option(
    error_epsilon_absolute, number, _ErrorEpsilonAbsolute,
    'absolute error for the numerical solver'
  ),
  option(
    error_epsilon_relative, number, _ErrorEpsilonRelative,
    'relative error for the numerical solver'
  ),
  option(
    initial_step_size, number, _InitialStepSize,
    'initial step size for the numerical solver'
  ),
  option(
    maximum_step_size, number, _MaximumStepSize,
    'maximum step size for the numerical solver, as a fraction of time'
  ),
  option(
    precision, number, _Precision,
    'precision for the numerical solver'
  ),
  option(
    openmpi_procs,
    integer,
    _NProc,
    'Number of processors to use with OpenMPI.'
  ),
  doc('
    specifies in addition to save 
     the image in file \\texttt{filename.png} and the data in file \\texttt{filename.txt}.
     '),

  
  atom_concat(Filename, '.txt', Land_data),
  (exists_file(Land_data) -> delete_file(Land_data) ; true),
  open(Land_data, write, TXT),

  statistics(walltime, _),

% TODO: parallelization
  get_option(resolution, N),
  
  \+ scan_parameters(Parameter1, Parameter2, N, Formula, Objective, TXT),
  nl(TXT),
  flush_output(TXT),
  close(TXT),

  
  statistics(walltime, [_, MilliTime]),
  Time is MilliTime / 1000,
  format('Time: ~p s\n', [Time]),

  atom_concat(Filename, '.plot', Land_plot),
  (exists_file(Land_plot) -> delete_file(Land_plot) ; true),
  open(Land_plot,write,Gnuplot),

  atom_concat(Filename, '.png', Land_png),

  Parameter1 = (Min1 <= P1 <= Max1),
  Parameter2 = (Min2 <= P2 <= Max2),

%set tics scale default
%set format '%.5g'
%set format '%.2e'
  format(Gnuplot, "
set terminal pngcairo transparent enhanced font 'arial,8' fontscale 1.0 size 600, 400 
set output '~w'
unset key

set format '%g'
set xtics format '%.1e'
set ytics format '%.1e'
	
set palette negative
set cblabel 'Satisfaction' 
set cbrange [ 0 : 1 ] noreverse nowriteback

set xrange [ ~f:~f ] noreverse nowriteback
set yrange [ ~f:~f ] noreverse nowriteback
set xlabel '~w'
set ylabel '~w'

set view map

plot '~w' with image
	  ",
	[Land_png, Min1,Max1,Min2,Max2, P1,P2, Land_data]),

  flush_output(Gnuplot),
  close(Gnuplot),
  
  execute_plot(['-persist', file(Land_plot)]),
  delete_file(Land_plot),

  stat_distribution(satisfaction, Mean, _ , Deviation),
  stat_max(satisfaction, Max),
  format("Mean satisfaction (robustness): ~f, standard deviation: ~f\n", [Mean, Deviation]),

  stat_min(P1, MinP1),
  stat_max(P1, MaxP1),
  stat_min(P2, MinP2),
  stat_max(P2, MaxP2),
  format("Maximum satisfaction: ~f\nBest solutions found with ~f<=~w<=~f, ~f<=~w<=~f\n", [Max, MinP1,P1,MaxP1,MinP2,P2,MaxP2]),
  
  view_image(Land_png).


scan_parameters((Min1 <= P1 <= Max1), (Min2 <= P2 <= Max2), N, Formula, Objective, TXT):-
  N2 is N*N,
  clear_stat(satisfaction),
  compute_distribution(satisfaction, N2),
  compute_max(satisfaction),

  clear_stat(P1),
  compute_min(P1),
  compute_max(P1),
  
  clear_stat(P2),
  compute_min(P2),
  compute_max(P2),

  format(TXT,"# ~w ~w sat",[P1, P2]),
  between(1,N,I),
  V1 is Min1+(I-1.0)*(Max1-Min1)/(N-1.0),
  set_parameter(P1, V1),
  nl(TXT),

  between(1,N,J),
  V2 is Min2+(J-1.0)*(Max2-Min2)/(N-1.0),
  set_parameter(P2, V2),
  
  numerical_simulation,
  satisfaction_degree(Formula, Objective, Degree),
  Satisfaction is min(Degree, 1),
  format(TXT, "~f ~f ~f\n", [V1,V2,Satisfaction]), %format(TXT, "~f ", [Satisfaction]),

  % isn't it clever ?
  new_sample(
	     (
	      new_max(satisfaction, Satisfaction)
	     ->
	      compute_min(P1),
	      compute_max(P1),
	      compute_min(P2),
	      compute_max(P2),
	      new_sample(P1, V1),
	      new_sample(P2, V2)
	     ;
	      (
	       same_max(satisfaction, Satisfaction)
	      ->
	       new_sample(P1, V1),
	       new_sample(P2, V2)
	      ;
	       true
	      )
	     )
	    ),
	     
  new_sample(satisfaction,Satisfaction),
  
  fail.
  
