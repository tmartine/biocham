:- use_module(library(plunit)).

:- begin_tests(odefunction, [setup((clear_model, reset_options))]).


test('add_function'):-
  clear_model,
  command(present(x,3)),
  command(present(y,2)),
  command(parameter(k=0.00001)),
  command(k*[x] for x=>y),
  command(add_function(z=x+y, u=x*y)).

:- end_tests(odefunction).
