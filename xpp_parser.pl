/* XPP PARSING
This biocham module is intented to read and write xpp file [bard] as biocham ode_system.
The main command are thus read_xpp and export_xpp, they may be called directly from the
model level while loading/writing file with the .xpp extension.
The file format and order is of the form:

# comments
 option  <filename>
d<name>/dt=<formula> OR <name>'=<formula>
<name>(t)=<formula>
<name>(t+1)=<formula>
<name>=<formula>
<name>(<x1>,<x2>,...,<xn>)=<formula>
init <name>=<value>,...
<name>(0)=<value>
aux <name>=<formula>
bdry <expression>
wiener <name>,...
parameter <name>=<value>, ...
number <name>=<value>
table <name> <filename>
table <name> % <npts> <xlo> <xhi> <function(t)>
global sign {condition} {name1=form1;...}
markov <name> <nstates>
{t01} {t02} ... {t0k-1}
{t10} ...
{tk-1,0} ...
# comments
@  <name>=<value>  ...
done

[bard]: http://www.math.pitt.edu/~bard/bardware/tut/newstyle.html
Coder: M. Hemery
*/
:-module(
	xpp,
	[
    % Biocham Commands
    export_xpp/1,
    read_xpp/1,
    % API
    read_xpp_as_ode/1,
    read_xpp_as_ode/2,
    write_ode_as_xpp/0
  ]).

% Only for separate compilation/linking
:- use_module(doc).
:- use_module(library(pcre)).
:- use_module(ode).

/*** BIOCHAM COMMAND ***/

export_xpp(OutputFile) :-
  biocham_command,
  type(OutputFile, output_file),
  doc('Exports the ODE system of the current model as an XPP file.'),
  with_current_ode_system(with_output_to_file(OutputFile, write_ode_as_xpp)).

read_xpp(InputFile) :-
  biocham_command,
  type(InputFile, input_file),
  doc('Imports an ODE system from an XPP file.'),
  read_xpp_as_ode(InputFile, Id),
  set_current_ode_system(Id).


/*** INTERNAL API ***/

%! xpp_option(?Option)
%
% defines the option that should be included when writing an xpp file

xpp_option(method).
xpp_option(error_epsilon_absolute).
xpp_option(error_epsilon_relative).
xpp_option(initial_step_size).
xpp_option(maximum_step_size).
xpp_option(minimum_step_size).
xpp_option(precision).
xpp_option(time).
xpp_option(stochastic_conversion).

%! write_ode_as_xpp
%
% well, it is pretty self explained

write_ode_as_xpp :-
  (
    item([kind:model, key:Name]),!
  ;
    item([kind:ode_system, item:Name])
  ),
  get_current_ode_system(Id),
  format('# name: ~w~n', Name),
  get_time(Time), stamp_date_time(Time, date(Year,Month,Day,Hour,Min,_,_,_,_), 'UTC'),
  format('# date: ~w/~w/~w - ~w:~w~n', [Year,Month,Day,Hour,Min]),
  forall(
    item([parent: Id, kind: ode, item: (d(X)/dt = E)]),
    format('d~w/dt= ~w\n', [X, E])
  ),
  forall(
    item([parent: Id, kind: function, item: func(F = D)]),
    format('~w= ~w\n', [F, D])
  ),
  forall(
    item([parent: Id, kind: initial_concentration, item: init(X = E)]),
    format('init ~w= ~w\n', [X, E]) % ~p would quote atoms which is not accepted in xpp syntax
  ),
  forall(
    item([parent: Id, kind: parameter, item: par(K = V)]),
    format('parameter ~w= ~w\n', [K, V])
  ),
  forall(
    xpp_option(Option),
    catch(
      (get_option(Option, Value), format('# option: ~w= ~w~n', [Option, Value])),
      error(option_required(_All), option), %option_required(_),
      true
    )
  ),
  format('done\n').


%! read_xpp_as_ode(+Filename, -Ode_system_Id)
%
%

% Connect the xpp_parser with the loading system of biocham
models:add_file_suffix('xpp', read_xpp_as_ode).

read_xpp_as_ode(File) :-
  read_xpp_as_ode(File, _).


read_xpp_as_ode(InputFile, Id) :-
  new_ode_system(Id),
  setup_call_cleanup(
    open(InputFile, read, Stream),
    read_xpp(Stream, Id),
    close(Stream)
  ).

read_xpp(Stream, Id) :-
  repeat,
    read_line_to_string(Stream, LineUD),
    (
      LineUD = end_of_file
    ->
      !
    ;
      string_lower(LineUD, LineDotted),
	    re_replace("([+,-,*,/,=, ])(\\.\\d)"/g,"\\{1}0\\{2}",LineDotted, Line),
      split_string(Line, " ", "", SeqLine),
      read_xpp_line(Line, SeqLine, Id)
    ).


%! read_xpp_line(_Line,SeqLine, Id)
%
% read an xpp line as a sequence of word and add the results to the ode system Id

read_xpp_line(_Line, ["#", "name:", Name|_Tail], Id) :-
  !,
  set_ode_system_name(Id, Name),
  fail.

read_xpp_line(_Line,["#", "option:", Option, "=", Value|_T], _Id) :-
  !,
  read_term_from_atom(Option, OptionT, []),
  read_term_from_atom(Value, ValueT, []),
  set_option(OptionT, ValueT),
  fail.

% Cut comment and specific xppaut option lines
read_xpp_line(_Line,["#"|_Tail], _Id) :- !, fail.
read_xpp_line(_Line,["@"|_Tail], _Id) :- !, fail.

read_xpp_line(_Line,["init"|Tail], Id) :-
  !,
  atomics_to_string(Tail, Line),
  split_string(Line, ",", "", InitList),
  maplist(add_initial_value(Id), InitList),
  fail.

read_xpp_line(Line, _Split, Id) :-
  re_matchsub("(.*)\\(0\\) *= *(.*)", Line, Sub, []),
  atomics_to_string([Sub.1, =, Sub.2], InitValue),
  add_initial_value(Id, InitValue),
  fail.

read_xpp_line(_Line,[Param|Tail], Id) :-
  is_param(Param),
  !,
  atomics_to_string(Tail, Line),
  split_string(Line, ',', '', ParamList),
  maplist(add_parameter(Id), ParamList),
  fail.

read_xpp_line(Line_raw, _Split, Id) :-
  (
    re_matchsub("^d(.*)/dt *= *(.*)$", Line_raw, Sub, [])
  ->
    atomic_list_concat(['d',Sub.1,'/dt = ',Sub.2], Line)
  ;
    re_matchsub("^(.*)\' *= *(.*)$", Line_raw, Sub, [])
  ->
    atomic_list_concat(['d',Sub.1,'/dt = ',Sub.2], Line)
  ),
  !,
  read_term_from_codes(Line, DX = Expr, [variable_names(VariableNames), module(ode)]),
  name_variables_and_anonymous(DX = Expr, VariableNames),
  distribute(Expr, Expanded),
  simplify(Expanded, Clear_Expr),
  add_ode(Id, DX = Clear_Expr),
  fail.

read_xpp_line(Line, _Split, Id) :-
  re_match("(.*)((.*)) *= *(.*)$", Line),
  read_term_from_codes(Line, Expr, [variable_names(VariableNames), module(ode)]),
  (
    Expr = (Functor = _E),
    Functor =.. [_N, 0]
  ->
    fail
  ;
    name_variables_and_anonymous(_Expression, VariableNames),
    ode:set_ode_function(Id, Expr),
    fail
  ).

is_param("p").
is_param("par").
is_param("param").
is_param("parameter").

add_initial_value(Id, Init_raw) :-
  re_replace('=', ' = ', Init_raw, Init),
  read_term_from_codes(Init, X=E, [variable_names(VariableNames), module(ode)]),
  name_variables_and_anonymous(_Expression, VariableNames),
  set_ode_initial_value(Id, X, E).

add_parameter(Id, Param_raw) :-
  re_replace('=', ' = ', Param_raw, Param),
  read_term_from_codes(Param, X=E, [variable_names(VariableNames), module(ode)]),
  name_variables_and_anonymous(_Expression, VariableNames),
  ode:set_ode_parameters(Id, X=E).
