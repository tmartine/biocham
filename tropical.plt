:- use_module(library(plunit)).

get_number_of_solutions(Output, N) :-
  findall(
    Pos,
    (
     sub_atom(Output, Pos, _, _, 'found a complete equilibration'),
     once(sub_atom(Output, _, _, _, 'ε'))
    ),
    Positions
  ),
  length(Positions, N).


:- begin_tests(tropical, [setup((clear_model, reset_options))]).


test(
  'tyson_full_equilibration',
  [
    setup(load('library:examples/cell_cycle/Tyson_1991.bc')),
    cleanup(clear_model),
    true(N == 3)
  ]
) :-
  with_output_to(
    atom(Output),
    with_option(
      tropical_max_degree: 2,
      tropicalize
    )
  ),
  get_number_of_solutions(Output, N).


%test(
%  'tropical_single_solution',
%  [
%    condition(flag(slow_test, true, true)),
%    setup(load('library/biomodels/BIOMD0000000103.xml')),
%    cleanup(clear_model),
%    true(N == 1)
%  ]
%) :-
%  with_output_to(
%    atom(Output),
%    with_option(
%      [
%        tropical_max_degree: 2,
%        tropical_epsilon: 0.2,
%        tropical_single_solution: yes
%      ],
%      tropicalize
%    )
%  ),
%  get_number_of_solutions(Output, N).


:- end_tests(tropical).
