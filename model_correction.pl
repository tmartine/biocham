/*
Implement the automatic correction of models loaded from a sbml file
Coder: M. Hemery
*/

:- module(
  correction,
  [
  % Commands
  correct_model/0
  % API
  ]
).

% Only for separate compilation/linting
:- use_module(arithmetic_rules).
:- use_module(doc).
:- use_module(formal_derivation).
:- use_module(reaction_rules).
:- use_module(util).

correct_model :-
  biocham_command,
  doc('Improve wellformedness and strictness of a CRN model by removing reactions of null kinetics, splitting reactions that are implicitely reversible in two and adding correct annotation for modifiers.'),
  single_model(ModelId),
  setof(
    Molecule,
    identifier_kind(ModelId, Molecule, object),
    List_Molecule
  ),
  apply_to_all(List_Molecule, deal_with_null_kinetic),
  apply_to_all(List_Molecule, split_reaction(List_Molecule)),
  apply_to_all(List_Molecule, reverse_reaction),
  apply_to_all(List_Molecule, correct_modifiers(List_Molecule)).


%! apply_to_all(+List_Molecule, +Action)
%
% Extract from the model all the reaction and apply the action to all of them

apply_to_all(List_Molecule, Action) :-
  reactions_with_species(List_Molecule, List_Reaction),
  maplist(Action, List_Reaction).


%! deal_with_null_kinetic(+Reaction)
%
% Detect and modify reactions with identically null kinetics
% If the reaction has rate set to 0, it is surely a parsing error and is delete
% otherwise a new parameter called "p0_i" is introduce and set to 0.

deal_with_null_kinetic(Expression for Reaction) :-
  (
    Expression = 0
  ->
    delete_reaction(Expression for Reaction)
  ;
    zero_like(Value),
    contains_term(Value, Expression)
  ->
    parameterize_zeros(Expression, New_Expression),
    delete_reaction(Expression for Reaction),
    add_reaction(New_Expression for Reaction)
  ;
    true
  ).

parameterize_zeros(Expression, New_Expression) :-
  (
    zero_like(Value),
    contains_term(Value, Expression)
  ->
    new_parameter(New),
    set_parameter(New, Value),
    substitute_once(Value, New, Expression, Tempo_expression),
    parameterize_zeros(Tempo_expression, New_Expression)
  ;
    New_Expression = Expression
  ).


%! split_reaction(+List_Mol, +Reaction)
%
% Split a bidirection reaction in two

split_reaction(List_Mol, Expression for Reactant => Product) :-
  (
    ode:substitute_functions(Expression, Expr),
    % distribute(Expr, DistExpr),
    DistExpr = Expr,
    (
      DistExpr = _A + _B
    ->
      delete_reaction(Expression for Reactant => Product),
      split_reaction_sr(List_Mol, DistExpr for Reactant => Product)
    ;
      DistExpr = _Up - Down,
      % test if it is effectively a bidir. reaction
      member(P, List_Mol),
      models:formal_product(P, Reactant => Product),
      contains_term(P, Down)
    ->
      delete_reaction(Expression for Reactant => Product),
      split_reaction_sr(List_Mol, DistExpr for Reactant => Product)
    ;
      true
    )
  ).

split_reaction_sr(List_Mol, Expression + Monomial for Reactant => Product) :-
  !,
  add_reaction(Monomial for Reactant => Product),
  split_reaction_sr(List_Mol, Expression for Reactant => Product).

split_reaction_sr(List_Mol, Expression - Monomial for Reactant => Product) :-
  !,
  (
    Reactant = Reac/Inhib
  ->
    add_reaction(Monomial for Product/Inhib => Reac)
  ;
    add_reaction(Monomial for Product => Reactant)
  ),
  split_reaction_sr(List_Mol, Expression for Reactant => Product).

split_reaction_sr(_List_Mol, Expression for Reactant => Product) :-
  add_reaction(Expression for Reactant => Product).


%! reverse_reaction(+Reaction)
%
% Reverse a reaction if its kinetic is negative

reverse_reaction(Expression for Reactant => Product) :-
  (
    ode:substitute_functions(Expression, - RevExpr)
  ->
    delete_reaction(Expression for Reactant => Product),
    (
      Reactant = Reac/Inhib
    ->
      add_reaction(RevExpr for Product/Inhib => Reac)
    ;
      add_reaction(RevExpr for Product => Reactant)
    )
  ;
    true
  ).


%! correct_modifiers(+List_Molecule, +Reaction)
%
% Ensure that the modifiers are correctly formulated

correct_modifiers(List_Molecule, Expression_raw for Reaction) :-
  debug(correction, "correct_modifiers: ~w", [Expression_raw]),
  ( % Avoid the correction of expression like MA and Hill
    (
      Expression_raw = 'MA'(_Rate);
      Expression_raw = 'MM'(_Rate, _K);
      Expression_raw = 'Hill'(_Rate, _K, _N)
    )
  ->
    true
  ;
    (
      Reaction = (Reac => _Prod)
    ->
      reaction_editor:solution_to_canonical_list(Reac, KReac),
      KInhib = []
    ;
      Reaction = (Reac/Inhib => _Prod)
    ->
      reaction_editor:solution_to_canonical_list(Reac, KReac),
      reaction_editor:solution_to_canonical_list(Inhib, KInhib)
    ;
      throw(error(bad_syntax(Reaction, "from correct_modifiers")))
    ),
    kinetics:kinetics(KReac, KInhib, Expression_raw, Expression),
    detect_modifiers(List_Molecule, Expression, Catalysts, Inhibitors),
    % purify reaction
    findall(M, (member(M, List_Molecule), models:formal_catalyst(M, Reaction)), LC),
    remove_all_catalysts(LC, Reaction, Purified_Reaction),
    (
      %  append(Catalysts, Inhibitors, [])
      %->
      %true
      %;
      add_catalysts(Catalysts, Purified_Reaction, ReactionTempo),
      add_inhibitors(Inhibitors, ReactionTempo, NewReaction),
      delete_reaction(Expression_raw for Reaction),
      add_reaction(Expression_raw for NewReaction)
    )
  ).


%! detect_modifiers(+List_Molecule, +Expression, -Catalyst, -Inhibitor)
%
% Search among the list of molecules for catalysts and inhibitors
% note that as only Expression is look at, reactants are sorted as catalysts

detect_modifiers([], _Expression, [], []).

detect_modifiers([Mol|TailM], Expression, NewC, NewI) :-
  derivate(Expression, Mol, Deriv),
  (
    is_null(Deriv)
  ->
    NewC = TailC,
    NewI = TailI
  ;
    always_negative(Deriv)
  ->
    NewC = TailC,
    NewI = [Mol|TailI]
  ;
    NewC = [Mol|TailC],
    NewI = TailI
  ),
  detect_modifiers(TailM, Expression, TailC, TailI).


%! add_catalysts(+List_catalyst, +Reaction, -Modified_Reaction)
%
% modify reaction to add catalysts (let untouch if already present)

add_catalysts([], Reaction, Reaction).

add_catalysts([Head|Tail], Reaction, NewReaction) :-
  models:formal_reactant(Head, Reaction),
  !,
  add_catalysts(Tail, Reaction, NewReaction). 

add_catalysts([Head|Tail], Reactants => Products, NewReactants => NProducts+Head) :-
  add_catalysts(Tail, Reactants => Products, NReactants => NProducts),
  (
    NReactants = Reac / Inhib
  ->
    NewReactants = (Reac+Head) / Inhib
  ;
    NewReactants = NReactants+Head
  ).


%! add_inhibitors(+List_inhibitors, +Reaction, -Modified_Reaction)
%
% same as add_catalysts for inhibitors

add_inhibitors([], Reaction, Reaction).

add_inhibitors([Head|Tail], Reaction, NewReaction) :-
  models:formal_inhibitor(Head, Reaction),
  !,
  add_inhibitors(Tail, Reaction, NewReaction).

add_inhibitors([Head|Tail], Reactants => Products, NewReactants => NProducts) :-
  add_inhibitors(Tail, Reactants => Products, ReactantsTempo => ProductsTempo),
  (
    remove_molecule(ReactantsTempo, Head, NReactants),
    remove_molecule(ProductsTempo, Head, NProducts)
  ;
    NReactants = ReactantsTempo,
    NProducts = ProductsTempo
  ),
  (
    NReactants = Reac / Inhib
  ->
    NewReactants = Reac / (Head,Inhib)
  ;
    NewReactants = NReactants / Head
  ).

%! remove_molecule(+Species, +Molecule, -Without)
%
% Remove one occurence of Molecule from the Species input

remove_molecule(Molecule, Molecule, '_') :- !.

remove_molecule(P*Molecule, Molecule, Result) :- 
  (
    P = 1
  ->
    Result = '_'
  ;
    P = 2
  ->
    Result = Molecule
  ;
    Pm is P-1,
    Result = Pm*Molecule
  ),!.

remove_molecule(Species+Molecule, Molecule, Species) :- !.

remove_molecule(Species+P*Molecule, Molecule, Result) :-
  (
    P = 1
  ->
    Result = Species
  ;
    P = 2
  ->
    Result = Species+Molecule
  ;
    Pm is P-1,
    Result = Species+Pm*Molecule
  ),!.

remove_molecule(Molecule+Other, Molecule, Result) :- !,
  remove_molecule(Other+Molecule, Molecule, Result).

remove_molecule(P*Molecule+Other, Molecule, Result) :- !,
  remove_molecule(P*Molecule, Molecule, ResultTempo),
  (
    ResultTempo = '_'
  ->
    Result = Other
  ;
    Result = ResultTempo+Other
  ).

remove_molecule(Species+Other, Molecule, Without+Other) :-
  remove_molecule(Species, Molecule, Without).


%! remove_all_catalysts(+List_Catalyst, +Reaction, -NewReaction)
%
%

remove_all_catalysts([], Reaction, Reaction).

remove_all_catalysts([Head|Tail], R/I=>P, RR/I=>PP) :-
  remove_all_catalysts(Tail, R/I=>P, RT/I=>PT),
  remove_catalyst(RT, PT, Head, RR, PP).

remove_all_catalysts([Head|Tail], R=>P, RR=>PP) :-
  remove_all_catalysts(Tail, R=>P, RT=>PT),
  remove_catalyst(RT, PT, Head, RR, PP).

remove_catalyst(Reac, Prod, Cat, NReac, NProd) :-
  (
    remove_molecule(Reac, Cat, TReac),
    remove_molecule(Prod, Cat, TProd)
  ->
    remove_catalyst(TReac, TProd, Cat, NReac, NProd)
  ;
    NReac = Reac,
    NProd = Prod
  ).
