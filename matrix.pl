/* Matrix and linalg module
 *
 * This is a prolog matrix implementation using arg/setarg scheme to ensure a linear
 * access to the element of the matrices.
 * 
 * Then came an implementation of some basic linagl operation
 *
 * Coder: M. Hemery & ???
 */

:- module(matrix,
  [
    % I/O Matrix
    void_vector/2,
    void_matrix/3,
    set_vector/2,
    set_matrix/2,
    constant_vector/3,
    constant_matrix/4,
    diag_matrix/3,
    get_value/4,
    get_value/3,
    get_row/3,
    get_column/3,
    set_value/4,
    set_value/3,
    set_row/3,
    get_matrix_as_list/2,
    get_vector_as_list/2,
    slice_element/4,
    shape/2,
    shape/3,
    write_matrix/1,
    % Operation on matrices
    matrix_sum/3,
    matrix_substraction/3,
    matrix_transpose/2,
    scalar_prod/3,
    matrix_prod/3,
    matrix_apply/3,
    % Linalg part
    ludcmp_matrix/3,
    lusubst/4,
    solve_linear_problem/3
  ]
).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% MATRIX (TERM) IMPLEMENTATION %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%! void_vector(+Column, -Vector)
%
% assign Vector to a vector of Column uninstantiated variables

void_vector(Column, Vector) :-
  functor(Vector, vector, Column).


%! void_matrix(+Row, +Column, -Matrix)
%
% assign Matrix to a Row x Rox matrix of uninstantiated variables

void_matrix(Row, Column, Matrix) :-
  findall(I, (between(1,Row,_N), void_vector(Column, I)), VV),
  Matrix =.. [matrix|VV].


%! constant_vector(Column, Value, Vector)

constant_vector(Column, Value, Vector) :-
  void_vector(Column, Vector),
  forall(
    between(1, Column, Index),
    set_value(Vector, Index, Value)
  ).


%! constant_matrix(Row, Column, Value, Vector)

constant_matrix(Row, Column, Value, Matrix) :-
  findall(I, (between(1,Row,_N), constant_vector(Column, Value, I)), VV),
  Matrix =.. [matrix|VV].


%! diag_matrix(Size, Value, Matrix)
%
% Matrix is a matrix with Value on the diagonal and zero elsewhere

diag_matrix(Size, Value, Matrix) :-
  constant_matrix(Size, Size, 0, Matrix),
  forall(
    between(1, Size, Index),
    set_value(Matrix, Index, Index, Value)
  ).

%! set_vector(+List, -Vector)

set_vector(List, Vector) :-
  (
    List =.. [vector|_Tail]
  ->
    Vector = List
  ;
    Vector =.. [vector|List]
  ).


%! set_matrix(+List, -Matrix)
%
% Set the List as a matrix, do nothing if already a matrix

set_matrix(List, Matrix) :-
  (
    List =.. [matrix|_Tail]
  ->
    Matrix = List
  ;
    maplist(set_vector, List, VList),
    Matrix =.. [matrix|VList]
  ).


%! get_value(+Matrix, +Row, +Column, -Value)

get_value(Matrix, Row, Column, Value) :-
  arg(Row, Matrix, Vector),
  arg(Column, Vector, Value).


get_row(Matrix, Index, Row) :-
  arg(Index, Matrix, Row).


get_column(Matrix, Index, Column) :-
  findall(
    Value,
    (
      arg(_I, Matrix, Vector),
      arg(Index, Vector, Value)
    ),
    List
  ),
  Column =.. [vector|List].


%! get_value(+Vector, +Column, -Value)

get_value(Vector, Column, Value) :-
  arg(Column, Vector, Value).


%! set_value(+Matrix, +Row, +Column, +Value)

set_value(Matrix, Row, Column, Value) :-
  arg(Row, Matrix, Vector),
  set_value(Vector, Column, Value).


%! set_value(+Vector, +Column, +Value)

set_value(Vector, Column, Value) :-
  nb_setarg(Column, Vector, Value).


%! set_row(+Matrix, +Row, +Vector)

set_row(Matrix, Row, Vector) :-
  nb_setarg(Row, Matrix, Vector).

%! get_vector_as_list(Vector, List)

get_vector_as_list(Vector, List) :-
  Vector =.. [vector|List].


%! slice_element(+Vector, +Start, +Stop, +SliceVector)

slice_element(Vector, Start, Stop, SliceVector) :-
  findall(
    Element,
    (between(Start, Stop, I), get_value(Vector, I, Element)),
    SliceList
  ),
  set_vector(SliceList, SliceVector).

%! get_matrix_as_list(Matrix, List)

get_matrix_as_list(Matrix, List) :-
  findall(
    SubList,
    (
      arg(_I, Matrix, Vector),
      get_vector_as_list(Vector, SubList)
    ),
    List
  ).


%! shape(+Vector, -Column)

shape(Vector, Column) :-
  functor(Vector, vector, Column).


%! shape(+Matrix, -Row, -Column)

shape(Matrix, Row, Column) :-
  functor(Matrix, matrix, Row),
  arg(1, Matrix, Vector),
  functor(Vector, vector, Column).


%! write_matrix(+Matrix)

write_matrix(Matrix) :-
  format("[", []),
  Matrix =.. [matrix,Head|Tail],
  Head=..[vector|Row],
  format("~w",[Row]),
  write_matrix_sr(Tail).

write_matrix_sr([]) :-
  !,
  format("].~n",[]).

write_matrix_sr([Head|Tail]) :-
  Head=..[vector|Row],
  format("~n ~w",[Row]),
  write_matrix_sr(Tail).


%! matrix_sum(+A, +B, -ApB)

vector_sum(A, B, ApB) :-
  A =.. [vector|LA],
  B =.. [vector|LB],
  maplist(([X,Y,XpY]>>(XpY is X+Y)), LA, LB, LAB),
  ApB =.. [vector|LAB].

matrix_sum(A, B, ApB) :-
  A =.. [matrix|LA],
  B =.. [matrix|LB],
  maplist(vector_sum, LA, LB, LAB),
  ApB =.. [matrix|LAB].


%! matrix_substraction(+A, +B, -AmB)

vector_substraction(A, B, AmB) :-
  A =.. [vector|LA],
  B =.. [vector|LB],
  maplist(([X,Y,XpY]>>(XpY is X-Y)), LA, LB, LAB),
  AmB =.. [vector|LAB].

matrix_substraction(A, B, AmB) :-
  A =.. [matrix|LA],
  B =.. [matrix|LB],
  maplist(vector_substraction, LA, LB, LAB),
  AmB =.. [matrix|LAB].


%! matrix_transpose(A, TA)

matrix_transpose(A, TA) :-
  shape(A, Imax, Jmax),
  void_matrix(Jmax, Imax, TA),
  forall(
    (
      between(1, Imax, I),
      between(1, Jmax, J)
    ),
    (
      get_value(A, I, J, V),
      set_value(TA, J, I, V)
    )
  ).


%! scalar_prod(+A, +B, -P)

scalar_prod(A, B, P) :-
  A =.. [vector|LA],
  B =.. [vector|LB],
  foldl([X,Y,S,SS]>>(SS is X*Y+S), LA, LB, 0, P).


%! matrix_prod(+A, +B, -AtB)

matrix_prod(A, B, AtB) :-
  shape(A, Imax, Kmax),
  matrix_transpose(B, TB),
  shape(TB, Jmax, Kmax),
  void_matrix(Imax, Jmax, AtB),
  forall(
    (
      between(1, Imax, I),
      between(1, Jmax, J)
    ),
    (
      arg(I, A, V1),
      arg(J, TB, V2),
      scalar_prod(V1, V2, P),
      set_value(AtB, I, J, P)
    )
  ).


%! matrix_apply(+A, +V, -AxV)
%
% outer product between a matrix and a vector, AxV is a vector

matrix_apply(A, V, AxV) :-
  findall(
    OP,
    (
      arg(_I, A, AV),
      scalar_prod(AV, V, OP)
    ),
    LAXV
  ),
  AxV =.. [vector|LAXV].


%%%%%%%%%%%%%%%%%%%
%%% LINALG PART %%%
%%%%%%%%%%%%%%%%%%%

%! matrix_square(+Matrix, +N, +I, +J, -Element)
%
% Element is \sum_{K=1..N-1} M[I,K]*M[K,J]

matrix_square(Matrix, N, I, J, Element) :-
  Nm is N-1,
  findall(
    M1*M2,
    (
      between(1, Nm, Index),
      get_value(Matrix, I, Index, M1),
      get_value(Matrix, Index, J, M2)
    ),
    List
  ),
  sum_list(List, Element).


%! ludcmp_matrix(+Matrix, -TransformedMatrix, -Formal_index)
%
% Perform an LU decomposition, that is find two matrices L and U such that Matrix = L.U,
% L and U begin respectively lower and upper diagonal.
% By convention, L has only 1 on its diagonal so that we gather them in a single matrix M
% such that M[i,j] = L[i,j] if i<j and M[i,j] = U[i,j] if i>=j.  M is the transformed
% matrix and is stored in the global variable at the end of the call.

ludcmp_matrix(A, StoredMatrix, Formal_index) :-
   set_matrix(A, StoredMatrix),
   shape(StoredMatrix, N, N),
   TINY=1.0E-100, % Value if pivot element is zero (singular)
   void_vector(N, Formal_index),
   void_vector(N, Formal_scaling),
   % fill implicit scaling info, formal_scaling contains 1/RowMax where RowMax is the max(abs(RowElement)) for each Row
   forall(
         between(1, N, I),
         (
           nb_setval(formal_tmp, 0),
           forall(
                 between(1, N, J),
                 (
                   get_value(StoredMatrix,I,J,T2),
                   TMP is abs(T2),
                   nb_getval(formal_tmp,T1),
                   (
                      TMP > T1
                   ->
                      nb_setval(formal_tmp,TMP)
                   ;
                      true
                   )
                 )
           ),
           nb_getval(formal_tmp,TMP),
           (
              TMP =:= 0
           ->
              throw(error(singular_matrix)) 
           ;
              true
           ),
           T3 is 1/TMP,
           set_value(Formal_scaling, I, T3)
         )
   ),
   forall(
         between(1,N,J),
         (
           nb_setval(formal_tmp,0),
           forall(
                 between(1,N,I),
                 (
                    I<J
                 ->
                    matrix_square(StoredMatrix, I, I, J, PS),
                    get_value(StoredMatrix, I, J, Val),
                    NewVal is Val - PS,
                    set_value(StoredMatrix,I,J,NewVal)
                 ;
                    matrix_square(StoredMatrix, J, I, J, PS),
                    get_value(StoredMatrix, I, J, Val),
                    NewVal is Val - PS,
                    set_value(StoredMatrix,I,J,NewVal),
                    get_value(Formal_scaling, I, VV),
                    TMP is abs(NewVal)*VV,
                    nb_getval(formal_tmp,T1),
                    (
                       TMP > T1
                    ->
                       nb_setval(formal_tmp,TMP),
                       nb_setval(formal_imax,I)
                    ;
                       true
                    )
                 )     
           ),
           nb_getval(formal_imax,IMAX),
           %format("for ~w max row is ~w~n",[J,IMAX]),
           (
              J \= IMAX
           ->
              % we need to interchange rows
              get_row(StoredMatrix, IMAX, DUM),
              get_row(StoredMatrix, J, TMP),
              get_value(Formal_scaling, J, VV),
              set_row(StoredMatrix, IMAX, TMP),
              set_row(StoredMatrix, J, DUM),
              set_value(Formal_scaling, IMAX, VV)
           ;
              true
           ),
           set_value(Formal_index, J, IMAX),
           get_value(StoredMatrix,J,J,AJJ),
           (
              AJJ =:= 0
           ->
              % don't keep a zero here, we will divide by it!
              AAJJ = TINY,
              set_value(StoredMatrix,J,J,AAJJ)
           ;
              AAJJ = AJJ
           ), 
           (
              J < N
           ->
              JJ is J+1,
              forall(
                    between(JJ,N,II),
                    (
                      get_value(StoredMatrix,II,J,AIJ),
                      AAIJ is AIJ/AAJJ,
                      set_value(StoredMatrix,II,J,AAIJ)
                    )
              )
           ;
              true
           )     
         )
       ).


%! lusubst(+DcmpA, +Formal_index, +B, -X)
%
% Use LU decomposition to solve Ax=b by fwd/backwd substitution
% Supposing A has already been decomposed using the ludcmp predicate

lusubst(DcmpA, Formal_index, B, Vector) :-
  set_matrix(DcmpA, StoredMatrix),
  shape(StoredMatrix, N, N),
  set_vector(B, Vector),
  forall(
        between(1,N,I),
        (
          get_value(Formal_index, I, IP),
          get_value(Vector, IP, Y),
          get_value(Vector, I, TMP),
          set_value(Vector, IP, TMP),
          II is I-1,
          findall(
                -A*YY,
                (
                  between(1,II,J),
                  get_value(StoredMatrix, I, J, A),
                  get_value(Vector, J, YY)
                ),
                SumList
          ),
          sum_list([Y|SumList], S),
          set_value(Vector, I, S)
        )
  ),
  get_value(Vector, N, Z),
  get_value(StoredMatrix,N,N,BB),
  ZZ is Z/BB,
  set_value(Vector, N, ZZ),
  forall(
        between(2,N,I),
        (
          II is N-I+1,
          III is II+1,
          findall(
                -A*Y,
                (
                  between(III,N,J),
                  get_value(StoredMatrix,II,J,A),
                  get_value(Vector, J, Y)
                ),
                SumList
          ),
          get_value(Vector, II, Y),
          sum_list([Y|SumList], S),
          get_value(StoredMatrix,II,II,BBB),
          YY is S/BBB,
          set_value(Vector, II, YY)
        )
  ).


%! solve_linear_problem(+A, +B, -X)
%
% Solve the pbm A.X = B through delegate to ludcmp and lusubst
 
solve_linear_problem(A, B, X) :-
  ludcmp_matrix(A, Decomposed_A, Formal_index),
  lusubst(Decomposed_A, Formal_index, B, X).
