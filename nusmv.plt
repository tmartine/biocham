:- use_module(library(plunit)).
:- use_module(library(debug)).
% not necessary, except for separate compilation
:- use_module(reaction_rules).
:- use_module(influence_rules).

:- begin_tests(nusmv, [setup((clear_model, reset_options))]).

test('export_reac', [
  setup(set_a_dummy_reaction_model),
  cleanup((clear_model, delete_file('unittest.smv')))
]) :-
  export_nusmv('unittest').

test(
  'check_ctl_reac',
  [
    setup(set_a_dummy_reaction_model),
    cleanup(clear_model),
    true(Result == 'false')
  ]
) :-
  with_option(
    [nusmv_counter_example: no, boolean_semantics: negative],
    nusmv:check_ctl_impl('AG'(c -> b), all, Result)
  ).


test('export_infl', [
  setup(set_a_dummy_influence_model),
  cleanup((clear_model, delete_file('unittest.smv')))
]) :-
  export_nusmv('unittest').

test(
  'check_ctl_infl',
  [
    setup(set_a_dummy_influence_model),
    cleanup(clear_model),
    true(Result == 'false')
  ]
) :-
  with_option(
    [nusmv_counter_example: no, boolean_semantics: negative],
    nusmv:check_ctl_impl('AG'(not(c) \/ b), all, Result)
  ).


test(
  'Lotka-Voltera',
  [
    forall(member(File, [
      'library:examples/lotka_volterra/LVr.bc',
      'library:examples/lotka_volterra/LVi.bc'
    ])),
    setup(load(File)),
    cleanup(clear_model)
  ]
) :-
  with_option(
    [nusmv_counter_example: no, boolean_semantics: negative],
    (
      assertion(nusmv:check_ctl_impl('EG'('R') /\ 'P', all, true)),
      assertion(nusmv:check_ctl_impl('EF'('AG'('R' /\ not('P'))), all, true)),
      assertion(nusmv:check_ctl_impl('EF'('AG'(not('R') /\ not('P'))), all, true)),
      assertion(nusmv:check_ctl_impl('EF'('AG'(not('R') /\ 'P')), all, false))
    )
  ).


test(
  'MAPK',
  [
    condition(flag(slow_test, true, true)),
    true(Result == 'true')
  ]
) :-
  command(clear_model),
  command(load('library:examples/mapk/mapk.bc')),
  with_option(
    [nusmv_counter_example: no, boolean_semantics: negative],
    nusmv:check_ctl_impl(
      reachable('MAPK~{p1,p2}') /\
      checkpoint('MEK~{p1}', 'MAPK~{p1,p2}') /\
      'EF'('MEK~{p1}-MEKPH' /\ 'EF'('MAPK~{p1,p2}')) /\
      'EU'(not('MEK~{p1}-MEKPH'), 'MAPK~{p1,p2}'),
      all,
      Result
    )
  ).


test(
  'LTL',
  [
    setup(set_a_dummy_reaction_model),
    cleanup(clear_model),
    true((R1, R2) == (false, true))
  ]
) :-
  command(absent(b)),
  with_option([nusmv_counter_example: no],
    (
      nusmv:check_ltl_impl(all, reachable(c), R1),
      nusmv:check_ltl_impl(some, reachable(c), R2)
    )
  ).


set_a_dummy_reaction_model :-
  clear_model,
  add_reaction(a => b),
  add_reaction(a + b => c),
  command(present(a)),
  command(absent(c)).


set_a_dummy_influence_model :-
  clear_model,
  add_influence(a -> b),
  add_influence((a, b) -< c),
  command(present(a)),
  command(present(c)).

:- end_tests(nusmv).
