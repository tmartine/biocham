:- use_module(library(plunit)).

:- use_module(podeize).

:- begin_tests(podeize, []).

test(detect_new_variable, []) :-
  detect_new_variable(a^2, [a], []).

test(polynomialize_ode_1, [nondet]) :-
  polynomialize_ode([d(a)/dt = exp(a)], _Out, Trans_raw),
  !,sort(Trans_raw, [('A'->exp(a))]).

test(polynomialize_ode_sin, [nondet]) :-
  polynomialize_ode([d(a)/dt = sin(a)], _Out, Trans_raw),
  !,sort(Trans_raw, [('A'->sin(a)),('B'->cos(a))]).

test(polynomialize_ode_parameters, [nondet]) :-
  polynomialize_ode([d(a)/dt = k*sin(a)], _Out, Trans_raw),
  !,sort(Trans_raw, [('A'->sin(a)),('B'->cos(a))]).

test(polynomialize_ode_quotient, [nondet]) :-
  polynomialize_ode([d(a)/dt = a/b, d(b)/dt = b/a], _Out, Trans_raw),
  !,sort(Trans_raw, [('A'->1/b),('B'->1/a)]).

test(polynomialize_ode_power, [nondet]) :-
  polynomialize_ode([d(a)/dt = a^0.25], _Out, Trans_raw),
  !,sort(Trans_raw, [('A'->a^0.25),('B'->1/a)]).

test(polynomialize_with_time, [nondet]) :-
  polynomialize_ode([d('A')/dt = tanh('Time'), d('B')/dt = 'Time'], _Out, Trans_raw),
  !,sort(Trans_raw, [('C'->tanh(Time)),(Time->Time)]).

test(polynomialize_ode_bournez, [nondet]) :-
  polynomialize_ode([d(a)/dt = sin(b)^2, d(b)/dt = a*cos(b) - exp(exp(a)+t)], _Out, Trans_raw),
  !,sort(Trans_raw, [('A'->sin(b)),('B'->exp(t+exp(a))),('C'->cos(b)),('D'->exp(a))]).

test(polynomialize_init, [nondet]) :-
  command(clear_model),
  command(d('A')/dt = cos('A')),
  with_output_to(atom(_O), polynomialize_ode),
  !,
  get_current_ode_system(Id),
  findall(
    Var - Conc,
    item([parent:Id, kind: initial_concentration, item: init(Var= Conc)]),
    ListInit
  ),
  !,
  sort(ListInit, ['A'-0,'B'-1.0,'C'-0.0]).


user:message_hook(initialization_impossible(_), warning, _).

test(polynomialize_warning, [nondet, true(Warnings == 1)]) :-
  command(clear_model),
  set_counter(warnings, 0),
  command(d('A')/dt = 1/'A'),
  with_output_to(atom(_Out), polynomialize_ode),
  count(warnings, Warnings).
  

test(derivation) :-
  pode:complete_derivation(b+'Time', [d(a)/dt = a, d(b)/dt = 'Time'], Expr),
  simplify(Expr, 1+'Time').

test(new_variable) :-
  set_counter(variable_id, 0),
  pode:new_variable(['B'], 'A'),
  pode:new_variable(['B'], 'C').

:- end_tests(podeize).
