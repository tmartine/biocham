#ifndef _gsl_types_hh_
#define _gsl_types_hh_

#include <vector>
#include <list>

typedef std::vector<double> Row;
typedef std::list<Row> Table;

#endif /* _gsl_types_hh_ */
