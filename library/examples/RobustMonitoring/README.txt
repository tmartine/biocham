Examples of robustness computation and optimization in BIOCHAM-4
http://lifeware.inria.fr/biocham4
Inria Saclay Ile de France

The .ipynb files are BIOCHAM-4 notebooks directly executable on our server
(use shift-return to execute a cell)

The .bc files are BIOCHAM-4 files loaded by the notebooks

The reference manual and tutorials of BIOCHAM-4 are available at http://lifeware.inria.fr/biocham4




