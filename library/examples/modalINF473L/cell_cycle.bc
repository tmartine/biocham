% Cell cycle adapted from Qu et al, Biophysical journal 
% kwpcn=5, Kweem=0.2 instead of 2 and 1 because of Cry-deficient mice
% ksmpf=0.35 (instead of 0.3) for shorter cell cycle in autonomous cell cycle model.

% Cell cycle molecules
present(preMPF,0.279939269).
present(MPF,0.747404087).
present(C25,0.629659946).
present(C25P,0.370340054).
present(Wee1,0.00223979517).
present(Wee1p,0.132059534).
present(APC,0.0303504407).
present(IE,0.41856651).
present(mWee1,0.141190132).
present(APCi,0.973282491).


% Cell cycle parameters
parameter(kd25=1).
parameter(kdmpfp=0.25).
%%% kampf values
% 5 gives free period ~20h
% 2.2 ~24h
% 1.35 ~28h -> period doubling
parameter(kampf=2.2).
parameter(kaapc=1).
parameter(ksweemp=0.001).
parameter(kdweem=1).
parameter(Kweem=0.2).
parameter(kwpcn=5).
parameter(Vac=1).
parameter(Kac25=0.01).
parameter(Kic25=0.01).
parameter(kdmpf=2).
parameter(Vaw=1).
parameter(Vapc=0.1).
parameter(Kaw=0.005).
parameter(Kiw=0.005).
parameter(Vapw=0.1).
parameter(Kapc=0.01).
parameter(ks25=1).
%parameter(kswee=0.5).
parameter(kswee=1.5).
%parameter(kimpf=1.5).
parameter(kimpf=2.5).
parameter(Vic=0.2).
%%% period when kapmf is 1.35
% 1.42 -> 19.0h
% 1.4  -> 21.6h
% 1.2  -> 24.9h
% 0.5  -> 28.0h
% 0.3  -> 30.5h
parameter(kiapc=0.5).
parameter(kdwee=1).
parameter(Viw=0.2).
parameter(ksmpf=0.35).
parameter(kdie=0.1).
parameter(ksiep=0).
parameter(ksie=0.3).
parameter(kaapcp=0).


% Reaction rules
% MPF - preMPF
ksmpf 			for _=>preMPF.
kdmpfp*preMPF		for preMPF=>_.
kdmpfp*MPF		for MPF=>_.
kdmpf*APC*MPF 	for MPF=[APC]=>_.
kdmpf*APC*preMPF 	for preMPF=[APC]=>_.


kampf*C25P*preMPF 	for preMPF=[C25P]=>MPF.
kimpf*Wee1*MPF 	for MPF=[Wee1]=>preMPF.

% Wee1
Viw*Wee1p/(Kiw+Wee1p)		for Wee1p=>Wee1.
%(Vapw+Vaw*MPF)*Wee1/(Kaw+Wee1)	for Wee1=[MPF]=>Wee1p.  
Vapw*Wee1/(Kaw+Wee1)	for Wee1=[MPF]=>Wee1p.  
Vaw*MPF*Wee1/(Kaw+Wee1)	for Wee1=[MPF]=>Wee1p.  

ksweemp for _=> mWee1.
kdweem*mWee1					for mWee1=>_.

kswee*mWee1		for _=[mWee1]=>Wee1.
kdwee*Wee1 		for Wee1=>_.
kdwee*Wee1p 		for Wee1p=>_.

% Cdc25
Vic*C25P/(Kic25+C25P)			for C25P=>C25.
%(Vapc+Vac*MPF)*C25/(Kac25+C25)	for C25=[MPF]=>C25P.
Vapc*C25/(Kac25+C25)	for C25=[MPF]=>C25P.
Vac*MPF*C25/(Kac25+C25)	for C25=[MPF]=>C25P.

ks25			for _=>C25.
kd25*C25 		for C25=>_.
kd25*C25P 		for C25P=>_.


% APC
ksiep			for _=>IE.
ksie*MPF		for _=[MPF]=>IE.
kdie*IE		for IE=>_.

%(kaapcp+kaapc*IE)*APCi/(Kapc+APCi) for APCi=[IE]=>APC.
kaapcp*APCi/(Kapc+APCi) for APCi=[IE]=>APC.
kaapc*IE*APCi/(Kapc+APCi) for APCi=[IE]=>APC.
kiapc*APC/(Kapc+APC)		 for APC=>APCi.
