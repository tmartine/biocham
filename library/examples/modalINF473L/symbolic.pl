/** <symbolic> Symbolic computation in SWI-Prolog
 *
 *  Partial derivatives and arithmetic simplifications
 *
 * @author F. Fages, Inria, March 2020
 * @author Sylvain Soliman
 * @license GPL2
 */


:- module(
  symbolic,
  [
    derivative/3,
    simplify/2
  ]
).


%%%%%%%%%%%%

%! derivative(+Expression, +Variable:atom, -Derivative) is det.
%
% Derivative is the partial derivative Expression with respect to the atom
% Variable (Prolog variables are excluded)
% Expression should be a polynomial
derivative(Expression, Variable, Derivative) :-
  deriv(Expression, Variable, Expr),
  simplify(Expr, Derivative).


% definition by pattern matching: bidirectional unification solves equality
% constraints between terms
deriv(E, X, R) :-
  atomic(E),      % not a compound term
  (               % condition -> then ; else
    E=X
  ->
    R=1
  ;
    R=0
  ).

deriv(E1 + E2, X, DE1+DE2) :- 
  deriv(E1, X, DE1),
  deriv(E2, X, DE2).

deriv(E1 - E2, X, DE1-DE2) :- 
  deriv(E1, X, DE1),
  deriv(E2, X, DE2).

deriv(- E2, X, - DE2) :- 
  deriv(E2, X, DE2).

deriv(E1 * E2, X, DE1*E2+DE2*E1) :- 
  deriv(E1, X, DE1),
  deriv(E2, X, DE2).

deriv(E^N, X, N*E^N1*DE) :-
  integer(N),
  N1 is N-1,
  deriv(E, X, DE).


%! simplify(+Expr, -Simplified) is det
%
% Simplify the polynomial expression Expr into Simplified
simplify(- A, R) :-
  !,
  simplify(A, As),
  (
    As=0
  ->
    R=0
  ;
    As=(- At)
  ->
    R=At
  ;
    R=(- As)
  ).

simplify(A*B, R) :-
  !,
  simplify(A, As),
  simplify(B, Bs),
  (
    (As=0; Bs=0)
  ->
    R=0
  ;
    As=1
  ->
    R=Bs
  ;
    Bs=1
  ->
    R=As
  ;
    R=(As*Bs)
  ).

simplify(A+B, R) :-
  !,
  simplify(A, As),
  simplify(B, Bs),
  (
    As=0
  ->
    R=Bs
  ;
    Bs=0
  ->
    R=As
  ;
    (Bs= (- Bt);Bs= (-1*Bt))
  ->
    R=(As-Bt)
  ;
    R=(As+Bs)
  ).

simplify(A-B, R) :-
  !,
  simplify(A, As),
  simplify(B, Bs),
  (
    As=0
  ->
    ((Bs= - Bt) -> R = Bt ; R = - Bs)
  ;
    Bs=0
  ->
    R=As
  ;
    ((Bs= - Bt) -> R = As+Bt ; R = As-Bs)
  ).

simplify(A^N, R) :-
  !,
  simplify(A, As),
  (
    As=0
  ->
    R=0
  ;
    N=0
  ->
    R=1
  ;
    N=1
  ->
    R=As
  ;
    R=As^N
  ).

simplify(A/B, R) :-
  !,
  simplify(A, As),
  simplify(B, Bs),
  (
    As=0
  ->
    R=0
  ;
    Bs=1
  ->
    R=As
  ;
    R=(As/Bs)
  ).

simplify(A, A).
