/** <first_order> first-order moment closure
 *
 *  example of use of the derivatives module to compute first-order ODEs
 *  Example session:
 *
 *  ?- use_module(first_order).
 *  ?- bc2pl('T7.bc').
 *  ?- print_crn.
 *  ?- print_xpp.
 *  ?- first_order.
 *  ?- print_xpp.
 *  ?- pl2xpp('T7.ode').
 *
 * @author Sylvain Soliman
 * @license GPL2
 */

:- module(first_order, [first_order/0]).
:- reexport(minibiocham).
:- use_module(symbolic).

%! first_order is det.
%
% using the current CRN create the ODE system corresponding to the first_order
% moment closure
first_order :-
  new_ode,
  forall(           % iterate some side-effect
    species(S),     % for all species
    first_order(S)  % call the predicate first_order/1
  ),
  forall(           % map the initial state in a trivial way
    initial(S, I),
    assertz(ode_init(S, I))
  ),
  forall(           % map the initial state in a trivial way
    parameter(K, V),
    assertz(ode_par(K, V))
  ).                % no function to add with ode_func


%! first_order(-Species:atom) is det
%
% adds the dx/dt part for x=Species corresponding to the first-order moment
% closure with respect to current CRN
first_order(S) :-
  % first ensure there's some existing dx/dt
  (
    ode_dxdt(S, _)
  ->
    true
  ;
    assertz(ode_dxdt(S, 0))
  ),
  % now iterate an update on this dx/dt
  forall(
    reaction(K, C),       % iterate for all reactions with rate K and change C
    add_change(C, K, S)   % add to dx/dt the corresponding variation of S
  ),
  % postprocessing: remove the ugly sum, simplify it, add it back
  retract(ode_dxdt(S, E)),
  simplify(E, SE),
  assertz(ode_dxdt(S, SE)).


%! add_change(-ChangeVector:list, -KineticLaw, -Species) is det
%
% iterate on the ChangeVector list to find the change for Species and add the
% corresponding KineticLaw*Change to dSpecies/dt

% No change, nothing to do
add_change([], _, _).

add_change([(S1, _) | L], K, S2) :-
  % the change is on another species, ignore and continue the change vector
  S1 \= S2,
  add_change(L, K, S2).

add_change([(S, C) | _], K, S) :-
  % the change is on S, get the current dS/dt, replace it with its sum with
  % C*K
  retract(ode_dxdt(S, E)),
  assertz(ode_dxdt(S, E + C*K)).
