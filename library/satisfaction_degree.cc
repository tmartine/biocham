#include <iostream>
#include <sstream>
#include <fstream>
#include <math.h>
#include <ppl.hh>
#include "ppl_validity_domain.hh"
#include "gsl_solver.hh"
#include "cmaes_interface.h"
#include "satisfaction_degree.hh"
#include "check.inc"
#include "domain_distance.inc"
#include "csv_reader.hh"

int
main(int argc, char *argv[])
{
    PPL::set_rounding_for_PPL();
    std::ifstream file(argv[1]);
    Table table = read_CSV_stream(file);
    Domain domain = compute_domain(table);
    double result = domain_distance(domain, 0);
    std::cout << (1 - (result / (1 + result))) << "." << std::endl;
    #ifdef DEBUG_FOLTL
    using PPL::IO_Operators::operator<<;
    std::cerr << domain << "." << std::endl;
    std::cerr << "distance: " << result << std::endl;
    #endif
    return EXIT_SUCCESS;
}
