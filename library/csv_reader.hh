#ifndef _csv_reader_hh_
#define _csv_reader_hh_
#include <cstddef>
#include <cstdlib>
#include <string>
#include <sstream>
#include "gsl_types.hh"

Table
read_CSV_stream(std::istream &stream);

#endif /* _csv_reader_hh_ */
