#include <stdlib.h>
#include <math.h>
#include <cstddef>
#include <ppl.hh>
#include "ppl_validity_domain.hh"
#include "gsl_solver.hh"
#include "cmaes_interface.h"
#include "satisfaction_degree.hh"
#include "ode.inc"
#include "check.inc"
#include "search_parameters.inc"
#include "domain_distance.inc"
#include "filter.inc"

#include <mpi.h>

static double
fitfun(gsl_solver *solver, gsl_solver_config *config, double values[DIMENSION])
{
    gsl_solver_state state;
    Table table;
    double result = 0;
    double max_distance = -INFINITY;
    Domain domain;

    for (int i = 0; i < CONDITIONS; i++) {
        init_gsl_solver_state(&state, solver);
        for (int j = 0; j < DIMENSION; j++) {
            const struct search_parameter *param = &search_parameters[j];
            double value = values[j];
            if (i == 0) { // first condition
                if (value < param->min) {
                    result += param->min - value;
                }
                else if (value > param->max) {
                    result += value - param->max;
                }
            }
            if (LOGNORMAL) {
                solver->p[param->index] = exp(value);
            } else {
                solver->p[param->index] = value;
            }
            #ifdef DEBUG_SEARCH
            std::cerr << "param " << j << " = "  << value << std::endl;
            #endif
        }
        // first condition, out-of-bounds penalty
        if (i == 0 && result > 0) {
            result += 10000;
            // if violation, simply return with no simulation
            return result;
        }
        (*setup_condition[i])(solver);

        // initial values might depend on parameters
        initial_values(state.x, solver->p);

        double hstart = fmax(config->initial_step_size, (config->time_final - config->time_initial) * config->minimum_step_size);
        #ifdef DEBUG_SEARCH
        fprintf(stderr, "hmin: %f, hmax: %f, hstart: %f\n", solver->d->hmin, solver->d->hmax, solver->d->h);
        #endif
        gsl_solver_iterate(&state, &table);
        free_gsl_solver_state(&state);
        #ifdef DEBUG_SEARCH
        fprintf(stderr, "hmin: %f, hmax: %f, hfinal: %f\n", solver->d->hmin, solver->d->hmax, solver->d->h);
        fprintf(stderr, "hstart to reset to: %f\n", hstart);
        #endif
        // reset step size and evolution, forces again initial step size because
        // otherwise seems useless
        gsl_odeiv2_driver_reset(solver->d);
        // we have to manually reset, since gsl_odeiv2_driver_reset_hstart seems to think fabs(x) < x
        solver->d->h = hstart;
        #ifdef DEBUG_SEARCH
        fprintf(stderr, "hstart after manual reset: %f\n", solver->d->h);
        #endif

        PPL::set_rounding_for_PPL();
        domain = (*compute_condition_domain[i])(table);
        #ifdef DEBUG_SEARCH
        using PPL::IO_Operators::operator<<;
        std::cerr << domain << "." << std::endl;
        std::cerr << j << " rows" << std::endl;
        #endif
        max_distance = fmax(max_distance, domain_distance(domain, i));
        #ifdef DEBUG_SEARCH
        std::cerr << "distance " << max_distance << std::endl;
        #endif
        PPL::restore_pre_PPL_rounding();
    }

    result += max_distance;
    return result;
}


int
main(int argc, char *argv[])
{
    cmaes_t evo;
    double *arFunvals, *const*pop, *xfinal;
    double xstart[DIMENSION], stddev[DIMENSION];
    int i;
    /* int j; */
    int lambda;
    gsl_solver_config config;
    gsl_solver solver;
    double best_vd = 1;

    MPI_Init(NULL, NULL);
    // Get the number of processes
    int world_size;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    // Get the rank of the process
    int world_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
    #ifdef DEBUG_SEARCH
    fprintf(stderr, "proc %d of %d\n", world_rank, world_size);
    #endif
    MPI_Request req;

    init_gsl_solver_config(&config);
    init_gsl_solver(&solver, &config);
    if (world_rank != 0) {
        double data[DIMENSION];
        double fit;
        MPI_Status status;
        int amount;
        while(true) {
            #ifdef DEBUG_SEARCH
            fprintf(stderr, "proc %d ready\n", world_rank);
            #endif
            MPI_Recv(&data, DIMENSION, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD, &status);
            MPI_Get_count(&status, MPI_DOUBLE, &amount);
            if (amount == DIMENSION) {
                #ifdef DEBUG_SEARCH
                fprintf(stderr, "proc %d received\n", world_rank);
                #endif
                fit = fitfun(&solver, &config, data);
                #ifdef DEBUG_SEARCH
                fprintf(stderr, "proc %d sending %f\n", world_rank, fit);
                #endif
                MPI_Isend(&fit, 1, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD, &req);
                MPI_Request_free(&req);
            } else {
                #ifdef DEBUG_SEARCH
                fprintf(stderr, "proc %d terminating\n", world_rank);
                #endif
                break;
            }
        }
    } else {
        for (i = 0; i < DIMENSION; i++) {
            const struct search_parameter *param = &search_parameters[i];
            double min = param->min;
            double max = param->max;
            xstart[i] = param->init;
            stddev[i] = (max - min) / 2;
        }
        arFunvals = cmaes_init(
            &evo, DIMENSION, xstart, stddev, SEED, 0, "cmaes_initials.par");
        cmaes_ReadSignals(&evo, "cmaes_signals.par");
        evo.sp.stStopFitness.flg = 1;
        evo.sp.stStopFitness.val = STOPFIT;
        // somehow still have to test for best_vd, not sure why...
        lambda = cmaes_Get(&evo, "lambda");
        #ifdef DEBUG_SEARCH
        fprintf(stderr, "population size %d spread on %d procs\n", lambda, world_size);
        #endif

        while(!cmaes_TestForTermination(&evo) && best_vd >= STOPFIT) {
            pop = cmaes_SamplePopulation(&evo);
            // Send all in a non-blocking way
            for (i = 0; i < lambda; ++i) {
                if (i % world_size != 0) {
                    #ifdef DEBUG_SEARCH
                    fprintf(stderr, "proc %d sending to %d\n", world_rank, i % world_size);
                    #endif
                    MPI_Isend(pop[i], DIMENSION, MPI_DOUBLE, i % world_size, 0, MPI_COMM_WORLD, &req);
                    MPI_Request_free(&req);
                }
            }
            // Do all locally
            for (i=0; i < lambda; ++i) {
                if (i % world_size == 0) {
                    arFunvals[i] = fitfun(&solver, &config, pop[i]);
                    #ifdef DEBUG_SEARCH
                    fprintf(stderr, "proc %d computing locally -> %f\n", world_rank, arFunvals[i]);
                    #endif
                }
            }
            // Wait for all results
            for (i=0; i < lambda; ++i) {
                if (i% world_size != 0) {
                    #ifdef DEBUG_SEARCH
                    fprintf(stderr, "proc %d waiting for %d\n", world_rank, i % world_size);
                    #endif
                    MPI_Recv(&arFunvals[i], 1, MPI_DOUBLE, i % world_size, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
                    #ifdef DEBUG_SEARCH
                    fprintf(stderr, "proc %d received %f from %d\n", world_rank, arFunvals[i], i % world_size);
                    #endif
                }
                #ifdef DEBUG_SEARCH
                fprintf(stderr, "distance: %f\n", arFunvals[i]);
                for (int j = 0; j < DIMENSION; ++j) {
                    fprintf(stderr, "   %.17g\n", pop[i][j]);
                }
                #endif
            }
            #ifdef DEBUG_SEARCH
            fprintf(stderr, "proc %d finished one loop\n", world_rank);
            #endif
            cmaes_UpdateDistribution(&evo, arFunvals);
            best_vd = cmaes_Get(&evo, "fbestever");
            #ifdef DEBUG_SEARCH
            fprintf(stderr, "Best satisfaction degree: %f\n",
                    1 / (1 + best_vd));
            xfinal = cmaes_GetNew(&evo, "xbestever");
            for (i = 0; i < DIMENSION; i++) {
                fprintf(stderr, "   %f\n", xfinal[i]);
            }
            #endif
            cmaes_ReadSignals(&evo, "cmaes_signals.par");
            fflush(stdout);
        }
        for (i=1; i < world_size; i++) {
            // send end message
            MPI_Send(&best_vd, 0, MPI_DOUBLE, i, 0, MPI_COMM_WORLD);
        }
        #ifdef DEBUG_SEARCH
        fprintf(stderr, "proc %d done\n", world_rank);
        #endif
        cmaes_WriteToFile(&evo, "all", "allcmaes.dat");
        if (cmaes_TestForTermination(&evo)) {
            char reason[3024];
            strncpy(reason, cmaes_TestForTermination(&evo), 3024);
            strtok(reason, "\n");
            printf("%s\n", reason);
        } else {
            printf("Biocham stop with best fitness %g < stop fitness %g\n",
                   best_vd, STOPFIT);
        }
        xfinal = cmaes_GetNew(&evo, "xbestever");
        for (i = 0; i < DIMENSION; i++) {
            #ifdef DEBUG_SEARCH
            fprintf(stderr, "%.17g\n", xfinal[i]);
            #endif
            if (LOGNORMAL) {
                printf("%.17g\n", exp(xfinal[i]));
            }
            else {
                printf("%.17g\n", xfinal[i]);
            }
        }
        printf("%f\n", 1 / (1 + best_vd));
        cmaes_exit(&evo);

        free(xfinal);
    }
    free_gsl_solver(&solver);

    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Finalize();

    return EXIT_SUCCESS;
}
