:- use_module(library(plunit)).

:- begin_tests(types, [setup((clear_model, reset_options))]).


test('compound', [true(Solution == 'RAF-RAFK')]) :-
  check_type(solution, 'RAF'-'RAFK', Solution).

test(
  'compound_reaction',
  [true(Reaction == ('RAF' + 'RAFK' <=> 1 * 'RAF-RAFK'))]
) :-
  check_type(reaction, 'RAF' + 'RAFK' <=> 1 * 'RAF'-'RAFK', Reaction).

:- end_tests(types).
