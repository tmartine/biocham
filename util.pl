:- module(
  util,
  [
    multiset_inclusion/2,
    add_list/3,
    multiply_list/3,
    name_variables_and_anonymous/2,
    camel_case/2,
    set_to_list/2,
    list_enumeration/2,
    write_successes/3,
    equals_to_list/2,
    list_to_equals/2,
    list/1,
    nth0_eqq/3,
    executable_filename/1,
    indent/1,
    indent/2,
    view_image/1,
    set_image_viewer_driver/1,
    list_file_lines/2,
    list_stream_lines/2,
    file_line_by_line/2,
    with_output_to_file/2,
    stream_line_by_line/2,
    term_morphism/3,
    substitute/4,
    substitute_all/4,
    substitute_once/4,
    rewrite/3,
    grammar_iter/3,
    grammar_map/4,
    call_subprocess/3,
    with_clean/2,
    clean/1,
    check_cleaned/1,
    join_with_op/3,
    extract_sublist/2,
    separate_list/4,
    uppercase_char/1,
    lowercase_char/1,
    alphabetic_char/1,
    alphanumeric_char/1,
    numeric_char/1,
    identifier_name/1,
    check_valid_identifier_name/1,
    new_molecule/1,
    new_parameter/1,
    unify_records/2,
    field_default/4,
    watch_error/2,
    open_file/1,
    wait_timeout_option/1,
    invert/2,
    abscmp/3,
    write_expression_to_c/2,
    write_expression_to_c_with_parentheses/2,
    write_condition_to_c/2,
    write_condition_to_c_with_parentheses/2,
    expand_polynomial/2,
    atom_to_int/2,
    realsetof/3,
   is_integer/2
   ]).

:- use_module(library(process)).
:- use_module(doc).


multiset_inclusion([],_).
multiset_inclusion([(S*M)|L], Multiset):-
  member((T*M), Multiset),
  S=<T,
  multiset_inclusion(L, Multiset).


name_variables(L) :-
  doc('
    instantiates a list of bindings returned by \\texttt{read_term/3}\'s
    \\texttt{variable_names} option (\\texttt{X = \'X\'}).
  '),
  maplist(call, L).


name_anonymous(L) :-
  doc('associates the atom \\texttt{\'_\'} to each variable in \\argument{L}.'),
  maplist(=('_'), L).


name_variables_and_anonymous(Goal, VariableNames) :-
  doc('
    instantiates all variables in \\argument{Goal}, either through the
    \\argument{VariableNames} bindings or to the atom \\texttt{\'_\'}.
  '),
  name_variables(VariableNames),
  term_variables(Goal, AnonymousVariables),
  name_anonymous(AnonymousVariables).


camel_case(Atom, CamelCaseAtom) :-
  atom_chars(Atom, AtomChars),
  camel_case_chars(AtomChars, CamelCaseAtomChars),
  atom_chars(CamelCaseAtom, CamelCaseAtomChars).


camel_case_chars([], []).

camel_case_chars([First | Chars], [UpperFirst | CamelChars]) :-
  upper_lower(UpperFirstCode, First),
  atom_codes(UpperFirst, [UpperFirstCode]),
  camel_case_chars_tail(Chars, CamelChars).


camel_case_chars_tail([], []).

camel_case_chars_tail(['_' | Chars], CamelChars) :-
  !,
  camel_case_chars(Chars, CamelChars).

camel_case_chars_tail([Head | Chars], [Head | CamelChars]) :-
  camel_case_chars_tail(Chars, CamelChars).


set_to_list({ Enum }, List) :-
  !,
  list_enumeration(List, Enum).

set_to_list(Item, [Item]).


list_enumeration([], '_') :-
  !.

list_enumeration([H], H) :-
  H \= (_, _),
  !.

list_enumeration([H | List], (H, Enum)) :-
  list_enumeration(List, Enum).


:- dynamic(first/0).


write_successes(Goal, WriteSeparator, WriteSuccess) :-
  assertz(first),
  \+ (
    Goal,
    \+ (
      (
        retract(first)
      ->
        true
      ;
        WriteSeparator
      ),
      WriteSuccess
    )
  ).


equals_to_list(A = B, List) :-
  !,
  equals_to_list(A, LA),
  append(LA, LB, List),
  equals_to_list(B, LB).

equals_to_list(A, [A]).


list_to_equals([A], A) :-
  !.

list_to_equals([Head | Tail], Head = EqTail) :-
  list_to_equals(Tail, EqTail).


%! list(+L)
%
% Check that L is a list, deprecated, should use the built-in is_list instead

list(List) :-
  is_list(List).


%! add_list(+List1,+List2,-ListSum)
%
% Add two lists term at term
% (Note that using maplist(plus(...)) work only for integers

add_list([Head1|Tail1], [Head2|Tail2], [Head12|Tail12]) :-
  Head12 is Head1 + Head2,
  add_list(Tail1, Tail2, Tail12).

add_list([], [], []).


%! multiply_list(+List, +A, -Alist)
%
% Alist = A x List

multiply_list([], _, []).

multiply_list([Head| Tail], A, [AHead| ATail]) :-
  AHead is A*Head,
  multiply_list(Tail, A, ATail).


nth0_eqq(0, [Head | _Tail], Result) :-
  Head == Result.

nth0_eqq(N, [_Head | Tail], Result) :-
  nth0_eqq(M, Tail, Result),
  N is M + 1.


executable_filename(Filename) :-
  current_prolog_flag(os_argv, [Exec | _]),
  (
    absolute_file_name(Exec, Absolute, [access(execute), file_errors(fail)])
  ->
    true
  ;
    absolute_file_name(path(Exec), Absolute, [access(execute)])
  ),
  (
     read_link(Absolute, _, Filename)
  ->
     true
  ;
     Filename = Absolute
   ).


indent(Level) :-
  indent(current_output, Level).


indent(Stream, Level) :-
  \+ (
    between(1, Level, _),
    \+ (
      write(Stream, '  ')
    )
  ).


view_image(Filename) :-
  get_image_viewer_driver(Driver),
  Goal =.. [Driver, Filename],
  Goal.


img_tag(Filename) :-
  format('<img src="~a" alt="Simulation results">\n', [Filename]).


get_image_viewer_driver(Driver) :-
  nb_getval(image_viewer_driver, Driver).


set_image_viewer_driver(Driver) :-
  nb_setval(image_viewer_driver, Driver).


list_file_lines(Filename, List) :-
  findall(
    Line,
    file_line_by_line(Filename, Line),
    List
  ).


list_stream_lines(Stream, List) :-
  findall(
    Line,
    stream_line_by_line(Stream, Line),
    List
  ).


file_line_by_line(Filename, Line) :-
  setup_call_cleanup(
    open(Filename, read, Stream),
    stream_line_by_line(Stream, Line),
    close(Stream)
  ).


stream_line_by_line(Stream, Line) :-
  repeat,
  read_line_to_codes(Stream, Codes),
  (
    Codes = end_of_file
  ->
    !,
    fail
  ;
    atom_codes(Line, Codes)
  ).


:- meta_predicate with_output_to_file(+, 0).


with_output_to_file(Filename, Goal) :-
  setup_call_cleanup(
    open(Filename, write, Stream),
    with_output_to(Stream, Goal),
    close(Stream)
  ).


:- meta_predicate term_morphism(2, +, -).

term_morphism(F, TermIn, TermOut) :-
  devdoc('General term morphism.'),
  functor(TermIn, Functor, Arity),
  functor(TermOut, Functor, Arity),
  TermIn =.. [Functor | ArgumentsIn],
  TermOut =.. [Functor | ArgumentsOut],
  maplist(F, ArgumentsIn, ArgumentsOut).


%! substitute(+Parameters, +Arguments, +ExpressionIn, -ExpressionOut)
%
% Replace every occurence of the elements of the list Parameters by their corresponding
% element in the list Arguments in ExpressionIn thus giving ExpressionOut.

substitute(Parameters, Arguments, ExpressionIn, ExpressionOut) :-
  devdoc('General substitution of terms by other terms in an expression.'),
  nth0(Index, Parameters, ExpressionIn),
  !,
  nth0(Index, Arguments, ExpressionOut).

substitute(Parameters, Arguments, ExpressionIn, ExpressionOut) :-
  term_morphism(substitute(Parameters, Arguments), ExpressionIn, ExpressionOut).

%! substitute_all(+Par, +Arg, +In, -Out)

substitute_all(P,A,I,O) :-
  copy_term([P,A], [CP,CA], _G),
  substitute(CP,CA,I,T),
  (
    I = T
  ->
    O = T
  ;
    substitute_all(P,A,T,O)
  ).

%! subsitute_once(+Par, +Arg, +In, -Out)
%
% the one-shot version of substitute

substitute_once(P, A, In, Out) :-
  (
    substitute_once_sr(P, A, [In], [Out]),!
  ;
    Out = In
  ).

substitute_once_sr(P, A, [P|Tail], [A|Tail]) :- !.

substitute_once_sr(P, A, [HeadIn|TailIn], [HeadOut|TailOut]) :-
  (
    HeadIn =.. TryIn,
    \+(length(TryIn, 1)),
    substitute_once_sr(P, A, TryIn, TryOut)
  ->
    HeadOut =.. TryOut,
    TailIn = TailOut
  ;
    HeadOut = HeadIn,
    substitute_once_sr(P, A, TailIn, TailOut)
  ).


:- meta_predicate rewrite(2, +, -).

rewrite(System, In, Out) :-
  (
    term_morphism(rewrite(System), In, Step0)
  ->
    (
      call(System, Step0, Step1),
      Step1 \= Step0
    ->
      rewrite(System, Step1, Out)
    ;
      Out = Step0
    )
  ;
    call(System, In, Step),
    In \= Step
  ->
    rewrite(System, In, Out)
  ;
    Out = In
  ).


grammar_iter(Grammar, Rules, Term) :-
  devdoc('
Iterates over the nodes of a \\argument{Term} of type \\argument{Grammar}.
\\argument{Rules} is an associative list that maps (a subset of)
non-terminals of \\argument{Grammar} to predicates.
If \\texttt{non_terminal: p(Args)} is in \\argument{Rules}, then
\\texttt{p(Args, Subterm)} is executed for every sub-term of
\\argument{Term} of type \\texttt{non_terminal}.
  '),
  Head =.. [Grammar, Term],
  (
    clause(Head, Body),
    Body
  ->
    true
  ;
    throw(error(unexpected_term(Term, Grammar), grammar_iter))
  ),
  !,
  grammar_iter_aux(Body, Rules).


grammar_iter_aux(true, _Rules) :-
  !.

grammar_iter_aux((A, B), Rules) :-
  !,
  grammar_iter_aux(A, Rules),
  grammar_iter_aux(B, Rules).

grammar_iter_aux(function_application(Grammar, Term), Rules) :-
  !,
  Term =.. [_F | Arguments],
  maplist(grammar_iter(Grammar, Rules), Arguments).

grammar_iter_aux(G, Rules) :-
  G =.. [F, Argument],
  (
    memberchk((F: P), Rules)
  ->
    call(P, Argument)
  ;
    atomic_type(F)
  ->
    true
  ;
    grammar_iter(F, Rules, Argument)
  ).


atomic_type(untyped).

atomic_type(atom).

atomic_type(number).

atomic_type(integer).


grammar_map(Grammar, Rules, In, Out) :-
  devdoc('
Transforms the term \\argument{In} of type \\argument{Grammar}
into the term \\argument{Out}.
\\argument{Rules} is an associative list that maps (a subset of)
non-terminals of \\argument{Grammar} to predicates.
If \\texttt{non_terminal: p(Args)} is in \\argument{Rules}, then
\\texttt{p(Args, SubIn, SubOut)} is executed for every sub-term
\\argument{SubIn} of \\argument{Term} of type \\texttt{non_terminal},
and the sub-term is substituted by \\argument{SubOut}.
Nodes that have a type that does not correspond to a
non-terminal mentioned in \\argument{Rules} are left as is.
  '),
  (
    atomic_type(Grammar)
  ->
    Out = In
  ;
    ProtoHead =.. [Grammar, ProtoArg],
    clause(ProtoHead, ProtoBody),
    copy_term((ProtoArg, ProtoBody), (In, BodyIn)),
    BodyIn
  ->
    Out = ProtoArg,
    BodyOut = ProtoBody,
    grammar_map_aux(BodyIn, BodyOut, Rules)
  ;
    throw(error(unexpected_term(In, Grammar), grammar_map))
  ).


grammar_map_aux(true, true, _Rules) :-
  !.

grammar_map_aux((AIn, BIn), (AOut, BOut), Rules) :-
  !,
  grammar_map_aux(AIn, AOut, Rules),
  grammar_map_aux(BIn, BOut, Rules).

grammar_map_aux(
  function_application(Grammar, In), function_application(Grammar, Out), Rules
) :-
  !,
  In =.. [F | InArguments],
  length(InArguments, L),
  length(OutArguments, L),
  Out =.. [F | OutArguments],
  maplist(grammar_map(Grammar, Rules), InArguments, OutArguments).

grammar_map_aux(PIn, POut, Rules) :-
  PIn =.. [F, ArgumentIn],
  POut =.. [F, ArgumentOut],
  (
    memberchk((F: P), Rules)
  ->
    call(P, ArgumentIn, ArgumentOut)
  ;
    grammar_map(F, Rules, ArgumentIn, ArgumentOut)
  ).


call_subprocess(ExecutableFilename, Arguments, Options) :-
  absolute_file_name(ExecutableFilename, Absolute),
  process_create(Absolute, Arguments, [process(Pid) | Options]),
  process_wait(Pid, exit(0)).


user:message_hook(_Msg, warning, _Lines) :-
  catch(
    count(warnings, _),
    _,
    true
  ),
  fail.


prolog:message(Atom) -->
  { atomic(Atom) },
  [ '~w'-[Atom]].


:- meta_predicate with_clean(+, 0).

with_clean(Prototypes, Goal) :-
  setup_call_cleanup(
    maplist(check_cleaned, Prototypes),
    Goal,
    maplist(clean, Prototypes)
  ).


clean(F / N) :-
  functor(H, F, N),
  retractall(H).


clean(M : F / N) :-
  functor(H, F, N),
  retractall(M : H).


check_cleaned(F / N) :-
  functor(H, F, N),
  (
    H
  ->
    throw(error(check_cleaned(F / N)))
  ;
    true
  ).


check_cleaned(M : F / N) :-
  functor(H, F, N),
  (
    M : H
  ->
    throw(error(check_cleaned(M : F / N)))
  ;
    true
  ).


join_with_op([Item], _Op, Item) :-
   % indexing not enough to efficiently remove choice point
   !.

join_with_op([I1, I2 | Items], Op, Term) :-
  Term =..[Op, I1, T],
  join_with_op([I2 | Items], Op, T).


%! extract_sublist(+List, -SubList)
%
% generates all sublists of List

extract_sublist([],[]).

extract_sublist([H | T], [H | TT]) :-
   extract_sublist(T, TT).

extract_sublist([_ | T], TT) :-
   extract_sublist(T, TT).


%! separate_list(+List12, +N, -List1, -List2)
%
% separate a list in two at a predefined length
% may also be used to extract the N first elements of a list

separate_list(List12, N, List1, List2) :-
  length(List1, N),
  append(List1, List2, List12).


uppercase_char(Char) :-
  Char @>= 'A', Char @=< 'Z'.


lowercase_char(Char) :-
  Char @>= 'a', Char @=< 'z'.


alphabetic_char(Char) :-
  (
    uppercase_char(Char)
  ;
    lowercase_char(Char)
  ),
  !.


numeric_char(Char) :-
  Char @>= '0', Char @=< '9'.


alphanumeric_char(Char) :-
  (
    alphabetic_char(Char)
  ;
    numeric_char(Char)
  ).


%! identifier_name(Atom)
%
% check if Atom is a correct name for a function
% due to its high usage in integration, we choose to memoize the correct name.

:-dynamic(valid_name/1).

identifier_name(Atom) :-
  (
    valid_name(Atom)
  ->
    true
  ;
    atom_chars(Atom, [First | Chars]),
    alphabetic_char(First),
    \+ (
      member(Char, Chars),
      \+ (
        (
          alphanumeric_char(Char)
        ;
          % ad-hoc bypassing for species name
          member(Char,['-','_','.',',','~','{','}','(',')'])
        )
      )
    ),
    assertz(valid_name(Atom))
  ).


check_valid_identifier_name(Atom) :-
  (
    identifier_name(Atom)
  ->
    true
  ;
    throw(error(invalid_identifier_name(Atom)))
  ).


%! new_molecule(-Name)
%
% Generate the name of a new (and non-existent) molecule of the form A,B,C,..,AA,BB, etc.

new_molecule(Name) :-
  count(molecule_id, N),
  divmod(N,26,R,C),
  Code is C+65,
  Rep is R+1,
  char_code(Letter, Code),
  stutter(Rep, Letter, Try),
  ( % test if the molecule already exist
    single_model(ModelId),
    identifier_kind(ModelId, Try, object)
  ->
    new_molecule(Name)
  ;
    Name = Try
  ).

%! stutter(N,In,Out)

stutter(1, In, In) :- !.

stutter(N, In, Out) :-
  Nm is N-1,
  stutter(Nm, p, Tempo),
  atom_concat(In, Tempo, Out).


%! new_parameter(-Name)
%
% Generate the name of a new (and non-existent) parameter of the form kn

new_parameter(Name) :-
  count(parameter_id,  N),
  atom_concat('k_',N,Try),
  (
    single_model(ModelId),
    identifier_kind(ModelId, Try, parameter)
  ->
    new_parameter(Name)
  ;
    Name = Try
  ).


unify_records(Record0, Record1) :-
  (
    (var(Record0) ; var(Record1))
  ->
    Record0 = Record1
  ;
    maplist(unify_field(Record1), Record0)
  ).


unify_field(Record1, Field: Value0) :-
  (
    member(Field: Value1, Record1)
  ->
    Value0 = Value1
  ;
    throw(error(unknown_field(Field)))
  ).


field_default(Field, Default, Record, Value) :-
  (
    member(Field: Value, Record)
  ->
    true
  ;
    Value = Default
  ).


watch_error(Goal, OnError) :-
  devdoc('
    Executes \\argument{Goal} once.
    If \\argument{Goal} fails or throws an exception,
    \\argument{OnError} is executed once and the failure or the exception is
    propagated.
  '),
  catch(
    (
      Goal
    ->
      true
    ;
      once(OnError),
      fail
    ),
    E,
    (
      once(OnError),
      throw(E)
    )
  ).


open_file(Filename) :-
  current_prolog_flag(arch, Arch),
  (
    sub_atom(Arch, _, 6, _, darwin)
  ->
    Open = 'open'
  ;
    Open = 'xdg-open'
  ),
  process_create(path(Open), [Filename], [process(Pid)]),
  process_wait(Pid, exit(0)).


%! wait_timeout_option(Id)
%
% wait for Id until timeout defined by biocham option
wait_timeout_option(Id) :-
  get_option(timeout, Timeout),
  catch(
    call_with_time_limit(Timeout, wait(Id, _)), 
    time_limit_exceeded, 
    (
      process_kill(Id), 
      format('~ntimed out ~wsec~n', [Timeout]),
      fail
    )
  ).


invert(X, Y) :-
  Y is -X.


abscmp(Order, X, Y) :-
  XX is abs(X),
  YY is abs(Y),
  compare(Order, XX, YY).


write_expression_to_c(- A, CallBack) :-
  write('- '),
  write_expression_to_c_with_parentheses(A, CallBack).

write_expression_to_c(A + B, CallBack) :-
  write_expression_to_c_with_parentheses(A, CallBack),
  write(' + '),
  write_expression_to_c_with_parentheses(B, CallBack).

write_expression_to_c(A - B, CallBack) :-
  write_expression_to_c_with_parentheses(A, CallBack),
  write(' - '),
  write_expression_to_c_with_parentheses(B, CallBack).

write_expression_to_c(A * B, CallBack) :-
  write_expression_to_c_with_parentheses(A, CallBack),
  write(' * '),
  write_expression_to_c_with_parentheses(B, CallBack).

write_expression_to_c(A / B, CallBack) :-
  write_expression_to_c_with_parentheses(A, CallBack),
  write(' / '),
  write_expression_to_c_with_parentheses(B, CallBack).

write_expression_to_c(A ^ B, CallBack) :-
  write('pow('),
  call(CallBack, A),
  write(', '),
  call(CallBack, B),
  write(')').

write_expression_to_c(sqrt(A), CallBack) :-
  write('sqrt('),
  call(CallBack, A),
  write(')').

write_expression_to_c(cos(A), CallBack) :-
  write('cos('),
  call(CallBack, A),
  write(')').

write_expression_to_c(sin(A), CallBack) :-
  write('sin('),
  call(CallBack, A),
  write(')').

write_expression_to_c(exp(A), CallBack) :-
  write('exp('),
  call(CallBack, A),
  write(')').

write_expression_to_c(log(A), CallBack) :-
  write('log('),
  call(CallBack, A),
  write(')').

write_expression_to_c(max(A, B), CallBack) :-
  write('fmax('),
  call(CallBack, A),
  write(', '),
  call(CallBack, B),
  write(')').

write_expression_to_c(min(A, B), CallBack) :-
  write('fmin('),
  call(CallBack, A),
  write(', '),
  call(CallBack, B),
  write(')').

write_expression_to_c_with_parentheses(Expression, CallBack) :-
  write('('),
  call(CallBack, Expression),
  write(')').


write_condition_to_c(A = B, CallBack) :-
  write_expression_to_c_with_parentheses(A, CallBack),
  write(' == '),
  write_expression_to_c_with_parentheses(B, CallBack).

write_condition_to_c(A < B, CallBack) :-
  write_expression_to_c_with_parentheses(A, CallBack),
  write(' < '),
  write_expression_to_c_with_parentheses(B, CallBack).

write_condition_to_c(A > B, CallBack) :-
  write_expression_to_c_with_parentheses(A, CallBack),
  write(' > '),
  write_expression_to_c_with_parentheses(B, CallBack).

write_condition_to_c('<='(A, B), CallBack) :-
  write_expression_to_c_with_parentheses(A, CallBack),
  write(' <= '),
  write_expression_to_c_with_parentheses(B, CallBack).

write_condition_to_c(A >= B, CallBack) :-
  write_expression_to_c_with_parentheses(A, CallBack),
  write(' >= '),
  write_expression_to_c_with_parentheses(B, CallBack).

write_condition_to_c(true, _CallBack) :-
  write('true').

write_condition_to_c(false, _CallBack) :-
  write('false').

write_condition_to_c(or(A, B), CallBack) :-
  write_condition_to_c_with_parentheses(A, CallBack),
  write(' || '),
  write_condition_to_c_with_parentheses(B, CallBack).

write_condition_to_c(and(A, B), CallBack) :-
  write_condition_to_c_with_parentheses(A, CallBack),
  write(' && '),
  write_condition_to_c_with_parentheses(B, CallBack).

write_condition_to_c(not(A), CallBack) :-
  write('!'),
  write_condition_to_c_with_parentheses(A, CallBack).


write_condition_to_c_with_parentheses(Condition, CallBack) :-
  write('('),
  write_condition_to_c(Condition, CallBack),
  write(')').




%! expand_polynomial(+Polynomial, -Expanded_polynomial)
%
% Expand a polynomial for parsing purpose

expand_polynomial(Poly, Expanded) :-
	expand_polynomial_sr(Poly, Test),
	(
		Poly = Test
	->
		Expanded = Test
	;
		expand_polynomial(Test,Expanded)
	).

expand_polynomial_sr(A*(B+C), (A*B) + (A*C)) :- !.
expand_polynomial_sr(A*(B-C), (A*B) - (A*C)) :- !.
expand_polynomial_sr((B+C)*A, (B*A) + (C*A)) :- !.
expand_polynomial_sr((B-C)*A, (B*A) - (C*A)) :- !.
expand_polynomial_sr(A*(B+C)*D, (A*B*D) + (A*C*D)) :- !.
expand_polynomial_sr(A*(B-C)*D, (A*B*D) - (A*C*D)) :- !.
expand_polynomial_sr(A*(B*C), (A*B)*C) :- !.
expand_polynomial_sr((B+C)/A, (B/A) + (C/A)) :- !.
expand_polynomial_sr((B-C)/A, (B/A) - (C/A)) :- !.
expand_polynomial_sr(A*B/C, A/C*B) :- !.
expand_polynomial_sr(E-(A+B), E-A-B) :- !.
expand_polynomial_sr(-(A+B), -A-B) :- !.
expand_polynomial_sr(A+B, AA+BB) :- !,
	expand_polynomial_sr(A,AA),
	expand_polynomial_sr(B,BB).
expand_polynomial_sr(A-B, AA-BB) :- !,
	expand_polynomial_sr(A,AA),
	expand_polynomial_sr(B,BB).
expand_polynomial_sr(A*B, AA*BB) :- !,
	expand_polynomial_sr(A,AA),
	expand_polynomial_sr(B,BB).
expand_polynomial_sr(A, A).

%! atom_to_int(+String:atom, -Int:integer) is det.
%
% Read an Int from a String (atom)
atom_to_int(String, Int) :-
  read_term_from_atom(String, Int, []),
  integer(Int).


% what setof should have always been

realsetof(X, Goal, List):-
  (
   setof(X, Goal, L)
  ->
   List=L
  ;
   List=[]
  ).


% is_integer(+Number, -Value)
%
% Determine if Expr is an integer

is_integer(Number, Value) :- 
  (
    number(Number),
    !,
    N = Number
  ;
    item([kind:parameter, key:Number, item:parameter(Number=N)]),
    !
  ;
    get_current_ode_system(Id),
    item([parent:Id, kind:parameter, item:par(Number = N)])
  ),
  normalize_number(N, Value),
  integer(Value).


