:- use_module(library(plunit)).

:- use_module(gpac).

:- begin_tests(gpac, [setup((clear_model, reset_options))]).


%%% Test for format_pivp %%%

test(format_pivp_exp) :-
   gpac:format_pivp(((1,(d(y)/dt=(-2*y)))),
                    [1,[[[K_1,[1]]]],[1]],
                    [y]),
   K_1 =:= -2.

test(format_pivp_cosinus) :-
   gpac:format_pivp(((1,(d(y)/dt=z)); (0,(d(z)/dt= (-1*y)))),
                    [K_1,[[[1,[0, 1]]],[[K_2,[1, 0]]]],[1,0]],
                    [y,z]),
   K_1 =:= 2,
   K_2 =:= -1.

% This PIVP generates choicepoints in the cutting of y*z+3*z, hence the once
test(format_pivp_higher_order) :-
   once(gpac:format_pivp(((1,(d(y)/dt=y*z+3*z)); (5,(d(z)/dt= (-1*y^3)))),
                    [K_1, [[[K_2, [0, 1]], [1, [1, 1]]], [[K_3, [3, 0]]]], [1, 5]],
                    [y,z])),
   K_1 =:= 2,
   K_2 =:= 3,
   K_3 =:= -1.

test(sort_output) :-
  gpac:sort_output((1,d(cos)/dt=(-1*sin);0,d(sin)/dt=cos),cos,(1,d(cos)/dt=(-1*sin);0,d(sin)/dt=cos)),
  gpac:sort_output((0,d(sin)/dt=cos;1,d(cos)/dt=(-1*sin)),cos,(1,d(cos)/dt=(-1*sin);0,d(sin)/dt=cos)),
  gpac:sort_output((1.0, (d(f)/dt = f^3));(1.0, (d(g)/dt = f^3));(1.0, (d(h)/dt = g^3)), h,
                   (1.0, d(h)/dt = g^3);(1.0, d(f)/dt = f^3);(1.0, d(g)/dt = f^3)).

test(extract_names) :-
  gpac:extract_names((1, d(a)/dt = 1; 2, d(b)/dt = 2), [a,b]).

%%% Test of g_to_c %%%
test(g_to_c_PIVP) :-
   Old_PIVP = [1, [[[-1,[1]]]], [1]],
   New_PIVP = [2, [[[-1,[1,1]]],[[-1,[0,1]]]], [1,input]],
   gpac:g_to_c_PIVP(Old_PIVP,New_PIVP, 1.0).

test(invert_PIVP) :-
   gpac:exp_PIVP(Exp),
   ResPIVP = [2,[[[-1,[2,1]]],[[1,[0,1]]]],[1,1]],
   gpac:invert_PIVP(Exp, ResPIVP).

test(add_PIVP) :-
   PIVP1 = [1, [ [ [1, [1]] ] ], [1]],
   PIVP2 = [1, [ [ [-1, [2]] ] ], [1]],
   gpac:add_PIVP(PIVP1, PIVP2, PIVP12),
   PIVP12 = [3,[[[-1,[0,0,2]],[1,[0,1,0]]],[[1,[0,1,0]]],[[-1,[0,0,2]]]],[2,1,1]].

test(multiply_PIVP) :-
   PIVP1 = [1, [ [ [1, [1]] ] ], [1]],
   PIVP2 = [1, [ [ [-1, [2]] ] ], [1]],
   gpac:multiply_PIVP(PIVP1, PIVP2, PIVP12),
   PIVP12 = [3,[[[1,[0,1,1]],[-1,[0,1,2]]],[[1,[0,1,0]]],[[-1,[0,0,2]]]],[1,1,1]].

test(parse_monomial) :-
  gpac:parse_monomial(2.0*a*b, [a,b], [2.0, [1, 1]]),
  gpac:parse_monomial(2.0*(a*b), [a,b], [2.0, [1, 1]]),
  gpac:parse_monomial(a*(2.0*b), [a,b], [2.0, [1, 1]]),
  gpac:parse_monomial(a*(2.0*b)/3, [a,b], [2.0/3, [1, 1]]).

test(negate_keep) :-
  gpac:negate_keep([[1,0,0]],[a,b,c],[a,b_p,b_m,c],[[1,0,0,0]]),
  gpac:negate_keep([[0,1,0]],[a,b,c],[a,b_p,b_m,c],[[0,1,0,0],[0,0,1,0]]),
  gpac:negate_keep([[1,0,0],[0,0,1]],[a,b,c],[a,b_p,b_m,c],[[1,0,0,0],[0,0,0,1]]).

:- end_tests(gpac).
