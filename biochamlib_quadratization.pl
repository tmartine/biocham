#!/usr/bin/prolog
/*
 * The prolog script to create the stand-alone saved state for quadratization, it reads
 * and returns an .ode file.
 * Coder: M. Hemery
 */

% Loading libraries
:- use_module(arithmetic_rules).
:- use_module(biocham).
:- use_module(counters).
:- use_module(doc).
:- use_module(kinetics).
:- use_module(models).
:- use_module(namespace).
:- use_module(ode). 
:- use_module(quadratic_reduction). 
:- use_module(sat). 
:- use_module(toplevel).
:- use_module(types).
:- use_module(util).
:- use_module(xpp_parser).

set_my_option(Option, Value) :-
  change_item([], option, Option, option(Option: Value)).

% parse_arguments(+Arguments, -Input, -Method, -Timeout)
%
% Handle the various option of the standalone:
% --method: method used to perform the reduction
% --timeout: timeout for sat call
% --help: print help and quit
% -i: input file

parse_arguments([], none, sat_species, 120).

parse_arguments(['--method', Method| Argv], Input, Method, Timeout) :-
  parse_arguments(Argv, Input, _M, Timeout).

parse_arguments(['--timeout', Timeout| Argv], Input, Method, Timeout) :-
  parse_arguments(Argv, Input, Method, _T).

parse_arguments(['-i', Input| Argv], Input, Method, Timeout) :-
  parse_arguments(Argv, _I, Method, Timeout).

parse_arguments(['--help'| _Argv], _I, _M, _T) :-
  print_help.

% print_help

print_help :-
  format("help~n", []),
  halt.

% main(+argv)

main(Argv) :-
  (Argv = [] -> print_help ; true),
  % Handling naming and options
  parse_arguments(Argv, Input, Method, Timeout),
  file_name_extension(InputBase, 'ode', Input),
  string_concat(InputBase, '_quad', OutputBase),
  file_name_extension(OutputBase, 'ode', Output),
  format("Input: ~w~nOutput: ~w~n", [Input, Output]),
  % Initialization (see biocham:initialize)
  set_prolog_flag(allow_variable_name_as_functor, true),
  set_counter(list_item_counter, 0),
  set_counter(item_id, 0),
  set_counter(model, 0),
  set_counter(molecule_id, 0),
  set_counter(parameter_id, 1),
  nb_setval(ode_viewer, inline),
  nb_setval(current_models, []),
  new_model,
  set_my_option(quadratic_reduction, Method),
  set_my_option(timeout, Timeout),
  set_my_option(stats, no),
  % Main procedure
  read_xpp(Input),
  list_ode,
  quadratic_reduction_ODE,
  select_ode_system(quadratic_ode),
  export_xpp(Output).


