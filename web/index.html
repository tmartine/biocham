<!DOCTYPE doctype PUBLIC "-//w3c//dtd html 4.0 transitional//en">
<html>
<head>
  <meta http-equiv="Content-Type"
 content="text/html; charset=utf-8">
  <title>BIOCHAM 4 web page</title>
  <style type="text/css">
    h1 {
      font-size: 1.5em;
      font-weight: bold;
      text-align: center;
      padding: 0;
      margin: 0;
    }
    .authors {
      text-align: center;
      padding-bottom: .5em;
    }
    h2 {
      padding: 0;
      margin: 0;
      font-size: 1em;
      font-weight: bold;
      text-decoration: underline;
    }
    .new {
      color: red;
      font-weight: bold;
      text-decoration: underline;
    }
  </style>
</head>
<body>
<h1>The Biochemical Abstract Machine BIOCHAM 4</h1>

<h1>version 4.6.23 February 2023</h1>

<div class="authors">
<p>
<a href="http://lifeware.inria.fr/">Inria EPI Lifeware</a>, Centre Inria Saclay Ile de France <br>
<a href="http://lifeware.inria.fr/biocham4/">http://lifeware.inria.fr/biocham4/</a><br>
<a href="https://gitlab.inria.fr/lifeware/biocham">https://gitlab.inria.fr/lifeware/biocham</a>
<br>
developpers mailing list: <a href="mailto:biocham@inria.fr">biocham@inria.fr</a><br>
</p>
<img src="logo.png" alt="Biocham logo"/>
</div>




<h2>Overview</h2>
<p>The Biochemical Abstract Machine (Biocham) is a modelling
environment for systems biology and synthetic biology, based on the computational theory of Chemical Reaction Networks (CRN).<br>
</p>
<p>Biocham is mainly composed of :<br>
</p>
<ul>
  <li>a rule-based language for modeling biochemical processes
with reactions (i.e. CRN, compatible with SBML) and/or influences (compatible with QualSBML),</li>
  <li>a hierarchy of semantics (differential, stochastic, Petri Net, Boolean) to interpret, analyze or synthesize CRNs,</li>
  <li>a temporal logic based language to formalize behaviors of biochemical systems and validate, or synthesize, CRNs with respect to temporal specifications,<br>
  </li>
<li>a command line language interfaced with Jupyter for both notebook (i.e. time line view of the commands) and graphical user interface (i.e. static view of the commands) uses.
  </li>
</ul>

<p>Biocham is a free software protected by the <a
 href="http://www.gnu.org/copyleft/gpl.html">GNU General Public License
GPL version 2</a>. This is an Open Source license that allows free
usage
of this software. Biocham is implemented in SWI-Prolog (43000 LOC) and interfaced with several third-party software.<br>
</p>


<p>
  Biocham 4 was first released in September 2017.
  Biocham 4 is a complete rewriting of Biocham 3 for easier maintenance.
  It integrates new features, in particular concerning
  <ul>
    <li>
      the synthesis of CRN implementing a mathematical function (either of time or of some input),</li>
    <li> robustness optimisation with respect to behavior specification in quantitative temporal logic,</li>
    <li> influence systems combined with CRNs.</li>.
    </ul>
  For the sake of reproducible research, Biocham 3 is still active on <a href="http://lifeware.inria.fr/biocham3">http://lifeware.inria.fr/biocham3</a> but no longer maintained. Biocham 0 was first released in 2002.
</p>

<p>Feedback on the use of Biocham in applications, research or teaching
are particularly welcomed on <a href="mailto:biocham@inria.fr">biocham@inria.fr</a> </p>
<h2>Demonstrations</h2>
<ul>
<li> <a href="http://lifeware.inria.fr/biocham4/online/notebooks/tutorial/tutorialShort.ipynb">short tutorial</a> notebook (presented at <a href="https://assb.lri.fr/fr/programme.php"> aSSB 2018</a>)</li>
<br>
  <li>Demonstration <a href="biocham4.mov" biocham4.mov>video</a> (with comments in French) and associated jupyter notebook online <a href="http://lifeware.inria.fr/biocham4/online/notebooks/doctor_in_the_cell/diagnosis.ipynb">doctor in the cell</a> </li>
  <br>
<li> <a href="http://lifeware.inria.fr/biocham4/online/notebooks/tutorial/tutorialLongHistory.ipynb">historical tutorial</a> notebook (presented at <a href="https://www.etit.tu-darmstadt.de/cmsb2017/cmsb_2/home/index.en.jsp"> CMSB 2017</a>).</li>
<br>
<li>
<a href="http://lifeware.inria.fr/biocham4/online/">Jupyter notebook with Biocham kernel </a> (http://lifeware.inria.fr/biocham4/online/) to try the other examples or upload your own ones on our server.
</li>
</ul>
<h2>Documentation</h2>

<ul>
  <li> <a href="doc/index.html">Reference manual in html</a></li>
</ul>

<h2>Unique features</h2>
<p>
Biocham implements some unique features for:
<ul>  
<li>
synthesizing reaction systems from mathematical specification of an input/output or time function <a href="https://lifeware.inria.fr/wiki/Main/Publications#FLBP17cmsb">[FLBP17cmsb]</a><a href="https://lifeware.inria.fr/wiki/Main/Publications#HFS20cmsb">[HFS20cmsb]</a><a href="https://lifeware.inria.fr/wiki/Main/Publications#HFS21cmsb">[HFS21cmsb]</a>
<li>
infering reaction systems from ODEs <a href="https://lifeware.inria.fr/wiki/Main/Publications#FGS15tcs">[FGS15tcs]</a> (used in <a href="https://github.com/sbmlteam/moccasin">Mocassin</a> for importing Matlab models in SBML),
</li>
<li>
combining reaction and influence systems <a href="https://lifeware.inria.fr/wiki/Main/Publications#FMRS18tcbb">[FMRS18tcbb]</a> and interpreting them in a hierarchy of continuous, stochastic, discrete and Boolean semantics related by abstraction <a href="https://lifeware.inria.fr/wiki/Main/Publications#FS08tcs">[FS08tcs]</a>,
</li>
<li>
detecting model reductions between reaction models and in model repositories such as biomodels <a href="https://lifeware.inria.fr/wiki/Main/Publications#GFS10bi">[FS10bi]</a> , based on reaction graph structures (concept of subgraph epimorphisms <a href="https://lifeware.inria.fr/wiki/Main/Publications#GFMSS14dam">[GFMSS14dam]</a>).
</li>
<li>
  checking necessary conditions for multistationarity in reaction systems <a href="https://lifeware.inria.fr/wiki/Main/Publications#BFS18jtb">[BFS18jtb]</a>,
or  sufficient conditions for rate independence <a href="https://lifeware.inria.fr/wiki/Main/Publications#DFS20cmsbcmsb">[DFS20cmsbcmsb]</a>.
</li>
<li>
checking or enumerating temporal properties of the (asynchronous non-deterministic) Boolean dynamics in Computation Tree Logic (CTL) using a symbolic model-checker <a href="https://lifeware.inria.fr/wiki/Main/Publications#CF03cmsb">[CF03cmsb]</a>,
</li>
<li>
reducing reaction systems with a CTL specification of their behavior <a href="https://lifeware.inria.fr/wiki/Main/Publications#CCFS06tcsb">[CCFS06tcsb]</a>,
</li>
<li>
analyzing numerical traces in First-Order Linear Time Logic with constraints over the reals, FO-LTL(Rlin) <a href="https://lifeware.inria.fr/wiki/Main/Publications#FT14book">[FT14book]</a>,
</li>
<li>
measuring the satisfaction, sensitivity and robustness of FO-LTL(Rlin) properties w.r.t. parameter distributions in differential or stochastic models <a href="https://lifeware.inria.fr/wiki/Main/Publications#RBFS11tcs">[RBFS11tcs]</a> <a href="https://lifeware.inria.fr/wiki/Main/Publications#RBFS09bi">[RBFS09bi]</a>  <a href="https://lifeware.inria.fr/wiki/Main/Publications#RBFS09bi">[FR08tcs]</a>,
</li>
<li>
optimizing model parameters for satisfying FO-LTL(Rlin) constraints including robustness constraints <a href="https://lifeware.inria.fr/wiki/Main/Publications#RBFS11tcs">[FS18cmsb]</a>,
</li>
<li>
solving tropical equilibration problems for reasoning about orders of magnitudes in quantitative models <a href="https://lifeware.inria.fr/wiki/Main/Publications#SFR14amb">[SFR14amb]</a>,
</li>
<li>
synthesizing reaction systems for executing imperative programs or computing real functions presented as solutions of polynomial initial value problems (PIVPs) <a href="https://lifeware.inria.fr/wiki/Main/Publications#FLBP17cmsb">[FLBP17cmsb]</a>,
</li>
</ul>

<h2>Online access to notebook - Docker image download - Installation from source files</h2>
<ul>
  <li><span style="text-decoration: underline;">Jupyter notebook</span> with Biocham kernel:<br>
    <ul>
      <li> on Inria server
	<a href="http://lifeware.inria.fr/biocham4/online/">http://lifeware.inria.fr/biocham4/online/</a>
	</li>
  <li>
on mybinder.org:
  <a href="https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.inria.fr%2Flifeware%2Fbiocham.git/develop"><img src="https://mybinder.org/badge_logo.svg" alt="Binder" /> https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.inria.fr%2Flifeware%2Fbiocham.git/develop </a>
  </li>
  </ul>
<br>
  <li><span style="text-decoration: underline;">docker images:</span>&nbsp;
    <a
       href="https://gitlab.inria.fr/lifeware/biocham/container_registry">https://gitlab.inria.fr/lifeware/biocham/container_registry</a>.<br/>Launch
    with <pre>docker run -p 8888:8888 registry.gitlab.inria.fr/lifeware/biocham:[tag]</pre>
    You can add <pre>start.sh biocham --notebook --NotebookApp.token=''</pre> if you don't want
    token-based authentication.</li><br>
  <li><span style="text-decoration: underline;">source files:</span>&nbsp;
    <a href="https://gitlab.inria.fr/lifeware/biocham">https://gitlab.inria.fr/lifeware/biocham</a>.<br>

  The installation then basically proceeds by using the Unix shell script<br>
    <code>./install.sh</code>
</li>
<br>
<li><span style="text-decoration: underline;">Previous versions</span> (for reproducible research):
  <ul>
    <li>on <a href="https://gitlab.inria.fr/lifeware/biocham">https://gitlab.inria.fr/lifeware/biocham</a> since 2019</li>
    <li><a href="http://lifeware.inria.fr:8888">biocham4.1.9 online notebook</a> for 2017-2019 publications</li>
    <li> <a href="biocham4.0.zip">biocham4.0.zip</a> for 2016-2017 publications</li>
    <li><a href="http://lifeware.inria.fr/biocham3/">biocham3</a> for previous publications</li>
  </ul>
</li>
</ul>
<h2>Feedback and bug reports:</h2>

    <p><a href="https://gitlab.inria.fr/lifeware/biocham">https://gitlab.inria.fr/lifeware/biocham/issues</a> or mail to <a href="mailto:biocham@inria.fr">biocham@inria.fr</a></p>
</p>
</body>
</html>
