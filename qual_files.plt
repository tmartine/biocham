:- use_module(library(plunit)).

:- use_module(reaction_rules).
:- use_module(influence_rules).

:- begin_tests(
  qual_files,
  [condition(flag(slow_test, true, true)),
    setup((clear_model, reset_options))]
).

% FIXME no unit test, only one integration test, could be better
test(
  'load_qual',
  [true(Influences = [
    ('A' -> 'B'),
    (/'A' -< 'B'),
    ('C', 'A' -> 'A'), ('C' /'A' -> 'A'),
    (/'C' -< 'A'),
    ('B'->'C'),
    (/'B' -< 'C')])
  ]
) :-
  load('library:examples/tests/foo_grn.sbml'),
  all_items([kind: influence], Influences).


test(
  'load_ginml'
) :-
  load('library:examples/mapk/MAPK_large_19june2013.ginml').


:- end_tests(qual_files).
